# 說明
※為保護前公司資料，<br>
　所有的資料庫名稱被替換為「DatabaseName」<br>
　所有的資料表名稱被替換為「TableName」<br>
<br>
※前端程式碼放在「front_end」裡<br>
　後端程式碼放在「back_end」裡，<br>
　EmpHealthService.cs　對應　EmpHealthManager與EmpHealthType<br>
　MedFeeService.cs　對應　AnalysisMedExpenses<br>
　其他的後端不是我寫的，故沒有留存<br>
<br>
※前端資料夾架構<br>
　Name.html ----------- 畫面呈現<br>
　NameController.js --- angularJs與javascript<br>
　Name.js -------------- 與後端溝通<br>
　dependencies.json<br>
<br>
# 簡單的溝通範例
### empHealthType.html
<pre>
部門名稱：
&lt;select data-kendo-drop-down-list required
 	data-k-filter="'contains'"
 	data-k-data-text-field="'departName'"
	data-k-data-value-field="'departNo'"
	data-k-data-source="viewModel.deptList"     //viewModel.deptList變數從controller.js來
	data-ng-model="viewModel.searchDept"
	style="width: 250px"&gt;
&lt;/select&gt;
</pre>
### empHealthTypeController.js
<pre>
//#region 取得部門清單
var getDeptList = function () {
	empHealthType.getDeptList().then(           //呼叫empHealthType.js的getDeptList方法
		function (success) {
			if (success.data.length != 0) {
				$scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
				$scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
			} else {
				toaster.showErrorMessage("對不起，沒有資料");
			}
		},
		function (error) {
			toaster.showErrorMessage("對不起，連線失敗");
		}
	);
}
//#endregion
</pre>
### empHealthType.js
<pre>
//#region 取得部門清單
this.getDeptList = function () {                 //呼叫EmpHealthService.cs的GetDepart方法
	var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
	return $http.get(url);
}
//#endregion
</pre>
### EmpHealthService.cs
<pre>
/// &lt;summary&gt;
/// 取得部門清單
/// &lt;/summary&gt;
public IEnumerable<EmpInfo> GetDepart()
{
    using (DatabaseName context = new DatabaseName())
    {
        var o = from a in context.TableName
                where a.ChineseName != ""
                select new EmpInfo
                {
                    DepartNo = a.DepartNo,
                    DepartName = a.ChineseName
                };
        return o.ToList<EmpInfo>();
    }
}
</pre>