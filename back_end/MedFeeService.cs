﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmuh.Contract.CostAnalysis;
using Cmuh.Contract.CostAnalysis.Models;
using Cmuh.Data.HealthWareHouse;
using Cmuh.Data.HealthCare;
using System.Data.Entity.Infrastructure;
using System.Data.Objects.SqlClient;
using Cmuh.Enumerations;

namespace Cmuh.Service.CostAnalysis
{
    public class MedFeeService : IMedFeeService
    {
        #region 方法

        public Statistics GetStatistics(Search search)
        {
            IEnumerable<Patients> patients = GetPatients(search);
            IEnumerable<int> patientVisitNos = patients.Select(x => x.VisitNo).ToList<int>();
            IEnumerable<int> diagVisitNos = GetDiagOperVisitNos(search);
            IEnumerable<int> medVisitNos = GetMedInfos(search);
            IEnumerable<int> visitNos = GetFilterVisitNos(search,patientVisitNos, diagVisitNos, medVisitNos);
            if (visitNos.Any() == false || patients.Any() == false)
            {
                return null;
            }
            IEnumerable<FeeInfo> allfees = GetAllFees(search, patients);
            IEnumerable<FeeInfo> feeInfos = GetFeeInfos(visitNos, allfees, patients);
            Statistics statistics = GetStatistic(feeInfos, patients);
            return statistics;
        }

        /// <summary>
        /// 取出統計區間內所有符合user輸入的性別年齡的VisitNo
        /// </summary>
        private IEnumerable<Patients> GetPatients(Search search)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        where (a.VisitType == "A") &&
                              (a.DischargeDate >= search.StartDate) &&
                              (a.DischargeDate <= search.EndDate) &&
                              (a.IdNo.Substring(0, 1).CompareTo("A") >= 0) &&
                              (a.IdNo.Substring(0, 1).CompareTo("Z") <= 0) &&
                              (b.IdNo == a.IdNo) &&
                              (SqlFunctions.DateDiff("YEAR", b.Birthday, a.DischargeDate) >= search.Age1 || search.Age1 == 0) &&
                              (SqlFunctions.DateDiff("YEAR", b.Birthday, a.DischargeDate) <= search.Age2 || search.Age2 == 0) &&
                              (b.SexName == search.Sex || search.Sex == string.Empty)
                        select new Patients
                        {
                            VisitNo = a.VisitNo,
                            Birthday = b.Birthday,
                            DischargeDate = a.DischargeDate,
                            AdmDays = (int)SqlFunctions.DateDiff("DAY", a.VisitDate, a.DischargeDate),
                            Age = (int)SqlFunctions.DateDiff("YEAR", b.Birthday, a.DischargeDate),
                            Sex = b.SexName
                        };

                return o.ToList<Patients>().Distinct(new PatientComparer());
            }
        }

        /// <summary>
        /// 取得符合診斷碼、處置碼的VisitNos
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        private IEnumerable<int> GetDiagOperVisitNos(Search search)
        {
            IEnumerable<string> diagCodes = new List<string>() { search.DiagCode1, search.DiagCode2 };
            IEnumerable<string> operCodes = new List<string>() { search.OperCode1, search.OperCode2 };
            PrimaryDiag primaryDiag = new PrimaryDiag(search.DiagCode0, (int)DiagEnum.DiagSeqNo.PriDiag);
            PrimaryDiag primaryOper = new PrimaryDiag(search.OperCode0, (int)DiagEnum.DiagSeqNo.PriOper);
            SecondaryDiags secondaryDiag = new SecondaryDiags(diagCodes, (int)DiagEnum.DiagSeqNo.SecDiagMin, (int)DiagEnum.DiagSeqNo.SecDiagMax);
            SecondaryDiags secondaryOper = new SecondaryDiags(operCodes, (int)DiagEnum.DiagSeqNo.SecOperMin, (int)DiagEnum.DiagSeqNo.SecOperMax);
            if (primaryDiag.DiagCode != string.Empty && primaryOper.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any() &&  secondaryOper.DiagCodes.Any())
            {   //有主診斷、次診斷、主處置、次處置
                return GetDiagVisitNos(search, primaryDiag, primaryOper, secondaryDiag, secondaryOper);
            }
            if (primaryDiag.DiagCode != string.Empty && primaryOper.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any() )
            {   //有主診斷、主處置、次診斷
                return GetDiagVisitNos(search, primaryDiag, primaryOper, secondaryDiag);
            }
            if (primaryDiag.DiagCode != string.Empty && primaryOper.DiagCode != string.Empty && secondaryOper.DiagCodes.Any())
            {   //有主診斷、主處置、次處置
                return GetDiagVisitNos(search, primaryDiag, primaryOper, secondaryOper);
            }
            if (primaryDiag.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any() && secondaryOper.DiagCodes.Any())
            {  //有主診斷、次診斷、次處置
                return GetDiagVisitNos(search, primaryDiag, secondaryDiag, secondaryOper);
            }
            if (primaryOper.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any() &&  secondaryOper.DiagCodes.Any())
            {  //有主處置、次診斷、次處置
                return GetDiagVisitNos(search, primaryOper, secondaryDiag, secondaryOper);
            }
            if (primaryDiag.DiagCode != string.Empty && primaryOper.DiagCode != string.Empty)
            {   //有主診斷、主處置
                return GetDiagVisitNos(search, primaryDiag, primaryOper);
            }
            if (primaryDiag.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any())
            {   //有主診斷、次診斷
                return GetDiagVisitNos(search, primaryDiag, secondaryDiag);
            }
            if (primaryDiag.DiagCode != string.Empty && secondaryOper.DiagCodes.Any())
            {  //有主診斷、次處置
                return GetDiagVisitNos(search, primaryDiag, secondaryOper);
            }
            if (primaryOper.DiagCode != string.Empty && secondaryDiag.DiagCodes.Any())
            {  //有主處置、次診斷
                return GetDiagVisitNos(search, primaryOper, secondaryDiag);
            }
            if (primaryOper.DiagCode != string.Empty && secondaryOper.DiagCodes.Any())
            {   //有主處置、次處置
                return GetDiagVisitNos(search, primaryOper, secondaryOper);
            }
            if (secondaryDiag.DiagCodes.Any() && secondaryOper.DiagCodes.Any())
            {  //有次診斷、次處置
                return GetDiagVisitNos(search, secondaryDiag, secondaryOper);
            }
            if (primaryDiag.DiagCode != string.Empty)
            {   //有主診斷
                return GetDiagVisitNos(search, primaryDiag);
            }
            if (primaryOper.DiagCode != string.Empty)
            {   //有主處置
                return GetDiagVisitNos(search, primaryOper);
            }
            if (secondaryDiag.DiagCodes.Any() == true)
            {  //有次診斷
                return GetDiagVisitNos(search, secondaryDiag);
            }
            if (secondaryOper.DiagCodes.Any() == true)
            {  //有次處置
                return GetDiagVisitNos(search, secondaryOper);
            }
            return new List<int>();
        }
        /// <summary>
        /// 取得診斷碼與處置碼的VisitNos
        /// </summary>
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag, PrimaryDiag primaryOper, SecondaryDiags secondaryDiag, SecondaryDiags secondaryOper)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        from d in context.TableName
                        from e in context.TableName
                        where a.VisitType == "A" &&
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              d.VisitNo == c.VisitNo &&
                              e.VisitNo == d.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode &&
                              c.SeqNo == primaryOper.SeqNo && c.DiagCode.Trim() == primaryOper.DiagCode &&
                              d.SeqNo >= secondaryDiag.SeqNo1 && d.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(d.DiagCode.Trim()) &&
                              e.SeqNo >= secondaryOper.SeqNo1 && e.SeqNo <= secondaryOper.SeqNo2 && secondaryOper.DiagCodes.Contains(e.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag, PrimaryDiag primaryOper, SecondaryDiags secondaryDiag)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        from d in context.TableName
                        where a.VisitType == "A" &&
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              d.VisitNo == c.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode &&
                              c.SeqNo == primaryOper.SeqNo && c.DiagCode.Trim() == primaryOper.DiagCode &&
                              d.SeqNo >= secondaryDiag.SeqNo1 && d.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(d.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag, SecondaryDiags secondaryDiag, SecondaryDiags secondaryOper)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        from d in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              d.VisitNo == c.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode &&
                              c.SeqNo >= secondaryDiag.SeqNo1 && c.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(c.DiagCode.Trim()) &&
                              d.SeqNo >= secondaryOper.SeqNo1 && d.SeqNo <= secondaryOper.SeqNo2 && secondaryOper.DiagCodes.Contains(d.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag, PrimaryDiag primaryOper)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode &&
                              c.SeqNo == primaryOper.SeqNo && c.DiagCode.Trim() == primaryOper.DiagCode
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag, SecondaryDiags secondaryDiag)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;

                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode &&
                              c.SeqNo >= secondaryDiag.SeqNo1 && c.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(c.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, SecondaryDiags secondaryDiag, SecondaryDiags secondaryOper)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.VisitNo == b.VisitNo &&
                              b.SeqNo >= secondaryDiag.SeqNo1 && b.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(b.DiagCode.Trim()) &&
                              c.SeqNo >= secondaryOper.SeqNo1 && c.SeqNo <= secondaryOper.SeqNo2 && secondaryOper.DiagCodes.Contains(c.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, PrimaryDiag primaryDiag)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              b.SeqNo == primaryDiag.SeqNo && b.DiagCode.Trim() == primaryDiag.DiagCode
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }
        private IEnumerable<int> GetDiagVisitNos(Search search, SecondaryDiags secondaryDiag)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        from b in context.TableName
                        where a.VisitType == "A" && 
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              b.SeqNo >= secondaryDiag.SeqNo1 && b.SeqNo <= secondaryDiag.SeqNo2 && secondaryDiag.DiagCodes.Contains(b.DiagCode.Trim())
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }

        /// <summary>
        /// 取出統計區間內所有符合user輸入的醫令碼的VisitNo
        /// </summary>
        private IEnumerable<int> GetMedInfos(Search search)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                if (search.MedCode1 == string.Empty)
                {
                    return new List<int>();
                }
                IEnumerable<string> medCodes = new List<string>() { search.MedCode1, search.MedCode2, search.MedCode3 };
                medCodes = medCodes.Where(x => x != string.Empty);
                var o = from a in context.TableName
                        where medCodes.Contains(a.MedCode)
                        select a.VisitNo;
                return o.Distinct().ToList<int>();
            }
        }

        /// <summary>
        /// 取得所有符合條件的VisitNos
        /// </summary>
        private IEnumerable<int> GetFilterVisitNos(Search search,IEnumerable<int> patientVisitNos, IEnumerable<int> diagVisitNos, IEnumerable<int> medVisitNos)
        {
            IEnumerable<int> visitNos = patientVisitNos;
            if (search.DiagCode0 != string.Empty || search.OperCode0 != string.Empty)
            {
                visitNos = visitNos.Intersect(diagVisitNos);
            }
            if (search.MedCode1 != string.Empty)
            {
                visitNos = visitNos.Intersect(medVisitNos);
            }
            return visitNos.Distinct().ToList<int>();
        }

        /// <summary>
        /// 取得統計區間內所有人的費用類別與總額
        /// </summary>
        private IEnumerable<FeeInfo> GetAllFees(Search search, IEnumerable<Patients> patients)
        {
            using (DatabaseName context = new DatabaseName())
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 0;
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        where a.VisitType == "A" &&
                              a.DischargeDate >= search.StartDate &&
                              a.DischargeDate <= search.EndDate &&
                              b.VisitNo == a.VisitNo &&
                              c.MedCode == b.MedCode
                        select new FeeInfo
                        {
                            VisitNo = a.VisitNo,
                            FeeType = c.FeeType,
                            Fee = c.IsSelf ? b.Qty * c.GenPrice * search.GenRate * search.Rate : b.Qty * c.NhiPrice * search.NhiRate * search.Rate
                        };
                
                List<FeeInfo> feeInfos = o.ToList<FeeInfo>();
                feeInfos = GetAdditionFee(feeInfos, patients);
                feeInfos = GroupFeeInfos(feeInfos);
                Dictionary<int, FeeInfo> feeInfos3 = GetFeeName();
                feeInfos.ForEach(x => x.FeeName = feeInfos3[x.FeeType].FeeName);
                return feeInfos;
            }
        }

        private List<FeeInfo> GetAdditionFee(IEnumerable<FeeInfo> feeInfos, IEnumerable<Patients> patients)
        {
            Dictionary<int, TimeSpan> timeSpanInfos = patients.ToDictionary(x => x.VisitNo, x => x.DischargeDate - x.Birthday);
            IEnumerable<FeeInfo> newFeeInfos = feeInfos.Where(x => timeSpanInfos.Keys.Contains(x.VisitNo)).ToList<FeeInfo>();
            var o = from a in newFeeInfos
                    select new FeeInfo
                    {
                        VisitNo = a.VisitNo,
                        FeeName = a.FeeName,
                        FeeType = a.FeeType,
                        Fee = GetFee(a.Fee, a.FeeType, timeSpanInfos[a.VisitNo])
                    };
            return o.ToList<FeeInfo>();
        }

        private decimal GetFee(decimal fee, int feeType, TimeSpan timeSpanInfos)
        {
            IEnumerable<int> additionType = new List<int> { 7, 10, 18, 23, 24, 25, 26, 27, 29, 30, 31, 33, 35, 38 };
            if(additionType.Contains(feeType) == false)
            {
                return fee;
            }
            decimal surgeryAddition = feeType == 29 ? (decimal)1.53 : 1;
            decimal childAddition = 1;
            double diff = timeSpanInfos.Days / 30;
            if (diff < 72)
            {
                if (diff < 24)
                {
                    if (diff < 6)
                    {
                        childAddition = feeType == 10 ? 2 : (decimal)1.6;
                    }
                    else
                    {
                        childAddition = feeType == 10 ? (decimal)1.8 : (decimal)1.3;
                    }
                }
                else
                {
                    childAddition = feeType == 10 ? (decimal)1.6 : (decimal)1.2;
                }
            }
            return fee * surgeryAddition * childAddition;
        }

        private List<FeeInfo> GroupFeeInfos(List<FeeInfo> feeInfos)
        {
            var o = from a in feeInfos
                    group a by new { a.VisitNo, a.FeeType } into g
                    select new FeeInfo
                    {
                        VisitNo = g.Key.VisitNo,
                        FeeType = g.Key.FeeType,
                        FeeSum = (double)g.Sum(x => x.Fee)
                    };
            return o.ToList<FeeInfo>();
        }

        /// <summary>
        /// 取得符合條件的病人的費用清單
        /// </summary>
        private IEnumerable<FeeInfo> GetFeeInfos(IEnumerable<int> visitNos, IEnumerable<FeeInfo> feeInfos, IEnumerable<Patients> patients)
        {
            var o = from a in feeInfos
                    join b in visitNos on a.VisitNo equals b
                    select a;
            List<FeeInfo> feeInfos2 = o.ToList<FeeInfo>();

            feeInfos2.AddRange(GetTotalFees(visitNos, feeInfos2));
            feeInfos2.AddRange(GetAdmDays(visitNos, patients));
            return feeInfos2;
        }
        private Dictionary<int, FeeInfo> GetFeeName()
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        select new FeeInfo
                        {
                            FeeType = a.FeeType,
                            FeeName = a.FeeName
                        };
                return o.ToDictionary(x => x.FeeType);
            }
        }
        /// <summary>
        /// 取得總計金額
        /// </summary>
        private IEnumerable<FeeInfo> GetTotalFees(IEnumerable<int> visitNos, IEnumerable<FeeInfo> feeInfos)
        {
            var o = from a in feeInfos
                    join b in visitNos on a.VisitNo equals b
                    group a by a.VisitNo into g
                    select new FeeInfo
                    {
                        VisitNo = g.Key,
                        FeeName = "總計金額",
                        FeeSum = (int)g.Sum(x => x.FeeSum),
                        FeeType = -1
                    };
            return o.ToList<FeeInfo>();
        }
        /// <summary>
        /// 取得住院天數
        /// </summary>
        private IEnumerable<FeeInfo> GetAdmDays(IEnumerable<int> visitNos, IEnumerable<Patients> patients)
        {
            var o = from a in patients
                    join b in visitNos on a.VisitNo equals b
                    group a by a.VisitNo into g
                    select new FeeInfo
                    {
                        VisitNo = g.Key,
                        FeeName = "住院天數",
                        FeeSum = g.First().AdmDays,
                        FeeType = -2
                    };
            return o.ToList<FeeInfo>();
        }

        /// <summary>
        /// 取得統計值
        /// </summary>
        private Statistics GetStatistic(IEnumerable<FeeInfo> feeInfos, IEnumerable<Patients> patients)
        {
            Statistics statistics = new Statistics();

            IEnumerable<FeeInfo> feeInfo = feeInfos.Where(x => x.FeeName == "總計金額").OrderBy(x => x.FeeSum).ToList<FeeInfo>();
            //計算項目個數與百分比分隔數量
            int Count = feeInfo.Count();
            Count = Count < 4 ? 4 : Count;
            int AverageCount = Count / 4;

            statistics.Patient = GetPercentPatients(patients, feeInfo.Skip(AverageCount).Take(AverageCount*2).ToList<FeeInfo>());
            IEnumerable<Patients> oldPatient = GetOldPartient(statistics.Patient);
            for(int i = 0;i< statistics.Patient.Count(); i++)
            {
                statistics.Patient.ElementAt(i).Percentage = (i+ AverageCount+1) * 100 / Count;
                statistics.Patient.ElementAt(i).OldVisitNo = oldPatient.Where(x => x.VisitNo == statistics.Patient.ElementAt(i).VisitNo).Select(x => x.OldVisitNo).First();
            }

            statistics.FeeSummary = GetFeeSummarys(feeInfos);

            return statistics;
        }

        private IEnumerable<Patients> GetOldPartient (IEnumerable<Patients> patients)
        {
            IEnumerable<int> visitNos = patients.Select(x => x.VisitNo).ToList<int>();
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where visitNos.Contains(a.VisitNo)
                        select new Patients
                        {
                            VisitNo = a.VisitNo,
                            OldVisitNo = a.RegisterNo
                        };
                return o.ToList<Patients>();
            }
        }

        private IEnumerable<Patients> GetPercentPatients(IEnumerable<Patients> patients, IEnumerable<FeeInfo> feeInfos)
        {
            var o = from a in patients
                    join b in feeInfos on a.VisitNo equals b.VisitNo
                    select new Patients
                    {
                        VisitNo = a.VisitNo,
                        Age = a.Age,
                        Sex = a.Sex,
                        AdmDays = a.AdmDays,
                        Total = b.FeeSum
                    };
            return o.OrderBy(x=>x.Total).ToList<Patients>();
        }

        /// <summary>
        /// 取得費用摘要
        /// </summary>
        private IEnumerable<FeeSummary> GetFeeSummarys(IEnumerable<FeeInfo> feeInfos)
        {
            List<FeeSummary> feeSummarys = new List<FeeSummary>();

            //取得這些病人的所有費用清單
            var groups = feeInfos.GroupBy(x => new { x.FeeName , x.FeeType}).ToList();
            foreach (var group in groups)
            {
                int count = group.Count();
                count = count < 4 ? 4 : count;
                int averageCount = count / 4;
                int last = count - averageCount * 3;
                var fee = group.OrderBy(x => x.FeeSum).ToList();
                FeeSummary record = new FeeSummary();
                record.ItemName = group.Key.FeeName;
                record.ItemType = group.Key.FeeType;
                record.Percent25 = fee.Take(averageCount).Any() == true ? fee.Take(averageCount).Last().FeeSum : 0;
                record.Percent50 = fee.Skip(averageCount).Take(averageCount).Any() == true ? fee.Skip(averageCount).Take(averageCount).Last().FeeSum : 0;
                record.Percent75 = fee.Skip(averageCount*2).Take(averageCount).Any() == true? fee.Skip(averageCount * 2).Take(averageCount).Last().FeeSum : 0;
                record.Percent100 = fee.Skip(averageCount*3).Take(last).Any() == true ? fee.Skip(averageCount * 3).Take(last).Last().FeeSum : 0;
                record.Mode = fee.GroupBy(x => x.FeeSum).Max(x => x.Count()) == 0 ? fee.GroupBy(x => x.FeeSum).OrderByDescending(x => x.Count()).First().Key : -1;
                feeSummarys.Add(record);
            }
            return feeSummarys.OrderBy(x=>x.ItemType);
        }
        
        /// <summary>
        /// 取得病人費用清單
        /// </summary>
        public IEnumerable<FeeInfo> GetPatientFees(Search search)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        from b in context.TableName
                        from c in context.TableName
                        where a.VisitNo == search.VisitNo &&
                              b.FeeType == a.FeeType &&
                              c.MedCode == a.MedCode
                        select new FeeInfo
                        {
                            VisitNo = a.VisitNo,
                            FeeName = b.FeeName,
                            FeeType = b.FeeType,
                            Fee = c.IsSelf ? a.Qty * c.GenPrice * search.GenRate * search.Rate : a.Qty * c.NhiPrice * search.NhiRate * search.Rate
                        };
                List<FeeInfo> feeInfos = o.ToList<FeeInfo>();
                IEnumerable<Patients> patients = GetPatient(search.VisitNo);
                feeInfos = GetAdditionFee(feeInfos, patients);
                feeInfos = GroupFeeInfos2(feeInfos);
                return feeInfos.OrderByDescending(x => x.FeeSum);
            }
        }

        private IEnumerable<Patients> GetPatient (int visitNo)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        from b in context.TableName
                        where a.VisitNo == visitNo && 
                              b.IdNo == a.IdNo
                        select new Patients
                        {
                            VisitNo = a.VisitNo,
                            Birthday = b.Birthday,
                            DischargeDate = a.DischargeDate
                        };
                return o.ToList<Patients>();
            }
        }

        private List<FeeInfo> GroupFeeInfos2(List<FeeInfo> feeInfos)
        {
            var o = from a in feeInfos
                    group a by new { a.VisitNo, a.FeeName } into g
                    select new FeeInfo
                    {
                        VisitNo = g.Key.VisitNo,
                        FeeName = g.Key.FeeName,
                        FeeSum = (double)g.Sum(x => x.Fee)
                    };
            return o.ToList<FeeInfo>();
        }
        #endregion
    }
}
