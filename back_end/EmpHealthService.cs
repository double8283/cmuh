﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmuh.Contract.EmpHealthManager;
using Cmuh.Contract.EmpHealthManager.Models;
using Cmuh.Data.HealthCare;
using Cmuh.Data.HealthResource;
using System.Data.Objects.SqlClient;
using System.Data;
using Cmuh.Messenger;
using System.Data.Entity.Validation;
using Cmuh.Enumerations;

namespace Cmuh.Service.EmpHealthManager
{
    public class EmpHealthService : IEmpHealthService
    {
        #region 屬性

        private List<string> driverDept;
        public List<string> DriverDept
        {
            get
            {
                if (this.driverDept == null)
                {
                    this.driverDept = new List<string> { "1AC3" };
                }
                return this.driverDept;
            }
        }

        private List<string> radiationDept;
        public List<string> RadiationDept
        {
            get
            {
                if (this.radiationDept == null)
                {
                    this.radiationDept = new List<string> { "11C0", "11X2", "1860", "1864", "1270", "L1XA",
                    "L120", "1R00", "1R40", "1400", "1401", "1T30", "1T32", "1T33", "1280", "1284", "1000",
                    "1300", "1T23", "1T2G", "1X30", "11C3", "FR00", "1R00", "1400", "HY00", "HR00", "F300",
                    "4000", "5R00", "5400" };
                }
                return this.radiationDept;
            }
        }

        private List<string> feedingDept;
        public List<string> FeedingDept
        {
            get
            {
                if (this.feedingDept == null)
                {
                    this.feedingDept = new List<string> { "18C1", "18C2" };
                }
                return this.feedingDept;
            }
        }

        private List<string> dialysisDept;
        public List<string> DialysisDept
        {
            get
            {
                if (this.dialysisDept == null)
                {
                    this.dialysisDept = new List<string> { "1140", "11B0", "N140", "1141", "L140", "1A20",
                    "1142", "F141", "4141", "H141", "6141" };
                }
                return this.dialysisDept;
            }
        }
        #endregion

        #region 方法

        /// <summary>
        /// 取得部門清單
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmpInfo> GetDepart()
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where a.ChineseName != ""
                        select new EmpInfo
                        {
                            DepartNo = a.DepartNo,
                            DepartName = a.ChineseName
                        };
                return o.ToList<EmpInfo>();
            }
        }

        /// <summary>
        /// 員工體檢建置-匯入按鈕-取得匯入的主管清單資料
        /// </summary>
        /// <param name="healthManages"></param>
        /// <returns></returns>
        public IEnumerable<EmpHealth> GetEmpManageList(IEnumerable<EmpHealth> healthManages)
        {
            using (DatabaseName context = new DatabaseName())
            {
                IEnumerable<int> empNoLists = healthManages.Select(x => x.EmpNo).ToList<int>();
                var o = from a in context.TableName
                        from b in context.TableName
                        where a.DepartNo == b.DepartNo &&
                              empNoLists.Contains(a.EmpNo)
                        select new EmpHealth
                        {
                            EmpCode = a.EmpCode,
                            DepartName = b.ChineseName,
                            EmpNo = a.EmpNo,
                            EmpName = a.EmpName,
                            Birthday = a.Birthday.Value,
                            IdNo = a.IdNo
                        };
                IEnumerable<EmpHealth> healthManage = o.ToList<EmpHealth>();
                healthManage = GetHealthDate(healthManage, healthManages);
                return healthManage;
            }
        }

        /// <summary>
        /// 主管清單-依照健檢類別決定健檢月份
        /// </summary>
        /// <param name="healthManages"></param>
        /// <returns></returns>
        private IEnumerable<EmpHealth> GetHealthDate(IEnumerable<EmpHealth> healthManage, IEnumerable<EmpHealth> healthManages)
        {
            foreach (EmpHealth item in healthManage)
            {
                EmpHealth data = healthManages.Where(x => x.EmpNo == item.EmpNo).FirstOrDefault();
                item.HealthType = data.HealthType;
                item.HealthTypeNo = GetHealthTypeNo2(item.HealthType);
                switch (item.HealthType)
                {
                    case "主管健檢&透析":
                    case "主管健檢+透析&校院務":
                    case "司機健檢":
                    case "透析健檢":
                    case "檢驗部健檢":
                        item.StartDate = new DateTime(data.HealthYear, 7, 1);
                        break;
                    case "主管健檢&輻防":
                    case "主管健檢+輻防&校院務":
                    case "輻防健檢":
                        item.StartDate = new DateTime(data.HealthYear, 9, 1);
                        break;
                    case "供膳健檢":
                        item.StartDate = new DateTime(data.HealthYear + 1, 1, 1);
                        break;
                    default:
                        item.StartDate = new DateTime(data.HealthYear, item.Birthday.Month, 1);
                        break;

                }
                item.EndDate = item.StartDate.AddMonths(1).AddDays(-1);
            }
            return healthManage;
        }

        private int GetHealthTypeNo2(string healthType)
        {
            switch (healthType)
            {
                case "主管健檢":
                    return 10;
                case "主管健檢&校院務":
                    return 11;
                case "主管健檢&透析":
                    return 12;
                case "主管健檢&輻防":
                    return 13;
                case "主管健檢+透析&校院務":
                    return 14;
                case "主管健檢+輻防&校院務":
                    return 15;
                case "員工健檢":
                    return 20;
                case "司機健檢":
                    return 30;
                case "輻防健檢":
                    return 40;
                case "供膳健檢":
                    return 50;
                case "透析健檢":
                    return 60;
                case "檢驗部健檢":
                    return 70;
                default:
                    return 99;
            }
        }

        /// <summary>
        /// 員工體檢建置-存檔按鈕-建置健檢資料至EmpHealthRecord資料表
        /// </summary>
        /// <param name="healthManages"></param>
        /// <returns></returns>
        public bool SetHealthRecord(IEnumerable<EmpHealth> healthManages)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int Year = healthManages.FirstOrDefault().StartDate.Year;
                IEnumerable<int> empNoLists = healthManages.Select(x => x.EmpNo).ToList<int>();
                IEnumerable<EmpHealthRecord> empHealths = GetEmpHealth(Year, empNoLists);
                foreach (EmpHealth healthManage in healthManages)
                {
                    EmpHealthRecord record = new EmpHealthRecord
                    {
                        EmpNo = healthManage.EmpNo,
                        HealthYear = (short)Year,
                        HealthType = GetHealthType(healthManage),
                        StartDate = healthManage.StartDate,
                        EndDate = healthManage.EndDate,
                        HealthDate = new DateTime(2999, 12, 31),
                        IsHealth = false,
                        ReportDate = new DateTime(2999, 12, 31),
                        ReportResult = 0,
                        ReVisitDate = new DateTime(2999, 12, 31),
                        Remark = healthManage.Remark,
                        SystemUser = 99999,
                        SystemTime = DateTime.Now
                    };
                    EmpHealthRecord empHealth = empHealths.Where(x => x.EmpNo == record.EmpNo).FirstOrDefault();
                    if (empHealth == null)
                    {
                        context.TableName.Add(record);
                    }
                    else
                    {
                        context.Entry(record).State = EntityState.Modified;
                    }
                }
                return context.SaveChanges() > 0;
            }
        }
        private IEnumerable<EmpHealthRecord> GetEmpHealth(int year, IEnumerable<int> empNoLists)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where empNoLists.Contains(a.EmpNo) &&
                              a.HealthYear == year
                        select a;

                return o.ToList<EmpHealthRecord>();

            }
        }

        private byte GetHealthType(EmpHealth healthManage)
        {
            switch (healthManage.HealthTypeNo)
            {
                case 11://主管健檢
                    return (byte)HealthManagerEnum.HealthType.Responsible;
                case 12://主管健檢&校院務
                    return (byte)HealthManagerEnum.HealthType.ResponsibleFaculty;
                case 13://主管健檢&透析
                    return (byte)HealthManagerEnum.HealthType.ResponsibleDialysis;
                case 14://主管健檢&輻防
                    return (byte)HealthManagerEnum.HealthType.ResponsibleRadiation;
                case 15://主管健檢+透析&校院務
                    return (byte)HealthManagerEnum.HealthType.ResponsibleDialysisFaculty;
                case 16://主管健檢+輻防&校院務
                    return (byte)HealthManagerEnum.HealthType.ResponsibleRadiationFaculty;
                case 99://免檢
                    return (byte)HealthManagerEnum.HealthType.Exemption;
                default:
                    return healthManage.HealthTypeNo2 == 0 ? GetHealthResult(healthManage) : (byte)(healthManage.HealthTypeNo + healthManage.HealthTypeNo2);
            }
        }

        private byte GetHealthResult(EmpHealth healthManage)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where a.EmpNo == healthManage.EmpNo
                        select new
                        {
                            BirthdayYear = a.Birthday.Value.Year,
                            HireYear = a.HireDate.Year
                        };
                var p = o.FirstOrDefault();
                string n = GetHealthType(healthManage.HealthYear, p.HireYear, p.BirthdayYear);
                return GetHealthTypeNo(healthManage.HealthTypeNo, n);
            }
        }

        private string GetHealthType(int year, int hireYear, int birthdayYear)
        {
            switch (year)
            {
                case 2012:
                    return Get2012Year(year, hireYear, birthdayYear);
                case 2013:
                    return Get2013Year(year, hireYear, birthdayYear);
                default:
                    return GetOtherYear(year, hireYear, birthdayYear);
            }
        }
        private string Get2012Year(int year, int hireYear, int birthdayYear)
        {
            if (hireYear < 2010 && (year - birthdayYear > 40 || (year - birthdayYear <= 40 && hireYear < 2007)))
            {
                return "進階";
            }
            return "普通";
        }
        private string Get2013Year(int year, int hireYear, int birthdayYear)
        {
            if (hireYear <= 2010 && ((year - birthdayYear <= 40 && hireYear >= 2007) || hireYear == 2010))
            {
                return "進階";
            }
            return "普通";
        }
        private string GetOtherYear(int year, int hireYear, int birthdayYear)
        {
            if (year - hireYear >= 3)
            {
                if (year - birthdayYear >= 55)
                {
                    return "進階";
                }
                else if (year - birthdayYear >= 40)
                {

                    return GetHealthType(year - 1, hireYear, birthdayYear) == "普通" ? "進階" : "普通";

                }
                else
                {
                    if (GetHealthType(year - 1, hireYear, birthdayYear) == "普通" && GetHealthType(year - 2, hireYear, birthdayYear) == "普通")
                    {
                        return "進階";
                    }
                }
            }
            return "普通";
        }

        private byte GetHealthTypeNo(int healthTypeNo, string type)
        {
            switch (healthTypeNo)
            {
                case 20://員工健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.EmpOrdinary : (byte)HealthManagerEnum.HealthType.EmpAdvanced;
                case 30://司機健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.DriverOrdinary : (byte)HealthManagerEnum.HealthType.DriverAdvanced;
                case 40://輻防健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.RadiationOrdinary : (byte)HealthManagerEnum.HealthType.RadiationAdvanced;
                case 50://供膳健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.FeedingOrdinary : (byte)HealthManagerEnum.HealthType.FeedingAdvanced;
                case 60://透析健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.DialysisOrdinary : (byte)HealthManagerEnum.HealthType.DialysisAdvanced;
                case 70://檢驗部健檢
                    return type == "普通" ? (byte)HealthManagerEnum.HealthType.ExamineOrdinary : (byte)HealthManagerEnum.HealthType.ExamineAdvanced;
                default:
                    return (byte)HealthManagerEnum.HealthType.Exemption;
            }
        }

        /// <summary>
        /// 員工體檢建置-查詢按鈕-取得員工體檢建置清單（職等、部門、職稱、員代、姓名、出生日期、應檢期間、健檢種類）
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmpHealth> GetEmpHealthList(SearchInfo searchInfo)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        from b in context.TableName
                        where (a.EmpStatus == (byte)EmployeeEnum.EmpStatusInfo.FullTime ||
                               a.EmpStatus == (byte)EmployeeEnum.EmpStatusInfo.LeaveWithoutPay) &&
                               a.Birthday.Value.Month >= searchInfo.StartMonth &&
                               a.Birthday.Value.Month <= searchInfo.EndMonth &&
                               a.DepartNo == b.DepartNo &&
                               b.DepartNo == (searchInfo.DepartNo == "0" ? b.DepartNo : searchInfo.DepartNo) &&
                               a.HireDate.Year < searchInfo.HealthYear &&
                               a.EmpNo == (searchInfo.EmpNo == 0 ? a.EmpNo : searchInfo.EmpNo)
                        select new EmpHealth
                        {
                            EmpCode = a.EmpCode,
                            HealthType = "員工健檢",
                            HealthTypeNo = 20,
                            DepartNo = b.DepartNo,
                            DepartName = b.ChineseName,
                            EmpNo = a.EmpNo,
                            EmpName = a.EmpName,
                            Birthday = a.Birthday.Value,
                            IdNo = a.IdNo
                        };
                List<EmpHealth> healthManage = o.ToList<EmpHealth>();
                foreach (EmpHealth item in healthManage)
                {
                    item.StartDate = new DateTime(searchInfo.HealthYear, (item.Birthday.Month + 1) / 2, 1);
                    item.EndDate = item.StartDate.AddMonths(1).AddDays(-1);
                }
                healthManage = FilterHospital(searchInfo, healthManage).ToList<EmpHealth>();
                healthManage = FilterData(searchInfo, healthManage).ToList<EmpHealth>();
                return healthManage;
            }
        }

        /// <summary>
        /// 篩選院所
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <param name="healthManage"></param>
        /// <returns></returns>
        private IEnumerable<EmpHealth> FilterHospital(SearchInfo searchInfo, IEnumerable<EmpHealth> healthManage)
        {
            switch (searchInfo.Hospital)
            {
                case 1://臺北
                    healthManage = healthManage.Where(x => x.DepartNo.StartsWith("N"));
                    break;
                case 2://陽光
                    healthManage = healthManage.Where(x => x.DepartNo.StartsWith("S"));
                    break;
                default://本院
                    healthManage = healthManage.Where(x => x.DepartNo.StartsWith("N") == false && x.DepartNo.StartsWith("S") == false);
                    break;
            }
            return healthManage;
        }

        /// <summary>
        /// 過濾資料
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <param name="healthManage"></param>
        /// <returns></returns>
        private IEnumerable<EmpHealth> FilterData(SearchInfo searchInfo, IEnumerable<EmpHealth> healthManage)
        {
            IEnumerable<EmpHealth> records = GetRecordData(searchInfo.HealthYear);
            IEnumerable<EmpHealth> thisYear = records.Where(x => x.HealthYear == searchInfo.HealthYear);
            //IEnumerable<EmpHealth> preYearEmp = records.Where(x => x.HealthYear == searchInfo.HealthYear - 1 && x.HealthType.CompareTo("30") > 0 && x.HealthType != "99");
            //IEnumerable<EmpHealth> preYearProject = records.Where(x => x.HealthYear == searchInfo.HealthYear - 1 && x.HealthType.CompareTo("20") > 0 && x.HealthType.CompareTo("30") <= 0 && x.HealthType != "99");
            //該年度已有建置資料的話就移除，不重複建置
            healthManage = healthManage.Except(thisYear, new EmpHealthComparer());
            //if (searchInfo.HealthType == "20")
            //{//選擇員工健檢，則不列出去年做專案健檢的人
            //    healthManage = healthManage.Except(preYearEmp, new EmpHealthComparer());
            //}
            //else
            //{//選擇專案健檢，則不列出去年做普通健檢的人
            //    healthManage = healthManage.Except(preYearProject, new EmpHealthComparer());
            //}
            return healthManage;
        }

        /// <summary>
        /// 取得今年度、去年度已建置清單
        /// </summary>
        /// <param name="healthYear"></param>
        /// <returns></returns>
        private IEnumerable<EmpHealth> GetRecordData(int healthYear)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where a.HealthYear == healthYear || a.HealthYear == healthYear - 1
                        select new EmpHealth
                        {
                            EmpNo = a.EmpNo,
                            HealthYear = a.HealthYear,
                            HealthType = SqlFunctions.StringConvert((decimal)a.HealthType).Trim()
                        };
                return o.ToList<EmpHealth>();
            }
        }

        /// <summary>
        /// 員工健康管理-刪除按鈕-刪除資料
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public bool RemoveHealth(List<EmpHealth> items)
        {
            using (DatabaseName context = new DatabaseName())
            {
                IEnumerable<int> empNoLists = items.Select(x => x.EmpNo).ToList<int>();
                int year = items[0].HealthYear;
                var o = from a in context.TableName
                        where a.HealthYear == year &&
                              empNoLists.Contains(a.EmpNo)
                        select a;
                List<EmpHealthRecord> recodes = o.ToList<EmpHealthRecord>();
                foreach (EmpHealthRecord recode in recodes)
                {
                    context.TableName.Remove(recode);
                }
                return context.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 員工健康管理-查詢按鈕-取得員工健康管理清單（部門、員代、姓名、出生日期、應檢期間、預約健檢日期、是否完成健檢、報告日期、報告結果、員工狀態）
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmpHealth> GetEmpHealthManage(SearchInfo searchInfo)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where SqlFunctions.StringConvert((decimal)a.HealthType).Trim() == searchInfo.HealthType &&
                              a.HealthYear == searchInfo.HealthYear &&
                              a.StartDate.Month >= searchInfo.StartMonth &&
                              a.StartDate.Month <= searchInfo.EndMonth &&
                              a.EmpNo == (searchInfo.EmpNo != 0 ? searchInfo.EmpNo : a.EmpNo)
                        select new EmpHealth
                        {
                            EmpNo = a.EmpNo,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            HealthDate = a.HealthDate,
                            IsHealth = a.IsHealth == false ? "未完成" : "完成",
                            ReportDate = a.ReportDate,
                            ReportResultNo = a.ReportResult,
                            HealthTypeNo = a.HealthType,
                            ReVisitDate = a.ReVisitDate,
                            SystemUser = a.SystemUser,
                            HealthYear = searchInfo.HealthYear,
                            Remark = a.Remark
                        };
                IEnumerable<EmpHealth> healthManage = o.ToList<EmpHealth>();
                IEnumerable<EmpHealth> healthManage2 = SetEmpHealthInfo(searchInfo, healthManage);
                return healthManage2;
            }
        }

        /// <summary>
        /// 取得健檢清單的員工資料
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <param name="healthManages"></param>
        /// <returns></returns>
        private IEnumerable<EmpHealth> SetEmpHealthInfo(SearchInfo searchInfo, IEnumerable<EmpHealth> healthManages)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        from b in context.TableName
                        where a.DepartNo == b.DepartNo &&
                              a.EmpStatus <= (byte)EmployeeEnum.EmpStatusInfo.Resign
                        select new EmpHealth
                        {
                            EmpCode = a.EmpCode,
                            EmpNo = a.EmpNo,
                            DepartNo = a.DepartNo,
                            DepartName = b.ChineseName,
                            EmpName = a.EmpName,
                            Birthday = a.Birthday.Value,
                            EmpStatusNo = a.EmpStatus,
                            IdNo = a.IdNo
                        };
                List<EmpHealth> datas = o.ToList<EmpHealth>();
                foreach (EmpHealth healthManage in healthManages)
                {
                    EmpHealth data = datas.FirstOrDefault(x => x.EmpNo == healthManage.EmpNo);
                    if (data == null)
                    {
                        continue;
                    }
                    healthManage.EmpCode = data.EmpCode;
                    healthManage.DepartName = data.DepartName;
                    healthManage.DepartNo = data.DepartNo;
                    healthManage.EmpName = data.EmpName;
                    healthManage.IdNo = data.IdNo;
                    healthManage.Birthday = data.Birthday;
                    healthManage.EmpStatusNo = data.EmpStatusNo;
                    healthManage.EmpStatus = data.EmpStatusNo.GetDescription<EmployeeEnum.EmpStatusInfo>();
                }
                IEnumerable<EmpHealth> healthManages2 = searchInfo.DepartNo == "0" ? healthManages : healthManages.Where(x => x.DepartNo == searchInfo.DepartNo).ToList<EmpHealth>();
                return healthManages2;
            }
        }

        public EmailResult SendNoticeEmail(EmailInfo data)
        {
            MailEnvelope mailSend = new MailEnvelope();
            mailSend.Subject = data.Title;
            mailSend.Content = data.Content;
            MailMessenger mailMsg = MessengerFactory.CreateMailMessenger() as MailMessenger;
            mailMsg.FromMail = "admin@mail.cmuh.org.tw";
            mailMsg.FromName = "系統管理員";
            mailMsg.IsHtml = true;
            if (data.Attachment != null)
            {
                List<System.Net.Mail.Attachment> attachments = new List<System.Net.Mail.Attachment>();
                foreach (Attachment item in data.Attachment)
                {
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(item.FullPath);
                    attachments.Add(attachment);
                }
                mailMsg.Attachments = attachments;
            }
            EmailResult emailResult = new EmailResult();
            foreach (string item in data.Recipient)
            {
                mailSend.To = item + "@mail.cmuh.org.tw";
                try
                {
                    mailMsg.Send(mailSend);
                    emailResult.Success.Add(item);
                }
                catch
                {
                    emailResult.Failure.Add(item);
                }
                mailSend.To = "";
            }
            return emailResult;
        }

        /// <summary>
        /// 伊克希曼 - 用IdNo取得EmpNo
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <param name="healthManages"></param>
        /// <returns></returns>
        private int GetEmpNo(string idNo)
        {
            using (DatabaseName context = new DatabaseName())
            {
                var o = from a in context.TableName
                        where a.IdNo == idNo
                        select a.EmpNo;
                return o.FirstOrDefault<int>();
            }
        }
        /// <summary>
        /// 伊克希曼 - 用EmpNo取得EmpHealthRecord
        /// </summary>
        /// <param name="idNo"></param>
        /// <returns></returns>
        private EmpHealthRecord GetEmpHealthRecord(int empNo)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int year = DateTime.Now.Year;
                var o = from a in context.TableName
                        where a.HealthYear == (short)year &&
                              a.EmpNo == empNo
                        select a;
                return o.FirstOrDefault<EmpHealthRecord>();
            }
        }
        /// <summary>
        /// 伊克希曼 - 存健檢日期
        /// </summary>
        /// <returns></returns>
        public bool SetHealthDate(EmpHealth empHealth)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int empNo = GetEmpNo(empHealth.IdNo);
                
                if (empNo == default(int))
                {
                    return false;
                }
                
                EmpHealthRecord empHealthRecord = GetEmpHealthRecord(empNo);
                empHealthRecord.HealthDate = empHealth.HealthDate;
                empHealthRecord.SystemTime = DateTime.Now;
                empHealthRecord.SystemUser = 99999;
                context.Entry(empHealthRecord).State = EntityState.Modified;
                return context.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 伊克希曼 - 存是否完成健檢
        /// </summary>
        /// <returns></returns>
        public bool SetIsHealth(EmpHealth empHealth)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int empNo = GetEmpNo(empHealth.IdNo);

                if (empNo == default(int))
                {
                    return false;
                }

                EmpHealthRecord empHealthRecord = GetEmpHealthRecord(empNo);
                empHealthRecord.IsHealth = empHealth.IsHealth == "1" ? true : false;
                empHealthRecord.SystemTime = DateTime.Now;
                empHealthRecord.SystemUser = 99999;
                context.Entry(empHealthRecord).State = EntityState.Modified;
                return context.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 伊克希曼 - 存報告日期與結果
        /// </summary>
        /// <returns></returns>
        public bool SetReport(EmpHealth empHealth)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int empNo = GetEmpNo(empHealth.IdNo);

                if (empNo == default(int))
                {
                    return false;
                }

                EmpHealthRecord empHealthRecord = GetEmpHealthRecord(empNo);
                empHealthRecord.ReportDate = empHealth.ReportDate;
                empHealthRecord.ReportResult = (byte)empHealth.ReportResultNo;
                empHealthRecord.SystemTime = DateTime.Now;
                empHealthRecord.SystemUser = 99999;
                context.Entry(empHealthRecord).State = EntityState.Modified;
                return context.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 伊克希曼 - 存回診日期
        /// </summary>
        /// <returns></returns>
        public bool SetReVisitDate(EmpHealth empHealth)
        {
            using (DatabaseName context = new DatabaseName())
            {
                int empNo = GetEmpNo(empHealth.IdNo);

                if (empNo == default(int))
                {
                    return false;
                }

                EmpHealthRecord empHealthRecord = GetEmpHealthRecord(empNo);
                empHealthRecord.ReVisitDate = empHealth.ReVisitDate;
                empHealthRecord.SystemTime = DateTime.Now;
                empHealthRecord.SystemUser = 99999;
                context.Entry(empHealthRecord).State = EntityState.Modified;
                return context.SaveChanges() > 0;
            }
        }

        #endregion
    }
}
