﻿angular.module("cmuh").controller("swallowingAssessmentController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "swallowingAssessment", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, swallowingAssessment) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5901;
        $scope.shareModel.documentTypeName = "吞嚥評估單";
        $scope.shareModel.documentFolderName = "SwallowingAssessment";
        $scope.shareModel.documentBodyName = "swallowingAssessmentBody";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);