﻿angular.module("cmuh").controller("swallowingAssessmentBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "$filter", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];
        $scope.viewModel.assess = [
            { title: "normal", value: 0 },
            { title: "slow respond", value: 1 },
            { title: "less effort", value: 2 },
            { title: "very weak", value: 3 },
            { title: "non", value: 4 }
        ];
    }
    //#endregion

    //#region 將null值改為空值
    var ifnull = function (value) {
        return value == null ? "" : value;
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#region 單選點擊兩次則清除資料
    $scope.clickRadio = function (item, index) {
        if (item.code === index) {
            item.value = "";
            item.code = "";
        } else {
            item.code = index;
        }
    }
    //#endregion

    //#region 計算平均值
    $scope.calculate = function () {
        debugger;
        var totle = 0;
        for (var i = 0; i < 3; i++) {
            totle += parseInt($scope.viewModel.document.body.tongueStrength.option[i].other) || 0;
        }
        $scope.viewModel.document.body.tongueStrength.option[3].other = totle / 3;
    }
    //#endregion

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $filter('date')(new Date($scope.viewModel.document.body.date.value), 'yyyy-MM-dd') + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.speech.title + ifnull($scope.viewModel.document.body.speech.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.occupation.title + ifnull($scope.viewModel.document.body.occupation.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.onsetTime.title + ifnull($scope.viewModel.document.body.onsetTime.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.education.title + ifnull($scope.viewModel.document.body.education.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.medicalDiagnosis.title + ifnull($scope.viewModel.document.body.medicalDiagnosis.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.cancerRelated.title + ifnull($scope.viewModel.document.body.cancerRelated.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.complication.title + ifnull($scope.viewModel.document.body.complication.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.intakeStatus.title + ifnull($scope.viewModel.document.body.intakeStatus.value) + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.assessmentItems.title  + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.jawMovement.title + ifnull($scope.viewModel.document.body.jawMovement.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.lipClosure.title + ifnull($scope.viewModel.document.body.lipClosure.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.showingTeeth.title + ifnull($scope.viewModel.document.body.showingTeeth.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.puffing.title + ifnull($scope.viewModel.document.body.puffing.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chewing.title + ifnull($scope.viewModel.document.body.chewing.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueA.title + ifnull($scope.viewModel.document.body.tongueA.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueP.title + ifnull($scope.viewModel.document.body.tongueP.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueR.title + ifnull($scope.viewModel.document.body.tongueR.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueL.title + ifnull($scope.viewModel.document.body.tongueL.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueU.title + ifnull($scope.viewModel.document.body.tongueU.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.tongueD.title + ifnull($scope.viewModel.document.body.tongueD.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.palateMovement.title + ifnull($scope.viewModel.document.body.palateMovement.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chocking.title + ifnull($scope.viewModel.document.body.chocking.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.hypernasality.title + ifnull($scope.viewModel.document.body.hypernasality.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.hyoid.title + ifnull($scope.viewModel.document.body.hyoid.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.swallowingReflex.title + ifnull($scope.viewModel.document.body.swallowingReflex.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.gagReflex.title + ifnull($scope.viewModel.document.body.gagReflex.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.cough.title + ifnull($scope.viewModel.document.body.cough.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sensory.title + ifnull($scope.viewModel.document.body.sensory.value) + $scope.viewModel.document.body.sensory.value == "abnormal" ? "，" + $scope.viewModel.document.body.sensory.abnormal + "\r\n" : "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.drooling.title + ifnull($scope.viewModel.document.body.drooling.value) + $scope.viewModel.document.body.drooling.value == "yes" ? "，" + $scope.viewModel.document.body.drooling.yes + "\r\n" : "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.wetVoice.title + ifnull($scope.viewModel.document.body.wetVoice.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.nasal.title + ifnull($scope.viewModel.document.body.nasal.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.biting.title + ifnull($scope.viewModel.document.body.biting.value) + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.stagesSwallowing.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.preparatory.title + ifnull($scope.viewModel.document.body.preparatory.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.oralPhase.title + ifnull($scope.viewModel.document.body.oralPhase.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.pharyngeal.title + ifnull($scope.viewModel.document.body.pharyngeal.value) + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.impression.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.suggestion.title + ifnull($scope.viewModel.document.body.suggestion.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.treatmentPlan.title + ifnull($scope.viewModel.document.body.treatmentPlan.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.postureChanges.title + ifnull($scope.viewModel.document.body.postureChanges.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.swallowManeuvers.title + ifnull($scope.viewModel.document.body.swallowManeuvers.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.others.title + ifnull($scope.viewModel.document.body.others.value) + "\r\n";
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);