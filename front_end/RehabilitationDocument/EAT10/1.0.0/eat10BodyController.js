﻿angular.module("cmuh").controller("eat10BodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 統計分數
    $scope.statisticsScore = function () {
        $scope.viewModel.document.body.statisticsScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.question.length; i++) {
            $scope.viewModel.document.body.statisticsScore.value += Number($scope.viewModel.document.body.question[i].code);
            switch($scope.viewModel.document.body.question[i].code)
            {
                case "0":
                    $scope.viewModel.document.body.question[i].value = "0=沒有問題";
                    break;
                case "1":
                    $scope.viewModel.document.body.question[i].value = "1=稍微(不適或程度)";
                    break;
                case "2":
                    $scope.viewModel.document.body.question[i].value = "2=感覺不適，尚可忍受";
                    break;
                case "3":
                    $scope.viewModel.document.body.question[i].value = "3=問題嚴重或程度";
                    break;
                case "4":
                    $scope.viewModel.document.body.question[i].value = "4=問題很嚴重";
                    break;
            }
        }
    }
    //#endregion

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $scope.viewModel.document.body.date.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.theme.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.theme2.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.question.length; i++) {
            var item = $scope.viewModel.document.body.question[i];
            $scope.viewModel.text += item.title + "\t" + item.value + "\r\n";
        }
        $scope.viewModel.text += $scope.viewModel.document.body.statisticsScore.title + $scope.viewModel.document.body.statisticsScore.value + "\r\n";
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);