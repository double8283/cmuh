﻿angular.module("cmuh").controller("eat10Controller", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "eat10", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, eat10) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5917;
        $scope.shareModel.documentTypeName = "EAT-10中文評量";
        $scope.shareModel.documentFolderName = "EAT10";
        $scope.shareModel.documentBodyName = "eat10Body";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);