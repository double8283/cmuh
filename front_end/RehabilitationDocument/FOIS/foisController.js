﻿angular.module("cmuh").controller("foisController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "fois", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, fois) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5915;
        $scope.shareModel.documentTypeName = "功能性由口進食量表";
        $scope.shareModel.documentFolderName = "FOIS";
        $scope.shareModel.documentBodyName = "foisBody";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);