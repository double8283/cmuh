﻿angular.module("cmuh").controller("foisBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $scope.viewModel.document.body.date.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.level.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.level.value + "\r\n";
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);