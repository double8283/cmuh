﻿angular.module("cmuh").controller("pronunciationController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "pronunciation", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, pronunciation) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5914;
        $scope.shareModel.documentTypeName = "國語構音評量表";
        $scope.shareModel.documentFolderName = "Pronunciation";
        $scope.shareModel.documentBodyName = "pronunciationBody";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);