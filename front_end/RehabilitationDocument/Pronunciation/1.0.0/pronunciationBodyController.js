﻿angular.module("cmuh").controller("pronunciationBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "$filter", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $filter('date')(new Date($scope.viewModel.document.body.date.value), 'yyyy-MM-dd') + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.diag.title + $scope.viewModel.document.body.diag.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.education.title + $scope.viewModel.document.body.education.value + "\r\n\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.wordsQuiz.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.wordsQuiz.details.length; i++) {
            var item = $scope.viewModel.document.body.wordsQuiz.details[i];
            if (item.title !== undefined) {
                $scope.viewModel.text += "\t" + item.title + "\r\n";
                for (var j = 0; j < item.questions.length; j++) {
                    var question = item.questions[j].title.replace(/&emsp;/g, "\t");
                    $scope.viewModel.text += "\t\t" + question + "\t" + item.questions[j].value + "\r\n";
                }
            }
            else {
                for (var j = 0; j < item.questions.length; j++) {
                    var question = item.questions[j].title.replace(/&emsp;/g, "\t");
                    $scope.viewModel.text += "\t" + question + "\t" + item.questions[j].value + "\r\n";
                }
            }
        }
        var names = ["articulation", "sentence", "diagnosis"];
        for (var i = 0; i < names.length; i++) {
            var name = names[i];
            $scope.viewModel.text += $scope.viewModel.document.body[name].title + "\r\n";
            for (var j = 0; j < $scope.viewModel.document.body[name].details.length; j++) {
                var item = $scope.viewModel.document.body[name].details[j];
                $scope.viewModel.text += "\t" + item.title + "\t" + item.value + "\r\n";
            }
        }
        

        //$scope.viewModel.text += $scope.viewModel.document.body.articulation.title + "\r\n";
        //for (var i = 0; i < $scope.viewModel.document.body.articulation.value.length; i++) {
        //    var item = $scope.viewModel.document.body.articulation.value[i];
        //    $scope.viewModel.text += "\t" + item.title + "\t" + item.value + "\r\n";
        //}
        //$scope.viewModel.text += $scope.viewModel.document.body.sentence.title + "\r\n";
        //for (var i = 0; i < $scope.viewModel.document.body.sentence.value.length; i++) {
        //    var item = $scope.viewModel.document.body.sentence.value[i];
        //    $scope.viewModel.text += "\t" + item.title + "\t" + item.value + "\r\n";
        //}
        //$scope.viewModel.text += $scope.viewModel.document.body.diagnosis.title + "\r\n";
        //$scope.viewModel.text += "\t" + $scope.viewModel.document.body.diagnosis.impression + "\r\n";
        //$scope.viewModel.text += "\t" + $scope.viewModel.document.body.diagnosis.plan + "\r\n";
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);