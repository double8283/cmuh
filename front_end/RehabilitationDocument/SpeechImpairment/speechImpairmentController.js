﻿angular.module("cmuh").controller("speechImpairmentController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "speechImpairment", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, speechImpairment) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5918;
        $scope.shareModel.documentTypeName = "運動性言語障礙評估表";
        $scope.shareModel.documentFolderName = "SpeechImpairment";
        $scope.shareModel.documentBodyName = "speechImpairmentBody";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);