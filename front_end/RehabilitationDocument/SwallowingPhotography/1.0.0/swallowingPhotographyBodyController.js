﻿angular.module("cmuh").controller("swallowingPhotographyBodyController", ["$scope", "$q", "$http", "cmuhBase", "toaster", "$timeout", "$filter", function ($scope, $q, $http, cmuhBase, toaster, $timeout, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.lateralView = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];
        $scope.viewModel.location = [{ code: 'A' }, { code: 'B' }, { code: 'C' }, { code: 'D' }, { code: 'E' }, { code: 'F' }];
        $scope.viewModel.tablUrls = [
            cmuhBase.stringFormat("App/RehabilitationDocument/SwallowingPhotography/1.0.0/content/basicInfo.html?{0}", cmuhBase.getVersionNo()),
            cmuhBase.stringFormat("App/RehabilitationDocument/SwallowingPhotography/1.0.0/content/mbsImpScores.html?{0}", cmuhBase.getVersionNo()),
            cmuhBase.stringFormat("App/RehabilitationDocument/SwallowingPhotography/1.0.0/content/penetration.html?{0}", cmuhBase.getVersionNo()),
            cmuhBase.stringFormat("App/RehabilitationDocument/SwallowingPhotography/1.0.0/content/evaluationStrategies.html?{0}", cmuhBase.getVersionNo()),
            cmuhBase.stringFormat("App/RehabilitationDocument/SwallowingPhotography/1.0.0/content/evaluationEndeavors.html?{0}", cmuhBase.getVersionNo())
        ];
        $scope.viewModel.studyNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        $scope.viewModel.painScales = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }
    //#endregion

    //#region 將null值改為空值
    var ifnull = function (value) {
        return value == null ? "" : value;
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
    }
    //#endregion

    //#region 統計口腔分數
    $scope.statisticsOralScore = function () {
        $scope.viewModel.document.body.oralImpairmentScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.oralImpairmentOption.length; i++) {
            $scope.viewModel.document.body.oralImpairmentScore.value += Number($scope.viewModel.document.body.oralImpairmentOption[i].code);
        }
    }
    //#endregion

    //#region 統計咽喉分數
    $scope.statisticsPharyngealScore = function () {
        $scope.viewModel.document.body.pharyngealImpairmentScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.pharyngealImpairmentOption.length; i++) {
            $scope.viewModel.document.body.pharyngealImpairmentScore.value += Number($scope.viewModel.document.body.pharyngealImpairmentOption[i].code);
        }
    }
    //#endregion

    //#region 統計食道分數
    $scope.statisticsEsophagealScore = function () {
        $scope.viewModel.document.body.esophagealImpairmentScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.esophagealImpairmentOption.length; i++) {
            $scope.viewModel.document.body.esophagealImpairmentScore.value += Number($scope.viewModel.document.body.esophagealImpairmentOption[i].code);
        }
    }
    //#endregion

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $filter('date')(new Date($scope.viewModel.document.body.date.value), 'yyyy-MM-dd') + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.section1.title + "\r\n";
        var names = ["timeOut", "examStartTime", "examEndTime", "lastDate", "sinceTime", "status", "studyNumber"];
        for (var i = 0; i < names.length; i++) {
            $scope.viewModel.text += $scope.viewModel.document.body[names[i]].title + ifnull($scope.viewModel.document.body[names[i]].value) + "\r\n";
        }
        $scope.viewModel.text += "\r\n";

        $scope.viewModel.text += $scope.viewModel.document.body.painAssessment.title + ifnull($scope.viewModel.document.body.painAssessment.value) + "\r\n";
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.painAssessment.location.title + ifnull($scope.viewModel.document.body.painAssessment.location.value) + "\r\n";
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.painAssessment.scale.title + ifnull($scope.viewModel.document.body.painAssessment.scale.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.preStatus.title + ifnull($scope.viewModel.document.body.preStatus.value) + "\r\n";
        if ($scope.viewModel.document.body.preStatus.code != 0) {
            $scope.viewModel.text += "\t" + ifnull($scope.viewModel.document.body.preStatus.options[$scope.viewModel.document.body.preStatus.code].value) + "\r\n";
        }
        debugger;
        $scope.viewModel.text += $scope.viewModel.document.body.preDietGrade.title + ifnull($scope.viewModel.document.body.preDietGrade.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.preLiquidGrade.title + ifnull($scope.viewModel.document.body.preLiquidGrade.value) + "\r\n";
        $scope.viewModel.text += "\r\n";

        $scope.viewModel.text += $scope.viewModel.document.body.oralTransit.title + ifnull($scope.viewModel.document.body.oralTransit.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.pharyngealDelay.title + ifnull($scope.viewModel.document.body.pharyngealDelay.value) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.pharyngealTransit.title + ifnull($scope.viewModel.document.body.pharyngealTransit.value) + "\r\n";
        $scope.viewModel.text += "\r\n";

        $scope.viewModel.text += $scope.viewModel.document.body.section2.title + "\r\n";

        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.oralImpairment.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.oralImpairment.questions.length; i++) {
            var question = $scope.viewModel.document.body.oralImpairment.questions[i];
            $scope.viewModel.text += "\t\t" + question.title + question.range + "：\r\n"
            if (question.code !== '') {
                $scope.viewModel.text += "\t\t\t" + ifnull(question.value) + "\r\n";
            }
            if (i == 4 && question.code !== '') {
                for (var j = 0; j < question.options.length; j++) {
                    if (question.options[j].code === question.code) {
                        $scope.viewModel.text += "\t\t\t\t" + ifnull(question.options[j].value) + "\r\n";
                    }
                }

            }
        }
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.oralImpairment.score.title + ifnull($scope.viewModel.document.body.oralImpairment.score.value) + "\r\n";

        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.pharyngealImpairment.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.pharyngealImpairment.questions.length; i++) {
            var question = $scope.viewModel.document.body.pharyngealImpairment.questions[i];
            $scope.viewModel.text += "\t\t" + question.title + question.range + "：\r\n"
            if (question.code !== '') {
                $scope.viewModel.text += "\t\t\t" + ifnull(question.value) + "\r\n";
            }
            if (i == 9 && question.code !== '') {
                for (var j = 0; j < question.options.length; j++) {
                    if (question.options[j].code === question.code) {
                        $scope.viewModel.text += "\t\t\t\t" + ifnull(question.options[j].value) + "\r\n";
                    }
                }

            }
        }
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.pharyngealImpairment.score.title + ifnull($scope.viewModel.document.body.pharyngealImpairment.score.value) + "\r\n";

        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.esophagealImpairment.title + "\r\n";
        $scope.viewModel.text += "\t\t" + $scope.viewModel.document.body.esophagealImpairment.question.title + $scope.viewModel.document.body.esophagealImpairment.question.range + "：\r\n"
        if ($scope.viewModel.document.body.esophagealImpairment.question.code !== '') {
            $scope.viewModel.text += "\t\t\t" + ifnull($scope.viewModel.document.body.esophagealImpairment.question.value) + "\r\n";
        }
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.esophagealImpairment.score.title + ifnull($scope.viewModel.document.body.esophagealImpairment.score.value) + "\r\n";

        var names = ["penetrationAspiration", "swallowingAbility"]
        for (var i = 0; i < names.length; i++) {
            $scope.viewModel.text += $scope.viewModel.document.body[names[i]].title + "\r\n";
            for (var j = 0; j < $scope.viewModel.document.body[names[i]].options.length; j++) {
                var option = $scope.viewModel.document.body[names[i]].options[j];
                if (option.isChecked == "true") {
                    $scope.viewModel.text += "\t" + option.title + "\r\n";
                    $scope.viewModel.text += "\t\t" + option.value + "\r\n";
                }
            }
        }

        $scope.viewModel.text += $scope.viewModel.document.body.postStatus.title + $scope.viewModel.document.body.postStatus.value + "\r\n";
        if ($scope.viewModel.document.body.postStatus.code != 0) {
            $scope.viewModel.text += "\t" + $scope.viewModel.document.body.postStatus.options[$scope.viewModel.document.body.postStatus.code].value + "\r\n";
        }
        $scope.viewModel.text += $scope.viewModel.document.body.postDietGrade.title + $scope.viewModel.document.body.postDietGrade.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.postLiquidGrade.title + $scope.viewModel.document.body.postLiquidGrade.value + "\r\n";

        var names = ["evaluationStrategies", "evaluationEndeavors"];
        for (var i = 0; i < names.length; i++) {
            $scope.viewModel.text += $scope.viewModel.document.body[names[i]].title + "\r\n";
            for (var j = 0; j < $scope.viewModel.document.body[names[i]].questions.length; j++) {
                var question = $scope.viewModel.document.body[names[i]].questions[j];
                if (question.isChecked == "true") {
                    $scope.viewModel.text += "\t" + question.title + "\r\n";
                    $scope.viewModel.text += "\t\t" + question.impact.title + ifnull(question.impact.value) + "\r\n";
                    if (question.impact.code === 0 || question.impact.code === 1) {
                        $scope.viewModel.text += "\t\t" + question.influenced.title + "\r\n";
                        for (var k = 0; k < question.influenced.options.length; k++) {
                            $scope.viewModel.text += "\t\t\t" + question.influenced.options[k].title + ifnull(question.influenced.options[k].value) + "\r\n";
                        }
                    }
                    $scope.viewModel.text += "\t\t" + question.history.title + "\r\n";
                    if (question.history.value !== '') {
                        var array = question.history.value.split("、");
                        for (var k = 0; k < array.length; k++) {
                            $scope.viewModel.text += "\t\t\t" + array[k] + "\r\n";
                        }
                    }

                }
            }
        }

        $scope.viewModel.text += $scope.viewModel.document.body.prognosisImprovement.title + ifnull($scope.viewModel.document.body.prognosisImprovement.value) + "\r\n";
        debugger;
        var names = ["prognosisBasedOn", "longGoals"]
        for (var i = 0; i < names.length; i++) {
            var item = $scope.viewModel.document.body[names[i]];
            $scope.viewModel.text += item.title + "\r\n";
            for (var j = 0; j < item.options.length; j++) {
                if (item.options[j].isChecked == "true") {
                    $scope.viewModel.text += "\t" + item.options[j].title + ifnull(item.options[j].value) + "\r\n";
                }
            }
        }

        $scope.viewModel.text += $scope.viewModel.document.body.shortGoals.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.shortGoals.options.length; i++) {
            var option = $scope.viewModel.document.body.shortGoals.options[i];
            if (option.isChecked == "true") {
                $scope.viewModel.text += "\t" + option.title + "\r\n";
                //Diet
                if (i === 0) {
                    for (var j = 0; j < option.questions.length; j++) {
                        $scope.viewModel.text += "\t\t" + option.questions[j].title + "\t" + option.questions[j].value + "\r\n";

                        if (j == 0 && option.questions[j].code === 0) {
                            for (var k = 0; k < option.questions[j].questions.length; k++) {
                                $scope.viewModel.text += "\t\t\t" + option.questions[j].questions[k].title + "\t" + ifnull(option.questions[j].questions[k].value);
                                if (k == 0) {
                                    $scope.viewModel.text += "\t" + "\t" + ifnull(option.questions[j].questions[k].options[5].value);
                                } else if (k == 2) {
                                    $scope.viewModel.text += ifnull(option.questions[j].questions[k].unit);
                                }
                                $scope.viewModel.text += "\r\n";
                            }
                        }

                    }
                }
                    //Guidelines/strategies
                else if (i == 1) {
                    for (var j = 0; j < option.options.length; j++) {
                        if (option.options[j].isChecked == "true") {
                            $scope.viewModel.text += "\t\t\t" + option.options[j].title + "\t" + ifnull(option.options[j].value) + "\r\n";
                        }
                    }
                    for (var j = 0; j < option.questions.length; j++) {
                        $scope.viewModel.text += "\t\t" + option.questions[j].title + "\t" + ifnull(option.questions[j].value) + "\t" + ifnull(option.questions[j].unit) + "\r\n";
                    }
                }
                    //Independent Home Exercises
                else if (i == 2) {
                    for (var j = 0; j < option.questions.length; j++) {
                        $scope.viewModel.text += "\t\t" + option.questions[j].title;
                        if (j == 0) {
                            for (var k = 0; k < option.questions[j].options.length; k++) {
                                if (option.questions[j].options[k].isChecked == "true") {
                                    $scope.viewModel.text += "\r\n\t\t\t" + option.questions[j].options[k].title + "\t" + ifnull(option.questions[j].options[k].value);
                                }
                            }
                        } else {
                            $scope.viewModel.text += "\t" + ifnull(option.questions[j].value) + "\t" + ifnull(option.questions[j].unit)
                        }
                        $scope.viewModel.text += "\r\n";
                    }
                }
                    //Structured therapy with a speech-Language Pathologist
                else if (i == 3) {
                    for (var j = 0; j < option.questions.length; j++) {
                        $scope.viewModel.text += "\t\t" + option.questions[j].title;
                        if (j == 0 || j == 1) {
                            for (var k = 0; k < option.questions[j].options.length; k++) {
                                if (option.questions[j].options[k].isChecked == "true") {
                                    $scope.viewModel.text += "\r\n\t\t\t" + option.questions[j].options[k].title + "\t" + ifnull(option.questions[j].options[k].value);
                                }
                            }
                        } else {
                            $scope.viewModel.text += "\t" + ifnull(option.questions[j].value) + "\t" + ifnull(option.questions[j].unit);
                        }
                        $scope.viewModel.text += "\r\n";
                    }
                }
                    //Education
                else if (i == 4) {
                    for (var j = 0; j < option.questions.length; j++) {
                        $scope.viewModel.text += "\t\t" + option.questions[j].title + "\r\n";
                        for (var k = 0; k < option.questions[j].options.length; k++) {
                            if (option.questions[j].options[k].isChecked == "true") {
                                $scope.viewModel.text += "\t\t\t" + option.questions[j].options[k].title + "\r\n";
                            }
                        }
                    }
                }
                    //Other
                else {
                    $scope.viewModel.text += "\t\t" + ifnull(option.value) + "\r\n";
                }
            }
        }

        $scope.viewModel.text += $scope.viewModel.document.body.clinical.title + "\r\n";
        $scope.viewModel.text += "\t" + ifnull($scope.viewModel.document.body.clinical.value) + "\r\n";
    };
    //#endregion
    $scope.computeScore = function (item) {
        item.score.value = 0;
        if (item.questions !== undefined) {
            for (var i = 0; i < item.questions.length; i++) {
                var num = Number(item.questions[i].title.split('-')[0]);
                var array = [1, 5, 15, 16];
                if (item.questions[i].code >= 0) {
                    if (item.questions[i].code == 1 && array.indexOf(num) >= 0) {
                        continue;
                    }
                    item.score.value += Number(item.questions[i].code);
                }
            }
        } else {
            if (item.question.code >= 0) {
                item.score.value += Number(item.question.code);
            }
        }
    }
    $scope.clickRadio = function (item, index) {
        if (item.code === index) {
            item.value = "";
            item.code = "";
        } else {
            item.code = index;
        }
    }
    $scope.clickCheckbox = function (item, value) {
        var titleList = [];
        if (cmuhBase.isNullOrEmpty(item.value) == false) {
            titleList = item.value.split("、");
        }
        var index = titleList.indexOf(value);
        if (index >= 0) {
            titleList.splice(index, 1);
        } else {
            titleList.push(value);
        }
        item.value = titleList.join("、");
    }
    $scope.isItemChecked = function (item, value) {
        if (item === undefined || cmuhBase.isNullOrEmpty(item.value) == true) {
            return;
        }
        var titleList = item.value.split("、");
        for (var title in titleList) {
            if (titleList[title] == value) {
                return true;
            }
        }
        return false;
    }

    $scope.clearValue = function (items) {
        for (var i = 0; i < items.length; i++) {
            items[i].code = "";
            items[i].value = "";
        }
    }
    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    
    //#endregion

}]);