﻿angular.module("cmuh").controller("swallowingPhotographyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "medDocumentManager", "toaster", "swallowingPhotography", function ($rootScope, $scope, $q, $http, cmuhBase, medDocumentManager, toaster, swallowingPhotography) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5902;
        $scope.shareModel.documentTypeName = "吞嚥攝影紀錄單";
        $scope.shareModel.documentFolderName = "SwallowingPhotography";
        $scope.shareModel.documentBodyName = "swallowingPhotographyBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);