﻿angular.module("cmuh").controller("lifeQualityController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "lifeQuality", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, lifeQuality) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5916;
        $scope.shareModel.documentTypeName = "UW-QQLV4生活品質問卷";
        $scope.shareModel.documentFolderName = "LifeQuality";
        $scope.shareModel.documentBodyName = "lifeQualityBody";
        $scope.shareModel.versionId = "1.0.0"; 
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);