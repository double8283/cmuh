﻿angular.module("cmuh").controller("lifeQualityBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "$filter", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        ////初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];
        
    }
    //#endregion 

    //#endregion

    //#region 事件
    
    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.options.length; i++)
        {
            if (item.options[i].isChecked == "true") {
                array.push(item.options[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#region 判斷13題是否勾選超過3個選項
    $scope.isDisabled = function (isChecked, value) {
        if (isChecked == "true") {
            return false;
        }
        
        var splits = value.split("、");
        if (splits.length == 3) {
            return true;
        }
        return false;
        //if (item.value == "true") {
        //    $scope.viewModel.document.body.question13.check++;
        //} else {
        //    $scope.viewModel.document.body.question13.check--;
        //}

        //if ($scope.viewModel.document.body.question13.check > 3) {
        //    $scope.viewModel.document.body.question13.check--;
        //    $scope.viewModel.over3Window.center().open();
        //    item.value = false;
        //}
    }
    //#endregion

    //#region 單選點擊兩次則清除資料
    $scope.clickRadio = function (item, index) {
        if (item.code === item.options[index].code) {
            item.value = "";
            item.code = "";
        } else {
            item.code = item.options[index].code;
        }
    }
    //#endregion

    //#region 開啟所有文字編輯視窗
    $scope.viewModel.openAllText = function () {
        
        $scope.viewModel.text = $scope.viewModel.document.body.information.title + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.date.title + $filter('date')(new Date($scope.viewModel.document.body.date.value), 'yyyy-MM-dd') + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.chartNo.title + $scope.viewModel.document.body.chartNo.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.sex.title + $scope.viewModel.document.body.sex.value + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.birthday.title + $scope.viewModel.document.body.birthday.value.substring(0, 10) + "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.age.title + $scope.viewModel.document.body.age.value + "\r\n";
        $scope.viewModel.text += "\r\n";
        $scope.viewModel.text += $scope.viewModel.document.body.theme.title + "\r\n";
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.lifeQuality.title + "\r\n";
        debugger;
        for (var i = 0; i < $scope.viewModel.document.body.lifeQuality.questions.length; i++) {
            var item = $scope.viewModel.document.body.lifeQuality.questions[i];
            $scope.viewModel.text += "\t\t" + item.title + "\t" + item.value + "\r\n";
        }
        $scope.viewModel.text += "\t" + $scope.viewModel.document.body.generalQuestion.title + "\r\n";
        for (var i = 0; i < $scope.viewModel.document.body.generalQuestion.questions.length; i++) {
            var item = $scope.viewModel.document.body.generalQuestion.questions[i];
            $scope.viewModel.text += "\t\t" + item.title + "\t" + item.value + "\r\n";
        }
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);