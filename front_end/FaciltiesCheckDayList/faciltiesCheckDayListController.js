﻿angular.module("cmuh").controller("faciltiesCheckDayListController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "faciltiesCheckDayList", function ($scope, $http, $q, cmuhBase, toaster, faciltiesCheckDayList) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //檢查日期
        $scope.viewModel.checkDay = new Date();
        $scope.viewModel.checkDayOption = {
            format: 'yyyy-MM-dd',
            max: new Date()
        };

        //取得單位地點清單
        getLocationInfos();

        //取得部門清單
        getDeptList();

        $scope.viewModel.locationNo = 0;

        document.getElementById("contentDiv").style.height = window.innerHeight - 200 + "px";
    }
    //#endregion

    //#region 取得部門清單
    var getDeptList = function () {
        faciltiesCheckDayList.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
                    $scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
                    //取得使用者單位
                    $scope.getUserDept();
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 儲存檢查表內容
    var setFacilitiesCheck = function () {
        faciltiesCheckDayList.setFacilitiesCheck($scope.viewModel.check).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，存檔成功");
                    $scope.getCheckList();
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得上次檢查人員名稱
    var getEmpName = function () {
        faciltiesCheckDayList.getEmpName($scope.viewModel.check.checkUser).then(
            function (success) {
                $scope.viewModel.check.checkUserName = angular.fromJson(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得單位地點清單
    var getLocationInfos = function () {
        faciltiesCheckDayList.getLocationInfos().then(
            function (success) {
                $scope.viewModel.locationNames = [];
                $scope.viewModel.locationInfos = [];
                $scope.viewModel.locationInfos[0] = [{ "locationNo": "0", "locationName": "", "details": [] }];
                $scope.viewModel.locationInfos[0] = $scope.viewModel.locationInfos[0].concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 存檔按鈕
    $scope.save = function () {
        if ($scope.viewModel.check.checkTime != "2999-12-31T00:00:00+08:00") {
            if (confirm("存檔會覆蓋上次存檔資料，確定存檔嗎？") != true) {
                return;
            }
        }
        $scope.viewModel.check.locationNo = $scope.viewModel.locationNo;
        $scope.viewModel.check.checkUser = $scope.shareModel.userInfos.userId.replace(/[^\d]/g, '');
        $scope.viewModel.check.checkTime = new Date();
        setFacilitiesCheck();
        for (var i = 0; i < $scope.viewModel.check.facilitiesDts.length; i++) {
            if ($scope.viewModel.check.facilitiesDts[i].itemNo % 100 != 0 && $scope.viewModel.check.facilitiesDts[i].checkStatus == 0) {
                if (confirm("請問要跳轉至維修申請頁面嗎？")) {
                    $scope.shareModel.navigation(350, 530);
                }
                return;
            }
        }
    }
    //#endregion

    //#region 取得使用者單位
    $scope.getUserDept = function () {
        faciltiesCheckDayList.getUserDept($scope.shareModel.userInfos.userId).then(
            function (success) {
                $scope.viewModel.checkDepart = success.data.departNo;
                //從自主檢查表管理頁面跳過來
                if (!!$scope.shareModel.date && !!$scope.shareModel.checkDepart) {
                    $scope.viewModel.checkDay = $scope.shareModel.date;
                    $scope.viewModel.checkDepart = $scope.shareModel.checkDepart;
                    $scope.viewModel.locationNo = $scope.shareModel.locationNo;
                    $scope.viewModel.locationInfos = $scope.shareModel.locationInfos;
                    $scope.viewModel.locationInfos0 = $scope.shareModel.locationInfos0;
                    $scope.viewModel.locationInfos1 = $scope.shareModel.locationInfos1;
                    $scope.viewModel.locationInfos2 = $scope.shareModel.locationInfos2;
                    $scope.viewModel.locationInfos3 = $scope.shareModel.locationInfos3;
                    $scope.shareModel.date = "";
                    $scope.shareModel.checkDepart = "";
                    $scope.shareModel.locationNo = "";
                    $scope.shareModel.locationInfos0 = "";
                    $scope.shareModel.locationInfos1 = "";
                    $scope.shareModel.locationInfos2 = "";
                    $scope.shareModel.locationInfos3 = "";
                }
                //取得檢查表內容
                $scope.getCheckList();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 檢查表全選
    $scope.selectAll = function (itemNo) {
        for (var i = 0; i < $scope.viewModel.check.facilitiesDts.length; i++) {
            if ($scope.viewModel.check.facilitiesDts[i].itemNo > itemNo && $scope.viewModel.check.facilitiesDts[i].itemNo < itemNo + 100)
            {
                $scope.viewModel.check.facilitiesDts[i].checkStatus = $scope.viewModel.select;
            }
        }
    }
    //#endregion

    //#region 取得檢查表內容
    $scope.getCheckList = function () {
        var data = {
            checklistNo: 2,
            recordDate: cmuhBase.getDateFormat($scope.viewModel.checkDay),
            departNo: $scope.viewModel.checkDepart,
            locationNo: $scope.viewModel.locationNo
        };
        faciltiesCheckDayList.getCheckList(data).then(
            function (success) {
                $scope.viewModel.check = success.data;
                $scope.viewModel.check.recordDate = cmuhBase.getDateFormat($scope.viewModel.checkDay);
                if ($scope.viewModel.check.checkUser != 999999) {
                    getEmpName();
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 地點改變
    $scope.locationChange = function (no) {
        var data = [{
            locationNo: 0,
            locationName: "",
            details: []
        }];
        switch (no) {
            case 0:
                var dropdownlist = $("#locationInfos0").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[1] = !!$scope.viewModel.locationInfos[0][selectedIndex] == true ? $scope.viewModel.locationInfos[0][selectedIndex].details : [];
                $scope.viewModel.locationInfos[2] = !!$scope.viewModel.locationInfos[1][0] == true ? $scope.viewModel.locationInfos[1][0].details : [];
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][0] == true ? data.concat($scope.viewModel.locationInfos[2][0].details) : [];
                $scope.viewModel.locationInfos1 = !!$scope.viewModel.locationInfos[1][0] == true ? $scope.viewModel.locationInfos[1][0].locationNo : '';
                $scope.viewModel.locationInfos2 = !!$scope.viewModel.locationInfos[2][0] == true ? $scope.viewModel.locationInfos[2][0].locationNo : '';
                $scope.viewModel.locationInfos3 = '';
                break;
            case 1:
                var dropdownlist = $("#locationInfos1").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[2] = !!$scope.viewModel.locationInfos[1][selectedIndex] == true ? $scope.viewModel.locationInfos[1][selectedIndex].details : [];
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][0] == true ? data.concat($scope.viewModel.locationInfos[2][0].details) : [];
                $scope.viewModel.locationInfos2 = !!$scope.viewModel.locationInfos[2][0] == true ? $scope.viewModel.locationInfos[2][0].locationNo : '';
                $scope.viewModel.locationInfos3 = '';
                break;
            case 2:
                var dropdownlist = $("#locationInfos2").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][selectedIndex] == true ? data.concat($scope.viewModel.locationInfos[2][selectedIndex].details) : [];
                $scope.viewModel.locationInfos3 = '';
                break;
        }
        if (!!$scope.viewModel.locationInfos3 == true && $scope.viewModel.locationInfos3 != "0") {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos3;
        } else if (!!$scope.viewModel.locationInfos2 == true) {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos2;
        } else if (!!$scope.viewModel.locationInfos1 == true) {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos1;
        } else {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos0;
        }
        //取得檢查表內容
        $scope.getCheckList();
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);