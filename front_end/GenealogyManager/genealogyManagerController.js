﻿angular.module('cmuh').controller("genealogyManagerController", ["$scope", "$q", "cmuhBase", "toaster", "genealogyManager", function ($scope, $q, cmuhBase, toaster, genealogyManager) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        //以病人性別決定配偶父母稱謂
        if ($scope.appModel.patientInfos.sex == '男') {
            var spouseDad = '岳父';
            var spouseMom = '岳母';
        } else {
            var spouseDad = '公公';
            var spouseMom = '婆婆';
        }

        //家族陣列
        $scope.viewModel.family = [
                { no: 0, show: false, name: "配偶", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 1, show: false, name: "爸爸", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 2, show: false, name: "媽媽", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 3, show: false, name: "爺爺", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 4, show: false, name: "奶奶", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 5, show: false, name: "外公", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 6, show: false, name: "外婆", age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 7, show: false, name: spouseDad, age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 8, show: false, name: spouseMom, age: "", marriage: "婚", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 9, show: false, name: "叔伯", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 10, show: false, name: "姑姑", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 11, show: false, name: "舅舅", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 12, show: false, name: "阿姨", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 13, show: false, name: "兄弟1", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 14, show: false, name: "兄弟2", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 15, show: false, name: "姊妹1", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 16, show: false, name: "姊妹2", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'Y' },
                { no: 17, show: false, name: "兒子1", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 18, show: false, name: "兒子2", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 19, show: false, name: "兒子3", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 20, show: false, name: "兒子4", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 21, show: false, name: "女兒1", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 22, show: false, name: "女兒2", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 23, show: false, name: "女兒3", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' },
                { no: 24, show: false, name: "女兒4", age: "", marriage: "否", life: "存", live: "同", annotation: "", display: 'N' }
        ];

        //頁籤點選狀態
        $scope.viewModel.tabs = ['active', '', '', ''];
    }
    //#endregion

    //#endregion


    //#region 事件

    //#region 儲存
    $scope.saveImage = function () {
        //"data:image/svg+xml;base64" + window.btoa(new XMLSerializer().serializeToString(document.getElementById("svgFamily")));
        $scope.$emit('setGenealogyText', $scope.viewModel.family);
    }
    //#endregion

    //#region 上傳圖片
    $scope.uploadImage = function () {
        document.getElementById("noShowUploadImage").addEventListener('change', readFile, false);
        function readFile() {
            var file = this.files[0];
            if (!/image\/\w+/.test(file.type)) {
                alert("必須為圖片");
                return false;
            }
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                document.getElementById("svgFamily").innerHTML = "<image xlink:href='" + this.result + "' src='" + this.result + "' x='0' y='0' width='600' height='300' />";
            }
        }
    }
    //#endregion


    //#region 頁籤改變
    $scope.tabChange = function (item) {
        for (i = 0; i < 25; i++) { $scope.viewModel.family[i].display = 'N'; }
        switch (item) {
            case 0:
                $scope.viewModel.tabs[0] = 'active'; $scope.viewModel.tabs[1] = ''; $scope.viewModel.tabs[2] = ''; $scope.viewModel.tabs[3] = '';
                $scope.viewModel.family[1].display = 'Y'; $scope.viewModel.family[2].display = 'Y'; $scope.viewModel.family[13].display = 'Y'; $scope.viewModel.family[14].display = 'Y'; $scope.viewModel.family[15].display = 'Y'; $scope.viewModel.family[16].display = 'Y';
                break;
            case 1:
                $scope.viewModel.tabs[0] = ''; $scope.viewModel.tabs[1] = 'active'; $scope.viewModel.tabs[2] = ''; $scope.viewModel.tabs[3] = '';
                $scope.viewModel.family[0].display = 'Y'; $scope.viewModel.family[7].display = 'Y'; $scope.viewModel.family[8].display = 'Y';
                break;
            case 2:
                $scope.viewModel.tabs[0] = ''; $scope.viewModel.tabs[1] = ''; $scope.viewModel.tabs[2] = 'active'; $scope.viewModel.tabs[3] = '';
                $scope.viewModel.family[17].display = 'Y'; $scope.viewModel.family[18].display = 'Y'; $scope.viewModel.family[19].display = 'Y'; $scope.viewModel.family[20].display = 'Y'; $scope.viewModel.family[21].display = 'Y'; $scope.viewModel.family[22].display = 'Y'; $scope.viewModel.family[23].display = 'Y'; $scope.viewModel.family[24].display = 'Y';
                break;
            case 3:
                $scope.viewModel.tabs[0] = ''; $scope.viewModel.tabs[1] = ''; $scope.viewModel.tabs[2] = ''; $scope.viewModel.tabs[3] = 'active';
                $scope.viewModel.family[3].display = 'Y'; $scope.viewModel.family[4].display = 'Y'; $scope.viewModel.family[5].display = 'Y'; $scope.viewModel.family[6].display = 'Y'; $scope.viewModel.family[9].display = 'Y'; $scope.viewModel.family[10].display = 'Y'; $scope.viewModel.family[11].display = 'Y'; $scope.viewModel.family[12].display = 'Y';
                break;
        }
    }
    //#endregion

    //#region 顯示連動限制
    $scope.showGearing = function () {
        if ($scope.viewModel.family[3].show == true || $scope.viewModel.family[4].show == true) { $scope.viewModel.family[1].show = true; } //顯示爺爺或奶奶就要顯示爸爸
        if ($scope.viewModel.family[5].show == true || $scope.viewModel.family[6].show == true) { $scope.viewModel.family[2].show = true; } //顯示外公或外婆就要顯示媽媽
        if ($scope.viewModel.family[7].show == true || $scope.viewModel.family[8].show == true) { $scope.viewModel.family[0].show = true; } //顯示岳父或岳母就要顯示配偶
        if ($scope.viewModel.family[9].show == true || $scope.viewModel.family[10].show == true) { $scope.viewModel.family[1].show = true; } //顯示叔伯或姑姑就要顯示爸爸
        if ($scope.viewModel.family[11].show == true || $scope.viewModel.family[12].show == true) { $scope.viewModel.family[2].show = true; } //顯示舅舅或阿姨就要顯示媽媽
        if ($scope.viewModel.family[14].show == true) { $scope.viewModel.family[13].show = true; } //顯示兄弟2就要顯示兄弟1
        if ($scope.viewModel.family[16].show == true) { $scope.viewModel.family[15].show = true; } //顯示姊妹2就要顯示姊妹1
        if ($scope.viewModel.family[20].show == true) { $scope.viewModel.family[19].show = true; } //顯示兒子4就要顯示兒子3
        if ($scope.viewModel.family[19].show == true) { $scope.viewModel.family[18].show = true; } //顯示兒子3就要顯示兒子2
        if ($scope.viewModel.family[18].show == true) { $scope.viewModel.family[17].show = true; } //顯示兒子2就要顯示兒子1
        if ($scope.viewModel.family[17].show == true) { $scope.viewModel.family[0].show = true; } //顯示兒子1就要顯示配偶
        if ($scope.viewModel.family[24].show == true) { $scope.viewModel.family[23].show = true; } //顯示女兒4就要顯示女兒3
        if ($scope.viewModel.family[23].show == true) { $scope.viewModel.family[22].show = true; } //顯示女兒3就要顯示女兒2
        if ($scope.viewModel.family[22].show == true) { $scope.viewModel.family[21].show = true; } //顯示女兒2就要顯示女兒1
        if ($scope.viewModel.family[21].show == true) { $scope.viewModel.family[0].show = true; } //顯示女兒1就要顯示配偶
    }
    //#endregion

    //#region 婚姻連動限制
    $scope.marriageGearing = function (item) {
        switch (item.no) {
            case 1: $scope.viewModel.family[2].marriage = $scope.viewModel.family[1].marriage; break; //媽媽的婚姻狀態 = 爸爸的婚姻狀態
            case 2: $scope.viewModel.family[1].marriage = $scope.viewModel.family[2].marriage; break; //爸爸的婚姻狀態 = 媽媽的婚姻狀態
            case 3: $scope.viewModel.family[4].marriage = $scope.viewModel.family[3].marriage; break; //奶奶的婚姻狀態 = 爺爺的婚姻狀態
            case 4: $scope.viewModel.family[3].marriage = $scope.viewModel.family[4].marriage; break; //爺爺的婚姻狀態 = 奶奶的婚姻狀態
            case 5: $scope.viewModel.family[6].marriage = $scope.viewModel.family[5].marriage; break; //外婆的婚姻狀態 = 外公的婚姻狀態
            case 6: $scope.viewModel.family[5].marriage = $scope.viewModel.family[6].marriage; break; //外公的婚姻狀態 = 外婆的婚姻狀態
            case 7: $scope.viewModel.family[8].marriage = $scope.viewModel.family[7].marriage; break; //岳母的婚姻狀態 = 岳父的婚姻狀態
            case 8: $scope.viewModel.family[7].marriage = $scope.viewModel.family[8].marriage; break; //岳父的婚姻狀態 = 岳母的婚姻狀態
        }
    }
    //#endregion

    //#region 存歿連動限制
    $scope.lifeGearing = function (item) {
        if (item.life == '歿') { item.live = '否'; }
    }
    //#endregion

    //#endregion


    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
