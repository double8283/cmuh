﻿angular.module('cmuh').service('medDrawing', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得科室陣列
    this.getDivInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/NoteManager/ImageService/GetDivInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得科室底圖
    this.getDivImages = function (divInfos) {
        var url = cmuhBase.stringFormat("../WebApi/NoteManager/ImageService/GetDivImages/{0}", divInfos);
        return $http.get(url);
    }
    //#endregion

    //#region 取得pacs底圖資訊
    this.getExamInfos = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/NoteManager/ImageService/GetExamInfos/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得pacs底圖
    this.getPacsImages = function (visitNo, examNo) {
        var url = cmuhBase.stringFormat("../WebApi/NoteManager/ImageService/GetPacsImages/{0}/{1}", visitNo, examNo);
        return $http.get(url);
    }
    //#endregion

}]);