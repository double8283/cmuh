﻿angular.module('cmuh').controller("medDrawingController", ["$scope", "$q", "cmuhBase", "toaster", "medDrawing", function ($scope, $q, cmuhBase, toaster, medDrawing) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        if ($scope.viewModel.isInitViewModel === true) {
            return;
        }

        //科室圖陣列, 底圖陣列, 科室名稱, SVG輸入文字, SVG暫存陣列, 當前底圖, 特殊圖案網址, 儲存圖片陣列, 常用字詞, 常用字詞陣列, 顯示底圖筆數
        $scope.viewModel.sectionsimage = [];
        $scope.viewModel.imageurl = [{ no: 0, url: 'App/TestMuRong/baseImage/P000/000.JPG', svg: '<image xlink:href="App/TestMuRong/baseImage/P000/000.JPG" src="App/TestMuRong/baseImage/P000/000.JPG" x="0" y="0" width="650" height="420" />' }];
        $scope.viewModel.section = null;
        $scope.viewModel.textContent = null;
        $scope.viewModel.svgScratch = [];
        $scope.viewModel.currentBaseimage = $scope.viewModel.imageurl[0].no;
        $scope.viewModel.specificImageurl = null;
        $scope.viewModel.saveImages = [];
        $scope.viewModel.sectionsText = null;
        $scope.viewModel.sectionsTextSource = [{ value: "general hospital" }, { value: "Department of ophtalmology" }, { value: "physician" }, { value: "Ward round" }, { value: "Registrar" }, { value: "Assistant pharmacist" }];
        $scope.viewModel.showImageurlNo = 0;

        //畫布起點X, 起點Y, 終點X, 終點Y, 寬度, 高度, 半徑, 要繪製的形狀, 顏色, 繪圖狀態, 線條粗細, 字體大小, 斜率
        $scope.viewModel.start_X = 0;
        $scope.viewModel.start_Y = 0;
        $scope.viewModel.end_X = 0;
        $scope.viewModel.end_Y = 0;
        $scope.viewModel.width = 0;
        $scope.viewModel.height = 0;
        $scope.viewModel.radius = 0;
        $scope.viewModel.shape = null;
        $scope.viewModel.color = 'rgb(0,0,0)';
        $scope.viewModel.drawing = false;
        $scope.viewModel.lineSize = 1;
        $scope.viewModel.textSize = 12;
        $scope.viewModel.slope = 0;

        $scope.viewModel.ll = 0;
        $scope.viewModel.tt = 0;

        //隱藏區塊初始化
        $scope.viewModel.ifInsertText = false;
        $scope.viewModel.ifAddBaseImage = false;

        //頁籤
        $scope.viewModel.tabs = [true, false];
        $scope.viewModel.tabUrls = [
            cmuhBase.stringFormat("App/TestMuRong/commonWords.html"),
            cmuhBase.stringFormat("App/TestMuRong/editDrawing.html")
        ];

        //是否顯示PACS圖區域
        $scope.viewModel.ifShowPacs = false;

        //初始化TemplateUrl
        $scope.viewModel.tabUrl = $scope.viewModel.tabUrls[0];

        //抓取科室陣列
        getDivInfos();
        //抓取PACS底圖資訊
        setPACS();

        //初始化
        $scope.viewModel.isInitViewModel = true;
    }
    //#endregion

    //#region 抓取科室陣列
    var getDivInfos = function () {
        medDrawing.getDivInfos().then(
            function (success) {
                $scope.viewModel.sectionsDataSource = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起,無法取得資料");
            }
        );
    }
    //#endregion

    //#region 抓取PACS底圖資訊
    var setPACS = function () {
        $scope.viewModel.PACSGridOptions = {
            selectable: 'row',
            dataSource: new kendo.data.DataSource(),
            columns: [
                { field: "reportTime", title: "reportTime", width: "200px" },
                { field: "examType", title: "examType", width: "200px" },
                { field: "examNo", title: "examNo", width: "200px" },
                { field: "remark", title: "remark", width: "200px" }
            ],
            change: function (e) {
                medDrawing.getPacsImages(29589774, this.dataItem(this.select()[0]).examNo).then(
                    function (success) {
                        $scope.viewModel.pacsImage = "data:image/png;base64," + angular.fromJson(success.data)[0];
                    },
                    function (error) {
                        toaster.showErrorMessage("對不起,無法取得資料");
                    }
                );
            }
        };
        //$scope.appModel.patientInfos.visitNo
        medDrawing.getExamInfos(29589774).then(
            function (success) {
                $scope.viewModel.PACSGridOptions.dataSource = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起,無法取得資料");
            }
        );
    }
    //#endregion

    //#region 改變科室
    $scope.changeSections = function () {
        $scope.viewModel.showImageurlNo = 0;
        medDrawing.getDivImages($scope.viewModel.section).then(
            function (success) {
                $scope.viewModel.sectionsimage = [];
                for (var i = 0; i < success.data[0].images.length; i++) {
                    $scope.viewModel.sectionsimage.push({ url: "data:image/png;base64," + success.data[0].images[i] })
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起,無法取得資料");
            }
        );
    }
    //#endregion

    //#endregion


    //#region 事件

    //#region 儲存圖片
    $scope.saveImage = function () {
        /*for (var i = 0; i <= $scope.viewModel.imageurl.length - 1; i++) {
            $scope.viewModel.saveImages.push("data:image/svg+xml;base64" + window.btoa(new XMLSerializer().serializeToString($scope.viewModel.imageurl[i].svg)));
        }*/
        
        var s = new XMLSerializer().serializeToString(document.getElementById("svg"));
        var b64 = window.btoa(s);
        var link = document.createElement("a");
        link.download = '';
        link.href = "data:image/svg+xml;base64," + b64;
        debugger;
        link.click();
    }
    //#endregion

    //#region 上傳圖片
    $scope.uploadImage = function () {
        document.getElementById("noShowUploadImage").addEventListener('change', readFile, false);
        function readFile() {
            var file = this.files[0];
            if (!/image\/\w+/.test(file.type)) {
                alert("必須為圖片");
                return false;
            }
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                if ($scope.viewModel.imageurl[$scope.viewModel.imageurl.length - 1].url != this.result) {
                    $scope.viewModel.imageurl.push({ no: $scope.viewModel.imageurl.length, url: this.result, svg: "<image xlink:href='" + this.result + "' src='" + this.result + "' x='0' y='0' width='650' height='420' />" });
                    $scope.viewModel.currentBaseimage = $scope.viewModel.imageurl[$scope.viewModel.imageurl.length - 1].no;
                    document.getElementById("svg").innerHTML = $scope.viewModel.imageurl[$scope.viewModel.imageurl.length - 1].svg;
                }
            }
        }
        $scope.viewModel.tabUrl = $scope.viewModel.tabUrls[1];
        $scope.viewModel.tabs = [false, true];
    }
    //#endregion

    //#region 加入圖片
    $scope.addsectionsImage = function (item) {
        $scope.viewModel.imageurl.push({ no: $scope.viewModel.imageurl.length, url: item, svg: "<image xlink:href='" + item + "' src='" + item + "' x='0' y='0' width='650' height='420' />" });
        $scope.viewModel.currentBaseimage = $scope.viewModel.imageurl[$scope.viewModel.imageurl.length - 1].no;
        document.getElementById("svg").innerHTML = $scope.viewModel.imageurl[$scope.viewModel.imageurl.length - 1].svg;
        $scope.viewModel.tabUrl = $scope.viewModel.tabUrls[1];
        $scope.viewModel.tabs = [false, true];
    }
    //#endregion

    //#region 改變形狀
    $scope.changeShape = function (item) {
        $scope.viewModel.shape = item;
        $scope.viewModel.ifInsertText = false;
        document.getElementById('insertText').value = '';
    }
    //#endregion

    //#region 按鈕外框顏色
    $("#buttonDiv button").click(function () {
        //$(this).css("border", "1px solid blue").siblings().css("border", "1px solid #DDDDDD");
        $("#buttonDiv button").css("border", "0px");
        $(".btn-group button").css("border", "0px");
        $(this).css("border", "1px solid blue");
    });
    //#endregion

    //#region 文字輸入框按下ENTER
    $scope.insertTextOk = function () {
        if (!event.shiftKey) {
            $scope.viewModel.ifInsertText = false;
            var s = document.getElementById('insertText').value;
            s = s.split("\n");
            $scope.viewModel.textContent = "<text x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' fill=" + $scope.viewModel.color + " style='font-size:" + $scope.viewModel.textSize + "px;'>";
            for (var i = 0 ; i < s.length - 1 ; i++) {
                $scope.viewModel.textContent += "<tspan x='" + $scope.viewModel.start_X + "' dy='1em'>" + s[i] + "</tspan>";
            }
            $scope.viewModel.textContent += "</text>";
            document.getElementById("svg").innerHTML += $scope.viewModel.textContent;
            document.getElementById('insertText').value = '';
        }
    }
    //#endregion

    //#region 點擊清除
    $scope.clear = function () {
        $scope.viewModel.svgScratch = [];
        $scope.viewModel.shape = '';
        document.getElementById('insertText').value = '';
        $scope.viewModel.ifInsertText = false;
        if (document.getElementById("svg").childNodes[0].localName == "image") {
            document.getElementById("svg").innerHTML = document.getElementById("svg").childNodes[0].outerHTML;
        } else {
            document.getElementById("svg").innerHTML = "";
        }
        $scope.viewModel.imageurl[$scope.viewModel.currentBaseimage].svg = document.getElementById("svg").innerHTML;
    }
    //#endregion

    //#region 點擊還原
    $scope.last = function () {
        $scope.viewModel.ifInsertText = false;
        $scope.viewModel.shape = '';
        do {
            $scope.viewModel.svgScratch.push(document.getElementById("svg").lastChild.outerHTML);
            document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
        } while (document.getElementById("svg").lastChild.innerHTML.toString().indexOf("start") == -1)
        $scope.viewModel.imageurl[$scope.viewModel.currentBaseimage].svg = document.getElementById("svg").innerHTML;
    }
    //#endregion

    //#region 點擊重繪
    $scope.next = function () {
        $scope.viewModel.ifInsertText = false;
        $scope.viewModel.shape = '';
        if ($scope.viewModel.svgScratch.length >= 1) {
            do {
                document.getElementById("svg").innerHTML += $scope.viewModel.svgScratch[$scope.viewModel.svgScratch.length - 1];
                $scope.viewModel.svgScratch.pop();
            } while ($scope.viewModel.svgScratch[$scope.viewModel.svgScratch.length - 1] != undefined)
        }
        $scope.viewModel.imageurl[$scope.viewModel.currentBaseimage].svg = document.getElementById("svg").innerHTML;
    }
    //#endregion

    //#region 滑鼠在畫布上按下
    $('#svg').mousedown(function (e) {
        $scope.viewModel.start_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
        $scope.viewModel.start_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
        $scope.viewModel.end_X = $scope.viewModel.start_X;
        $scope.viewModel.end_Y = $scope.viewModel.start_Y;
        $scope.viewModel.drawing = true;
        document.getElementById("svg").innerHTML += "start";
        switch ($scope.viewModel.shape) {
            case 'brushes':
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                break;
            case 'rect':
                document.getElementById("svg").innerHTML += "<rect x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' width='1' height='1' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + ";fill-opacity:0;stroke-opacity:1' />";
                break;
            case 'circle':
                document.getElementById("svg").innerHTML += "<circle cx='" + $scope.viewModel.start_X + "' cy='" + $scope.viewModel.start_Y + "' r='1' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + ";fill-opacity:0;stroke-opacity:1' />";
                break;
            case 'line':
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                break;
            case 'arrow':
                document.getElementById("svg").innerHTML += "<marker id='arrow' refX='0' refY='3' markerWidth='20' markerHeight='20' orient='auto'><path d='M0 0 L0 6 L10 3' style='fill:" + $scope.viewModel.color + ";'></path></marker>";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                break;
            case 'text':
                $scope.viewModel.start_Y -= 8;
                $scope.viewModel.ll = e.pageX;
                $scope.viewModel.tt = e.pageY - 10;
                $scope.viewModel.ifInsertText = true;
                break;
            case 'specificText':
                document.getElementById("svg").innerHTML += "<text x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' fill=" + $scope.viewModel.color + " style='font-size:" + $scope.Size + "px;'>" + $scope.viewModel.text + "</text>";
                break;
            case 'specificImage':
                $scope.viewModel.start_X -= 12;
                $scope.viewModel.start_Y -= 12;
                document.getElementById("svg").innerHTML += "<image xlink:href='" + $scope.viewModel.specificImageurl + "' src='" + $scope.viewModel.specificImageurl + "' x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' width='25' height='25' />";
                break;
            case 'sutures':
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                break;
            case 'commonText':
                document.getElementById("svg").innerHTML += "<text x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' fill=" + $scope.viewModel.color + " style='font-size:" + $scope.Size + "px;'>" + $scope.viewModel.sectionsText + "</text>";
                break;
        }
    });
    //#endregion

    //#region 滑鼠在畫布上移動
    $('#svg').mousemove(function (e) {
        if ($scope.viewModel.drawing) {
            switch ($scope.viewModel.shape) {
                case 'brushes':
                    $scope.viewModel.start_X = $scope.viewModel.end_X;
                    $scope.viewModel.start_Y = $scope.viewModel.end_Y;
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    break;
                case 'rect':
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    $scope.viewModel.width = Math.abs($scope.viewModel.end_X - $scope.viewModel.start_X);
                    if ($scope.viewModel.width < 1) $scope.viewModel.width = 1;
                    $scope.viewModel.height = Math.abs($scope.viewModel.end_Y - $scope.viewModel.start_Y);
                    if ($scope.viewModel.height < 1) $scope.viewModel.height = 1;
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").innerHTML += "<rect x='" + $scope.viewModel.start_X + "' y='" + $scope.viewModel.start_Y + "' width='" + $scope.viewModel.width + "' height='" + $scope.viewModel.height + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + ";fill-opacity:0;stroke-opacity:1' />";
                    break;
                case 'circle':
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    $scope.viewModel.radius = $scope.viewModel.end_X - $scope.viewModel.start_X;
                    if ($scope.viewModel.radius < 1) $scope.viewModel.radius = 1;
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").innerHTML += "<circle cx='" + $scope.viewModel.start_X + "' cy='" + $scope.viewModel.start_Y + "' r='" + $scope.viewModel.radius + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + ";fill-opacity:0;stroke-opacity:1' />";
                    break;
                case 'line':
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    break;
                case 'arrow':
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").innerHTML += "<line marker-end='url(#arrow)' x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    break;
                case 'sutures':
                    $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
                    $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
                    $scope.viewModel.centerX = ($scope.viewModel.end_X + $scope.viewModel.start_X) / 2;
                    $scope.viewModel.centerY = ($scope.viewModel.end_Y + $scope.viewModel.start_Y) / 2;
                    $scope.viewModel.centerX1 = ($scope.viewModel.centerX + $scope.viewModel.start_X) / 2;
                    $scope.viewModel.centerY1 = ($scope.viewModel.centerY + $scope.viewModel.start_Y) / 2;
                    $scope.viewModel.centerX2 = ($scope.viewModel.end_X + $scope.viewModel.centerX) / 2;
                    $scope.viewModel.centerY2 = ($scope.viewModel.end_Y + $scope.viewModel.centerY) / 2;
                    $scope.viewModel.centerX3 = ($scope.viewModel.start_X + $scope.viewModel.centerX1) / 2;
                    $scope.viewModel.centerY3 = ($scope.viewModel.start_Y + $scope.viewModel.centerY1) / 2;
                    $scope.viewModel.centerX4 = ($scope.viewModel.centerX1 + $scope.viewModel.centerX) / 2;
                    $scope.viewModel.centerY4 = ($scope.viewModel.centerY1 + $scope.viewModel.centerY) / 2;
                    $scope.viewModel.centerX5 = ($scope.viewModel.centerX2 + $scope.viewModel.centerX) / 2;
                    $scope.viewModel.centerY5 = ($scope.viewModel.centerY2 + $scope.viewModel.centerY) / 2;
                    $scope.viewModel.centerX6 = ($scope.viewModel.end_X + $scope.viewModel.centerX2) / 2;
                    $scope.viewModel.centerY6 = ($scope.viewModel.end_Y + $scope.viewModel.centerY2) / 2;
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").removeChild(document.getElementById("svg").lastChild);
                    document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX - 10) + "' y1='" + ($scope.viewModel.centerY + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX + 10) + "' y2='" + ($scope.viewModel.centerY - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX1 - 10) + "' y1='" + ($scope.viewModel.centerY1 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX1 + 10) + "' y2='" + ($scope.viewModel.centerY1 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX2 - 10) + "' y1='" + ($scope.viewModel.centerY2 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX2 + 10) + "' y2='" + ($scope.viewModel.centerY2 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX3 - 10) + "' y1='" + ($scope.viewModel.centerY3 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX3 + 10) + "' y2='" + ($scope.viewModel.centerY3 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX4 - 10) + "' y1='" + ($scope.viewModel.centerY4 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX4 + 10) + "' y2='" + ($scope.viewModel.centerY4 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX5 - 10) + "' y1='" + ($scope.viewModel.centerY5 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX5 + 10) + "' y2='" + ($scope.viewModel.centerY5 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    document.getElementById("svg").innerHTML += "<line x1='" + ($scope.viewModel.centerX6 - 10) + "' y1='" + ($scope.viewModel.centerY6 + $scope.viewModel.slope) + "' x2='" + ($scope.viewModel.centerX6 + 10) + "' y2='" + ($scope.viewModel.centerY6 - $scope.viewModel.slope) + "' style='stroke:" + $scope.viewModel.color + ";stroke-width:" + $scope.viewModel.lineSize + "' />";
                    break;
            }
        }
    });
    //#endregion

    //#region 滑鼠在畫布上放開
    $('#svg').mouseup(function (e) {
        $scope.viewModel.drawing = false;
        for (var i = 0; i <= $scope.viewModel.imageurl.length - 1; i++) {
            if ($scope.viewModel.imageurl[i].no == $scope.viewModel.currentBaseimage) {
                $scope.viewModel.imageurl[i].svg = document.getElementById("svg").innerHTML;
            }
        }
    });
    //#endregion

    //#region 顯示底圖
    $scope.addImage = function (item) {
        $scope.viewModel.currentBaseimage = item.no;
        document.getElementById("svg").innerHTML = item.svg;
    }
    //#endregion

    //#region 刪除底圖
    $scope.deleteImage = function (item) {
        for (var i = 0; i <= $scope.viewModel.imageurl.length - 1; i++) {
            if ($scope.viewModel.imageurl[i].svg == item.svg) {
                $scope.viewModel.imageurl.splice(i, 1);
                if (i > 0) {
                    document.getElementById("svg").innerHTML = $scope.viewModel.imageurl[i - 1].svg;
                } else {
                    document.getElementById("svg").innerHTML = "";
                }
            }
        }
    }
    //#endregion

    //#region 加入常用字詞
    $scope.addCommonText = function () {
        $scope.viewModel.sectionsTextSource.push({ value: $scope.viewModel.sectionsText });
        $scope.viewModel.sectionsTextSource.sort(function (a, b) {
            return a.value.localeCompare(b.value);
        });
    }
    //#endregion

    //#region 刪除常用字詞
    $scope.deleteCommonText = function () {
        for (var i = 0; i <= $scope.viewModel.sectionsTextSource.length - 1; i++) {
            if ($scope.viewModel.sectionsTextSource[i].value == $scope.viewModel.sectionsText) {
                $scope.viewModel.sectionsTextSource.splice(i, 1);
            }
        }
    }
    //#endregion

    //#endregion


    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);