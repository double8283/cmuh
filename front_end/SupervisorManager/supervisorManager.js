﻿angular.module('cmuh').service('supervisorManager', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion

    //#region 取得部門主管
    this.getDepartManagers = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/HRManagerService/GetDepartManagers/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得主管樹狀結構
    this.getEmpManagersTree = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/HRManagerService/GetEmpManagersTree/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得員工基本資料
    this.getEmpInfo = function (empNo) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/ResignManagerService/GetEmpInfo/{0}", empNo);
        return $http.get(url);
    }
    //#endregion

    //#region 儲存部門主管
    this.setDepartManagerInfos = function (data) {
        return $http.put("../WebApi/HumanResource/HRManagerService/SetDepartManagerInfos", data);
    }
    //#endregion

    //#region 儲存個人主管
    this.setEmpManagerInfos = function (data) {
        return $http.put("../WebApi/HumanResource/HRManagerService/SetEmpManagerInfos", data);
    }
    //#endregion

}]);