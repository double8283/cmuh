﻿angular.module("cmuh").controller('supervisorManagerController', ["$scope", "$sce", "toaster", "cmuhBase", "supervisorManager", function ($scope, $sce, toaster, cmuhBase, supervisorManager) {

    //#region 方法

    //#region 設定資料結構
    var initViewModel = function (pageModel) {

        $scope.viewModel = pageModel;

        if ($scope.viewModel.isInitSupervisorViewModel === true) {
            return;
        }

        kendo.culture("zh-TW");

        //預設結束日期
        $scope.viewModel.endDate = "2999-12-31T00:00:00+08:00";

        //設定主管清單TreeList
        SetSupervisorTreeListOptions();

        //取得部門清單
        getDeptList();

        //設定高度
        document.getElementById("supervisorTreeList").style.height = window.innerHeight - 230 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 580 + "px";
        }
        
        $scope.viewModel.isInitSupervisorViewModel = true;
    }
    //#endregion

    //#region 設定主管清單TreeList
    var SetSupervisorTreeListOptions = function (items) {
        $scope.viewModel.supervisorTreeListOptions = {
            selectable: true,
            toolbar: ["excel"],
            columns: [
                { field: "empInfo.empCode", title: "員工代號", width: "150px" },
                { field: "empInfo.empName", title: "員工姓名", width: "80px" },
                { field: "empInfo.gradeCode", title: "員工職等", width: "80px" },
                { field: "empInfo.titleName", title: "員工職稱" },
                { field: "empInfo.empStatus.optionName", title: "員工狀態", width: "80px" }
            ],
            dataSource: new kendo.data.TreeListDataSource(),
            excel: {
                fileName: "管理清單樹狀圖.xlsx"
            },
            change: function (e) {
                $scope.viewModel.dataItem = this.dataItem(this.select()[0]);
                //setPersonSupervisorGridSource(dataItem.managers);
            }
        };
    }
    //#endregion

    //#region 設定主管清單TreeList DataSource
    var setSupervisorTreeListSource = function (items) {
        var dataSource = new kendo.data.TreeListDataSource({
            data: items,
            schema: {
                model: {
                    id: "empInfo.empName",
                    fields: {
                        parentId: { field: "managerName", type: "string", nullable: true },
                        empName: { field: "empInfo.empName", type: "string", nullable: false },
                    }
                }
            }
        });
        $scope.viewModel.supervisorTreeListOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#supervisorTreeList").data("kendoTreeList");
            if (grid == null) {
                $scope.viewModel.supervisorTreeListOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得部門清單
    var getDeptList = function () {
        supervisorManager.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
                    $scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得部門主管
    var getDepartManagers = function () {
        supervisorManager.getDepartManagers($scope.viewModel.searchDept).then(
            function (success) {
                $scope.viewModel.departManagers = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得主管樹狀結構
    var getEmpManagersTree = function () {
        supervisorManager.getEmpManagersTree($scope.viewModel.searchDept).then(
            function (success) {
                $scope.viewModel.empManagersTree = success.data;
                setSupervisorTreeListSource($scope.viewModel.empManagersTree);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 員工代號補0
    var addLeftZero = function (param, number) {
        if (String(param).length < number) {
            return addLeftZero(cmuhBase.stringFormat("0{0}", param), number);
        } else {
            return param;
        }
    }
    //#endregion

    //#region 儲存部門主管
    var setDepartManagerInfos = function () {
        supervisorManager.setDepartManagerInfos($scope.viewModel.departManagers).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，部門主管存檔成功");
                } else {
                    toaster.showErrorMessage("對不起，部門主管存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，部門主管存檔連線失敗");
            }
        );
    }
    //#endregion

    //#region 儲存個人主管
    var setEmpManagerInfos = function () {
        supervisorManager.setEmpManagerInfos($scope.viewModel.supervisorTreeList.dataSource.data()).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，個人主管存檔成功");
                    $scope.search();
                } else {
                    toaster.showErrorMessage("對不起，個人主管存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，個人主管存檔連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 查詢
    $scope.search = function () {
        $scope.viewModel.dataItem = [];
        getDepartManagers();    //取得部門主管
        getEmpManagersTree();   //取得主管樹狀結構
    }
    //#endregion

    //#region 取得員工基本資料
    $scope.getEmpInfo = function (item) {
        empNo = !!item.manager.empNo == false ? 0 : item.manager.empNo;
        if (Number.isInteger(empNo) == false) {
            empNo = empNo.replace(/[^\d]/g, '');
        }
        supervisorManager.getEmpInfo(empNo).then(
            function (success) {
                if (success.data != "null") {
                    item.manager = success.data;
                    item.manager.empNo = addLeftZero(item.manager.empNo, 4);
                } else {
                    item.manager.empName = "";
                    item.manager.gradeCode = "";
                    item.manager.titleName = "";
                    item.manager.empStatus = {
                        optionName : ""
                    };
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion
    
    //#region 新增主管  type = 'Depart' / 'Person'
    $scope.addSupervisor = function (type) {
        var data = {
            departNo: $scope.viewModel.searchDept,
            levelNo: 0,
            manager: {
                empNo: ""
            },
            startDate: new Date(),
            endDate: "2999-12-31T00:00:00+08:00",
            systemUser: $scope.shareModel.userInfos.userNo,
            entityState: "Added"
        };
        if (type == "Depart") {
            $scope.viewModel.departManagers.push(data);
        } else {
            $scope.viewModel.dataItem.managers.push(data);
        }
        
    }
    //#endregion

    //#region 刪除
    $scope.deleteManager = function (dataItem) {
        dataItem.endDate = new Date();
        dataItem.systemUser = $scope.shareModel.userInfos.userNo;
        if (dataItem.entityState == "Added") {
            dataItem.entityState = "Detached";
        } else {
            dataItem.entityState = "Modified";
        }
    }
    //#endregion

    //#region 儲存
    $scope.save = function () {
        setDepartManagerInfos();    //儲存部門主管
        setEmpManagerInfos();       //儲存個人主管
    }
    //#endregion
    
    //#region 修改主管
    $scope.editManager = function (dataItem) {
        dataItem.systemUser = $scope.shareModel.userInfos.userNo;
        if (dataItem.entityState != "Added") {
            dataItem.entityState = "Modified";
        }
    }
    //#endregion
    
    //#region 清除個人主管
    $scope.clearSupervisor = function () {
        for (var i = 0; i < $scope.viewModel.empManagersTree.length; i++) {
            for (var j = 0; j < $scope.viewModel.empManagersTree[i].managers.length; j++) {
                $scope.viewModel.empManagersTree[i].managers[j].endDate = new Date();
                $scope.viewModel.empManagersTree[i].managers[j].entityState = "Modified";
                $scope.viewModel.empManagersTree[i].managers[j].systemUser = $scope.shareModel.userInfos.userNo;
            }
        }
        setSupervisorTreeListSource($scope.viewModel.empManagersTree);
        $scope.viewModel.dataItem = [];
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);