﻿angular.module('cmuh').service('maintenanceApplication', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得期間內自己申請的修繕資料清單
    this.getRepairMts = function (searchRequest) {
        return $http.post("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMts/ApplyUser", searchRequest);
    }
    //#endregion

    //#region 設定修繕資料
    this.setRepairMts = function (repairMtInfo) {
        return $http.put("../WebApi/FaciltiesCheckList/FacilitiesRepairService/SetRepairMts", repairMtInfo);
    }
    //#endregion

    //#region 取得使用者單位
    this.getUserDept = function (userId) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetUserDept/{0}", userId);
        return $http.get(url);
    }
    //#endregion

    //#region 取得申請部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion

    //#region 取得單位地點清單
    this.getLocationInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetLocationInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得設備類別清單
    this.getEquipmentCategoryList = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyClasses/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得財產編號清單
    this.getPropertyList = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyBasicInfos/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得特定財產名稱
    this.getPropertyBasicInfo = function (propertyCode) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyBasicInfo/{0}", propertyCode);
        return $http.get(url);
    }
    //#endregion

    //#region 取得故障情形清單
    this.getPropertyFailureInfo = function (propertyType) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyFailures/{0}", propertyType);
        return $http.get(url);
    }
    //#endregion

    //#region 設定修繕資料
    this.setRepair = function (repairMtInfo) {
        return $http.put("../WebApi/FaciltiesCheckList/FacilitiesRepairService/SetRepair", repairMtInfo);
    }
    //#endregion

    //#region 取得修繕資料
    this.getRepairMtInfo = function (repairNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMtInfo/{0}", repairNo);
        return $http.get(url);
    }
    //#endregion

}]);