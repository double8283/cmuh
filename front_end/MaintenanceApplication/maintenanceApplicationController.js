﻿angular.module("cmuh").controller("maintenanceApplicationController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenanceApplication", function ($scope, $http, $q, cmuhBase, toaster, maintenanceApplication) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //查詢變數
        $scope.viewModel.searchRequest = {
            applyUser: $scope.shareModel.userInfos.userNo,
            period: {
                type: "D",
                value: "-3"
            }
        }
        $scope.viewModel.select = "false";
        $scope.viewModel.period = "1";

        //附件Grid
        $scope.viewModel.attachmentGridOptions = {
            scrollable: false,
            columns: [
                { field: "", title: "刪除", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteAttachment(dataItem)\" /></center>" },
                { field: "", title: "下載", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/wtRl5iU.jpg\" style=\"width:20px;height:20px;\" data-ng-click=\"downloadAttachment(dataItem)\" /></center>" },
                { field: "attachName", title: "附件名稱" }
            ],
            dataSource: new kendo.data.DataSource()
        };

        //財產Grid
        $scope.viewModel.propertyGridOptions = {
            scrollable: false,
            selectable: 'row',
            sortable: true,
            columns: [
                { field: "propertyCode", title: "財產編碼", width: "200px" },
                { field: "propertyName", title: "財產名稱" },
                { field: "matCode", title: "資材號碼", width: "100px" },
                { field: "keepUser.empCode", title: "保管人員代", width: "100px" },
                { field: "keepUser.empName", title: "保管人姓名", width: "100px" }
            ],
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.viewModel.property = dataItem;
            }
        };

        $scope.new();

        //定義ApplyListGrid
        setApplyListGridOptions();

        document.getElementById("applyListGrid").style.height = window.innerHeight - 310 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 255 + "px";
        }
    }
    //#endregion

    //#region 定義ApplyListGrid
    var setApplyListGridOptions = function () {
        $scope.viewModel.applyListGridOptions = {
            scrollable: false,
            selectable: 'row',
            columns: [
                {
                    field: "check", title: "", width: "10px", sortable: false,
                    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/></center>",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem);\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/>"
                },
                { field: "repairNo", title: "維修編號", width: "80px" },
                { field: "applyTime", title: "申請時間", width: "200px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd hh:mm:ss'}}" },
                { field: "locationNo.optionName", title: "申請地點", width: "80px" },
                { field: "propertyType.optionName", title: "設備類別", width: "80px" },
                { field: "repairStatus.optionName", title: "維修狀況", width: "80px" },
                { field: "repairUser.empName", title: "維修人員", width: "110px" },
                { field: "completeTime", title: "完成時間", width: "200px", template: "<div data-ng-if=\"dataItem.completeTime != '2999-12-31T00:00:00+08:00'\">{{dataItem.completeTime | date:'yyyy-MM-dd hh:mm:ss'}}<div>" },
                { field: "signoffUser.empName", title: "簽核人員", width: "110px" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.shareModel.repairNo = dataItem.repairNo;
                $scope.shareModel.banner.showBanner(false, null, null, "維修編號：" + dataItem.repairNo, "", "");
                $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
                getRepairMtInfo();
            }
        };
    }
    //#endregion

    //#region 設定ApplyListGrid DataSource
    var setApplyListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#applyListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.applyListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定PropertyGrid DataSource
    var setPropertyGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.propertyGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#propertyGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.propertyGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得使用者單位
    var getUserDept = function () {
        maintenanceApplication.getUserDept($scope.shareModel.userInfos.userId).then(
            function (success) {
                $scope.viewModel.repairMtInfo.applyDepartNo = success.data.departNo;
                $scope.viewModel.repairMtInfo.applyDepartName = success.data.departName;
                $scope.viewModel.keepDept = success.data.departNo;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得單位地點清單
    var getLocationInfos = function () {
        maintenanceApplication.getLocationInfos().then(
            function (success) {
                $scope.viewModel.locationNames = [];
                $scope.viewModel.locationInfos = [];
                $scope.viewModel.locationInfos[0] = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得修繕資料
    var getRepairMtInfo = function () {
        maintenanceApplication.getRepairMtInfo($scope.shareModel.repairNo).then(
            function (success) {
                $scope.viewModel.repairMtInfo = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 取得申請部門清單
    $scope.getDeptList = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 5) {
            return;
        }
        $scope.viewModel.departWindow.center().open();
        if ($scope.viewModel.deptList != null) {
            return;
        }
        maintenanceApplication.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = success.data;
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 查詢
    $scope.search = function () {
        $scope.viewModel.select = "false";
        maintenanceApplication.getRepairMts($scope.viewModel.searchRequest).then(
            function (success) {
                setApplyListGridSource(success.data);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setApplyListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 刪除
    $scope.delete = function () {
        var data = [];
        for (var i = 0; i < $scope.viewModel.applyListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.applyListGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.applyListGrid.dataSource.data()[i].repairStatus.optionNo = 80;
                $scope.viewModel.applyListGrid.dataSource.data()[i].systemUser = $scope.shareModel.userInfos.userNo;
                data.push($scope.viewModel.applyListGrid.dataSource.data()[i]);
            }
        }
        maintenanceApplication.setRepairMts(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，刪除成功");
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
                $scope.search();
            },
            function (error) {
                setApplyListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case "1":
                $scope.viewModel.searchRequest.period.type = "D";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "2":
                $scope.viewModel.searchRequest.period.type = "W";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "3":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "4":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "5":
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.applyListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.applyListGrid.dataSource.data()[i].repairStatus.optionNo == 5) {
                $scope.viewModel.applyListGrid.dataSource.data()[i].check = $scope.viewModel.select;
            }
        }
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (dataItem) {
        $scope.viewModel.select = dataItem.check == "false" ? "false" : $scope.viewModel.select;
        if (dataItem.check == "true" && dataItem.repairStatus.optionNo != 5) {
            toaster.showErrorMessage("對不起，只有維修狀態為「暫存」的才可以勾選刪除");
            dataItem.check = "false";
        }
    }
    //#endregion

    //#region 新增
    $scope.new = function () {
        $scope.shareModel.banner.showBanner(false, null, null, "", "", "");
        $scope.shareModel.repairNo = 0;
        //申請資料
        $scope.viewModel.repairMtInfo = {
            applyDepartNo: "",
            applyTime: new Date(),
            applyUser: { empNo: $scope.shareModel.userInfos.userNo },
            compeleteTime: "2999-12-31",
            failureNo: { optionNo: "", optionName: "" },
            failureRemark: "",
            isScrapped: false,
            locationNo: { optionNo: "", optionName: "" },
            locationNames: [],
            locationRemark: "",
            propertyCode: { optionNo: "0000000-00000000000", optionName: "無財產編碼" },
            propertyType: { optionNo: "", optionName: "" },
            propertyNames: [],
            repairDepartNo: "",
            repairNo: "",
            repairRemark: "",
            repairStatus: { optionNo: "", optionName: "" },
            repairUser: { empNo: "" },
            telNo: "",
            workMinutes: "0",
            systemUser: $scope.shareModel.userInfos.userNo,
            signoffUser: { empNo: "" },
            repairDts: [],
            repairAttaches: []
        }
        if ($scope.viewModel.attachmentGrid != undefined) {
            var dataSource = new kendo.data.DataSource({
                data: []
            });
            setTimeout(function () {
                var grid = $("#attachmentGrid").data("kendoGrid");
                if (grid == null) {
                    $scope.viewModel.propertyGridOptions.dataSource = dataSource;
                } else {
                    grid.setDataSource(dataSource);
                }
            }, 100);
        }
        //取得使用者單位
        getUserDept();
        //取得單位地點清單
        getLocationInfos();
    }
    //#endregion

    //#region 上傳附件檔案
    $scope.handleFiles = function () {
        for (var i = 0, file; file = document.getElementById("attachment").files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function (file) {
                return function (e) {
                    $scope.viewModel.newAttachment = {
                        attachName: file.name,
                        base64: e.target.result,
                        entityState: "Added"
                    };
                    $scope.viewModel.attachmentGrid.dataSource.insert($scope.viewModel.newAttachment);
                };
            })(file);
            reader.readAsDataURL(file);
        }
    }
    //#endregion

    //#region 刪除附件檔案
    $scope.deleteAttachment = function (dataItem) {
        $scope.viewModel.attachmentGrid.dataSource.remove(dataItem);
    }
    //#endregion

    //#region 下載附件檔案
    $scope.downloadAttachment = function (dataItem) {
        var fileDownload = document.createElement('a');
        fileDownload.href = dataItem.base64;
        fileDownload.download = dataItem.attachName;
        fileDownload.click();
    }
    //#endregion

    //#region 暫存／送出
    $scope.save = function (repairStatusNo) {
        $scope.viewModel.repairMtInfo.repairStatus.optionNo = repairStatusNo;
        maintenanceApplication.setRepair($scope.viewModel.repairMtInfo).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage(repairStatusNo == 5 ? "恭喜您，暫存成功" : "恭喜您，送出成功");
                } else {
                    toaster.showErrorMessage(repairStatusNo == 5 ? "對不起，暫存失敗" : "對不起，送出失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 送出禁用
    $scope.sendDisabled = function () {
        if (cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.telNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.locationNo.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.propertyCode.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.propertyType.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.failureNo.optionNo) == true) {
            return true;
        }
        return false;
    }
    //#endregion

    //#region 取得設備類別清單
    $scope.getEquipmentCategoryList = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 5) {
            return;
        }
        if ($scope.viewModel.repairMtInfo.repairDepartNo == '') {
            toaster.showErrorMessage("請先選擇維修類別");
            return;
        }
        $scope.viewModel.devicesTypeWindow.center().open();
        maintenanceApplication.getEquipmentCategoryList($scope.viewModel.repairMtInfo.repairDepartNo).then(
            function (success) {
                $scope.viewModel.equipmentNames = [];
                $scope.viewModel.equipmentInfos = [];
                $scope.viewModel.equipmentInfos[0] = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得財產編號清單
    $scope.getPropertyList = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 5) {
            return;
        }
        $scope.viewModel.propertyWindow.center().open();
        maintenanceApplication.getPropertyList($scope.viewModel.keepDept).then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.propertyList = [
                        {
                            "propertyCode": "0000000-00000000000",
                            "propertyName": "無財產編碼",
                            "matCode": "0000000",
                            "keepDepartNo": "",
                            "keepUser": {
                                "empNo": "",
                                "empCode": "",
                                "empName": "",
                                "departNo": "",
                                "departName": ""
                            }
                        }];
                    $scope.viewModel.propertyList = $scope.viewModel.propertyList.concat(success.data);
                    setPropertyGridSource($scope.viewModel.propertyList);
                } else {
                    setPropertyGridSource();
                }
            },
            function (error) {
                setPropertyGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 用申請人篩選財產編號清單
    $scope.filterGrid = function () {
        var newsList = angular.copy($scope.viewModel.propertyList);
        var filterNews = newsList.filter(function (item) {
            return (item.keepUser.empName.indexOf($scope.viewModel.keepUser) != -1 ? true : false) ||
                (item.keepUser.empCode.indexOf($scope.viewModel.keepUser) != -1 ? true : false);
        });
        setPropertyGridSource(filterNews);
    }
    //#endregion

    //#region 取得特定財產名稱
    $scope.getPropertyBasicInfo = function () {
        maintenanceApplication.getPropertyBasicInfo($scope.viewModel.repairMtInfo.propertyCode.optionNo).then(
            function (success) {
                $scope.viewModel.repairMtInfo.propertyCode.optionName = success.data.propertyName;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得故障情形清單
    $scope.getPropertyFailureInfo = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 5) {
            return;
        }
        if ($scope.viewModel.repairMtInfo.propertyType.optionNo == '') {
            toaster.showErrorMessage("請先選擇設備類別");
            return;
        }
        $scope.viewModel.failureScenarioWindow.center().open();
        maintenanceApplication.getPropertyFailureInfo($scope.viewModel.repairMtInfo.propertyType.optionNo).then(
            function (success) {
                $scope.viewModel.propertyFailureInfo = success.data;
                $scope.viewModel.repairMtInfo.failureNo = "";
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 修改背景顏色
    $scope.changeBgColor = function (array, index) {
        for (var i = 0; i < array.length; i++) {
            array[i].backColor = "#FFFFFF";
        }
        array[index].backColor = "#428bca";
    }
    //#endregion

    //#region 選擇單位地點
    $scope.getLocation = function (item, index) {
        if (index == 4) {
            $scope.viewModel.repairMtInfo.locationNo.optionNo = $scope.viewModel.locationNo.locationNo;
            $scope.viewModel.repairMtInfo.locationNo.optionName = $scope.viewModel.locationNo.locationName;
            $scope.viewModel.repairMtInfo.locationNames = $scope.viewModel.locationNames;
            $scope.viewModel.faultLocationWindow.close();
            return;
        }
        for (var i = index; i < $scope.viewModel.locationNames.length; i++) {
            $scope.viewModel.locationNames[i] = '';
        }
        $scope.viewModel.locationNames[index] = item.locationName;
        for (var i = index + 1; i < $scope.viewModel.locationInfos.length; i++) {
            $scope.viewModel.locationInfos[i] = [];
        }
        $scope.viewModel.locationInfos[index + 1] = item.details;
        $scope.viewModel.locationNo = item;

    }
    //#endregion

    //#region 選擇設備類別
    $scope.getEquipment = function (item, index) {
        // index 0:選擇大類別 1:選擇中類別 2:選擇小類別 3:完成
        if (index == 3) {
            $scope.viewModel.repairMtInfo.propertyType.optionNo = $scope.viewModel.equipmentNo;
            $scope.viewModel.repairMtInfo.propertyNames = $scope.viewModel.equipmentNames;
            $scope.viewModel.devicesTypeWindow.close();
            return;
        }
        for (var i = index; i < $scope.viewModel.equipmentNames.length; i++) {
            $scope.viewModel.equipmentNames[i] = '';
        }
        $scope.viewModel.equipmentNames[index] = item.typeName;
        for (var i = index + 1; i < $scope.viewModel.equipmentInfos.length; i++) {
            $scope.viewModel.equipmentInfos[i] = [];
        }
        $scope.viewModel.equipmentInfos[index + 1] = item.details;
        $scope.viewModel.equipmentNo = item.propertyType;
    }
    //#endregion

    //#region 取得財產
    $scope.getProperty = function () {
        $scope.viewModel.repairMtInfo.propertyCode.optionNo = $scope.viewModel.property.propertyCode;
        $scope.viewModel.repairMtInfo.propertyCode.optionName = $scope.viewModel.property.propertyName;
        $scope.viewModel.propertyWindow.close();
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);