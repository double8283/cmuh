﻿angular.module("cmuh").controller('departManagerController', ["$scope", "$sce", "toaster", "cmuhBase", "departManager", function ($scope, $sce, toaster, cmuhBase, departManager) {

    //#region 方法

    //#region 設定資料結構
    var initViewModel = function (pageModel) {

        $scope.viewModel = pageModel;

        if ($scope.viewModel.isInitDepartViewModel === true) {
            return;
        }

        kendo.culture("zh-TW");

        //預設結束日期
        $scope.viewModel.endDate = new Date(2999, 11, 31);

        //預設顯示部門清單Grid
        $scope.viewModel.showGrid = true;

        //設定部門清單Grid
        SetDepartGridOptions();

        //設定部門清單TreeList
        SetDepartTreeListOptions();

        //取得院區代碼清單
        getBranchInfos();

        //取得部門清單
        getDepartments();

        //取得部門層級清單
        getDepartTypes();

        //設定高度
        document.getElementById("departGrid").style.height = window.innerHeight - 205 + "px";
        document.getElementById("departTreeList").style.height = window.innerHeight - 205 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }

        $scope.viewModel.isInitDepartViewModel = true;
    }
    //#endregion

    //#region 設定部門清單Grid
    var SetDepartGridOptions = function (items) {
        $scope.viewModel.departGridOptions = {
            rowTemplate: kendo.template($("#departGridRowTemplate").html()),
            sortable: true,
            pageable: true,
            editable: true,
            //selectable: "multiple, row",
            filterable: true,
            columns: [
                { field: "", title: "停用", width: "50px" },
                { field: "departNo", title: "部門代號", width: "110px" },
                { field: "levelNo", title: "層級編碼", width: "140px" },
                { field: "branchNo", title: "院區代碼", width: "300px" },
                { field: "levelType.optionName", title: "層級屬性", width: "110px" },
                { field: "departType.optionName", title: "部門屬性", width: "150px" },
                { field: "chineseName", title: "中文名稱", width: "200px" },
                { field: "englishName", title: "英文名稱" },
                { field: "shortName", title: "單位簡稱", width: "150px" },
                { field: "startDate", title: "開始日期", width: "100px", format: "{0: yyyy-MM-dd}" },
                { field: "endDate", title: "結束日期", width: "100px", format: "{0: yyyy-MM-dd}" },
                //{ field: "isCostCenter", title: "成本中心", width: "110px" },
                //{ field: "locationDesc", title: "位置說明" },
                //{ field: "drugStockLevel", title: "藥庫層級", width: "110px" },
                //{ field: "matStockLevel", title: "資材層級", width: "110px" },
                { field: "upperLevelNo", title: "上層層級", width: "140px"}
                //{ field: "upperShortName", title: "上層單位", width: "150px" }
                
            ],
            dataSource: new kendo.data.DataSource(),
            edit: function (e) {
                if (e.model.entityState == "Unchanged") {
                    e.model.entityState = "Modified";
                }
                e.model.systemUser = $scope.shareModel.userInfos.userNo;
                e.container.find("input[name=departNo]").attr("maxlength", 4);
                e.container.find("input[name=upperDepartNo]").attr("maxlength", 4);
                e.container.find("input[name=levelNo]").attr("maxlength", 14);
            }
        };
    }
    //#endregion

    //#region 設定部門清單Grid DataSource
    var setDepartGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            pageSize: 19,
            sort: { field: "departNo", dir: "asc" },
            schema: {
                model: {
                    fields: {
                        departNo: { type: "string", editable: true },
                        levelNo: { type: "string", editable: true },
                        branchNo: { type: "string", editable: false },
                        "levelType.optionName": { type: "string", editable: false },
                        "departType.optionName": { type: "string", editable: false },
                        chineseName: { type: "string", editable: true },
                        englishName: { type: "string", editable: true },
                        shortName: { type: "string", editable: true },
                        startDate: { type: "date", editable: true },
                        endDate: { type: "date", editable: false },
                        //isCostCenter: { type: "string", editable: true },
                        //locationDesc: { type: "string", editable: true },
                        //drugStockLevel: { type: "number", editable: true },
                        //matStockLevel: { type: "number", editable: true },
                        upperDepartNo: { type: "string", editable: true },
                        upperLevelNo: { type: "string", editable: false },
                        upperShortName: { type: "string", editable: false }
                    }
                }
            }
        });
        $scope.viewModel.departGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#departGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.departGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定部門清單TreeList
    var SetDepartTreeListOptions = function (items) {
        $scope.viewModel.departTreeListOptions = {
            toolbar: ["excel"],
            columns: [
                { field: "departNo", title: "部門代號", width: "110px" },
                { field: "levelNo", title: "層級編碼", width: "140px" },
                { field: "branchNo", title: "院區代碼", width: "110px" },
                { field: "chineseName", title: "中文名稱", width: "200px" },
                { field: "englishName", title: "英文名稱", width: "200px" },
                { field: "shortName", title: "單位簡稱", width: "150px" }
            ],
            dataSource: new kendo.data.TreeListDataSource(),
            excel: {
                fileName: "部門清單樹狀圖.xlsx"
            }
        };
    }
    //#endregion

    //#region 設定部門清單TreeList DataSource
    var setDepartTreeListSource = function (items) {
        var dataSource = new kendo.data.TreeListDataSource({
            data: items,
            schema: {
                model: {
                    id: "levelNo",
                    fields: {
                        parentId: { field: "upperLevelNo", type: "string", nullable: true },
                        levelNo: { field: "levelNo", type: "string", nullable: false }
                    }
                }
            }
        });
        $scope.viewModel.departTreeListOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#departTreeList").data("kendoTreeList");
            if (grid == null) {
                $scope.viewModel.departTreeListOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得院區代碼清單
    var getBranchInfos = function () {
        departManager.getBranchInfos().then(
            function (success) {
                $scope.viewModel.branchInfos = [{ "branchNo": "0", "hospitalName": "---------------請選擇---------------" }];
                $scope.viewModel.branchInfos = $scope.viewModel.branchInfos.concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得部門資料
    var getDepartments = function () {
        departManager.getDepartments().then(
            function (success) {
                $scope.viewModel.departments = success.data;
                for (var i = 0; i < $scope.viewModel.departments.length; i++) {
                    $scope.viewModel.departments[i].upperLevelNo = getUpperLevelNo($scope.viewModel.departments[i]);
                    $scope.viewModel.departments[i].upperShortName = "";
                    for (var j = 0; j < $scope.viewModel.departments.length; j++) {
                        if ($scope.viewModel.departments[i].upperLevelNo == $scope.viewModel.departments[j].levelNo) {
                            $scope.viewModel.departments[i].upperShortName = $scope.viewModel.departments[j].shortName;
                            break;
                        }
                    }
                }
                setDepartGridSource($scope.viewModel.departments);
                var newsList = angular.copy($scope.viewModel.departments);
                var filterNews = newsList.filter(function (item) {
                    return (item.levelNo == "00000000000000") ? false : true;
                });
                setDepartTreeListSource(filterNews);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 取得部門層級清單
    var getDepartTypes = function () {
        departManager.getDepartTypes().then(
            function (success) {
                $scope.viewModel.departTypes = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 取得部門上層資料
    var getUpperLevelNo = function (item) {
        if (item.levelNo == "10000000000000") {
            return "";
        }else if(item.levelNo.substring(1,14) == "0000000000000") {
            return "10000000000000";
        } else if (item.levelNo.substring(2, 14) == "000000000000") {
            return item.levelNo.substring(0, 2) + "000000000000";
        } else if (item.levelNo.substring(3, 14) == "00000000000") {
            return item.levelNo.substring(0, 2) + "000000000000";
        } else if (item.levelNo.substring(4, 14) == "0000000000") {
            return item.levelNo.substring(0, 2) + "000000000000";
        } else if (item.levelNo.substring(5, 14) == "000000000") {
            return item.levelNo.substring(0, 4) + "0000000000";
        } else if (item.levelNo.substring(6, 14) == "00000000") {
            return item.levelNo.substring(0, 4) + "0000000000";
        } else if (item.levelNo.substring(7, 14) == "0000000") {
            return item.levelNo.substring(0, 6) + "00000000";
        } else if (item.levelNo.substring(8, 14) == "000000") {
            return item.levelNo.substring(0, 6) + "00000000";
        } else if (item.levelNo.substring(9, 14) == "00000") {
            return item.levelNo.substring(0, 8) + "000000";
        } else if (item.levelNo.substring(10, 14) == "0000") {
            return item.levelNo.substring(0, 8) + "000000";
        } else if (item.levelNo.substring(11, 14) == "000") {
            return item.levelNo.substring(0, 10) + "0000";
        } else if (item.levelNo.substring(12, 14) == "00") {
            return item.levelNo.substring(0, 10) + "0000";
        } else {
            return item.levelNo.substring(0, 12) + "00";
        }

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 儲存
    $scope.save = function () {
        var newsList = angular.copy($scope.viewModel.departGrid.dataSource.data());
        var filterNews = newsList.filter(function (item) {
            return (item.entityState != "Unchanged" && item.entityState != "Detached") ? true : false;
        });
        for (var i = 0; i < filterNews.length; i++) {
            if (!!filterNews[i].departNo == false) {
                toaster.showErrorMessage("對不起，部門代號不可為空白");
                return;
            }
        }
        departManager.setDepartments(filterNews).then(
            function (success) {
                if (success.data == "true" || success.data == true) {
                    toaster.showSuccessMessage("恭喜您，存檔成功");
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
                getDepartments();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 新增
    $scope.new = function () {
        var data = {
            departNo: "",
            levelNo: "",
            branchNo: "",
            chineseName: "",
            englishName: "",
            shortName: "",
            isCostCenter: "0",
            locationDesc: "",
            drugStockLevel: "0",
            matStockLevel: "0",
            upperDepartNo: "",
            healthType: "0",
            startDate: new Date(),
            endDate: new Date(2999,11,31),
            systemUser: $scope.shareModel.userInfos.userNo,
            systemTime: new Date(),
            entityState: "Added"
        };
        $scope.viewModel.departGrid.dataSource.data().push(data);
    }
    //#endregion
    
    //#region 停用
    $scope.delete = function (dataItem) {
        if (confirm("確認停用部門代號 " + dataItem.departNo + " 嗎？") != true) {
            return;
        }
        if (dataItem.entityState == "Added") {
            dataItem.entityState = "Detached";
        } else {
            dataItem.entityState = 'Modified';
        }
        dataItem.endDate = cmuhBase.getDateFormat(new Date());
        dataItem.systemUser = $scope.shareModel.userInfos.userNo;
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{
            cells: [{ value: "部門代號" }, { value: "層級編碼" }, { value: "院區代碼" }, { value: "層級屬性" }, { value: "部門屬性" }, { value: "中文名稱" },
                { value: "英文名稱" }, { value: "單位簡稱" }, { value: "開始日期" }, { value: "結束日期" }, { value: "上層層級" }]
        }];
        for (var i = 0; i < $scope.viewModel.departGrid.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].departNo },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].levelNo },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].branchNo },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].levelType.optionName },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].departType.optionName },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].chineseName },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].englishName },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].shortName },
                    { value: cmuhBase.getDateFormat($scope.viewModel.departGrid.dataSource.data()[i].startDate) },
                    { value: cmuhBase.getDateFormat($scope.viewModel.departGrid.dataSource.data()[i].endDate) },
                    { value: $scope.viewModel.departGrid.dataSource.data()[i].upperLevelNo }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "部門管理資料.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);