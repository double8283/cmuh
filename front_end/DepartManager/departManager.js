﻿angular.module('cmuh').service('departManager', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得院區代碼清單
    this.getBranchInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/HRManagerService/GetBranchInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得部門清單
    this.getDepartments = function () {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/HRManagerService/GetDepartments");
        return $http.get(url);
    }
    //#endregion

    //#region 取得部門層級清單
    this.getDepartTypes = function () {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/HRManagerService/GetDepartTypes");
        return $http.get(url);
    }
    //#endregion

    //#region 儲存部門清單
    this.setDepartments = function (data) {
        return $http.put("../WebApi/HumanResource/HRManagerService/SetDepartments", data);
    }
    //#endregion

}]);