﻿angular.module("cmuh").controller("cancerQueryPatientsInfoController", ["$scope", "$http", "$timeout", "toaster", "cmuhBase", "cancerQueryPatientsInfo", "$filter", function ($scope, $http, $timeout, toaster, cmuhBase, cancerQueryPatientsInfo, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        if ($scope.viewModel.cancerQueryPatientsInitViewModel == true) {
            return;
        }

        //UADT center個案系統的caseType預設為0，復健科個案管理的caseType預設為70
        $scope.appModel.caseType = $scope.executingAppStore.appId == 75 ? 0 : 70;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //查詢變數
        $scope.viewModel.searchRequest = {
            searchType: "Period",
            caseType: "0",
            startDate: new Date(),
            endDate: new Date(),
            chartNo: "",
        }

        $scope.viewModel.searchType = 'time';

        $scope.viewModel.searchDate = new Date();
        $scope.viewModel.visitDate1 = new Date();
        $scope.viewModel.visitDate2 = new Date();

        //個案清單Grid
        $scope.viewModel.caseListGridOptions = {
            sortable: true,
            selectable: 'row',
            dataSource: new kendo.data.DataSource(),
            columns: [
             { field: "startDate", title: "收案日期", template: "<span>{{dataItem.startDate | date:'yyyy-MM-dd'}}</span>" },
             { field: "ptName", title: "姓名" },
             { field: "chartNo", title: "病歷號" },
             { field: "birthday", title: "生日", template: "<span>{{dataItem.birthday | date:'yyyy-MM-dd'}}</span>" },
             { field: "sex", title: "性別", template: "{{dataItem.sex.optionName}}" }
            ],
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.selectPatient(dataItem);
                $scope.viewModel.caseListGrid.element.on('dblclick', function (e) {
                    $scope.appNavMenuCtrl.slider = 'appMenu-open'; $scope.appNavMenuCtrl.fade = 'closeArea-fade';
                });
            }
        };

        //門診清單Grid
        $scope.viewModel.disclosedListGridOptions = {
            selectable: 'row',
            dataSource: new kendo.data.DataSource(),
            columns: [
             { field: "ptName", title: "姓名" },
             { field: "chartNo", title: "病歷號" },
             { field: "visitNo", title: "住院號" },
             { field: "birthday", title: "生日", template: "<span>{{dataItem.birthday | date:'yyyy-MM-dd'}}</span>" },
             { field: "sex", title: "性別", template: "{{dataItem.sex.optionName}}" }
            ],
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.viewModel.selectDisclosed = dataItem;
            }
        };

        //新增清單Grid
        $scope.viewModel.newPatientListGridOptions = {
            selectable: 'row',
            dataSource: new kendo.data.DataSource(),
            columns: [
             { field: "ptName", title: "姓名" },
             { field: "chartNo", title: "病歷號" },
             { field: "visitNo", title: "住院號" },
             { field: "birthday", title: "生日", template: "<span>{{dataItem.birthday | date:'yyyy-MM-dd'}}</span>" },
             { field: "sex", title: "性別", template: "{{dataItem.sex.optionName}}" }
            ],
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.viewModel.selectDisclosed = dataItem;
            }
        };

        $scope.viewModel.cancerQueryPatientsInitViewModel = true;
    }
    //#endregion

    //#region 設定CaseListGrid DataSource
    var setCaseListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.caseListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#caseListGrid").data("kendoGrid");
            if (grid != null) {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定DisclosedListGrid DataSource
    var setDisclosedListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });

        setTimeout(function () {
            var grid = $("#disclosedListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.disclosedListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定newPatientListGrid DataSource
    var setNewPatientListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#newPatientListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.newPatientListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 由左補0
    var padLeft = function (string, length) {
        if (string.length >= length) {
            return string;
        } else {
            return padLeft("0" + string, length);
        }
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 設定高度
    $scope.setHeight = function () {
        document.getElementById("caseListGrid").style.height = window.innerHeight - 250 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
    }
    //#endregion

    //#region 取得已收案清單
    $scope.getCaseRecordList = function () {
        $scope.appModel.caseType = $scope.viewModel.searchRequest.caseType;
        if ($scope.viewModel.searchRequest.searchType == 'chartNo') {
            $scope.viewModel.searchRequest.chartNo = padLeft($scope.viewModel.searchRequest.chartNo, 10);
        }
        cancerQueryPatientsInfo.getCaseRecordList($scope.viewModel.searchRequest).then(
            function (success) {
                if (success.data.length != 0) {
                    setCaseListGridSource(success.data);
                } else {
                    setCaseListGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setCaseListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 選取病人
    $scope.selectPatient = function (item) {
        $scope.appModel.patientInfos = item;
        var basicInfo = cmuhBase.stringFormat(
                                "{0} {1} {2} {3} ({4}歲)",
                                item.ptName,
                                item.chartNo,
                                $scope.appModel.patientInfos.sex.optionName,
                                item.birthday.substring(0, 10),
                                item.age);
        $scope.shareModel.banner.showBanner(true, null, $scope.appModel.patientInfos.sex.optionName, basicInfo, "", "");
    }
    //#endregion

    //#region 取得門診清單
    $scope.getDisclosedList = function () {
        var date = cmuhBase.getDateFormat($scope.viewModel.searchDate);
        cancerQueryPatientsInfo.getDisclosedList("RoomNo", "0", $scope.appModel.caseType, date).then(
            function (success) {
                if (success.data.length != 0) {
                    setDisclosedListGridSource(success.data);
                } else {
                    setDisclosedListGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setDisclosedListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 搜尋病人資料
    $scope.searchPt = function () {
        var date = cmuhBase.getDateFormat($scope.viewModel.searchDate);
        var chartNo = cmuhBase.addLeftZero($scope.viewModel.searchChartNo);
        cancerQueryPatientsInfo.getDisclosedList("ChartNo", chartNo, $scope.appModel.caseType, date).then(
            function (success) {
                if (success.data.length != 0) {
                    setNewPatientListGridSource(success.data);
                } else {
                    setNewPatientListGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setNewPatientListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 收案
    $scope.closeCase = function () {
        $scope.viewModel.ptSelectWindow.close();
        $scope.viewModel.newWindow.close();
        setDisclosedListGridSource();
        setNewPatientListGridSource();
        var newCase = {
            caseType: $scope.appModel.caseType,
            idNo: $scope.viewModel.selectDisclosed.idNo,
            branchNo: 0,
            visitType: "O",
            createTime: new Date(),
            years: (new Date()).getFullYear() - 1911,
            createUser: $scope.shareModel.userInfos.userNo,
            ptName: $scope.viewModel.selectDisclosed.ptName,
            sex: $scope.viewModel.selectDisclosed.sex,
            visitNo: $scope.viewModel.selectDisclosed.visitNo
        };
        cancerQueryPatientsInfo.setCaseManagement(newCase).then(
            function (success) {
                if (success.data == "true") {
                    $scope.getCaseRecordList();
                    toaster.showSuccessMessage("恭喜您，收案成功");
                    $scope.viewModel.selectDisclosed = [];
                } else {
                    toaster.showErrorMessage("對不起，收案失敗");
                }
            }, function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);
