﻿angular.module("cmuh").service("cancerQueryPatientsInfo", ["$http", "$q", "cmuhBase", function ($http, $q, cmuhBase) {

    //#region 取得已收案清單
    this.getCaseRecordList = function (searchRequest) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetCaseRecordList");
        return $http.put(url, searchRequest);
    }
    //#endregion

    //#region 取得門診清單
    this.getDisclosedList = function (type, value, caseType,visitDate) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetDisclosedList/{0}/{1}/{2}/{3}", type, value, caseType,visitDate);
        return $http.get(url);
    }
    //#endregion

    //#region 增加個案病人
    this.setCaseManagement = function (addCase) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/SetCaseManagement");
        return $http.put(url, addCase);
    }
    //#endregion

}]);