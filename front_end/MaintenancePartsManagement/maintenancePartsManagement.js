﻿angular.module('cmuh').service('maintenancePartsManagement', ["$http", "cmuhBase", function ($http, cmuhBase) {
   
    //#region 取得零件分類資料
    this.getMaintenanceStaff = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPartsClasses/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得零件資料
    this.getPartsBasics = function (partsType) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPartsBasics/{0}", partsType);
        return $http.get(url);
    }
    //#endregion

}]);