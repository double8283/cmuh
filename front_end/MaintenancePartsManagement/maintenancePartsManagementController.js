﻿angular.module("cmuh").controller("maintenancePartsManagementController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenancePartsManagement", function ($scope, $http, $q, cmuhBase, toaster, maintenancePartsManagement) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //維修部門代號
        $scope.viewModel.repairDepartNo = $scope.executingAppStore.appId == 351 ? "1AD0" : $scope.executingAppStore.appId == 352 ? "1A91" : "1A82";
        $scope.viewModel.selectd = 0;

        //取得零件分類資料
        $scope.getMaintenanceStaff();

        //設定高度
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 200 + "px";
        }
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 新增類別
    $scope.addClass = function () {
        var data = {
            PartsType:'',
            TypeName:'',
            DepartNo:'',
            SystemUser: '',
            SystemTime: ''
        };
        switch ($scope.viewModel.class) {
            case 1://大類別
                break;
            case 2://中類別
                break;
            case 3://小類別
                break;
        }
        $scope.viewModel.className = "";
        $scope.viewModel.addClassWindow.close();
    }
    //#endregion
    
    //#region 新增零件
    $scope.addParts = function () {
        
    }
    //#endregion

    //#region 存檔
    $scope.save = function () {
        //maintenancePartsManagement.getMaintenanceStaff($scope.viewModel.repairDepartNo).then(
        //    function (success) {
                
        //    },
        //    function (error) {
        //        toaster.showErrorMessage("對不起，連線失敗");
        //    }
        //);
    }
    //#endregion

    //#region 取得零件分類資料
    $scope.getMaintenanceStaff = function () {
        maintenancePartsManagement.getMaintenanceStaff($scope.viewModel.repairDepartNo).then(
            function (success) {
                $scope.viewModel.maintenanceStaff = [];
                $scope.viewModel.maintenanceStaff[0] = success.data;
                $scope.viewModel.partsBasics = [];
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 選擇零件類別
    $scope.getPartsType = function (item, index) {
        for (var i = index + 1; i < $scope.viewModel.maintenanceStaff.length; i++) {
            $scope.viewModel.maintenanceStaff[i] = [];
        }
        $scope.viewModel.maintenanceStaff[index + 1] = item.details;
        $scope.viewModel.partNo = item.partsType;
    }
    //#endregion

    //#region 取得零件資料
    $scope.getPartsBasics = function () {
        maintenancePartsManagement.getPartsBasics($scope.viewModel.partNo).then(
            function (success) {
                $scope.viewModel.partsBasics = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 修改背景顏色
    $scope.changeBgColor = function (array, index) {
        for (var i = 0; i < array.length; i++) {
            array[i].backColor = "#FFFFFF";
        }
        array[index].backColor = "#428bca";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);