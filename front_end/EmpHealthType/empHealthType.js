﻿angular.module('cmuh').service("empHealthType", ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得員工體檢建置清單
    this.getEmployeeExamList = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/GetEmpHealthList", data);
    }
    //#endregion
    
    //#region 建置健檢資料至EmpHealthRecord資料表
    this.setHealthRecord = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/SetHealthRecord", data);
    }
    //#endregion

    //#region 取得匯入的主管清單資料
    this.getResponsibleList = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/GetEmpManageList", data);
    }
    //#endregion

    //#region 取得部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion
}]);