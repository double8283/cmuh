﻿angular.module('cmuh').controller("empHealthTypeController", ["$scope", "$q", "cmuhBase", "toaster", "empHealthType", function ($scope, $q, cmuhBase, toaster, empHealthType) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //年度選擇器
        $scope.viewModel.searchYearPickerOptions = {
            format: 'yyyy',
            depth: "decade",
            start: "decade",
            min: new Date(new Date().getFullYear(), 1, 1),
            max: new Date(new Date().getFullYear()+1,1,1)
        };

        //員工Grid
        $scope.viewModel.select = "false";
        $scope.viewModel.employeeGridOptions = {
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "check", title: "", width: "50px", sortable: false,
                    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" ng-true-value=\"true\" ng-false-value=\"false\"/></center>",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem.check)\" ng-true-value=\"true\" ng-false-value=\"false\"/>"
                },
                { field: "", title: "健檢類別", width: "300px", template: "<select data-kendo-drop-down-list data-ng-model=\"dataItem.healthTypeNo\" style=\"width:200px\"><option value=\"10\">主管健檢</option><option value=\"11\">主管健檢&校院務</option><option value=\"12\">主管健檢&透析</option><option value=\"13\">主管健檢&輻防</option><option value=\"14\">主管健檢+透析&校院務</option><option value=\"15\">主管健檢+輻防&校院務</option><option value=\"20\">員工健檢</option><option value=\"30\">司機健檢</option><option value=\"40\">輻防健檢</option><option value=\"50\">供膳健檢</option><option value=\"60\">透析健檢</option><option value=\"70\">檢驗部健檢</option><option value=\"99\">免檢</option></select><select data-kendo-drop-down-list data-ng-model=\"dataItem.healthTypeNo2\" style=\"width:70px\"><option value=\"0\">自動</option><option value=\"1\">普通</option><option value=\"2\">進階</option></select>" },
                { field: "departName", title: "部門" },
                { field: "empCode", title: "員工代號", width: "80px" },
                { field: "empName", title: "姓名", width: "80px" },
                { field: "idNo", title: "身分證", width: "110px" },
                { field: "birthday", title: "出生日期", width: "100px", template: "{{dataItem.birthday | date:'yyyy-MM-dd'}}" },
                { field: "startDate", title: "開始日期", width: "140px", template: "<input kendo-date-picker data-k-ng-model=\"dataItem.startDate\" data-k-culture=\"'zh-TW'\" data-k-format=\"'yyyy-MM-dd'\" style=\"width:130px\"/>" },
                { field: "endDate", title: "結束日期", width: "140px", template: "<input kendo-date-picker data-k-ng-model=\"dataItem.endDate\" data-k-culture=\"'zh-TW'\" data-k-format=\"'yyyy-MM-dd'\" style=\"width:130px\"/>" },
                { field: "", title: "備註", width: "300px", template: "<input type=\"text\" class=\"k-textbox\" data-ng-model=\"dataItem.remark\" style=\"width: 250px\" />" }
            ],
            dataSource: new kendo.data.DataSource()
        };

        //預設搜尋值
        $scope.viewModel.searchType = 20;
        $scope.viewModel.searchYear = new Date();
        $scope.viewModel.searchStartMonth = new Date().getMonth() + 1;
        $scope.viewModel.searchEndMonth = new Date().getMonth() + 1;
        $scope.viewModel.searchDept = "0";
        $scope.viewModel.searchHospital = 0;

        //取得部門清單
        getDeptList();
    }
    //#endregion

    //#region 取得部門清單
    var getDeptList = function () {
        empHealthType.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
                    $scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 員工體檢建置清單
    var getEmployeeExamList = function () {
        var empCode,empDept;
        if ($scope.viewModel.searchEmpCode != undefined && $scope.viewModel.searchEmpCode != null && $scope.viewModel.searchEmpCode != '') {
            empCode = $scope.viewModel.searchEmpCode.replace(/[^\d]/g, '');
        } else {
            empCode = 0;
        }
        $scope.viewModel.searchType = $scope.viewModel.searchHospital != 0 ? 20 : $scope.viewModel.searchType;
        var data = {
            "healthType": $scope.viewModel.searchType,
            "healthYear":$scope.viewModel.searchYear.getFullYear(),
            "startMonth":$scope.viewModel.searchStartMonth,
            "endMonth":$scope.viewModel.searchEndMonth,
            "empNo":empCode,
            "departNo": $scope.viewModel.searchDept,
            "hospital": $scope.viewModel.searchHospital
        };
        empHealthType.getEmployeeExamList(data).then(
            function (success) {
                if (success.data.length != 0) {
                    setEmployeeGridSource(success.data);
                } else {
                    setEmployeeGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setEmployeeGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 設定EmployeeGrid DataSource
    var setEmployeeGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            pageSize: 14
        });
        setTimeout(function () {
            var grid = $("#employeeGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.employeeGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得健檢類別編號
    var getHealthTypeNo = function (healthType) {
        switch (healthType) {
            case "主管健檢":
                return 10; break;
            case "員工普通健檢":
                return 21; break;
            case "員工進階健檢":
                return 22; break;
            case "專案普通健檢":
                return 31; break;
            case "專案進階健檢":
                return 32; break;
        }
    }
    //#endregion
    
    //#region 建置健檢資料至EmpHealthRecord資料表
    var setHealthRecord = function (data) {
        if (data.length == 0) {
            toaster.showErrorMessage("請勾選欲存檔的資料");
        } else {
            empHealthType.setHealthRecord(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("建置完成");
                    $scope.search();
                } else {
                    toaster.showErrorMessage("對不起，建置失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
        }
    }
    //#endregion

    //#region 載入檔案
    var loadHandler = function (event) {
        var csv = event.target.result;
        var allTextLines = csv.split(/\r\n|\n/);
        var data = [];
        for (var i = 0; i < allTextLines.length; i++) {
            data.push({
                "healthYear": $scope.viewModel.searchYear.getFullYear(),
                "empNo": allTextLines[i].split(',')[0].replace(/[^\d]/g, ''),
                "healthType": allTextLines[i].split(',')[1]
            })
        }
        getResponsibleList(data);
    }
    //#endregion

    //#region 取得匯入的主管清單資料
    var getResponsibleList = function (data) {
        empHealthType.getResponsibleList(data).then(
            function (success) {
                if (success.data != null) {
                    setEmployeeGridSource(success.data);
                } else {
                    setEmployeeGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 查詢
    $scope.search = function () {
        if ($scope.viewModel.searchType == '' || $scope.viewModel.searchType == null) {
            toaster.showErrorMessage("請選擇查詢類別");
        } else if ($scope.viewModel.searchYear == '' || $scope.viewModel.searchYear == null) {
            toaster.showErrorMessage("請選擇查詢年度");
        } else if ($scope.viewModel.searchStartMonth == '' || $scope.viewModel.searchStartMonth == null) {
            toaster.showErrorMessage("請選擇員工生日開始月份");
        } else if ($scope.viewModel.searchEndMonth == '' || $scope.viewModel.searchEndMonth == null) {
            toaster.showErrorMessage("請選擇員工生日結束月份");
        } else if (Number($scope.viewModel.searchStartMonth) > Number($scope.viewModel.searchEndMonth)) {
            toaster.showErrorMessage("開始月份大於結束月份，請修改");
        } else {
            getEmployeeExamList();
            $scope.viewModel.healthYear = $scope.viewModel.searchYear.getFullYear();
            $scope.viewModel.healthType = $scope.viewModel.searchType;
            $scope.viewModel.select = "false";
        }
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (check) {
        $scope.viewModel.select = check == "false" ? "false" : $scope.viewModel.select;
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            $scope.viewModel.employeeGrid.dataSource.data()[i].check = $scope.viewModel.select;
        }
    }
    //#endregion

    //#region 設定健檢種類
    $scope.setHealthType = function (healthType) {
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.employeeGrid.dataSource.data()[i].healthType = healthType;
                $scope.viewModel.employeeGrid.dataSource.data()[i].check = "false";
                if ($scope.viewModel.employeeGrid.dataSource.data()[i].healthType == "專案健檢") {
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate = new Date();
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setFullYear($scope.viewModel.HealthYear);
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setMonth(((new Date($scope.viewModel.employeeGrid.dataSource.data()[i].birthday).getMonth()) / 2 + 6));
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setDate(1);
                    var date = Math.ceil(((new Date($scope.viewModel.HealthYear, $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.getMonth() + 1, 1)) - $scope.viewModel.employeeGrid.dataSource.data()[i].startDate) / (1000 * 60 * 60 * 24));
                    $scope.viewModel.employeeGrid.dataSource.data()[i].endDate = new Date($scope.viewModel.HealthYear, $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.getMonth(), date);
                } else {
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate = new Date();
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setFullYear($scope.viewModel.HealthYear);
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setMonth(((new Date($scope.viewModel.employeeGrid.dataSource.data()[i].birthday).getMonth()) / 2));
                    $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.setDate(1);
                    var date = Math.ceil(((new Date($scope.viewModel.HealthYear, $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.getMonth() + 1, 1)) - $scope.viewModel.employeeGrid.dataSource.data()[i].startDate) / (1000 * 60 * 60 * 24));
                    $scope.viewModel.employeeGrid.dataSource.data()[i].endDate = new Date($scope.viewModel.HealthYear, $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.getMonth(), date);
                }
            }
        }
        $scope.viewModel.select = "false";
        toaster.showSuccessMessage("設置成功");
    }
    //#endregion

    //#region 存檔
    $scope.save = function () {
        var data = [];
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.employeeGrid.dataSource.data()[i].healthYear = $scope.viewModel.searchYear.getFullYear();
                data.push($scope.viewModel.employeeGrid.dataSource.data()[i]);
            }
        }
        setHealthRecord(data);
    }
    //#endregion

    //#region 按下匯入
    $scope.import = function () {
        document.getElementById("csvFileInput").click();
    }
    //#endregion
    
    //#region 匯入主管健檢名單
    $scope.handleFiles = function (files) {
        if (files[0] == undefined) {
            return;
        }
        var reader = new FileReader();
        reader.onload = loadHandler;
        reader.readAsText(files[0], "big5");
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{ cells: [{ value: "健檢類別" }, { value: "部門" }, { value: "員工代號" }, { value: "姓名" }, { value: "身分證" }, { value: "出生日期" }, { value: "開始日期" }, { value: "結束日期" }] }];
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                data.push({
                    cells: [
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].healthType },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].departName },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empCode },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empName },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].idNo },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].birthday.substring(0,10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].endDate.substring(0, 10) }
                    ]
                });
            }
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "員工健檢建置資料.xlsx"
        });
    }
    //#endregion

    //#region 下載範本
    $scope.downloadTemplate = function () {
        document.getElementById("downloadTemplate").click();
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);

