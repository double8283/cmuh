﻿angular.module("cmuh").controller("cancerStatisticsReportController", ["$scope", "$http", "$timeout", "toaster", "cmuhBase", "cancerStatisticsReport", "$filter", function ($scope, $http, $timeout, toaster, cmuhBase, cancerStatisticsReport, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //預設治療查詢日期為今天
        $scope.viewModel.startDate = new Date();
        $scope.viewModel.endDate = new Date();

        //月報表Grid
        $scope.viewModel.monthRepartGridOptions = {
            dataSource: new kendo.data.DataSource(),
            columns: [
             { field: "", title: "收案人數" },
             { field: "", title: "結案人數" },
             { field: "", title: "收案來源：門診" },
             { field: "", title: "收案來源：住院" },
             { field: "", title: "治療後一個月完成率" },
             { field: "", title: "治療後六個月完成率" },
             { field: "", title: "治療後一年完成率" }
            ]
        };

        //個案追蹤報表Grid
        $scope.viewModel.caseRepartGridOptions = {
            sortable: true,
            dataSource: new kendo.data.DataSource(),
            columns: [
             { field: "", title: "收案日期" },
             { field: "", title: "姓名" },
             { field: "", title: "病歷號" },
             { field: "", title: "生日" },
             { field: "", title: "主治醫師" },
             { field: "", title: "手術日" },
             { field: "", title: "化學藥物治療結束日" },
             { field: "", title: "放射線治療結束日" },
             { field: "", title: "治療後一個月預回日期" },
             { field: "", title: "治療後六個月預回日期" },
             { field: "", title: "治療後一年預回日期" }
            ]
        };
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 調整高度
    $scope.setHeight = function () {
        document.getElementById("caseRepartGrid").style.height = window.innerHeight - 343 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 185 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 245 + "px";
        }
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var type = $scope.appModel.caseType == 69 ? "癌症" : $scope.appModel.caseType == 76 ? "中風" : "失智";

        //var data = [{ cells: [{ value: "個案類別：" + type }, { value: "查詢時間：" + cmuhBase.getDateFormat($scope.viewModel.startDate) + "至" + cmuhBase.getDateFormat($scope.viewModel.endDate) }] }];
        //data.push({ cells: [{ value: "" }] });
        //data.push({ cells: [{ value: "月報表" }] });
        var data = [{ cells: [{ value: "月報表" }] }];
        data.push({ cells: [{ value: "收案人數" }, { value: "結案人數" }, { value: "收案來源：門診" }, { value: "收案來源：住院" }, { value: "治療後一個月完成率" }, { value: "治療後六個月完成率" }, { value: "治療後一年完成率" }] });
        data.push({ cells: [{ value: "" }] });
        data.push({ cells: [{ value: "個案追蹤報表" }] });
        data.push({ cells: [{ value: "收案日期" }, { value: "姓名" }, { value: "病歷號" }, { value: "生日" }, { value: "主治醫師" }, { value: "手術日" }, { value: "化學藥物治療結束日" }, { value: "放射線治療結束日" }, { value: "治療後一個月預回日期" }, { value: "治療後六個月預回日期" }, { value: "治療後一年預回日期" }] });
        //for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
        //    if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
        //        data.push({
        //            cells: [
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].healthType },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].departName },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empCode },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empName },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].idNo },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].birthday.substring(0, 10) },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.substring(0, 10) },
        //                { value: $scope.viewModel.employeeGrid.dataSource.data()[i].endDate.substring(0, 10) }
        //            ]
        //        });
        //    }
        //}
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: cmuhBase.getDateFormat($scope.viewModel.startDate) + "至" + cmuhBase.getDateFormat($scope.viewModel.endDate) + type + "統計報表.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);
