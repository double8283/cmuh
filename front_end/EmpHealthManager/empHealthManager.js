﻿angular.module('cmuh').service("empHealthManager", ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得疲勞問卷JSON檔
    this.getFatigueQuestion = function (startDate, chartNo) {
        var url = cmuhBase.stringFormat("../web/App/EmpHealthManager/empHealthManager.json");
        return $http.get(url);
    }
    //#endregion

    //#region 取得員工健康管理清單
    this.getEmpHealthManage = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/GetEmpHealthManage", data);
    }
    //#endregion

    //#region 發送通知信件
    this.sendNoticeEmail = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/SendNoticeEmail", data);
    }
    //#endregion
    
    //#region 刪除資料
    this.setDeleteData = function (data) {
        return $http.put("../WebApi/EmpHealthManager/EmpHealthService/RemoveHealth", data);
    }
    //#endregion

    //#region 取得部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion

    //#region 取得文件清單
    this.getFileInfo = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/FileUploadService/GetFileInfo");
        return $http.get(url);
    }
    //#endregion

    //#region 刪除文件
    this.deleteAttachment = function (attachment) {
        return $http.put("../WebApi/EmpHealthManager/FileUploadService/DeleteFile", attachment);
    }
    //#endregion
}]);