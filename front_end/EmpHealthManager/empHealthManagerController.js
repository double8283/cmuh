﻿angular.module('cmuh').controller("empHealthManagerController", ["$scope", "$q", "cmuhBase", "toaster", "empHealthManager", "Upload", "$timeout", function ($scope, $q, cmuhBase, toaster, empHealthManager, Upload, $timeout) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        $scope.viewModel.files = [];
        $scope.viewModel.attachment = [];

        //年度選擇器
        $scope.viewModel.searchYearPickerOptions = {
            format: 'yyyy',
            depth: "decade",
            start: "decade",
            max: new Date(new Date().getFullYear() + 1, 1, 1)
        };

        //員工Grid
        $scope.viewModel.select = "false";
        $scope.viewModel.employeeGridOptions = {
            sortable: true,
            selectable: 'row',
            pageable: true,
            dataSource: new kendo.data.DataSource(),
            columns: [
                {
                    field: "check", title: "", width: "5%", sortable: false,
                    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" ng-true-value=\"true\" ng-false-value=\"false\"/></center>",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem.check)\" ng-true-value=\"true\" ng-false-value=\"false\"/>"
                },
                { field: "healthType", title: "健檢類別", width: "15%" },
                { field: "departName", title: "部門", width: "18%" },
                { field: "empCode", title: "員工代號", width: "10%" },
                { field: "empName", title: "姓名", width: "10%" },
                { field: "idNo", title: "身分證", width: "10%" },
                { field: "birthday", title: "出生日期", width: "10%", template: "{{dataItem.birthday | date:'yyyy-MM-dd'}}" },
                { field: "startDate", title: "開始日期", width: "10%", template: "{{dataItem.startDate | date:'yyyy-MM-dd'}}" },
                { field: "endDate", title: "結束日期", width: "10%", template: "{{dataItem.endDate | date:'yyyy-MM-dd'}}" },
                { field: "healthDate", title: "預約健檢日期", width: "10%", template: "<div data-ng-if=\"isShowDate(dataItem.healthDate)\">{{dataItem.healthDate | date:'yyyy-MM-dd'}}<div>" },
                { field: "isHealth", title: "是否完成健檢", width: "10%" },
                { field: "reportDate", title: "報告日期", width: "10%", template: "<div data-ng-if=\"isShowDate(dataItem.reportDate)\">{{dataItem.reportDate|date:'yyyy-MM-dd'}}<div>" },
                { field: "reportResult", title: "報告結果", width: "10%" },
                { field: "empStatus", title: "員工狀況", width: "10%" },
                { field: "remark", title: "備註", width: "300px" }
            ]
        };

        //附件Grid
        $scope.viewModel.attachmentGridOptions = {
            scrollable: false,
            columns: [
                {
                    field: "check", title: "", width: "30px",
                    headerTemplate: "選擇",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" ng-true-value=\"true\" ng-false-value=\"false\"/>"
                },
                { field: "", title: "刪除", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteAttachment(dataItem)\" /></center>" },
                { field: "OriginalPath", title: "附件名稱" }
            ],
            dataSource: new kendo.data.DataSource()
        };

        //Editor Options
        $scope.viewModel.editorOptions = {
            tools: [
                "fontSize","fontName","foreColor","bold", "italic", "underline", "strikethrough", "subscript", "superscript", "viewHtml"
            ]
        };

        //#region 通知
        $scope.viewModel.notice = [
            { title: "應檢通知", content: "<p><span style=\"font-family:標楷體;font-size:small;\">親愛的同仁，</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;font-size:small;\">您好！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;font-size:small;\">提醒您，請於</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">105</span></span><span style=\"font-family:標楷體;font-size:small;\">年</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2</span></span><span style=\"font-family:標楷體;font-size:small;\">月份完成您的健康檢查</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">!</span></p><p><span style=\"text-decoration:underline;\"></span></p><p><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">105</span></span><span style=\"font-family:標楷體;font-size:small;\">年</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2</span></span><span style=\"font-family:標楷體;font-size:small;\">月份員工健檢應檢名單和健檢種類已於</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">105/01/27&nbsp;</span></span><span style=\"font-family:標楷體;font-size:small;\">公告，自公告日起開始預約，請您至少於受檢日前</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">7</span><span style=\"font-family:標楷體;font-size:small;\">日完成網路預約，以利健檢中心開單並安排檢查</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">(</span><span style=\"font-family:標楷體;font-size:small;\">請勿自行開單受檢</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">)</span><span style=\"font-family:標楷體;font-size:small;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;font-size:small;\">所有檢查項目應於</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">105</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">年</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">月</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">29</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">日</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">一</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"font-family:標楷體;font-size:small;\">前完成，相關說明如下：</span><span style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><a><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">健檢中心年節開放健檢時間為</span></span></a><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">&nbsp;2/7(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">除夕</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">、</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2/8(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">初一</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">、</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2/9(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">初二</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">、</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2/10(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">初三</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">共四天，不開放預約，</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">2/11(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">初四</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)&nbsp;</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">開放上午，限預約普通健檢，其餘時間正常開放，請您儘早預約健檢。</span></span><span lang=\"EN-US\" style=\"font-size:small;font-family:'Times New Roman', serif;\"><a href=\"file:///E:/%E4%B8%AD%E5%9C%8B%E9%86%AB/%E5%93%A1%E5%B7%A5%E5%81%A5%E6%AA%A2/%E5%81%A5%E6%AA%A2%E7%9B%B8%E9%97%9C%E9%80%9A%E7%9F%A5%E6%96%87%E6%A1%88(%E7%B5%A6%E8%B3%87%E8%A8%8A%E5%AE%A4)1050504.doc#_msocom_1\" id=\"_anchor_1\" language=\"JavaScript\" name=\"_msoanchor_1\"></a>&nbsp;</span><span style=\"text-decoration:underline;\"></span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">1.</span><span style=\"font-family:標楷體;font-size:small;\">應檢期限為：</span><span style=\"font-family:'Times New Roman', serif;\">&nbsp;<span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"color:red;font-size:small;\">2</span></span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">月</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">1</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">日</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">一</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\"><span style=\"font-size:small;\">~</span><span style=\"text-decoration:underline;\"><span style=\"color:red;font-size:small;\">2</span></span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">月</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">29</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">日</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;font-size:small;\">一</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">)</span></span><span style=\"font-family:標楷體;font-size:small;\">，請您儘早預約健檢。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">2.</span><span style=\"font-family:標楷體;\"><span style=\"font-size:small;\">詳細注意事項及檢查項目詳如</span><a><span style=\"color:blue;font-size:small;background:yellow;\">附件「員工健檢注意事項」</span></a></span><span lang=\"EN-US\" style=\"font-size:small;font-family:'Times New Roman', serif;\"><a href=\"file:///E:/%E4%B8%AD%E5%9C%8B%E9%86%AB/%E5%93%A1%E5%B7%A5%E5%81%A5%E6%AA%A2/%E5%81%A5%E6%AA%A2%E7%9B%B8%E9%97%9C%E9%80%9A%E7%9F%A5%E6%96%87%E6%A1%88(%E7%B5%A6%E8%B3%87%E8%A8%8A%E5%AE%A4)1050504.doc#_msocom_2\" id=\"_anchor_2\" language=\"JavaScript\" name=\"_msoanchor_2\"></a>&nbsp;</span><span style=\"font-family:標楷體;font-size:small;\">。</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">3.</span><span style=\"font-family:標楷體;font-size:small;\">未於期限內完成應接受之健康檢查者，由職安室定期彙整名單後呈核公告緩發三節獎金。</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">4.</span><span style=\"font-family:標楷體;font-size:small;\">如有員工健檢之預約問題請電洽健檢中心行銷組</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">(</span><span style=\"font-family:標楷體;font-size:small;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">5620~5623)</span><span style=\"font-family:標楷體;font-size:small;\">，四大癌篩問題請電洽癌症篩檢窗口</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">(</span><span style=\"font-family:標楷體;font-size:small;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">2235~2237)</span><span style=\"font-family:標楷體;font-size:small;\">，其他問題請電洽職安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">(</span><span style=\"font-family:標楷體;font-size:small;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">4267~4270)</span><span style=\"font-family:標楷體;font-size:small;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">5.</span><span style=\"font-family:標楷體;font-size:small;\">網路預約操作說明、</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">K05</span><span style=\"font-family:標楷體;font-size:small;\">申請單填寫說明等資料，同步放置於院內網</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">&rarr;</span><span style=\"font-family:標楷體;font-size:small;\">網路文件夾</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">&rarr;</span><span style=\"font-family:標楷體;font-size:small;\">各單位文件公布區</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">&rarr;</span><span style=\"font-family:標楷體;font-size:small;\">職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;font-size:small;\">&rarr;</span><span style=\"font-family:標楷體;font-size:small;\">員工健康檢查相關資料，請自行查閱。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;font-size:small;\">敬請如期完成健檢，感謝您的配合！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;\">&nbsp;</span></p><p align=\"center\" style=\"text-align:center;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">~~</span><span style=\"font-family:標楷體;color:red;font-size:small;\">健康檢查中心、職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;\">&nbsp;&nbsp;</span><span style=\"font-family:標楷體;color:red;font-size:small;\">關心您的健康</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\">!~~</span></p><div><span lang=\"EN-US\" style=\"font-family:'Times New Roman', serif;color:red;font-size:small;\"><br /></span></div><div><div id=\"_com_1\" language=\"JavaScript\"></div></div>" },
            { title: "主管應檢通知", content: "<p><span style=\"font-family:標楷體;\">親愛的主管：</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您好！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您今年</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(105</span><span style=\"font-family:標楷體;\">年</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">的員工健康檢查為「主管健檢」</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">健檢內容和項目如附件</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">，依生日月份施作健檢，請於生日當月及次月預約並完成主管健檢所有項目</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">最遲於生日次月底完成</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">八、九職等以上主管將由健檢中心主動協助預約健檢事宜，六、七職等主管煩請自行跟健檢中心預約檢查。建議六、七職等主管於預定健檢日期至少前</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">14</span><span style=\"font-family:標楷體;\">日向健檢中心行銷組預約健檢方案及時間，預約分機：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623(</span><span style=\"font-family:標楷體;\">共四線</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">，以免檢查名額額滿。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">依院長核示</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">文號：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">C1030812005)</span><span style=\"font-family:標楷體;\">，「全院主管皆要每年健檢，即使放棄主管健檢仍要完成一般健檢，沒有按時健檢者，將緩發三節獎金。」如主管因公務繁忙不克施作主管健檢，請填寫電子簽核表單「</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">K05</span><span style=\"font-family:標楷體;\">員工定期健康檢查異動申請單」申請改做普通健檢，撥空完成普通健檢之理學檢查、抽血、</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">X</span><span style=\"font-family:標楷體;\">光等法定項目。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如有員工健檢之預約問題請電洽健檢中心</span><span style=\"font-family:標楷體;\">行銷組</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623)</span><span style=\"font-family:標楷體;\">，四大癌篩問題請電洽癌症篩檢窗口</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">2235~2237)</span><span style=\"font-family:標楷體;\">，其他問題請電洽職安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4267~4270)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p align=\"center\" style=\"text-align:center;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">~~</span><span style=\"font-family:標楷體;color:red;\">健康檢查中心、職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp; </span><span style=\"font-family:標楷體;color:red;\">關心您的健康</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">!~~</span></p>" },
            { title: "催檢通知", content: "<p><span style=\"font-family:標楷體;\">親愛的同仁，</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您好</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">!</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您今年度之員工健檢尚未完成，請儘速完成。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如您尚未預約檢查，請於檢查前</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">3~7</span><span style=\"font-family:標楷體;\">日以院內網路或是電話</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623</span><span style=\"font-family:標楷體;\">共四線</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">完成預約。已預約檢查者，請依預約日期完成健檢。缺項未檢者，請依缺檢項目之規定前往健檢中心完成。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">有懷孕或是其他原因無法完成</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">CXR</span><span style=\"font-family:標楷體;\">者，請填寫電子簽核表單「</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">K05</span><span style=\"font-family:標楷體;\">員工定期健康檢查異動申請單」申請免檢</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">CXR</span><span style=\"font-family:標楷體;\">項目。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">依據本院「員工健康管理辦法」，</span></p><p><span style=\"font-family:標楷體;\">第三十四條：「一般員工健檢應檢員工未於公告期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">含補檢期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">內完成每年應接受之健康檢查者，由勞安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">北院勞安業務承辦人</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">定期彙整名單後呈核公告緩發端午、中秋獎金，得連續緩發。獎金補發時程於每月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">10</span><span style=\"font-family:標楷體;\">日確認已完成健康檢查人員，於</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">15</span><span style=\"font-family:標楷體;\">日前將名單提交人事室及會計室，再行統一於月底發放。如直至當年度</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">12</span><span style=\"font-family:標楷體;\">月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">31</span><span style=\"font-family:標楷體;\">日止仍未完成當年度健檢者，當年度已緩發之端午及中秋獎金不予補發，亦不再補做當年度員工健檢。」</span></p><p><span style=\"font-family:標楷體;\">第三十五條：「輻防健檢和專案健檢屬法定每年應接受之檢查者，應檢員工如未於公告期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">含補檢期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">內完成健檢者，得連續緩發端午、中秋和年終獎金，待完成健康檢查後再行統一發放</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">獎金補發時程同前條</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">。如直至次年度公告專案健檢施作時程前仍未完成前一年度健檢者，該年度已緩發之獎金不予補發。」</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如有員工健檢之預約問題請電洽健檢中心</span><span style=\"font-family:標楷體;\">行銷組</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623)</span><span style=\"font-family:標楷體;\">，四大癌篩問題請電洽癌症篩檢窗口</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">2235~2237)</span><span style=\"font-family:標楷體;\">，其他問題請電洽職安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4267~4270)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">敬請盡速完成健檢，感謝您的配合！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p align=\"center\" style=\"text-align:center;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">~~</span><span style=\"font-family:標楷體;color:red;\">健康檢查中心、職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp; </span><span style=\"font-family:標楷體;color:red;\">關心您的健康</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">!~~</span></p>" },
            { title: "主管催檢通知", content: "<p><span style=\"font-family:標楷體;\">親愛的主管：</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您好！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您今年度之「主管健檢」尚未完成，請盡速預約安排健檢，謝謝</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">!</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">104</span><span style=\"font-family:標楷體;\">年起主管健檢更改為依生日月份施作健檢，請於生日當月及次月預約並完成主管健檢所有項目</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">最遲於生日次月底完成</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">八、九職等以上主管將由健檢中心主動協助預約健檢事宜，六、七職等主管煩請自行跟健檢中心預約檢查。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">建議六、七職等主管於預定健檢日期至少前</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">14</span><span style=\"font-family:標楷體;\">日向健檢中心行銷組預約健檢方案及時間，預約分機：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623(</span><span style=\"font-family:標楷體;\">共四線</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">，以免檢查名額額滿。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">依據本院「員工健康管理辦法」，</span></p><p><span style=\"font-family:標楷體;\">第三十六條：「主管未於當年度</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">12</span><span style=\"font-family:標楷體;\">月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">31</span><span style=\"font-family:標楷體;\">日前完成主管健檢者，得緩發年終獎金，待完成健康檢查後再行統一發放。」</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如有員工健檢之預約問題請電洽健檢中心</span><span style=\"font-family:標楷體;\">行銷組</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623)</span><span style=\"font-family:標楷體;\">，四大癌篩問題請電洽癌症篩檢窗口</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">2235~2237)</span><span style=\"font-family:標楷體;\">，其他問題請電洽職安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4267~4270)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">敬請盡速完成健檢，感謝您的配合！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p align=\"center\" style=\"text-align:center;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">~~</span><span style=\"font-family:標楷體;color:red;\">健康檢查中心、職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp; </span><span style=\"font-family:標楷體;color:red;\">關心您的健康</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">!~~</span></p>" },
            { title: "未完成通知", content: "<p><span style=\"font-family:標楷體;\">親愛的同仁，</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您好</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">!</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">您今年度之員工健檢尚未完成，請儘速完成。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如您尚未預約檢查，請於檢查前</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">3~7</span><span style=\"font-family:標楷體;\">日以院內網路或是電話</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623</span><span style=\"font-family:標楷體;\">共四線</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">完成預約。已預約檢查者，請依預約日期完成健檢。缺項未檢者，請依缺檢項目之規定前往健檢中心完成。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">有懷孕或是其他原因無法完成</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">CXR</span><span style=\"font-family:標楷體;\">者，請填寫電子簽核表單「</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">K05</span><span style=\"font-family:標楷體;\">員工定期健康檢查異動申請單」申請免檢</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">CXR</span><span style=\"font-family:標楷體;\">項目。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">依據本院「員工健康管理辦法」，</span></p><p><span style=\"font-family:標楷體;\">第三十四條：「一般員工健檢應檢員工未於公告期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">含補檢期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">內完成每年應接受之健康檢查者，由勞安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">北院勞安業務承辦人</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">定期彙整名單後呈核公告緩發端午、中秋獎金，得連續緩發。獎金補發時程於每月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">10</span><span style=\"font-family:標楷體;\">日確認已完成健康檢查人員，於</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">15</span><span style=\"font-family:標楷體;\">日前將名單提交人事室及會計室，再行統一於月底發放。如直至當年度</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">12</span><span style=\"font-family:標楷體;\">月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">31</span><span style=\"font-family:標楷體;\">日止仍未完成當年度健檢者，當年度已緩發之端午及中秋獎金不予補發，亦不再補做當年度員工健檢。」</span></p><p><span style=\"font-family:標楷體;\">第三十五條：「輻防健檢和專案健檢屬法定每年應接受之檢查者，應檢員工如未於公告期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">含補檢期限</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">內完成健檢者，得連續緩發端午、中秋和年終獎金，待完成健康檢查後再行統一發放</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">獎金補發時程同前條</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">。如直至次年度公告專案健檢施作時程前仍未完成前一年度健檢者，該年度已緩發之獎金不予補發。」</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如有員工健檢之預約問題請電洽健檢中心</span><span style=\"font-family:標楷體;\">行銷組</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623)</span><span style=\"font-family:標楷體;\">，四大癌篩問題請電洽癌症篩檢窗口</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">2235~2237)</span><span style=\"font-family:標楷體;\">，其他問題請電洽職安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4267~4270)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">敬請盡速完成健檢，感謝您的配合！</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp;</span></p><p align=\"center\" style=\"text-align:center;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">~~</span><span style=\"font-family:標楷體;color:red;\">健康檢查中心、職業安全衛生室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">&nbsp; </span><span style=\"font-family:標楷體;color:red;\">關心您的健康</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">!~~</span></p>" },
            { title: "領取報告通知", content: "" },
            { title: "緩發三節獎金通知", content: "<p><span style=\"font-family:標楷體;\">親愛的同仁</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">:</span></p><p><span style=\"font-family:標楷體;\">您今年度員工健檢尚未完成，已於</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">8/6(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;\">四</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">)</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;\">發布全院</span></span><span style=\"font-family:標楷體;\">公告，如於</span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">8/31(</span></span><span style=\"text-decoration:underline;\"><span style=\"font-family:標楷體;color:red;\">一</span></span><span style=\"text-decoration:underline;\"><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;color:red;\">)</span></span><span style=\"font-family:標楷體;\">前仍未完成年度健檢者將緩發三節獎金，已緩發端午獎金者將連續緩發中秋獎金，待完成健檢檢查後再行發放，最後完成期限為</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">12/31(</span><span style=\"font-family:標楷體;\">四</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">，如至</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">12/31</span><span style=\"font-family:標楷體;\">仍未完成健檢則依員工健康管理辦法之規定，健檢不再補作，已緩發之獎金亦不再補發。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">為維護您的權益，請儘速完成今年度員工健檢；單位主管煩請協助通知未完成健檢之同仁。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如您未預約檢查者，請利用電話預約</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機：</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623)</span><span style=\"font-family:標楷體;\">共四線。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;&nbsp;&nbsp; </span><span style=\"font-family:標楷體;\">已預約檢查者，請依預約時程完成健檢。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;&nbsp;&nbsp; </span><span style=\"font-family:標楷體;\">缺項未檢者，請依缺項項目之規定，前往健檢中心完成。</span></p><p><span style=\"font-family:標楷體;\">有懷孕或是其他原因無法完成</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">x-ray</span><span style=\"font-family:標楷體;\">者，請申請電子簽核表單</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">K05</span><span style=\"font-family:標楷體;\">員工健檢異動申請免檢</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">x-ray</span><span style=\"font-family:標楷體;\">項目</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp; </span></p><p><span style=\"font-family:標楷體;\">員工健檢之預約問題請電洽健檢中心</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">5620~5623</span><span style=\"font-family:標楷體;\">，共四線</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">，其他問題請電洽勞安室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4270)</span><span style=\"font-family:標楷體;\">。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;color:red;\">補發三節獎金通知文案</span></p><p><span style=\"font-family:標楷體;\">親愛的同仁</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">, </span></p><p><span style=\"font-family:標楷體;\">您好！</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\"> </span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">OOO</span><span style=\"font-family:標楷體;\">年度緩發之端午、中秋獎金，已於</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">1</span><span style=\"font-family:標楷體;\">月</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">7</span><span style=\"font-family:標楷體;\">日院長公文簽核決行，同時將補發名單給人事室及會計室。</span></p><p><span style=\"font-family:標楷體;\">非主治醫師者</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">-</span><span style=\"font-family:標楷體;\">擬於</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">105/1</span><span style=\"font-family:標楷體;\">月底發放，所得人可由薪資查詢系統查核明細。</span></p><p><span style=\"font-family:標楷體;\">主治醫師者</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">-</span><span style=\"font-family:標楷體;\">配合醫師薪資作業時補發三節獎金。</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;</span></p><p><span style=\"font-family:標楷體;\">如有對薪資補發疑問請電洽</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">(</span><span style=\"font-family:標楷體;\">主治醫師</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">會計室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">-</span><span style=\"font-family:標楷體;\">香梨專員</span><span style=\"font-family:'Times New Roman',serif;\"> </span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4389</span></p><p><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(</span><span style=\"font-family:標楷體;\">非主治醫師</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">)</span><span style=\"font-family:標楷體;\">人事室</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">-</span><span style=\"font-family:標楷體;\">癸分專員</span><span style=\"font-family:'Times New Roman',serif;\"> </span><span style=\"font-family:標楷體;\">分機</span><span lang=\"EN-US\" style=\"font-family:'Times New Roman',serif;\">4287&nbsp;&nbsp;</span></p>" }
        ];
        //#endregion

        //預設搜尋值
        $scope.viewModel.searchType = 10;
        $scope.viewModel.searchYear = new Date();
        $scope.viewModel.searchStartMonth = new Date().getMonth() + 1;
        $scope.viewModel.searchEndMonth = new Date().getMonth() + 1;
        $scope.viewModel.searchDept = "0";

        //取得部門清單
        getDeptList();
        //取得文件清單
        getFileInfo();
    }
    //#endregion

    //#region 取得部門清單
    var getDeptList = function () {
        empHealthManager.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
                    $scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得文件清單
    var getFileInfo = function () {
        empHealthManager.getFileInfo().then(
            function (success) {
                var dataSource = new kendo.data.DataSource({
                    data: success.data,
                    pageSize: 18
                });
                setTimeout(function () {
                    var grid = $("#attachmentGrid").data("kendoGrid");
                    if (grid == null) {
                        $scope.viewModel.attachmentGridOptions.dataSource = dataSource;
                    } else {
                        grid.setDataSource(dataSource);
                    }
                }, 100);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得員工健康管理清單
    var getEmpHealthManage = function () {
        var empCode, empDept;
        if ($scope.viewModel.searchEmpCode != undefined && $scope.viewModel.searchEmpCode != null && $scope.viewModel.searchEmpCode != '') {
            empCode = $scope.viewModel.searchEmpCode.replace(/[^\d]/g, '');
        } else {
            empCode = 0;
        }
        var data = {
            "healthType": $scope.viewModel.searchType,
            "healthYear": $scope.viewModel.searchYear.getFullYear(),
            "startMonth": $scope.viewModel.searchStartMonth,
            "endMonth": $scope.viewModel.searchEndMonth,
            "empNo": empCode,
            "departNo": $scope.viewModel.searchDept
        };
        empHealthManager.getEmpHealthManage(data).then(
            function (success) {
                if (success.data.length != 0) {
                    setEmployeeGridSource(success.data);
                } else {
                    setEmployeeGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setEmployeeGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 設定EmployeeGrid DataSource
    var setEmployeeGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            pageSize: 18
        });
        setTimeout(function () {
            var grid = $("#employeeGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.employeeGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 刪除資料
    var setDeleteData = function (data) {
        if (data.length == 0) {
            toaster.showErrorMessage("請勾選欲刪除的資料");
        } else {
            empHealthManager.setDeleteData(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("刪除資料完成");
                    $scope.search();
                } else {
                    toaster.showErrorMessage("對不起，刪除失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
        }
    }
    //#endregion

    //#region 新增附件物件
    var newAttachment = function (file) {
        this.name = file.name;
        this.base64 = "";
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 是否顯示日期
    $scope.isShowDate = function (Date) {
        if (Date == "2999-12-31T00:00:00+08:00") {
            return false;
        }
        return true;
    }
    //#endregion

    //#region 查詢
    $scope.search = function () {
        if ($scope.viewModel.searchType == '' || $scope.viewModel.searchType == null) {
            toaster.showErrorMessage("請選擇查詢類別");
        } else if ($scope.viewModel.searchYear == '' || $scope.viewModel.searchYear == null) {
            toaster.showErrorMessage("請選擇查詢年度");
        } else if ($scope.viewModel.searchStartMonth == '' || $scope.viewModel.searchStartMonth == null) {
            toaster.showErrorMessage("請選擇應檢開始月份");
        } else if ($scope.viewModel.searchEndMonth == '' || $scope.viewModel.searchEndMonth == null) {
            toaster.showErrorMessage("請選擇應檢結束月份");
        } else if (Number($scope.viewModel.searchStartMonth) > Number($scope.viewModel.searchEndMonth)) {
            toaster.showErrorMessage("開始月份大於結束月份，請修改");
        } else {
            getEmpHealthManage();
            $scope.viewModel.healthYear = $scope.viewModel.searchYear.getFullYear();
            $scope.viewModel.select = "false";
        }
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (check) {
        $scope.viewModel.select = check == "false" ? "false" : $scope.viewModel.select;
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            $scope.viewModel.employeeGrid.dataSource.data()[i].check = $scope.viewModel.select;
        }
    }
    //#endregion
    
    //#region 取得收信人
    $scope.getRecipient = function (noticeNo) {
        $scope.viewModel.noticeNo = noticeNo;
        $scope.viewModel.recipient = [];
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.recipient.push($scope.viewModel.employeeGrid.dataSource.data()[i].empCode.trim());
            }
        }
    }
    //#endregion

    //#region 刪除附件檔案
    $scope.deleteAttachment = function (dataItem) {
        var attachment = { "FullPath": dataItem.FullPath };
        empHealthManager.deleteAttachment(attachment).then(
            function (success) {
                getFileInfo();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 上傳附件檔案
    $scope.handleFiles = function (file) {
        file.upload = Upload.upload({
            url: "../WebApi/EmpHealthManager/FileUploadService/UploadFile",
            headers: {
                'optional-header': 'header-value'
            },
            data: file
        });
        file.upload.then(
        function (success) {
            $timeout(function () {
                getFileInfo();
            });
        },
        function (error) {
            toaster.showErrorMessage("對不起，上傳失敗");
        });
    }
    //#endregion

    //#region 發送通知信件
    $scope.sendEmail = function () {
        $scope.viewModel.noticeWindow.close();
        if ($scope.viewModel.recipient.length == 0)
        {
            return;
        }
        var attachment = [];
        for (var i = 0 ; i < $scope.viewModel.attachmentGrid.dataSource.data().length; i++)
        {
            if ($scope.viewModel.attachmentGrid.dataSource.data()[i].check == "true") {
                attachment.push($scope.viewModel.attachmentGrid.dataSource.data()[i]);
            }
        }
        var data = {
            "title":$scope.viewModel.notice[$scope.viewModel.noticeNo].title,
            "content": $scope.viewModel.notice[$scope.viewModel.noticeNo].content,
            "recipient": $scope.viewModel.recipient,
            "attachment": attachment
        };
        empHealthManager.sendNoticeEmail(data).then(
            function (success) {
                var message = "傳送成功：";
                for (var i = 0; i < success.data.success.length; i++) {
                    message += success.data.success[i] + "、";
                }
                message = message.substring(0, message.length - 1);
                message += "，共" + success.data.success.length + "人\r\n傳送失敗：";
                for (var i = 0; i < success.data.failure.length; i++) {
                    message += success.data.failure[i] + "、";
                }
                message = message.substring(0, message.length - 1);
                message += "，共" + success.data.failure.length + "人";
                confirm(message);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{ cells: [{ value: "健檢類別" }, { value: "部門" }, { value: "員工代號" }, { value: "姓名" }, { value: "身分證" }, { value: "出生日期" }, { value: "開始日期" }, { value: "結束日期" }, { value: "預約健檢日期" }, { value: "是否完成健檢" }, { value: "報告日期" }, { value: "報告結果" }, { value: "員工狀況" }, { value: "備註" }] }];
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                data.push({
                    cells: [
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].healthType },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].departName },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empCode },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empName },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].idNo },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].birthday.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].startDate.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].endDate.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].healthDate.substring(0, 10) == '2999-12-31' ? "" : $scope.viewModel.employeeGrid.dataSource.data()[i].reportDate.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].isHealth },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].reportDate.substring(0, 10) == '2999-12-31' ? "" : $scope.viewModel.employeeGrid.dataSource.data()[i].reportDate.substring(0, 10) },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].reportResult },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].empStatus },
                        { value: $scope.viewModel.employeeGrid.dataSource.data()[i].remark }
                    ]
                });
            }
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "員工健檢管理資料.xlsx"
        });
    }
    //#endregion

    //#region 刪除資料
    $scope.delete = function () {
        var data = [];
        for (var i = 0; i < $scope.viewModel.employeeGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.employeeGrid.dataSource.data()[i].check == "true") {
                data.push({ "healthYear": $scope.viewModel.healthYear, "empNo": $scope.viewModel.employeeGrid.dataSource.data()[i].empNo });
            }
        }
        setDeleteData(data);
    }
    //#endregion


    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);