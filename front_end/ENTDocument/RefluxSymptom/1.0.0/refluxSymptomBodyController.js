﻿angular.module("cmuh").controller("refluxSymptomBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 統計分數
    $scope.statisticsScore = function () {
        $scope.viewModel.document.body.statisticsScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.symptomOption.length; i++) {
            $scope.viewModel.document.body.statisticsScore.value += Number($scope.viewModel.document.body.symptomOption[i].value);
        }
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);