﻿angular.module("cmuh").controller("refluxSymptomController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5909;
        $scope.shareModel.documentTypeName = "逆流症狀指數問卷";
        $scope.shareModel.documentFolderName = "RefluxSymptom";
        $scope.shareModel.documentBodyName = "refluxSymptomBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
