﻿angular.module("cmuh").controller("voiceCapeController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5903;
        $scope.shareModel.documentTypeName = "嗓音聽覺共識評量表";
        $scope.shareModel.documentFolderName = "VoiceCape";
        $scope.shareModel.documentBodyName = "voiceCapeBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
