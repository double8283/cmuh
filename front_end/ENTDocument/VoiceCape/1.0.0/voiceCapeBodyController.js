﻿angular.module("cmuh").controller("voiceCapeBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", "Upload", "$timeout", function ($rootScope, $scope, $q, $http, cmuhBase, toaster, Upload, $timeout) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.sounder = [{ name: "MI" }, { name: "MO" }, { name: "SE" }, { name: "C" }, { name: "I" }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 上傳音訊檔
    $scope.uploadFiles = function (file) {
        file.upload = Upload.upload({
            url: "../WebApi/CancerCaseManager/FileUploadService/UploadAudio",
            headers: {
                'optional-header': 'header-value'
            },
            data: file
        });
        file.upload.then(
        function (success) {
            $timeout(function () {
                file.result = success.data;
                $scope.viewModel.document.body.audio.value = file.result.fileName;
            });
        },
        function (error) {
            toaster.showErrorMessage("對不起，上傳失敗");
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);