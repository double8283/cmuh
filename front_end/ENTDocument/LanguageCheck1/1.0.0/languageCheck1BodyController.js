﻿angular.module("cmuh").controller("languageCheck1BodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }, { code: 5 }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null && item.value.match('其他') == null ? "" : item.other;
    }
    //#endregion
    $scope.clickRadio = function (item, index) {
        debugger;
        if (item.code === index) {
            item.value = "";
            item.code = "";
        } else {
            item.code = index;
        }
    }
    $scope.clickCheckbox = function (item, value) {
        var titleList = [];
        if (cmuhBase.isNullOrEmpty(item.value) == false) {
            titleList = item.value.split("、");
        }
        var index = titleList.indexOf(value);
        if (index >= 0) {
            titleList.splice(index, 1);
        } else {
            titleList.push(value);
        }
        item.value = titleList.join("、");
    }
    $scope.isItemChecked = function (item, value) {
        if (item === undefined || cmuhBase.isNullOrEmpty(item.value) == true) {
            return;
        }
        var titleList = item.value.split("、");
        for (var title in titleList) {
            if (titleList[title] == value) {
                return true;
            }
        }
        if (item.isChecked == "false") {
            item.value = "";
        }
        return false;
    }
    $scope.appendText = function (item, value) {
        debugger;
        var titleList = [];
        var title = angular.copy(item.value);
        if (cmuhBase.isNullOrEmpty(title) == false) {
            title = title.replace(item.preText, "");
            title = title.replace(item.postText, "");
            titleList = title.split("、");
        }
        var index = titleList.indexOf(value);
        if (index >= 0) {
            titleList.splice(index, 1);
        } else {
            titleList.push(value);
        }
        if (titleList.length > 0) {
            item.value = item.preText + titleList.join("、") + item.postText;
        } else {
            item.value = "";
        }
        debugger;
    }
    $scope.isAppendChecked = function (item, value) {
        
        if (item === undefined || cmuhBase.isNullOrEmpty(item.value) == true) {
            return;
        }
        var titleList = item.value.split("、");
        for (var i in titleList) {
            var title = titleList[i];
            title = title.replace(item.preText, "");
            title = title.replace(item.postText, "");
            if (item.isChecked == "true" && title == value) {
                return true;
            } else if ((item.isChecked == "false" || cmuhBase.isNullOrEmpty(item.isChecked)) && title == value) {
                item.value = "";
            }
        }
        return false;
    }
    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);