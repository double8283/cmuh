﻿angular.module("cmuh").controller("languageCheck1Controller", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5905;
        $scope.shareModel.documentTypeName = "耳鼻喉部語言檢查表1";
        $scope.shareModel.documentFolderName = "LanguageCheck1";
        $scope.shareModel.documentBodyName = "languageCheck1Body";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
