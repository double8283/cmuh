﻿angular.module("cmuh").controller("vocalDysfunctionBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }, { code: 5 }];
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);