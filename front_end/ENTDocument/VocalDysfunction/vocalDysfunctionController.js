﻿angular.module("cmuh").controller("vocalDysfunctionController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5908;
        $scope.shareModel.documentTypeName = "聲帶功能異常評估表";
        $scope.shareModel.documentFolderName = "VocalDysfunction";
        $scope.shareModel.documentBodyName = "vocalDysfunctionBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
