﻿angular.module("cmuh").controller("voiceEvaluationBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.sounder = [{ name: "MI" }, { name: "MO" }, { name: "SE" }, { name: "C" }, { name: "I" }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);