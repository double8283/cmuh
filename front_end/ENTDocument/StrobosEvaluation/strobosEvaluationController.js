﻿angular.module("cmuh").controller("strobosEvaluationController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5911;
        $scope.shareModel.documentTypeName = "Stroboscopic Evaluation";
        $scope.shareModel.documentFolderName = "StrobosEvaluation";
        $scope.shareModel.documentBodyName = "strobosEvaluationBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
