﻿angular.module("cmuh").controller("languageCheck2BodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster","Upload","$timeout", function ($rootScope, $scope, $q, $http, cmuhBase, toaster,Upload,$timeout) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }, { code: 5 }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 上傳音訊檔
    $scope.uploadFiles = function (file) {
        file.upload = Upload.upload({
            url: "http://10.60.212.105/WebApi/CancerCaseManager/FileUploadService/UploadAudio",
            headers: {
                'optional-header': 'header-value'
            },
            data: file
        });
        file.upload.then(
        function (success) {
            $timeout(function () {
                file.result = success.data;
                $scope.viewModel.document.body.audio.value = file.result.fileName;
            }); 
        },
        function (error) {
            toaster.showErrorMessage("對不起，上傳失敗");
        });
    }
    //#endregion

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);