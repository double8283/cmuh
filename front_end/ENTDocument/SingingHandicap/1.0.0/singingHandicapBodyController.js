﻿angular.module("cmuh").controller("singingHandicapBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 統計分數
    $scope.statisticsScore = function () {
        $scope.viewModel.document.body.statisticsScore.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.questionOption.length; i++) {
            $scope.viewModel.document.body.statisticsScore.value += Number($scope.viewModel.document.body.questionOption[i].value);
            switch ($scope.viewModel.document.body.questionOption[i].value) {
                case "0":
                    $scope.viewModel.document.body.questionOption[i].value2 = "0=從未發生";
                    break;
                case "1":
                    $scope.viewModel.document.body.questionOption[i].value2 = "1=幾乎沒有";
                    break;
                case "2":
                    $scope.viewModel.document.body.questionOption[i].value2 = "2=有時候";
                    break;
                case "3":
                    $scope.viewModel.document.body.questionOption[i].value2 = "3=幾乎都是";
                    break;
                case "4":
                    $scope.viewModel.document.body.questionOption[i].value2 = "4=總是如此";
                    break;
            }
        }
    }
    //#endregion

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);