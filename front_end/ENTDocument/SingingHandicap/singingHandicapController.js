﻿angular.module("cmuh").controller("singingHandicapController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5910;
        $scope.shareModel.documentTypeName = "歌唱障礙指數量表";
        $scope.shareModel.documentFolderName = "SingingHandicap";
        $scope.shareModel.documentBodyName = "singingHandicapBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
