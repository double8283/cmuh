﻿angular.module("cmuh").controller("swallowingEvaluationController", ["$scope", "cmuhBase", "MainDocumnt", function ($scope, cmuhBase, MainDocumnt) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.shareModel.branchNo = 0;
        $scope.shareModel.documentType = 5904;
        $scope.shareModel.documentTypeName = "耳鼻喉部語言治療吞嚥評估表";
        $scope.shareModel.documentFolderName = "SwallowingEvaluation";
        $scope.shareModel.documentBodyName = "swallowingEvaluationBody";
        $scope.shareModel.versionId = "1.0.0";
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
