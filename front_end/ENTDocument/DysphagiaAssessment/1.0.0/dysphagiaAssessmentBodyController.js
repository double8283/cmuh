﻿angular.module("cmuh").controller("dysphagiaAssessmentBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }, { code: 5 }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 組合checkbox文字
    $scope.combinationText = function (item) {
        var array = [];
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                array.push(item.option[i].title);
            }
        }
        item.value = array.join("、");
        item.value += item.value.match('other') == null && item.value.match('Other') == null ? "" : item.other;
    }
    //#endregion

    //#region 組合PO checkbox文字
    $scope.combinationPOText = function (item) {
        item.value = "";
        for (var i = 0; i < item.option.length; i++) {
            if (item.option[i].value == "true") {
                item.value += item.option[i].title;
                item.value += i == 0 ? item.modifiedDiet : "";
                item.value += i == 3 ? item.positioning : "";
                item.value += i == 4 ? item.swallowing : "";
                item.value += "、";
            }
        }
        item.value = item.value.substring(0, item.value.length - 1);
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);