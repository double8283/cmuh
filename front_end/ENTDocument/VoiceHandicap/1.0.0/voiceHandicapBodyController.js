﻿angular.module("cmuh").controller("voiceHandicapBodyController", ["$rootScope", "$scope", "$q", "$http", "cmuhBase", "toaster", function ($rootScope, $scope, $q, $http, cmuhBase, toaster) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.score = [{ code: 0 }, { code: 1 }, { code: 2 }, { code: 3 }, { code: 4 }];
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 功能性分數
    $scope.statisticsScore1 = function () {
        $scope.viewModel.document.body.statisticsScore1.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.functionalOption.length; i++) {
            $scope.viewModel.document.body.statisticsScore1.value += Number($scope.viewModel.document.body.functionalOption[i].value);
        }
        $scope.viewModel.document.body.statisticsScore.value = $scope.viewModel.document.body.statisticsScore1.value + $scope.viewModel.document.body.statisticsScore2.value + $scope.viewModel.document.body.statisticsScore3.value;
    }
    //#endregion

    //#region 生理性分數
    $scope.statisticsScore2 = function () {
        $scope.viewModel.document.body.statisticsScore2.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.physicalOption.length; i++) {
            $scope.viewModel.document.body.statisticsScore2.value += Number($scope.viewModel.document.body.physicalOption[i].value);
        }
        $scope.viewModel.document.body.statisticsScore.value = $scope.viewModel.document.body.statisticsScore1.value + $scope.viewModel.document.body.statisticsScore2.value + $scope.viewModel.document.body.statisticsScore3.value;
    }
    //#endregion

    //#region 情緒性分數
    $scope.statisticsScore3 = function () {
        $scope.viewModel.document.body.statisticsScore3.value = 0;
        for (var i = 0; i < $scope.viewModel.document.body.emotionalOption.length; i++) {
            $scope.viewModel.document.body.statisticsScore3.value += Number($scope.viewModel.document.body.emotionalOption[i].value);
        }
        $scope.viewModel.document.body.statisticsScore.value = $scope.viewModel.document.body.statisticsScore1.value + $scope.viewModel.document.body.statisticsScore2.value + $scope.viewModel.document.body.statisticsScore3.value;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);