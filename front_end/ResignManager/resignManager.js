﻿angular.module('cmuh').service('resignManager', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得違約資訊
    this.getViolationMsg = function (applyInfo) {
        return $http.put("../WebApi/HumanResource/ResignManagerService/GetViolationMsg", applyInfo);
    }
    //#endregion

    //#region 取得離職清單
    this.getResignList = function (searchRequest) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/ResignManagerService/GetResignList/{0}", searchRequest.searchType);
        return $http.put(url, searchRequest);
    }
    //#endregion

    //#region 取得員工基本資料
    this.getEmpInfo = function (empCode) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/ResignManagerService/GetEmpInfo/{0}", empCode);
        return $http.get(url);
    }
    //#endregion

    //#region 取得表單資料
    this.getRecordMt = function (signoffNo, userNo) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetRecordMt/{0}/{1}", signoffNo, userNo);
        return $http.get(url);
    }
    //#endregion

    //#region 離職撤銷
    this.deleteRecordMt = function (signoffNo) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/DeleteRecordMt/{0}", signoffNo);
        return $http.get(url);
    }
    //#endregion

    //#region 儲存附件資料
    this.setAttachments = function (signoffNo,data) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/SetAttachments/{0}", signoffNo);
        return $http.put(url, data);
    }
    //#endregion
}]);