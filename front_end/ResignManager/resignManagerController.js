﻿angular.module("cmuh").controller('resignManagerController', ["$scope", "$sce", "toaster", "cmuhBase", "resignManager", function ($scope, $sce, toaster, cmuhBase, resignManager) {

    //#region 方法

    //#region 設定資料結構
    //@params {string: pageModel}
    var initViewModel = function (pageModel) {
        // HTML 畫面 與 service 的繫結
        $scope.viewModel = pageModel;
        
        if (!!$scope.viewModel.signoffNo) {
            getRecordMt($scope.viewModel.signoffNo);
        } else {
            $scope.viewModel.recordMt = new newResignForm();
            $scope.viewModel.content = new newResignContent();
            $scope.changeMinDate();
            if ($scope.executingAppStore.appId == 400) {
                $scope.getEmpInfo($scope.shareModel.userInfos.userNo);
            }
        }

        if ($scope.viewModel.isInitResignViewModel === true) {
            return;
        }
        kendo.culture("zh-TW");

        $scope.viewModel.precautions = "離職單已提送簽，每經各層級主管核准後，均會自動傳送訊息至您的院外信箱，請收悉追蹤，以利了解簽核進度；並儘早準備辦理相關工作交接與財產移轉，俾離院手續順利完成。";

        //查詢變數
        $scope.viewModel.searchRequest = {
            searchType: "ResignDate",
            empNo: "",
            startDate: new Date(),
            endDate: new Date()
        }

        //定義setResignListGridOptions
        //setResignListGridOptions();

        // 文件動態切換樣式
        $scope.viewModel.docViewCSS = '';

        $scope.viewModel.resignReasons = [
            { value: "", title: "家庭因素" },
            { value: "", title: "另有生涯規劃" },
            { value: "", title: "志趣不合" },
            { value: "", title: "升學進修" },
            { value: "", title: "發展受限" },
            { value: "", title: "學習受限" },
            { value: "", title: "與主管不合" },
            { value: "", title: "與同儕不合" },
            { value: "", title: "無法適應" },
            { value: "", title: "健康因素" },
            { value: "", title: "工作壓力過大" },
            { value: "", title: "薪資無法滿足" },
            { value: "", title: "定期契約截止" },
            { value: "", title: "受訓截止" },
            { value: "", title: "其他：", other: "" }
        ];
        //$scope.viewModel.resignReasons = ["家庭因素", "另謀他職", "不適應", "健康因素", "考試進修", "定期契約", "退休離職", "其他"];

        //SVG相關-畫布起點X, 起點Y, 終點X, 終點Y, 寬度, 高度, 繪圖狀態
        $scope.viewModel.start_X = 0;
        $scope.viewModel.start_Y = 0;
        $scope.viewModel.end_X = 0;
        $scope.viewModel.end_Y = 0;
        $scope.viewModel.width = 0;
        $scope.viewModel.height = 0;
        $scope.viewModel.drawing = false;

        //高度
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }

        $scope.viewModel.templateUrl = cmuhBase.stringFormat("App/ResignManager/resignApply.html?{0}", cmuhBase.getVersionNo());
        $scope.viewModel.isInitResignViewModel = true;
    }
    //#endregion

    //#region 新增離職表單
    var newResignForm = function () {
        this.formInfo = { formNo: 1 };
        this.applyUser = { empNo: $scope.shareModel.userInfos.userNo };
        this.applyTime = new Date();
        this.modifiedDate = new Date();
        this.applyStatus = { optionNo: "10" };
        this.versionNo = "1.0.0";
        this.templateUrl = cmuhBase.stringFormat("App/ResignManager/resignApply.html?{0}", cmuhBase.getVersionNo());
        this.content = "";
        this.attachments = []
    }
    //#endregion
    var newResignContent = function () {
        this.applyInfo = {
            resignDate: "",
            resignReason: "",
            suggestion: "",
            isAgree: false,
            violationMsg: "",
            email: "",
            tel: ""
        };
        this.questionnaire = {
            question: [
                { no: "1", value: "", title: "您對醫院的形象與發展感到：" },
                { no: "2", value: "", title: "您對醫院的薪資制度感到：" },
                { no: "3", value: "", title: "您對醫院的績效獎金制度感到：" },
                { no: "4", value: "", title: "您對醫院的陞遷制度感到：" },
                { no: "5", value: "", title: "您對醫院整體福利制度感到：" },
                { no: "6", value: "", title: "您對醫院的管理方式感到：" },
                { no: "7", value: "", title: "您對醫院的工作環境感到：" },
                { no: "8", value: "", title: "您對醫院主管領導風格感到：" },
                { no: "9", value: "", title: "您對同事間的相處氣氛感到：" },
                { no: "10", value: "", title: "您對醫院工作量，工作壓力感到：" },
                { no: "11", value: "", title: "您對工作地方與居家地點感到：" },
                { no: "12", value: "", title: "您對指派工作與個人專業感到：" },
                { no: "13", value: "", title: "您對指派工作困難度與挑戰性感到：" },
                { no: "14", value: "", title: "您對醫院提供在職訓練課程感到：" },
                { no: "15", value: "", title: "您對醫院鼓勵自我教育與成長感到：" }
            ],
            suggest: ""
        };
    }
    //#region 定義setResignListGridOptions
    var setResignListGridOptions = function () {
        $scope.viewModel.select = "false";
        $scope.viewModel.resignListGridOptions = {
            rowTemplate: kendo.template($("#resignListGridRowTemplate").html()),
            selectable: 'row',
            scrollable: false,
            sortable: true,
            columns: [
                { field: "", title: "刪除", width: "50px" },
                { field: "applyUser.empName", title: "申請人員", width: "80px" },
                { field: "applyTime", title: "申請日期", width: "120px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd'}}" },
                { field: "resignDate", title: "預離日期", width: "120px", template: "{{dataItem.resignDate | date:'yyyy-MM-dd'}}" },
                { field: "applyStatus.optionName", title: "簽核狀態", width: "100px" },
                { field: "signoffState", title: "簽核流程", template: "<div data-ng-bind-html=\"trustAsHtml(dataItem.signoffState)\"></div>" },
                { field: "resignState", title: "離院流程", template: "<div data-ng-bind-html=\"trustAsHtml(dataItem.resignState)\"></div>" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                var basicInfo = cmuhBase.stringFormat(
                                "表單編號：{0}　申請人：{1}（{2}）　職稱：{3}　部門：{4}　到職日期：{5}　申請日期：{6}",
                                dataItem.signoffNo,
                                dataItem.applyUser.empName,
                                dataItem.applyUser.empCode,
                                dataItem.applyUser.titleName,
                                dataItem.applyUser.departName,
                                dataItem.applyUser.hireDate.substring(0, 10),
                                dataItem.applyTime.substring(0,10));
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
            }
        };
    }
    //#endregion

    //#region 離職管理清單 dbClick
    //$scope.$on("kendoWidgetCreated", function (event, widget) {
    //    if (widget === $scope.viewModel.resignListGrid) {
    //        $scope.viewModel.resignListGrid.element.on('dblclick', function (e) {
    //            debugger;
    //            $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
    //            getRecordMt(dataItem.signoffNo);
    //        });
    //    }
    //});
    //#endregion

    //#region 離職管理清單 dbClick
    $("#resignListGrid").on("dblclick", " tbody > tr", function () {
        $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
        var grid = $("#resignListGrid").data("kendoGrid");
        getRecordMt(grid.dataItem($(this)).signoffNo);
    });
    //#endregion

    //#region 設定ResignListGrid DataSource
    var setResignListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.resignListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#resignListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.resignListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定AttachmentGrid DataSource
    var setAttachmentGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.attachmentGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#attachmentGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.attachmentGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得離職資料
    var getRecordMt = function (signoffNo) {
        resignManager.getRecordMt(signoffNo, $scope.shareModel.userInfos.userNo).then(
            function (success) {
                $scope.viewModel.recordMt = success.data;
                $scope.viewModel.content = JSON.parse($scope.viewModel.recordMt.content);
                $scope.viewModel.recordMt.content = JSON.parse($scope.viewModel.recordMt.content);
                if (!!$scope.viewModel.content.applyInfo.resignReason == true) {
                    for (var i = 0; i < $scope.viewModel.resignReasons.length; i++) {
                        $scope.viewModel.resignReasons[i].value = "false";
                        $scope.viewModel.resignReasons[14].other = "";
                        if ($scope.viewModel.content.applyInfo.resignReason.indexOf($scope.viewModel.resignReasons[i].title) > -1) {
                            $scope.viewModel.resignReasons[i].value = "true";
                        }
                    }
                }
                if ($scope.viewModel.resignReasons[14].value == "true") {
                    $scope.viewModel.resignReasons[14].other = $scope.viewModel.content.applyInfo.resignReason.split("：")[1];
                }
                $scope.viewModel.content.applyInfo.isAgree = angular.fromJson($scope.viewModel.content.applyInfo.isAgree);
                setAttachmentGridSource($scope.viewModel.recordMt.attachments);
                $scope.getViolationMsg($scope.viewModel.content.applyInfo.resignDate);

                if ($scope.viewModel.recordMt.formInfo.formNo == 1 && (!!$scope.viewModel.signOffItem == true ? $scope.viewModel.signOffItem.seqNo > '07' : false)) {
                    $scope.viewModel.hideData = true;
                } else {
                    $scope.viewModel.hideData = false;
                }
                //var basicInfo = cmuhBase.stringFormat(
                //                "表單編號：{0}　申請人：{1}",
                //                success.data.signoffNo,
                //                success.data.applyUser.empName);
                //$scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion
    
    //#region 儲存附件資料
    var setAttachments = function () {
        resignManager.setAttachments($scope.viewModel.recordMt.signoffNo, $scope.viewModel.attachmentGrid.dataSource.data()).then(
            function (success) {
                if (success.data == "true" || success.data == true) {
                    toaster.showSuccessMessage("恭喜您，存檔成功");

                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#endregion

    //#region 事件
    
    //#region 載入離職管理畫面時執行
    $scope.initResignManager = function () {
        setResignListGridOptions();
        var tabStrip = $("#tabstrip").kendoTabStrip().data("kendoTabStrip");
        tabStrip.disable(tabStrip.tabGroup.children().eq(1));
    }
    //#endregion

    //#region 載入離職申請畫面時執行
    $scope.initResignApply = function () {
        //附件Grid
        $scope.viewModel.attachmentGridOptions = {
            scrollable: false,
            columns: [
                { field: "", title: "刪除", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteAttachment(dataItem)\" /></center>" },
                { field: "", title: "下載", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/wtRl5iU.jpg\" style=\"width:20px;height:20px;\" data-ng-click=\"downloadAttachment(dataItem)\" /></center>" },
                { field: "attachName", title: "附件名稱" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion
    
    //#region 離職撤銷
    $scope.deleteResign = function (dateItem) {
        if (confirm("確認刪除嗎？") != true) {
            return;
        }
        resignManager.deleteRecordMt(dateItem.signoffNo).then(
            function (success) {
                if (success.data == "true" || success.data == true) {
                    toaster.showSuccessMessage("恭喜您，刪除成功");
                    $scope.search();
                } else {
                    toaster.showErrorMessage("對不起，刪除失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 改變最小日期時執行
    $scope.changeMinDate = function () {
        $scope.viewModel.minDate = angular.copy($scope.viewModel.recordMt.applyTime);
        $scope.viewModel.minDate.setMonth($scope.viewModel.minDate.getMonth() - 3, $scope.viewModel.minDate.getDate());
        var datepicker = $("#resignDate").data("kendoDatePicker");
        if (datepicker != null) {
            datepicker.setOptions({
                min: $scope.viewModel.minDate,
                max: $scope.viewModel.recordMt.applyUser.endDate
            });
        }
    }
    //#endregion

    //#region 取得違約資訊
    $scope.getViolationMsg = function (date) {
        var applyInfo = {
            applyUser: $scope.viewModel.recordMt.applyUser.empNo,
            applyTime: $scope.viewModel.recordMt.applyTime,
            resignDate: $scope.viewModel.content.applyInfo.modifiedDate
            //resignDate: date
        };
        resignManager.getViolationMsg(applyInfo).then(
            function (success) {
                $scope.viewModel.content.applyInfo.violationMsg = angular.fromJson(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得員工基本資料
    $scope.getEmpInfo = function (empNo) {
        if (Number.isInteger(empNo) == false) {
            empNo = empNo.replace(/[^\d]/g, '');
        }
        resignManager.getEmpInfo(empNo).then(
            function (success) {
                if (success.data != "null") {
                    $scope.viewModel.recordMt.applyUser = success.data;
                    $scope.viewModel.content.applyInfo.departNo = success.data.departInfo.optionNo;
                    $scope.viewModel.content.applyInfo.gradeCode = success.data.gradeCode;
                    $scope.viewModel.content.applyInfo.empCode = success.data.empCode;
                    $scope.viewModel.content.applyInfo.jobRegister = success.data.jobRegister;
                    $scope.viewModel.content.applyInfo.isTeach = success.data.isTeach;
                    $scope.viewModel.content.applyInfo.titleName = success.data.titleInfo.optionName;
                    $scope.viewModel.content.applyInfo.titleNo = success.data.titleInfo.optionNo;
                    var basicInfo = cmuhBase.stringFormat(
                                "申請人：{0}（{1}）　職稱：{2}　部門：{3}　到職日期：{4}　申請日期：{5}",
                                success.data.empName,
                                success.data.empCode,
                                success.data.titleInfo.optionName,
                                success.data.departInfo.optionName,
                                success.data.hireDate.substring(0, 10),
                                cmuhBase.getDateFormat($scope.viewModel.recordMt.applyTime));
                    $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
                }
                var datepicker = $("#resignDate").data("kendoDatePicker");
                if (datepicker != null) {
                    datepicker.setOptions({
                        min: $scope.viewModel.minDate,
                        max: $scope.viewModel.recordMt.applyUser.endDate
                    });
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 檢查必填欄位
    $scope.viewModel.checkRequire = function () {
        if ($("#myform").kendoValidator().data("kendoValidator").validate() == false) {
            return true;
        }
        if ($scope.viewModel.content.applyInfo.resignReason == -1) {
            return true;
        }
        if ($scope.viewModel.content.applyInfo.isAgree == false) {
            return true;
        }
        if ($scope.viewModel.content.applyInfo.tel.length < 10) {
            return true;
        }
        if ($scope.questionIfOk() == true) {
            return true;
        }
        return false;
    }
    //#endregion

    //#region 查詢
    $scope.search = function () {
        var data = {
            searchType: $scope.viewModel.searchRequest.searchType,
            empNo: $scope.viewModel.searchRequest.empNo.replace(/[a-zA-Z]/g, ""),
            startDate: $scope.viewModel.searchRequest.startDate,
            endDate: $scope.viewModel.searchRequest.endDate
        };
        resignManager.getResignList(data).then(
            function (success) {
                $scope.viewModel.resignList = success.data;
                setResignListGridSource($scope.viewModel.resignList);
                if (success.data.length == 0) {
                    toaster.showWarningMessage("對不起，沒有資料");
                }
                $scope.viewModel.finishedSign = "true";
                $scope.viewModel.unfinishedSign = "true";
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{ cells: [{ value: "申請人員" }, { value: "申請日期" }, { value: "預離日期" }, { value: "簽核流程" }] }];
        for (var i = 0; i < $scope.viewModel.resignListGrid.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.resignListGrid.dataSource.data()[i].applyUser.empName },
                    { value: $scope.viewModel.resignListGrid.dataSource.data()[i].applyTime.substring(0,10) },
                    { value: $scope.viewModel.resignListGrid.dataSource.data()[i].resignDate.substring(0, 10) },
                    { value: $scope.viewModel.resignListGrid.dataSource.data()[i].signoffState.replace(/(<([^>]+)>)/ig, "") },
                    { value: $scope.viewModel.resignListGrid.dataSource.data()[i].applyStatus.optionName }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "離職清單.xlsx"
        });
    }
    //#endregion

    //#region 設定高度
    $scope.setResignManagerHeight = function () {
        document.getElementById("resignListGrid").style.height = window.innerHeight - 340 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 255 + "px";
        }
    }
    //#endregion

    //#region 設定高度
    //$scope.setMyformHeight = function () {
    //    document.getElementById("myform").style.height = window.innerHeight - 265 + "px";
    //}
    //#endregion

    //#region 判斷電子信箱是否為院內信箱
    $scope.emailIfOk = function () {
        if ($scope.viewModel.content.applyInfo.email == undefined) {
            $scope.viewModel.content.applyInfo.email = "";
            $scope.viewModel.msgWindow.center().open();
            $scope.viewModel.message = "請輸入正確的電子信箱。";
            return;
        }
        if ($scope.viewModel.content.applyInfo.email.indexOf("@mail.cmuh.org.tw") != -1) {
            $scope.viewModel.content.applyInfo.email = "";
            $scope.viewModel.msgWindow.center().open();
            $scope.viewModel.message = "請使用院外信箱，若無者，請申請一個新帳號，以利離院後仍可接獲通知，感謝您。";
        }
    }
    //#endregion

    //#region 判斷手機號碼是否為10碼手機
    $scope.telIfOk = function () {
        if ($scope.viewModel.content.applyInfo.tel.length < 10) {
            $scope.viewModel.msgWindow.center().open();
            $scope.viewModel.message = "請輸入正確的手機號碼。";
        }
    }
    //#endregion

    //#region 判斷手機號碼是否為10碼手機
    $scope.telChange = function () {
        $scope.viewModel.content.applyInfo.tel = $scope.viewModel.content.applyInfo.tel.replace(/[^\d]/g, '');
    }
    //#endregion

    //#region 判斷問卷是否填完
    $scope.questionIfOk = function () {
        if ($scope.viewModel.content == undefined) {
            return;
        }
        for (var i = 0; i < $scope.viewModel.content.questionnaire.question.length; i++) {
            if ($scope.viewModel.content.questionnaire.question[i].value == "") {
                return true;
            }
        }
        return false;
    }
    //#endregion

    //#region 組合checkbox文字
    $scope.combinationText = function () {
        var array = [];
        for (var i = 0; i < $scope.viewModel.resignReasons.length; i++) {
            if ($scope.viewModel.resignReasons[i].value == "true") {
                array.push($scope.viewModel.resignReasons[i].title);
            }
        }
        $scope.viewModel.content.applyInfo.resignReason = array.join("、");
        $scope.viewModel.content.applyInfo.resignReason += $scope.viewModel.content.applyInfo.resignReason.match('其他') == null ? "" :  $scope.viewModel.resignReasons[12].other;
    }
    //#endregion

    //#region 要用data-ng-bind-html顯示的字串都要先執行此事件
    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };
    //#endregion

    //#region 篩選完簽與未完簽
    $scope.filterGrid = function () {
        var newsList = angular.copy($scope.viewModel.resignList);
        var filterNews = newsList.filter(function (item) {
            return ($scope.viewModel.finishedSign == "true" && item.applyStatus.optionNo >= 30) ||
                ($scope.viewModel.unfinishedSign == "true" && item.applyStatus.optionNo == 20) ? true : false;
        });
        setResignListGridSource(filterNews);
    }
    //#endregion

    //#region 滑鼠在畫布上按下
    $('#svg').mousedown(function (e) {
        $scope.viewModel.start_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
        $scope.viewModel.start_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
        $scope.viewModel.end_X = $scope.viewModel.start_X;
        $scope.viewModel.end_Y = $scope.viewModel.start_Y;
        $scope.viewModel.drawing = true;
        document.getElementById("svg").innerHTML += "start";
        document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:rgb(0,0,0);stroke-width:3' />";
    });
    //#endregion

    //#region 滑鼠在畫布上移動
    $('#svg').mousemove(function (e) {
        if ($scope.viewModel.drawing) {
            $scope.viewModel.start_X = $scope.viewModel.end_X;
            $scope.viewModel.start_Y = $scope.viewModel.end_Y;
            $scope.viewModel.end_X = e.pageX - document.getElementById("svg").getBoundingClientRect().left;
            $scope.viewModel.end_Y = e.pageY - document.getElementById("svg").getBoundingClientRect().top;
            document.getElementById("svg").innerHTML += "<line x1='" + $scope.viewModel.start_X + "' y1='" + $scope.viewModel.start_Y + "' x2='" + $scope.viewModel.end_X + "' y2='" + $scope.viewModel.end_Y + "' style='stroke:rgb(0,0,0);stroke-width:3' />";
        }
    });
    //#endregion

    //#region 滑鼠在畫布上放開
    $('#svg').mouseup(function (e) {
        $scope.viewModel.drawing = false;
    });
    //#endregion
    
    //#region 清空SVG
    $scope.clearSVG = function () {
        document.getElementById("svg").innerHTML = "";
        var canvas = document.getElementById('art');
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
    //#endregion

    //#region 儲存SVG
    $scope.saveSVG = function () {
        var svgString = new XMLSerializer().serializeToString(document.querySelector('svg'));
        var canvas = document.getElementById("canvas");
        var ctx = canvas.getContext("2d");
        var DOMURL = self.URL || self.webkitURL || self;
        var img = new Image();
        var svg = new Blob([svgString], { type: "image/svg+xml;charset=utf-8" });
        var url = DOMURL.createObjectURL(svg);
        img.onload = function () {
            ctx.drawImage(img, -1, -1);
            $scope.viewModel.recordMt.content.signSrc = canvas.toDataURL("image/png");
            $scope.viewModel.recordMt.content.signDate = cmuhBase.getDateFormat(new Date());
            //$scope.viewModel.newAttachment = {
            //    attachName: cmuhBase.getDateFormat(new Date()) + " - 領取簽收",
            //    base64: canvas.toDataURL("image/png"),
            //    systemUser: $scope.shareModel.userInfos.userNo,
            //    entityState: "Added"
            //};
            //$scope.viewModel.attachmentGrid.dataSource.insert($scope.viewModel.newAttachment);
            //setAttachments();
        };
        img.src = url;
    }
    //#endregion

    //#region 預離日期Keyup
    $scope.resignDateKeyup = function () {
        var datepicker = $("#resignDate").data("kendoDatePicker");
        datepicker.value(null);
        toaster.showErrorMessage("對不起，預離日期禁止手動輸入，請使用點選方式");
    }
    //#endregion

    //#region 調整日期鎖定
    $scope.modifiedDateDisabled = function () {
        if (!!$scope.viewModel.signOffItem == false) {
            return true;
        }
        if (!!$scope.viewModel.recordMt == false) {
            return true;
        }
        if ($scope.viewModel.recordMt.applyUser.empNo == $scope.shareModel.userInfos.userNo) {
            return true;
        }
        if ($scope.viewModel.signOffItem.seqNo < '07') {
            return false;
        }
        return true;
    }
    //#endregion
    
    //#region 列印
    $scope.print = function () {
        var printSection = document.getElementById("printSection");
        if (!printSection) {
            var printSection = document.createElement("div");
            printSection.id = "printSection";
            document.body.appendChild(printSection);
        }
        var domClone = document.getElementById("printDiv").cloneNode(true);
        printSection.appendChild(domClone);
        window.print();
        printSection.removeChild(domClone);
    }

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);