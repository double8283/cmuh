﻿angular.module("cmuh").service("cancerCaseManager", ["$http", "$q", "cmuhBase", function ($http, $q, cmuhBase) {

    //#region 取得病人看診紀錄
    this.getCaseViewList = function (caseNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetCaseViewList/{0}", caseNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得 Stroboscopic Evaluation 影片
    this.getVideoPaths = function (decumentNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetVideoPaths/{0}", decumentNo);
        return $http.get(url);
    }
    //#endregion
    
    //#region 取得病人復健門診紀錄
    this.getVisitRecords = function (data) {
        return $http.put("../WebApi/CancerCaseManager/CancerCaseManagerService/GetVisitRecords", data);
    }
    //#endregion

    //#region 取得病人復健治療紀錄
    this.getMedOrders = function (data) {
        return $http.put("../WebApi/CancerCaseManager/CancerCaseManagerService/GetMedOrders", data);
    }
    //#endregion

    //#region 取得治療紀錄
    this.getCancerBasicInfo = function (chartNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetCancerBasicInfo/{0}", chartNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得EmpName
    this.getEmpName = function (empNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetEmpName/{0}", empNo);
        return $http.get(url);
    }
    //#endregion

    //#region 修改表單
    this.setDocumentInfo = function (document) {
        var doc = {};
        angular.copy(document, doc);
        //存檔前將body轉成JSON格式的字串
        doc.body = JSON.stringify(doc.body);
        var url = "../WebApi/CancerCaseManager/CancerCaseManagerService/SetDocumentInfo";
        return $http.put(url, doc);
    }
    //#endregion

    //#region 取得左邊文件清單
    this.getDocumentList = function (idNo, documentType, caseType) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetDocumentList/{0}/{1}/{2}", idNo, documentType, caseType)
        return $http.get(url);
    }
    //#endregion

    //#region 取得文件資料
    this.getDocumentInfo = function (documentNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetDocumentInfo/{0}", documentNo)
        return $http.get(url);
    }
    //#endregion

    //#region 取得會議記錄
    this.getCaseAttaches = function (caseNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetCaseAttaches/{0}", caseNo)
        return $http.get(url);
    }
    //#endregion

    //#region 會議記錄存檔
    this.setCaseAttaches = function (data) {
        return $http.put("../WebApi/CancerCaseManager/CancerCaseManagerService/SetCaseAttaches", data);
    }
    //#endregion
}]);