﻿angular.module("cmuh").controller("cancerCaseManagerController", ["$scope", "$http", "$timeout", "toaster", "cmuhBase", "cancerCaseManager", "$filter", "$sce", function ($scope, $http, $timeout, toaster, cmuhBase, cancerCaseManager, $filter, $sce) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        $scope.viewModel = pageModel;

        //如果沒抓到病人資料就回到病人選取頁面
        if ($scope.appModel.patientInfos == undefined) {
            toaster.showErrorMessage("請先選擇病人");
            $scope.shareModel.navigation(75, 151);
        }

        //設置kendo的語言
        kendo.culture("zh-TW");

        //查詢變數
        $scope.viewModel.searchRequest = {
            chartNo: $scope.appModel.patientInfos.chartNo,
            divNo: 390,
            period: {
                type: "M",
                value: "-6"
            }
        }
        $scope.viewModel.period = 1;

        //預設治療查詢日期為今天
        //$scope.viewModel.cureDate1 = new Date();
        //$scope.viewModel.cureDate2 = new Date();

        //治療清單Grid
        $scope.viewModel.cureListGridOptions = {
            scrollable: false,
            sortable: true,
            dataSource: {
                data: new kendo.data.DataSource(),
                group: { field: "item", aggregates: [{ field: "item", aggregate: "count" }] }
            },
            columns: [
             { field: "date", title: "治療日期", template: "{{dataItem.date | date:'yyyy-MM-dd'}}" },
             { field: "item", title: "治療項目", groupHeaderTemplate: "#= value # 共 #= count # 筆" },
             { field: "medCode", title: "醫令代碼" }
            ]
        };

        //團隊會議Grid
        $scope.viewModel.teamMeetingGridOptions = {
            rowTemplate: kendo.template($("#teamMeetingGridRowTemplate").html()),
            sortable: true,
            dataSource: new kendo.data.DataSource(),
            columns: [
                { field: "", title: "刪除", width: "50px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteAttachment(dataItem)\" /></center>" },
                { field: "", title: "下載", width: "50px", template: "<center><img data-ng-src=\"http://i.imgur.com/wtRl5iU.jpg\" style=\"width:20px;height:20px;\" data-ng-click=\"downloadAttachment(dataItem)\" /></center>" },
                { field: "attachName", title: "多專科會議名稱" }
                //{ field: "date", title: "會議日期", width: "150px", template: "<input kendo-date-picker data-k-ng-model=\"dataItem.date\" data-k-format=\"'yyyy-MM-dd'\" style=\"width:130px\" />" },
                //{ field: "user", title: "主持人", width: "100px", template: "<input type=\"text\" class=\"k-textbox\" data-ng-model=\"dataItem.user\" style=\"width: 80px\" />" },
                //{ field: "ifScan", title: "是否為掃描檔", width: "120px", template: "<input type=\"text\" class=\"k-textbox\" data-ng-model=\"dataItem.ifScan\" style=\"width: 50px\" />" }
            ]
        };

        //預設表單內容為空
        $scope.viewModel.templateUrl = null;

        //取得病人填表紀錄
        getCaseViewList();
        //取得治療紀錄
        getCancerBasicInfo();
        //取得個案管理紀錄
        $scope.viewModel.document1 = undefined;
        getCaseManagement();
        //取得追蹤紀錄
        $scope.viewModel.document2 = undefined;
        getTrackRecord();
        //取得團隊會議
        getCaseAttaches();
    }
    //#endregion
    
    //#region 取得病人填表紀錄
    var getCaseViewList = function () {
        $scope.viewModel.caseViewList = [];
        $scope.viewModel.caseViewList2 = [];
        $scope.viewModel.caseViewList3 = [];
        cancerCaseManager.getCaseViewList($scope.appModel.patientInfos.caseNo).then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.caseViewList = success.data;
                } else {
                    $scope.viewModel.caseViewList = [];
                }
            },
            function (error) {
                $scope.viewModel.caseViewList = [];
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得治療紀錄
    var getCancerBasicInfo = function () {
        cancerCaseManager.getCancerBasicInfo($scope.appModel.patientInfos.chartNo).then(
            function (success) {
                $scope.viewModel.cancerBasicInfo = success.data;
                for (var i = 0; i < $scope.viewModel.cancerBasicInfo.length; i++) {
                    $scope.viewModel.cancerBasicInfo[i].backgroundColor = "#ACD5E4";
                }
                $scope.viewModel.cancerBasicDetail = "";
            },
            function (error) {
                $scope.viewModel.caseViewList = [];
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得個案管理
    var getCaseManagement = function () {
        getDocumentList(3552);
        if ($scope.viewModel.document1 == undefined) {
            $scope.viewModel.document1 = {
                branchNo: "0",
                createTime: new Date(),
                createUser: { empNo: $scope.shareModel.userInfos.userNo },
                documentNo: "0",
                documentType: "3552",
                templateUrl: "",
                tranStatus: "10",
                versionId: "1.0.0",
                caseNo: $scope.appModel.patientInfos.caseNo,
                body: {
                    chartNo: $scope.appModel.patientInfos.chartNo,
                    caseNo: $scope.appModel.patientInfos.caseNo,
                    isFirstVisit: $scope.appModel.patientInfos.isFirstVisit,
                    ptName: $scope.appModel.patientInfos.ptName,
                    sex: $scope.appModel.patientInfos.sex,
                    birthday: $scope.appModel.patientInfos.birthday,
                    phone1: $scope.appModel.patientInfos.telPhone,
                    phone2: "",
                    cellPhone: $scope.appModel.patientInfos.mobile,
                    contactPerson: "",
                    contactPersonPhone: "",
                    contactPersonCellPhone: "",
                    address: cmuhBase.stringFormat("{0}{1}", $scope.appModel.patientInfos.zipName, $scope.appModel.patientInfos.streetAddress),
                    family: "",
                    caseType: "",
                    caseSource: "",
                    startDate: $scope.appModel.patientInfos.startDate,
                    caseManagerUser: { empNo: "", empName: "" },
                    surgeonUser: { empNo: "", empName: "" },
                    chemotherapyUser: { empNo: "", empName: "" },
                    radiotherapyUser: { empNo: "", empName: "" },
                    radiationTherapy: { value: "", preoperative: "", prePTD: "", prePTDF: "", preRL: "", preRLF: "", preOther: "", preOther2: "", preOtherF: "", postoperative: "", postPTD: "", postPTDF: "", postRL: "", postRLF: "", postOther: "", postOther2: "", postOtherF: "" },
                    chemoTherapy: { value: "", preoperative: "", preR: "", preO: "", postoperative: "", postR: "", postO: "" },
                    targetDrugTherapy: { value: "", erbitux: "", other: "", other2: "" },
                    therapy: ""
                }
            };
        }
    }
    //#endregion

    //#region 取得追蹤紀錄
    var getTrackRecord = function () {
        getDocumentList(3553);
        if ($scope.viewModel.document2 == undefined) {
            $scope.viewModel.document2 = {
                branchNo: "0",
                createTime: new Date(),
                createUser: { empNo: $scope.shareModel.userInfos.userNo },
                documentNo: "0",
                documentType: "3553",
                templateUrl: "",
                tranStatus: "10",
                versionId: "1.0.0",
                caseNo: $scope.appModel.patientInfos.caseNo,
                body: {
                    data: [
                        {   //基礎評估
                            date: $scope.appModel.patientInfos.startDate,
                            caseResure: "",
                            evaluation: ["false", "false", "false", "false"],
                            complained: ["false", "false"],
                            incompleteCause: "",
                            surgeryDate: "",
                            chemoEndDate: "",
                            radiationEndDate: "",
                            summary: ""
                        },
                        {   //治療後1~2個月
                            date: "",
                            caseStatus: "",
                            evaluation: ["false", "false", "false", "false"],
                            incompleteCause: "",
                            ifClosedCase: "",
                            closeCaseDate: "",
                            summary: ""
                        },
                        {   //治療後4個月
                            date: "",
                            caseStatus: "",
                            evaluation: ["false", "false", "false", "false"],
                            incompleteCause: "",
                            ifClosedCase: "",
                            closedReason: "",
                            closeCaseDate: "",
                            summary: ""
                        },
                        {   //治療後1年
                            date: "",
                            caseStatus: "",
                            evaluation: ["false", "false", "false", "false"],
                            incompleteCause: "",
                            ifClosedCase: "",
                            closedReason: "",
                            closeCaseDate: "",
                            summary: ""
                        },
                        {   //備註
                            summary: ""
                        }
                    ]
                }
            };
        }
    }
    //#endregion

    //#region 取得團隊會議紀錄
    var getCaseAttaches = function () {
        cancerCaseManager.getCaseAttaches($scope.appModel.patientInfos.caseNo).then(
            function (success) {
                setTeamMeetingGridSource(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得 Stroboscopic Evaluation 影片
    var getVideoPaths = function (documentNo) {
        cancerCaseManager.getVideoPaths(documentNo).then(
            function (success) {
                $scope.viewModel.videoPaths = success.data;
                for (var i = 0; i < $scope.viewModel.videoPaths.length; i++) {
                    $scope.viewModel.videoPaths[i] = $sce.trustAsResourceUrl($scope.viewModel.videoPaths[i]);
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 設定CureGrid DataSource
    var setCureGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            group: { field: "item", aggregates: [{ field: "item", aggregate: "count" }] }
        });
        setTimeout(function () {
            var grid = $("#cureListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.cureListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定TeamMeetingGrid DataSource
    var setTeamMeetingGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#teamMeetingGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.teamMeetingGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得左邊文件清單
    var getDocumentList = function (documentType) {
        cancerCaseManager.getDocumentList($scope.appModel.patientInfos.idNo, documentType, $scope.appModel.caseType).then(
            function (success) {
                if (success.data != null || success.data != undefined) {
                    $scope.viewModel.documentList = success.data;
                    if ($scope.viewModel.documentList.length != 0) {
                        getDocumentInfo($scope.viewModel.documentList[0], documentType);
                    }
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 取得文件資料
    var getDocumentInfo = function (documentInfo, documentType) {
        // 1. 取得版號並組合對應版本的 dependencies.json 路徑
        cancerCaseManager.getDocumentInfo(documentInfo.documentNo).then(
            function (success1) {
                if (success1.data != null || success1.data != undefined) {
                    switch (documentType) {
                        case 3552:
                            $scope.viewModel.document1 = success1.data;
                            $scope.viewModel.document1.body = JSON.parse(success1.data.body);
                            break;
                        case 3553:
                            $scope.viewModel.document2 = success1.data;
                            $scope.viewModel.document2.body = JSON.parse(success1.data.body);
                            break;
                    }
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件
    
    //#region 會議記錄存檔
    $scope.setCaseAttaches = function () {
        cancerCaseManager.setCaseAttaches($scope.viewModel.teamMeetingGrid.dataSource.data()).then(
            function (success) {
                if (success.data == "true" || success.data == true) {
                    toaster.showSuccessMessage("恭喜您，存檔成功");
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    };
    //#endregion


    //#region 儲存
    $scope.save = function (document) {
        document.createTime = new Date();
        cancerCaseManager.setDocumentInfo(document).then(
            function (success) {
                if (success.data != "0") {
                    toaster.showSuccessMessage("恭喜您，存檔成功");
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    };
    //#endregion
    
    //#region 取得EmpName
    $scope.getEmpName = function (item) {
        cancerCaseManager.getEmpName(item.empNo.replace(/[^\d]/g, '')).then(
            function (success) {
                item.empName = angular.fromJson(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case 1:
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -6;
                break;
            case 2:
                $scope.viewModel.searchRequest.period.type = "Y";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case 3:
                $scope.viewModel.searchRequest.period.type = "Y";
                $scope.viewModel.searchRequest.period.value = -2;
                break;
            case 4:
                $scope.viewModel.searchRequest.period.type = "Y";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case 5:
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 上傳附件檔案
    $scope.handleFiles = function () {
        for (var i = 0, file; file = document.getElementById("attachment").files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function (file) {
                return function (e) {
                    $scope.viewModel.newAttachment = {
                        attachName: file.name,
                        base64: e.target.result,
                        caseNo: $scope.appModel.patientInfos.caseNo,
                        systemUser: $scope.shareModel.userInfos.userNo,
                        entityState:"Added"
                    };
                    $scope.viewModel.teamMeetingGrid.dataSource.insert($scope.viewModel.newAttachment);
                };
            })(file);
            reader.readAsDataURL(file);
        }
    }
    //#endregion

    //#region 刪除附件檔案
    $scope.deleteAttachment = function (dataItem) {
        if (dataItem.entityState == "Added") {
            dataItem.entityState = "Detached";
        } else {
            dataItem.entityState = 'Deleted';
        }
    }
    //#endregion

    //#region 下載附件檔案
    $scope.downloadAttachment = function (dataItem) {
        var fileDownload = document.createElement('a');
        fileDownload.href = dataItem.base64;
        fileDownload.download = dataItem.name;
        fileDownload.click();
    }
    //#endregion

    //#region 選擇基本紀錄
    $scope.selectBasic = function (item, index) {
        $scope.viewModel.cancerBasicDetail = item;
        for (var i = 0; i < $scope.viewModel.cancerBasicInfo.length; i++) {
            $scope.viewModel.cancerBasicInfo[i].backgroundColor = "#ACD5E4";
        }
        $scope.viewModel.cancerBasicInfo[index].backgroundColor = "#428bca";
    }
    //#endregion
    
    //#region 調整高度
    $scope.setHeight = function () {
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 185 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 245 + "px";
        }
    }
    //#endregion
    
    //#region 調整高度
    $scope.setCureListGridHeight = function () {
        document.getElementById("cureListGrid").style.height = window.innerHeight - 300 + "px";
    }
    //#endregion

    //#region 調整高度
    $scope.setTeamMeetingGridHeight = function () {
        document.getElementById("teamMeetingGrid").style.height = window.innerHeight - 302 + "px";
    }
    //#endregion

    //#region 連接病歷首頁
    $scope.getMedicalHomeC = function () {
        location.href = "cmuh: A " + $scope.appModel.patientInfos.chartNo + " " + $scope.shareModel.userInfos.userId;
    }
    //#endregion

    //#region 連接病歷首頁
    $scope.getMedicalHomeD = function () {
        location.href = "cmuhd: A " + $scope.appModel.patientInfos.chartNo + " " + $scope.shareModel.userInfos.userId;
    }
    //#endregion
    
    //#region 得到表單名稱
    $scope.getFormName = function (documentType) {
        switch(documentType){
            case 3535: return "吞嚥評估單";
            case 3536: return "吞嚥攝影紀錄單";
            case 3537: return "國語構音評量表";
            case 3538: return "功能性由口進食量表";
            case 3539: return "UW-QQLV4生活品質問卷";
            case 3540: return "EAT-10中文評量";
            case 3541: return "運動性言語障礙評估表";
            case 3542: return "嗓音聽覺共識評量表";
            case 3543: return "耳鼻喉部語言檢查表1";
            case 3544: return "耳鼻喉部語言檢查表2";
            case 3545: return "Dysphagia_Assessment";
            case 3546: return "聲帶功能異常評估表";
            case 3547: return "逆流症狀指數問卷";
            case 3548: return "歌唱障礙指數量表";
            case 3549: return "Stroboscopic_Evaluation";
            case 3550: return "音聲評估量表";
            case 3551: return "嗓音障礙指數量表";
              // 3552:         個案管理
              // 3553:         追蹤紀錄
              // 3554:         團隊會議
        }
    }
    //#endregion

    //#region 切換頁面
    $scope.changePage = function (record) {
        if (record.documentType == 3549) {
            getVideoPaths(record.documentNo);
        }
        var paths = record.templateUrl.split('/');
        var depPaths = "";
        for (var i = 0; i < paths.length - 1; i++) { depPaths = depPaths + paths[i] + "/"; }
        depPaths = cmuhBase.stringFormat("{0}{1}?{2}", depPaths, 'dependencies.json', cmuhBase.getVersionNo());
        cmuhBase.getDependencies(depPaths).then(
            function (success) {
                cmuhBase.loadDependencies(success.data).then(
                    function (success) {
                        $scope.viewModel.templateUrl = record.templateUrl;
                        $scope.viewModel.document = { "documentNo": record.documentNo, "body": JSON.parse(record.body) };
                    },
                    function (error) {
                        toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'頁面發生失敗", page.title));
                    }
                );
            },
            function (error) {
                toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'系統資訊發生失敗", page.title));
            });
    }
    //#endregion

    //#region 修改背景顏色
    $scope.changeBgColor = function (array, index) {
        for (var i = 0; i < array.length; i++) {
            array[i].backColor = "#FFFFFF";
        }
        array[index].backColor = "#428bca";
    }
    //#endregion

    //#region 取得病人復健門診紀錄
    $scope.getVisitRecords = function () {
        cancerCaseManager.getVisitRecords($scope.viewModel.searchRequest).then(
            function (success) {
                for (var i = 0; i < success.data.length; i++) {
                    $scope.viewModel.records.push({ "date": success.data[i].visitDate, "item": "復健門診","medCode":"" });
                }
                setCureGridSource($scope.viewModel.records);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region /取得病人復健治療紀錄
    $scope.getMedOrders = function () {
        cancerCaseManager.getMedOrders($scope.viewModel.searchRequest).then(
            function (success) {
                for (var i = 0; i < success.data.length; i++) {
                    $scope.viewModel.records.push({ "date": success.data[i].orderTime, "item": success.data[i].groupName, "medCode": success.data[i].medCode });
                }
                setCureGridSource($scope.viewModel.records);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 是否顯示未完成原因
    $scope.ifShowIncompleteCause = function (item) {
        for(var i=0;i<item.evaluation.length;i++){
            if(item.evaluation[i] == "false"){
                return true;
            }
        }
        return false;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);
