﻿angular.module('cmuh').controller("testMuRong2Controller", ["$scope", "cmuhBase", "toaster", "testMuRong2", function ($scope, cmuhBase, toaster, testMuRong2) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {
        
        //初始化ViewModel
        $scope.viewModel = pageModel;

        $scope.viewModel.infoBox = '';              //取得訊息控制項
        $scope.viewModel.textBox = '';              //最後結果
        $scope.viewModel.langCombo = '';            //辨識語言
        $scope.viewModel.startStopButton = '辨識';  //辨識按鈕
        $scope.viewModel.recognizing = false;       //是否辨識中

        makeRecognition();

        //顯示使用者資訊
        var basicInfo = cmuhBase.stringFormat("{0}", '員工代號：' + $scope.shareModel.userInfos.userId + '　員工姓名：' + $scope.shareModel.userInfos.userName);
        $scope.shareModel.banner.showBanner(true, null, $scope.shareModel.userInfos.sex, basicInfo, "", "");
    }
    //#endregion

    var makeRecognition = function () {
        $scope.viewModel.recognition = new webkitSpeechRecognition();   // 建立語音辨識物件 webkitSpeechRecognition
        $scope.viewModel.recognition.continuous = true;                 // 設定連續辨識模式
        $scope.viewModel.recognition.interimResults = true;             // 設定輸出中間結果。
    }

    //#endregion

    
    //#region 事件

    //#region 開始辨識
    $scope.startButton = function (event) {
        debugger;
        if ($scope.viewModel.recognizing) {
            $scope.viewModel.recognition.stop();                                // 如果正在辨識，則停止。
            $scope.viewModel.recognizing = false;                               // 設定為「非辨識中」
            $scope.viewModel.startStopButton = "開始辨識";                      // 辨識完成...按鈕改為「開始辨識」。
            $scope.viewModel.infoBox = "";                                      // 不顯示訊息
        } else {                                                                // 否則就開始辨識
            $scope.viewModel.textBox = '';                                      // 清除最終的辨識訊息
            $scope.viewModel.recognition.lang = $scope.viewModel.langCombo;     // 設定辨識語言
            $scope.viewModel.recognition.start();                               // 開始辨識
            $scope.viewModel.recognizing = true;                                // 設定為辨識中
            $scope.viewModel.startStopButton = "按此停止";                      // 辨識中...按鈕改為「按此停止」。  
            $scope.viewModel.infoBox = "辨識中...";                             // 顯示訊息為「辨識中」...

            $scope.viewModel.recognition.onresult = function (event) {                      // 辨識有任何結果時
                var content = '';                                                           // 辨識內容
                for (var i = 0; i < event.results.length; i++) {                            // 對於每一個辨識結果
                    if (event.results[i].isFinal) {                                         // 如果是最終結果
                        content += event.results[i][0].transcript;                          // 將其加入最終結果中
                        $scope.viewModel.textBox = content;
                    } else {
                        $scope.viewModel.textBox = content + event.results[i][0].transcript;
                    }
                }
            }
        }

        //$scope.viewModel.recognition.onresult = function (event) {                      // 辨識有任何結果時
        //    debugger;
        //    var interim_transcript = '';                                                // 中間結果
        //    for (var i = event.resultIndex; i < event.results.length; ++i) {            // 對於每一個辨識結果
        //        $scope.viewModel.textBox += event.results[i][0].transcript;
        //        /*if (event.results[i].isFinal) {                                         // 如果是最終結果
        //            $scope.viewModel.final_transcript += event.results[i][0].transcript;// 將其加入最終結果中
        //        } else {                                                                // 否則
        //            interim_transcript += event.results[i][0].transcript;               // 將其加入中間結果中
        //        }*/
        //    }
        //    /*if ($scope.viewModel.final_transcript.length > 0)                           // 如果有最終辨識文字
        //        $scope.viewModel.textBox = $scope.viewModel.final_transcript;           // 顯示最終辨識文字
        //    if (interim_transcript.trim().length > 0)                                   // 如果有中間辨識文字
        //        $scope.viewModel.tempBox = interim_transcript;                          // 顯示中間辨識文字*/
        //}
    }
    //#endregion

    

    //#endregion


    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);