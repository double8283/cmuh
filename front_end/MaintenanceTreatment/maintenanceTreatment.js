﻿angular.module('cmuh').service('maintenanceTreatment', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得維修狀況清單
    this.getRepairStatusInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairStatusInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得期間內自己維修的修繕資料清單
    this.getRepairMts = function (searchRequest) {
        return $http.post("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMts/RepairUser", searchRequest);
    }
    //#endregion

    //#region 帳號密碼驗證
    this.authorized = function (param) {
        return $http.put('../WebApi/AppPortal/Login/Authorized', param);
    }
    //#endregion

    //#region 取得單位地點清單
    this.getLocationInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetLocationInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得設備類別清單
    this.getEquipmentCategoryList = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyClasses/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得財產編號清單
    this.getPropertyList = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyBasicInfos/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得特定財產名稱
    this.getPropertyBasicInfo = function (propertyCode) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyBasicInfo/{0}", propertyCode);
        return $http.get(url);
    }
    //#endregion

    //#region 取得故障情形清單
    this.getPropertyFailureInfo = function (propertyType) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPropertyFailures/{0}", propertyType);
        return $http.get(url);
    }
    //#endregion

    //#region 取得零件分類資料
    this.getMaintenanceStaff = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPartsClasses/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得零件資料
    this.getPartsBasics = function (partsType) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPartsBasics/{0}", partsType);
        return $http.get(url);
    }
    //#endregion

    //#region 取得條碼名稱
    this.getPartsName = function (partsCode) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetPartsName/{0}", partsCode);
        return $http.get(url);
    }
    //#endregion

    //#region 取得修繕資料
    this.getRepairMtInfo = function (repairNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMtInfo/{0}", repairNo);
        return $http.get(url);
    }
    //#endregion

    //#region 設定修繕資料
    this.setRepair = function (repairMtInfo) {
        return $http.put("../WebApi/FaciltiesCheckList/FacilitiesRepairService/SetRepair", repairMtInfo);
    }
    //#endregion

}]);