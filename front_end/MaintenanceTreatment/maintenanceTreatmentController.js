﻿angular.module("cmuh").controller("maintenanceTreatmentController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenanceTreatment", function ($scope, $http, $q, cmuhBase, toaster, maintenanceTreatment) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //定義MaintenanceListGrid
        setMaintenanceListGridOptions();

        //查詢變數
        $scope.viewModel.searchRequest = {
            repairDepartNo: $scope.executingAppStore.appId == 351 ? "1AD0" : $scope.executingAppStore.appId == 352 ? "1A91" : "1A82",
            repairUser: $scope.shareModel.userInfos.userNo,
            repairStatus: {
                optionNo: 0
            },
            period: {
                type: "D",
                value: "-3"
            }
        }
        $scope.viewModel.period = "1";

        $scope.viewModel.signoffUser = '';

        //財產Grid
        $scope.viewModel.propertyGridOptions = {
            scrollable: false,
            selectable: 'row',
            sortable: true,
            columns: [
                { field: "propertyCode", title: "財產編碼", width: "200px" },
                { field: "propertyName", title: "財產名稱" },
                { field: "matCode", title: "資材號碼", width: "100px" },
                { field: "keepUser.empCode", title: "保管人員代", width: "100px" },
                { field: "keepUser.empName", title: "保管人姓名", width: "100px" }
            ],
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.viewModel.property = dataItem;
            }
        };

        //零件Grid
        setComponentsGridOptions();

        //取得修繕狀態資料
        getRepairStatusInfos();
        document.getElementById("maintenanceListGrid").style.height = window.innerHeight - 310 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 255 + "px";
        }
    }
    //#endregion

    //#region 定義MaintenanceListGrid
    var setMaintenanceListGridOptions = function () {
        $scope.viewModel.select = "false";
        $scope.viewModel.maintenanceListGridOptions = {
            selectable: 'row',
            scrollable: false,
            sortable:true,
            columns: [
                { field: "repairNo", title: "維修編號", width: "80px" },
                { field: "applyTime", title: "申請時間", width: "200px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd hh:mm:ss'}}" },
                { field: "applyDepartNo", title: "申請部門", width: "80px" },
                { field: "applyUser.empName", title: "申請人員", width: "100px" },
                { field: "telNo", title: "連絡電話", width: "80px" },
                { field: "locationNames[0]", title: "棟別", width: "200px" },
                { field: "locationNames[1]", title: "樓層", width: "50px" },
                { field: "", title: "設備類別", template: "<div data-ng-repeat=\"item in dataItem.propertyNames\" style=\"display:inline\">{{item}}&emsp;</div>" },
                { field: "signoffUser.empName", title: "簽核人員", width: "100px" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.shareModel.repairNo = dataItem.repairNo;
                var basicInfo = cmuhBase.stringFormat(
                                "維修編號：{0}　申請人：{1}　地點：{2}　連絡電話：{3}",
                                dataItem.repairNo,
                                dataItem.applyUser.empName,
                                dataItem.locationNames.join("-"),
                                dataItem.telNo);
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
                $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
                getRepairMtInfo();
            }
        };
    }
    //#endregion

    //#region 設定MaintenanceListGrid DataSource
    var setMaintenanceListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.maintenanceListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#maintenanceListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.maintenanceListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得修繕狀態資料
    var getRepairStatusInfos = function () {
        maintenanceTreatment.getRepairStatusInfos().then(
            function (success) {
                $scope.viewModel.repairStatusInfos = [{ "optionNo": "0", "optionName": "---請選擇---" }];
                $scope.viewModel.repairStatusInfos = $scope.viewModel.repairStatusInfos.concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得維修狀態
    var getRepairStatus = function (option) {
        for (var i = 0; i < $scope.viewModel.repairStatusInfos.length; i++) {
            if (option == $scope.viewModel.repairStatusInfos[i].optionNo) {
                return $scope.viewModel.repairStatusInfos[i].optionName;
            }
        }
    }
    //#endregion

    //#region 設定PropertyGrid DataSource
    var setPropertyGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#propertyGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.propertyGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得修繕資料
    var getRepairMtInfo = function () {
        maintenanceTreatment.getRepairMtInfo($scope.shareModel.repairNo).then(
            function (success) {
                $scope.viewModel.repairMtInfo = success.data;
                $scope.viewModel.repairMtInfo.completeTime = new Date($scope.viewModel.repairMtInfo.completeTime);
                //取得單位地點清單
                getLocationInfos();
                setComponentsGridSource($scope.viewModel.repairMtInfo.repairDts);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    
    //#region 定義ComponentsGrid
    var setComponentsGridOptions = function () {
        $scope.viewModel.componentsGridOptions = {
            editable: true,
            rowTemplate: kendo.template($("#componentsGridRowTemplate").html()),
            columns: [
                { field: "", title: "刪除", width: "50px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteComponents(dataItem)\" /></center>" },
                { field: "partsCode", title: "代碼", width: "100px", editable: false },
                { field: "partsName", title: "名稱", editable: false },
                { field: "qty", title: "數量", width: "70px" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ComponentsGrid DataSource
    var setComponentsGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            schema: {
                model: {
                    fields: {
                        partsCode: { type: "string", editable: false },
                        partsName: { type: "string", editable: false },
                        qty: { type: "number", editable: $scope.viewModel.repairMtInfo.repairStatus.optionNo > 10 ? false : true }
                    }
                }
            }
        });
        $scope.viewModel.componentsGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#componentsGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.componentsGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得單位地點清單
    var getLocationInfos = function () {
        maintenanceTreatment.getLocationInfos().then(
            function (success) {
                $scope.viewModel.locationNames = [];
                $scope.viewModel.locationInfos = [];
                $scope.viewModel.locationInfos[0] = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 設定修繕資料
    var setRepair = function () {
        maintenanceTreatment.setRepair($scope.viewModel.repairMtInfo).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，操作成功");
                    getRepairMtInfo();
                } else {
                    toaster.showErrorMessage("對不起，操作失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 匯入零件
    $scope.handleFiles = function (files) {
        if (files[0] == undefined) {
            return;
        }
        var reader = new FileReader();
        reader.onload = loadHandler;
        reader.readAsText(files[0], "big5");
    }
    var loadHandler = function (event) {
        var csv = event.target.result;
        var allTextLines = csv.split(/\r\n|\n/);
        var data = [];
        for (var i = 0; i < allTextLines.length; i++) {
            $scope.viewModel.componentsGrid.dataSource.insert(
                {
                    partsCode: allTextLines[i].split(',')[0],
                    partsName: allTextLines[i].split(',')[1],
                    qty: allTextLines[i].split(',')[2],
                    entityState: "Added"
                }
            );
        }
    }
    //#endregion

    //#region 查詢
    $scope.search = function () {
        maintenanceTreatment.getRepairMts($scope.viewModel.searchRequest).then(
            function (success) {
                setMaintenanceListGridSource(success.data);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case "1":
                $scope.viewModel.searchRequest.period.type = "D";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "2":
                $scope.viewModel.searchRequest.period.type = "W";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "3":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "4":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "5":
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{ cells: [{ value: "維修編號" }, { value: "申請時間" }, { value: "申請部門" }, { value: "申請人員" }, { value: "連絡電話" }, { value: "單位地點" }, { value: "設備類別" }, { value: "維修狀況" }] }];
        for (var i = 0; i < $scope.viewModel.maintenanceListGrid.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].repairNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyTime },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyDepartNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyUser.empName },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].telNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].locationNames.join("-") },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].propertyNames.join("-") },
                    { value: getRepairStatus($scope.viewModel.searchRequest.repairStatus.optionNo) }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "維修資料.xlsx"
        });
    }
    //#endregion

    //#region 完成禁用
    $scope.completeDisabled = function () {
        if ($scope.viewModel.repairMtInfo == undefined) {
            return
        }
        if (cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.telNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.locationNo.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.propertyCode.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.propertyType.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.failureNo.optionNo) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.completeTime) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.workMinutes) == true ||
            cmuhBase.isNullOrEmpty($scope.viewModel.repairMtInfo.repairRemark) == true) {
            return true;
        }
        return false;
    }
    //#endregion

    //#region 取得設備類別清單
    $scope.getEquipmentCategoryList = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 10) {
            return;
        }
        if ($scope.viewModel.repairMtInfo.repairDepartNo == '') {
            toaster.showErrorMessage("請先選擇維修類別");
            return;
        }
        $scope.viewModel.devicesTypeWindow.center().open();
        maintenanceTreatment.getEquipmentCategoryList($scope.viewModel.repairMtInfo.repairDepartNo).then(
            function (success) {
                $scope.viewModel.equipmentNames = [];
                $scope.viewModel.equipmentInfos = [];
                $scope.viewModel.equipmentInfos[0] = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得財產編號清單
    $scope.getPropertyList = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 10) {
            return;
        }
        $scope.viewModel.propertyWindow.center().open();
        maintenanceTreatment.getPropertyList($scope.viewModel.repairMtInfo.applyDepartNo).then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.propertyList = [
                        {
                            "propertyCode": "0000000-00000000000",
                            "propertyName": "無財產編碼",
                            "matCode": "0000000",
                            "keepDepartNo": "",
                            "keepUser": {
                                "empNo": "",
                                "empCode": "",
                                "empName": "",
                                "departNo": "",
                                "departName": ""
                            }
                        }];
                    $scope.viewModel.propertyList = $scope.viewModel.propertyList.concat(success.data);
                    setPropertyGridSource($scope.viewModel.propertyList);
                } else {
                    setPropertyGridSource();
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setPropertyGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 用申請人篩選財產編號清單
    $scope.filterGrid = function () {
        var newsList = angular.copy($scope.viewModel.propertyList);
        var filterNews = newsList.filter(function (item) {
            return (item.keepUser.empName.indexOf($scope.viewModel.keepUser) != -1 ? true : false) ||
                (item.keepUser.empCode.indexOf($scope.viewModel.keepUser) != -1 ? true : false);
        });
        setPropertyGridSource(filterNews);
    }
    //#endregion

    //#region 取得特定財產名稱
    $scope.getPropertyBasicInfo = function () {
        maintenanceTreatment.getPropertyBasicInfo($scope.viewModel.repairMtInfo.propertyCode.optionNo).then(
            function (success) {
                $scope.viewModel.repairMtInfo.propertyCode.optionName = success.data.propertyName;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得故障情形清單
    $scope.getPropertyFailureInfo = function () {
        if ($scope.viewModel.repairMtInfo.repairStatus.optionNo > 10) {
            return;
        }
        if ($scope.viewModel.repairMtInfo.propertyType.optionNo == '') {
            toaster.showErrorMessage("請先選擇設備類別");
            return;
        }
        $scope.viewModel.failureScenarioWindow.center().open();
        maintenanceTreatment.getPropertyFailureInfo($scope.viewModel.repairMtInfo.propertyType.optionNo).then(
            function (success) {
                $scope.viewModel.propertyFailureInfo = success.data;
                $scope.viewModel.repairMtInfo.failureNo = "";
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 下載附件檔案
    $scope.downloadAttachment = function (dataItem) {
        var fileDownload = document.createElement('a');
        fileDownload.href = dataItem.base64;
        fileDownload.download = dataItem.attachName;
        fileDownload.click();
    }
    //#endregion

    //#region 存檔／完成
    $scope.signature = function (optionNo) {
        $scope.viewModel.repairMtInfo.repairStatus.optionNo = optionNo;
        $scope.viewModel.repairMtInfo.repairDts = $scope.viewModel.componentsGrid.dataSource.data();
        $scope.viewModel.repairMtInfo.systemUser = $scope.shareModel.userInfos.userNo;
        setRepair();
    }
    //#endregion

    //#region 轉單
    $scope.turnForm = function (repairDepartNo) {
        $scope.viewModel.repairMtInfo.repairDepartNo = repairDepartNo;
        $scope.viewModel.repairMtInfo.propertyType.optionNo = "";
        $scope.viewModel.repairMtInfo.failureNo.optionNo = "";
        $scope.viewModel.repairMtInfo.repairUser.empNo = "";
        $scope.viewModel.repairMtInfo.systemUser = $scope.shareModel.userInfos.userNo;
        $scope.viewModel.repairMtInfo.compeleteTime = "2999-12-31";
        $scope.viewModel.repairMtInfo.workMinutes = "0";
        $scope.viewModel.repairMtInfo.repairDts = [];
        $scope.viewModel.repairMtInfo.isScrapped = false;
        setRepair();
        $scope.shareModel.navigation($scope.executingAppStore.appId, 533);
        $scope.shareModel.banner.showBanner(null, null, "", "", "", "");
        $scope.shareModel.repairNo = "";
    }
    //#endregion

    //#region 使用者帳密登入驗證
    $scope.login = function () {
        //帳號小寫轉大寫
        $scope.viewModel.userParam.userId = $scope.viewModel.userParam.userId.toUpperCase();
        //帳密沒有確實填寫處理
        if ($scope.viewModel.userParam.userId == "" && $scope.viewModel.userParam.password == "") {
            // 清除資料
            document.getElementById('loginUserId').focus();
            toaster.showWarningMessage('請輸入帳號密碼!');
            return;
        }
        if ($scope.viewModel.userParam.userId != "" && $scope.viewModel.userParam.password == "") {
            // 自動切換 到 輸入密碼 的輸入框 (2014-07-16 使用者提出需求)
            document.getElementById('loginPassword').focus();
            return;
        }
        $scope.viewModel.userParam.isSignOn = true;
        //驗證帳密是否正確處理
        maintenanceTreatment.authorized($scope.viewModel.userParam).then(
            function (success) {
                if (success.data != null && success.data.userId == $scope.viewModel.userParam.userId) {
                    toaster.showSuccessMessage(success.data.userId + '登入成功');
                    $scope.viewModel.signoffUser = success.data.userId;
                }
            },
            function (error) {
                $scope.viewModel.userParam.password = '';
                document.getElementById('loginUserId').focus();
                toaster.showWarningMessageBox('登入失敗!!', '請檢查帳號密碼是否輸入正確');
            }
        );
    };
    //#endregion

    //#region 簽核
    $scope.signOff = function (array, index) {
        $scope.viewModel.signOffWindow.close();
        $scope.viewModel.repairMtInfo.signoffUser.empNo = $scope.viewModel.signoffUser.replace(/[^\d]/g, '');
        $scope.viewModel.repairMtInfo.repairStatus.optionNo = 30;
        $scope.viewModel.repairMtInfo.systemUser = $scope.shareModel.userInfos.userNo;
        setRepair();
    }
    //#endregion

    //#region 修改背景顏色
    $scope.changeBgColor = function (array, index) {
        for (var i = 0; i < array.length; i++) {
            array[i].backColor = "#FFFFFF";
        }
        array[index].backColor = "#428bca";
    }
    //#endregion

    //#region 取得零件分類資料
    $scope.getMaintenanceStaff = function () {
        maintenanceTreatment.getMaintenanceStaff($scope.viewModel.repairMtInfo.repairDepartNo).then(
            function (success) {
                $scope.viewModel.maintenanceStaff = [];
                $scope.viewModel.maintenanceStaff[0] = success.data;
                $scope.viewModel.partsBasics = [];
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得零件資料
    $scope.getPartsBasics = function () {
        maintenanceTreatment.getPartsBasics($scope.viewModel.partNo).then(
            function (success) {
                $scope.viewModel.partsBasics = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 新增零件
    $scope.addComponents = function (item) {
        $scope.viewModel.componentsGrid.dataSource.insert({ partsCode: item.partsCode, partsName: item.partsName, qty: 1, entityState: "Added" });
    }
    //#endregion

    //#region 刪除零件
    $scope.deleteComponents = function (dataItem) {
        if (dataItem.entityState == "Unchanged") {
            dataItem.entityState = "Deleted";
        } else {
            dataItem.entityState = "Detached";
        }
    }
    //#endregion

    //#region 取得條碼名稱
    $scope.getPartsName = function () {
        maintenanceTreatment.getPartsName($scope.viewModel.barCodeNo).then(
            function (success) {
                $scope.viewModel.barCodeName = JSON.parse(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 新增零件
    $scope.addParts = function () {
        if (cmuhBase.isNullOrEmpty($scope.viewModel.barCodeName) == true) {
            $scope.viewModel.usedPartsWindow.center().open();
            $scope.getMaintenanceStaff();
        } else {
            $scope.viewModel.componentsGrid.dataSource.insert({ partsCode: $scope.viewModel.barCodeNo, partsName: $scope.viewModel.barCodeName, qty: 1, entityState: "Added" });
            $scope.viewModel.barCodeNo = "";
            $scope.viewModel.barCodeName = "";
        }
        document.getElementById('barCodeText').focus();
    }
    //#endregion

    //#region 選擇單位地點
    $scope.getLocation = function (item, index) {
        if (index == 4) {
            $scope.viewModel.repairMtInfo.locationNo.optionNo = $scope.viewModel.locationNo.locationNo;
            $scope.viewModel.repairMtInfo.locationNo.optionName = $scope.viewModel.locationNo.locationName;
            $scope.viewModel.repairMtInfo.locationNames = $scope.viewModel.locationNames;
            $scope.viewModel.faultLocationWindow.close();
            return;
        }
        for (var i = index; i < $scope.viewModel.locationNames.length; i++) {
            $scope.viewModel.locationNames[i] = '';
        }
        $scope.viewModel.locationNames[index] = item.locationName;
        for (var i = index + 1; i < $scope.viewModel.locationInfos.length; i++) {
            $scope.viewModel.locationInfos[i] = [];
        }
        $scope.viewModel.locationInfos[index + 1] = item.details;
        $scope.viewModel.locationNo = item;

    }
    //#endregion

    //#region 選擇設備類別
    $scope.getEquipment = function (item, index) {
        if (index == 3) {
            $scope.viewModel.repairMtInfo.propertyType.optionNo = $scope.viewModel.equipmentNo;
            $scope.viewModel.repairMtInfo.propertyNames = $scope.viewModel.equipmentNames;
            $scope.viewModel.devicesTypeWindow.close();
            return;
        }
        for (var i = index; i < $scope.viewModel.equipmentNames.length; i++) {
            $scope.viewModel.equipmentNames[i] = '';
        }
        $scope.viewModel.equipmentNames[index] = item.typeName;
        for (var i = index + 1; i < $scope.viewModel.equipmentInfos.length; i++) {
            $scope.viewModel.equipmentInfos[i] = [];
        }
        $scope.viewModel.equipmentInfos[index + 1] = item.details;
        $scope.viewModel.equipmentNo = item.propertyType;
    }
    //#endregion

    //#region 選擇零件類別
    $scope.getPartsType = function (item, index) {
        for (var i = index + 1; i < $scope.viewModel.maintenanceStaff.length; i++) {
            $scope.viewModel.maintenanceStaff[i] = [];
        }
        $scope.viewModel.maintenanceStaff[index + 1] = item.details;
        $scope.viewModel.partNo = item.partsType;
    }
    //#endregion

    //#region 取得財產
    $scope.getProperty = function () {
        $scope.viewModel.repairMtInfo.propertyCode.optionNo = $scope.viewModel.property.propertyCode;
        $scope.viewModel.repairMtInfo.propertyCode.optionName = $scope.viewModel.property.propertyName;
        $scope.viewModel.propertyWindow.close();
    }
    //#endregion

    //#region 條碼鎖定
    $scope.removeQuickSearch = function () {
        //$scope.viewModel.disabled = $scope.viewModel.isAutoPress == "true" ? true : false;
        if ($scope.viewModel.isAutoPress == "true") {
            document.getElementById('barCodeText').focus();
            $scope.viewModel.disabled = true;
        } else {
            $scope.viewModel.disabled = false;
        }
    };
    //#endregion

    //#region 主管鎖定（主管不鎖定false，非主管鎖定true）
    $scope.supervisorDisabled = function () {
        if ($scope.shareModel.userInfos.userId == "A8240" ||
            $scope.shareModel.userInfos.userId == "A7637" ||
            $scope.shareModel.userInfos.userId == "A8384" ||
            $scope.shareModel.userInfos.userId == "A11141" ||
            $scope.shareModel.userInfos.userId == "A6832" ||
            $scope.shareModel.userInfos.userId == "A24164" ||
            $scope.shareModel.userInfos.userId == "A29534" ||
            $scope.shareModel.userInfos.userId == "A6910") {
            return false;
        } else {
            return true;
        }
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);