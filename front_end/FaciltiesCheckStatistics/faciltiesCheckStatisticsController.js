﻿angular.module("cmuh").controller("faciltiesCheckStatisticsController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "faciltiesCheckStatistics", function ($scope, $http, $q, cmuhBase, toaster, faciltiesCheckStatistics) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");
        
        //檢查日期
        $scope.viewModel.checkDay = new Date();
        $scope.viewModel.checkDayOption = {
            format: 'yyyy-MM-dd',
            max: new Date()
        };
        //檢查月份
        $scope.viewModel.checkStartMonth = new Date();
        $scope.viewModel.checkEndMonth = new Date();
        $scope.viewModel.checkMonthOption = {
            format: 'yyyy-MM',
            depth: "year",
            start: "year",
            max: new Date()
        };

        //設定DayListGrid
        setDayListGridOptions();
        //設定MonthListGrid
        setMonthListGridOptions();


        //設定高度
        document.getElementById("tabstrip").style.height = window.innerHeight - 260 + "px";
        document.getElementById("dayListGrid").style.height = window.innerHeight - 290 + "px";
        document.getElementById("monthListGrid").style.height = window.innerHeight - 290 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 240 + "px";
        }
    }
    //#endregion

    //#region 設定DayListGrid
    var setDayListGridOptions = function () {
        $scope.viewModel.dayListGridOptions = {
            rowTemplate: kendo.template($("#dayListGridRowTemplate").html()),
            sortable: true,
            scrollable: false,
            pageable: true,
            filterable: true,
            columns: [
                { field: "departName", title: "部門名稱", width: "350px" },
                { field: "locationName", title: "地點名稱", width: "350px" },
                { field: "", title: "良好" },
                { field: "", title: "異常"},
                { field: "", title: "未檢查" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定DayListGrid DataSource
    var setDayListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            pageSize: 17
        });
        $scope.viewModel.dayListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#dayListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.dayListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定MonthListGrid
    var setMonthListGridOptions = function () {
        $scope.viewModel.monthListGridOptions = {
            //rowTemplate: kendo.template($("#monthListGridRowTemplate").html()),
            scrollable: false,
            sortable: true,
            pageable: true,
            dataSource: {
                data: new kendo.data.DataSource(),
                group: { field: "departName", aggregates: [{ field: "departName" }] }
            },
            columns: [
                { field: "departName", title: "部門名稱", groupHeaderTemplate: "#= value #" },
                { field: "dateString", title: "檢查月份", template: "{{dataItem.dateString | date:'yyyy-MM-dd'}}" },
                { field: "locationName", title: "檢查地點" },
                { field: "", title: "良好", template: "<div data-ng-if=\"dataItem.status == 'true'\" style=\"color:green\">●</div>" },
                { field: "", title: "異常", template: "<div data-ng-if=\"dataItem.status == 'false'\" style=\"color:red\" data-ng-click=\"getCheckList(1,dataItem);viewModel.checklistWindow.center().open();\">●</div>" },
                { field: "", title: "未檢查", template: "<div data-ng-if=\"dataItem.status == 'na'\" style=\"color:black\">●</div>" }
            ]
        };
    }
    //#endregion

    //#region 設定MonthListGrid DataSource
    var setMonthListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items,
            pageSize: 16,
            group: { field: "departName", aggregates: [{ field: "departName", aggregate: "count" }] }
        });
        $scope.viewModel.monthListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#monthListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.monthListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 取得某一天的所有單位所有地點的每日檢查表內容
    $scope.getDayReport = function () {
        var data = {
            recordDate: $scope.viewModel.checkDay,
            checklistNo: 2
        };
        faciltiesCheckStatistics.getDayReport(data).then(
            function (success) {
                setDayListGridSource(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得某一段時間的所有單位所有地點的每月檢查表內容
    $scope.getMonthReport = function () {
        var data = {
            startDate: $scope.viewModel.checkStartMonth,
            endDate: $scope.viewModel.checkEndMonth,
            checklistNo: 1
        };
        faciltiesCheckStatistics.getMonthReport(data).then(
            function (success) {
                setMonthListGridSource(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得檢查表內容
    $scope.getCheckList = function (checklistNo, dataItem) {
        var data = {
            checklistNo: checklistNo,
            recordDate: dataItem.startDate.substring(0,10),
            departNo: dataItem.departNo,
            locationNo: dataItem.locationNo
        };
        faciltiesCheckStatistics.getCheckList(data).then(
            function (success) {
                $scope.viewModel.check = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 匯出
    $scope.export = function () {
        var data = [{ cells: [{ value: "部門名稱" }, { value: "地點名稱" }, { value: "良好" }, { value: "異常" }, { value: "未檢查" }] }];
        for (var i = 0; i < $scope.viewModel.dayListGridOptions.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].departName },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].locationName },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'true' ? '●' : "" },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'false' ? '●' : "" },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'na' ? '●' : "" }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: cmuhBase.getDateFormat($scope.viewModel.checkDay) + "-日統計報表.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);