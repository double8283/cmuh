﻿angular.module('cmuh').service('faciltiesCheckStatistics', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion

    //#region 取得使用者單位
    this.getUserDept = function (userId) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetUserDept/{0}", userId);
        return $http.get(url);
    }
    //#endregion
    
    //#region 取得使用者名稱
    this.getEmpName = function (empNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetEmpName/{0}", empNo);
        return $http.get(url);
    }
    //#endregion
    
    //#region 取得檢查表內容
    this.getCheckList = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetCheckList", data);
    }
    //#endregion
    
    //#region 儲存檢查表內容
    this.setFacilitiesCheck = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/SetFacilitiesCheck", data);
    }
    //#endregion

    //#region 取得某一天的所有單位所有地點的每日檢查表內容
    this.getDayReport = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetDayReport", data);
    }
    //#endregion

    //#region 取得某一段時間的所有單位所有地點的每月檢查表內容
    this.getMonthReport = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetMonthReport", data);
    }
    //#endregion
}]);