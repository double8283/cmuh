﻿angular.module('cmuh').service('faciltiesCheckManagement', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得部門清單
    this.getDeptList = function () {
        var url = cmuhBase.stringFormat("../WebApi/EmpHealthManager/EmpHealthService/GetDepart");
        return $http.get(url);
    }
    //#endregion

    //#region 取得使用者單位
    this.getUserDept = function (userId) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetUserDept/{0}", userId);
        return $http.get(url);
    }
    //#endregion
    
    //#region 取得使用者名稱
    this.getEmpName = function (empNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetEmpName/{0}", empNo);
        return $http.get(url);
    }
    //#endregion
    
    //#region 儲存檢查表內容
    this.setFacilitiesCheck = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/SetFacilitiesCheck", data);
    }
    //#endregion

    //#region 取得單位地點清單
    this.getLocationInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetLocationInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得某單位某地點的檢查表內容
    this.getCheckDay = function (data) {
        return $http.put("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetCheckDay", data);
    }
    //#endregion
}]);