﻿angular.module("cmuh").controller("faciltiesCheckManagementController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "faciltiesCheckManagement", function ($scope, $http, $q, cmuhBase, toaster, faciltiesCheckManagement) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //檢查月份
        $scope.viewModel.checkMonth = new Date();
        $scope.viewModel.checkMonthOption = {
            format: 'yyyy-MM',
            depth: "year",
            start: "year",
            max: new Date()
        };

        //檢查年度
        $scope.viewModel.checkYear = new Date();
        $scope.viewModel.checkYearOption = {
            format: 'yyyy',
            depth: "decade",
            start: "decade",
            min: new Date(2016, 1, 1),
            max: new Date(new Date().getFullYear(), 1, 1)
        };

        //每日
        $scope.viewModel.dayListSchedulerOptions = {
            selectable: false,
            editable: false,
            views: ["month"],
            eventTemplate: $("#event-template").html(),
            dataSource: new kendo.data.SchedulerDataSource()
        };
        
        //取得部門清單
        getDeptList();

        //取得單位地點清單
        $scope.viewModel.locationNo = 0;
        getLocationInfos();

        document.getElementById("contentDiv").style.height = window.innerHeight - 200 + "px";
    }
    //#endregion

    //#region 設定每日Scheduler DataSource
    var setDayListSchedulerSource = function (items) {
        var dataSource = new kendo.data.SchedulerDataSource({
            data: items
        });
        $scope.viewModel.dayListSchedulerOptions.dataSource = dataSource;
        setTimeout(function () {
            var scheduler = $("#dayListScheduler").data("kendoScheduler");
            if (scheduler == null) {
                $scope.viewModel.dayListSchedulerOptions.dataSource = dataSource;
            } else {
                scheduler.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得部門清單
    var getDeptList = function () {
        faciltiesCheckManagement.getDeptList().then(
            function (success) {
                if (success.data.length != 0) {
                    $scope.viewModel.deptList = [{ "departNo": "0", "departName": "-----請選擇-----" }];
                    $scope.viewModel.deptList = $scope.viewModel.deptList.concat(success.data);
                    //取得使用者單位
                    $scope.getUserDept();
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得單位地點清單
    var getLocationInfos = function () {
        faciltiesCheckManagement.getLocationInfos().then(
            function (success) {
                $scope.viewModel.locationNames = [];
                $scope.viewModel.locationInfos = [];
                $scope.viewModel.locationInfos[0] = [{ "locationNo": "0", "locationName": "", "details": [] }];
                $scope.viewModel.locationInfos[0] = $scope.viewModel.locationInfos[0].concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 取得使用者單位
    $scope.getUserDept = function () {
        faciltiesCheckManagement.getUserDept($scope.shareModel.userInfos.userId).then(
            function (success) {
                $scope.viewModel.checkDepart = success.data.departNo;
                //取得某單位某地點的檢查表內容
                $scope.getCheckDay(1);
                $scope.getCheckDay(2);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 跳到每日自主檢查表頁面
    $scope.gotoDayList = function (index) {
        for (var i = 0; i < $scope.viewModel.dayListSchedulerOptions.dataSource.data().length; i++) {
            if (index == $scope.viewModel.dayListSchedulerOptions.dataSource.data()[i].index)
            {
                $scope.shareModel.date = new Date($scope.viewModel.dayListSchedulerOptions.dataSource.data()[i].start);
            }
        }
        $scope.shareModel.checkDepart = $scope.viewModel.checkDepart;
        $scope.shareModel.locationNo = $scope.viewModel.locationNo;
        $scope.shareModel.locationInfos = $scope.viewModel.locationInfos;
        $scope.shareModel.locationInfos0 = $scope.viewModel.locationInfos0;
        $scope.shareModel.locationInfos1 = $scope.viewModel.locationInfos1;
        $scope.shareModel.locationInfos2 = $scope.viewModel.locationInfos2;
        $scope.shareModel.locationInfos3 = $scope.viewModel.locationInfos3;
        $scope.shareModel.navigation(181, 204);
    }
    //#endregion

    //#region 跳到每月自主檢查表頁面
    $scope.gotoMonthList = function (index) {
        $scope.shareModel.date = new Date($scope.viewModel.checkMonthData[index].startDate);
        $scope.shareModel.checkDepart = $scope.viewModel.checkDepart;
        $scope.shareModel.locationNo = $scope.viewModel.locationNo;
        $scope.shareModel.locationInfos = $scope.viewModel.locationInfos;
        $scope.shareModel.locationInfos0 = $scope.viewModel.locationInfos0;
        $scope.shareModel.locationInfos1 = $scope.viewModel.locationInfos1;
        $scope.shareModel.locationInfos2 = $scope.viewModel.locationInfos2;
        $scope.shareModel.locationInfos3 = $scope.viewModel.locationInfos3;
        $scope.shareModel.navigation(181, 205);
    }
    //#endregion

    //#region 地點改變
    $scope.locationChange = function (no) {
        var data = [{
            locationNo: 0,
            locationName: "",
            details: []
        }];
        switch (no) {
            case 0:
                var dropdownlist = $("#locationInfos0").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[1] = !!$scope.viewModel.locationInfos[0][selectedIndex] == true ? $scope.viewModel.locationInfos[0][selectedIndex].details : [];
                $scope.viewModel.locationInfos[2] = !!$scope.viewModel.locationInfos[1][0] == true ? $scope.viewModel.locationInfos[1][0].details : [];
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][0] == true ? data.concat($scope.viewModel.locationInfos[2][0].details) : [];
                $scope.viewModel.locationInfos1 = !!$scope.viewModel.locationInfos[1][0] == true ? $scope.viewModel.locationInfos[1][0].locationNo : '';
                $scope.viewModel.locationInfos2 = !!$scope.viewModel.locationInfos[2][0] == true ? $scope.viewModel.locationInfos[2][0].locationNo : '';
                $scope.viewModel.locationInfos3 = '';
                break;
            case 1:
                var dropdownlist = $("#locationInfos1").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[2] = !!$scope.viewModel.locationInfos[1][selectedIndex] == true ? $scope.viewModel.locationInfos[1][selectedIndex].details : [];
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][0] == true ? data.concat($scope.viewModel.locationInfos[2][0].details) : [];
                $scope.viewModel.locationInfos2 = !!$scope.viewModel.locationInfos[2][0] == true ? $scope.viewModel.locationInfos[2][0].locationNo : '';
                $scope.viewModel.locationInfos3 = '';
                break;
            case 2:
                var dropdownlist = $("#locationInfos2").data("kendoDropDownList");
                var selectedIndex = dropdownlist.select();
                $scope.viewModel.locationInfos[3] = !!$scope.viewModel.locationInfos[2][selectedIndex] == true ? data.concat($scope.viewModel.locationInfos[2][selectedIndex].details) : [];
                $scope.viewModel.locationInfos3 = '';
                break;
        }
        if (!!$scope.viewModel.locationInfos3 == true && $scope.viewModel.locationInfos3 != "0") {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos3;
        } else if (!!$scope.viewModel.locationInfos2 == true) {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos2;
        } else if (!!$scope.viewModel.locationInfos1 == true) {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos1;
        } else {
            $scope.viewModel.locationNo = $scope.viewModel.locationInfos0;
        }
        //取得某單位某地點的檢查表內容
        $scope.getCheckDay(1);
        $scope.getCheckDay(2);
    }
    //#endregion

    //#region 取得某單位某地點的檢查表內容
    $scope.getCheckDay = function (checklistNo) {
        //1是每月，2是每日
        var data = {
            departNo: $scope.viewModel.checkDepart,
            locationNo: $scope.viewModel.locationNo,
            recordDate: checklistNo == 1 ? $scope.viewModel.checkYear : $scope.viewModel.checkMonth,
            checklistNo: checklistNo
        };
        faciltiesCheckManagement.getCheckDay(data).then(
            function (success) {
                if (checklistNo == 1) {
                    $scope.viewModel.checkMonthData = success.data;
                } else {
                    for (var i = 0; i < success.data.length; i++) {
                        success.data[i].start = new Date(success.data[i].startDate);
                        success.data[i].end = new Date(success.data[i].endDate);
                    }
                    setDayListSchedulerSource(success.data);
                    var scheduler = $("#dayListScheduler").data("kendoScheduler");
                    scheduler.date($scope.viewModel.checkMonth);
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 顯示CheckMonthData
    $scope.showCheckMonthData = function (index) {
        if (!!$scope.viewModel.checkMonthData == false) {
            return;
        }
        if (!!$scope.viewModel.checkMonthData[index] == true) {
            return $scope.viewModel.checkMonthData[index].status;
        }else{
            return "";
        }
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);