﻿angular.module('cmuh').controller('flowEngineController', ["$scope", "$q", "toaster", "cmuhBase", "flowEngine", function ($scope, $q, toaster, cmuhBase, flowEngine) {

    //#regin 方法

    //#region 設定資料結構
    var initViewModel = function (pageModel) {
        // HTML 畫面 與 service 的繫結
        $scope.viewModel = pageModel;

        if ($scope.viewModel.isInitFlowViewModel === true) {
            return;
        }
        kendo.culture("zh-TW");

        // 文件動態切換樣式
        $scope.viewModel.docViewCSS = '';
        $scope.viewModel.defaultDate = new Date('2999-12-31');
        $scope.viewModel.formUrls = [
            { appId: 400, url: cmuhBase.stringFormat("App/ResignManager/resignApply.html?{0}", cmuhBase.getVersionNo()) },
            { appId: 208, url: cmuhBase.stringFormat("App/FlowEngine/signoffManager.html?{0}", cmuhBase.getVersionNo()) },
            { appId: 403, url: cmuhBase.stringFormat("App/FlowEngine/signoffApply.html?{0}", cmuhBase.getVersionNo()) },
        ];
        getFormUrl($scope.executingAppStore.appId);
        $scope.viewModel.today = new Date();
        //附件Grid
        $scope.viewModel.attachmentGridOptions = {
            scrollable: false,
            columns: [
                { field: "", title: "刪除", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/nRPnjxu.png\" style=\"width:20px;height:20px;\" data-ng-click=\"deleteAttachment(dataItem)\" /></center>" },
                { field: "", title: "下載", width: "30px", template: "<center><img data-ng-src=\"http://i.imgur.com/wtRl5iU.jpg\" style=\"width:20px;height:20px;\" data-ng-click=\"downloadAttachment(dataItem)\" /></center>" },
                { field: "attachName", title: "附件名稱" }
            ],
            dataSource: new kendo.data.DataSource()
        };

        //轉簽
        $scope.viewModel.transfer = {
            suggestion: "",
            user: {
                empCode: "",
                empName: ""
            }
        };

        $scope.viewModel.groupNo = 0;   //平行加簽用的

        setSignoffGridOptions();
        if ($scope.executingAppStore.appId == 403) {
            setSignoffApplyGridOptions();
        } else {
            setSignoffListGridOptions();
        }


        //查詢條件
        $scope.viewModel.searchInfo = {
            signoffNo: 0,
            formInfo: { optionNo: 0 },
            applyUser: { empNo: $scope.executingAppStore.appId == 403 ? $scope.shareModel.userInfos.userNo : 0 },
            signUser: { empNo: $scope.shareModel.userInfos.userNo },
            applyStatus: { optionNo: 0 },
            startDate: new Date(new Date().getFullYear(), 0, 1),
            endDate: new Date()
        };

        //取得表單清單
        getFormInfos();
        //取得簽核狀態清單
        getApplyStatusTypes();
        //初始加簽
        //addSignInit();

        //設定高度
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 190 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 255 + "px";
        }

        $scope.viewModel.isInitFlowViewModel = true;
    }
    //#endregion

    //#region 取得表單內容的url
    var getFormUrl = function (appId) {
        var items = $scope.viewModel.formUrls.filter(function (obj) {
            return obj.appId == appId;
        });
        var promise = getDependencies(items[0].url);
        promise.then(function (data) {
            $scope.viewModel.formUrl = data;
        })
    }
    //#endregion

    //#region 載入dependencies
    var getDependencies = function (url) {
        var deferred = $q.defer();
        var paths = url.split('/');
        var depPath = cmuhBase.stringFormat("{0}/{1}/{2}?{3}", paths[0], paths[1], 'dependencies.json', cmuhBase.getVersionNo());
        cmuhBase.getDependencies(depPath).then(
            function (success) {
                return cmuhBase.loadDependencies(success.data).then(
                     function (success) {
                         deferred.resolve(cmuhBase.stringFormat("{0}?{1}", url, cmuhBase.getVersionNo()));
                     },
                     function (error) {
                         toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'頁面發生失敗", url));
                     }
                 );
            },
            function (error) {
                toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'系統資訊發生失敗", url));
            });
        return deferred.promise;
    }
    //#endregion

    //#region 設定 GridOptions signoffGrid
    var setSignoffGridOptions = function () {
        $scope.viewModel.signoffGridOptions = {
            sortable: true,
            selectable: 'row',
            dataSource: new kendo.data.DataSource(),
            columns: [
                { field: "signUser.empName", title: "簽核人", width: "" },
                { field: "signStatus.optionName", title: "結果" },
                { field: "suggestion", title: "簽核意見" },
                { field: "endDate", title: "簽核日期", template: "<span data-ng-if=\"dataItem.endDate != viewModel.defaultDate \">{{dataItem.endDate | date:'yyyy-MM-dd'}}" },
                { field: "phaseName", title: "簽核角色" }
            ]
        }
    }
    //#endregion

    //#region 設定 DataSource signoffGrid
    var setSignoffGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });

        setTimeout(function () {
            var grid = $("#signoffGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.signoffGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 定義 SignoffListGrid
    var setSignoffListGridOptions = function () {
        $scope.viewModel.select = "false";
        $scope.viewModel.signoffListGridOptions = {
            selectable: 'row',
            scrollable: false,
            sortable: true,
            columns: [
                //{
                //    field: "check", title: "", width: "50px", sortable: false,
                //    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" ng-true-value=\"true\" ng-false-value=\"false\"/></center>",
                //    attributes: { style: "text-align:center" },
                //    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem.check)\" ng-true-value=\"true\" ng-false-value=\"false\"/>"
                //},
                { field: "phaseName", title: "簽核階層", width: "150px" },
                { field: "signoffNo", title: "表單號", width: "80px" },
                { field: "formInfo.formName", title: "表單名稱", width: "160px" },
                { field: "applyUser.empCode", title: "員工代號", width: "120px", template: "<div data-ng-if=\"viewModel.searchInfo.signUser.empNo == '30900' || viewModel.searchInfo.signUser.empNo == '30901'\">{{dataItem.applyUser.idNo}}</div><div data-ng-if=\"viewModel.searchInfo.signUser.empNo != '30900' && viewModel.searchInfo.signUser.empNo != '30901'\">{{dataItem.applyUser.empCode}}</div>" },
                { field: "applyUser.empName", title: "申請人員", width: "80px" },
                { field: "applyUser.titleInfo.optionName", title: "員工職稱", width: "160px" },
                { field: "applyUser.departInfo.optionName", title: "部門名稱", width: "160px" },
                { field: "applyTime", title: "申請日期", width: "100px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd'}}" },
                { field: "applyStatus.optionName", title: "簽核狀態" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                //$scope.shareModel.repairNo = dataItem.repairNo;
                //var basicInfo = cmuhBase.stringFormat(
                //                "申請部門：{0}　申請人職稱：{1}　申請人員代：{2}　申請人：{3}　申請人到職日：{4}",
                //                dataItem.applyUser.departName,
                //                dataItem.applyUser.titleName,
                //                dataItem.applyUser.empCode,
                //                dataItem.applyUser.empName,
                //                dataItem.applyUser.hireDate.substring(0, 10));
                var basicInfo = cmuhBase.stringFormat(
                                "表單編號：{0}　申請人：{1}（{2}）　職稱：{3}　部門：{4}　到職日期：{5}　申請日期：{6}",
                                dataItem.signoffNo,
                                dataItem.applyUser.empName,
                                dataItem.applyUser.empCode,
                                dataItem.applyUser.titleName,
                                dataItem.applyUser.departName,
                                dataItem.applyUser.hireDate.substring(0, 10),
                                dataItem.applyTime.substring(0, 10));
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
                $scope.viewModel.signoffListGrid.element.on('dblclick', function (e) {
                    $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
                    var promise = getDependencies(dataItem.templateUrl);
                    promise.then(function (data) {
                        $scope.viewModel.formContent = data;
                        var tabStrip = $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip");
                        tabStrip.enable(tabStrip.tabGroup.children().eq(1));
                        tabStrip.enable(tabStrip.tabGroup.children().eq(2));
                    })
                });
                $scope.viewModel.signOffItem = dataItem;
                $scope.viewModel.signoffNo = dataItem.signoffNo;//載入簽核申請畫面時需要
            }
        };
    }
    //#endregion

    //#region 設定 SignoffListGrid DataSource
    var setSignoffListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.signoffListGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#signoffListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.signoffListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 定義 SignoffApplyGrid
    var setSignoffApplyGridOptions = function () {
        $scope.viewModel.signoffApplyGridOptions = {
            selectable: 'row',
            scrollable: false,
            sortable: true,
            columns: [
                { field: "signoffNo", title: "表單號", width: "80px" },
                { field: "formInfo.formName", title: "表單名稱", width: "160px" },
                { field: "applyTime", title: "申請日期", width: "100px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd'}}" },
                { field: "applyStatus.optionName", title: "簽核狀態" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                var basicInfo = cmuhBase.stringFormat(
                                "表單編號：{0}　申請人：{1}（{2}）　職稱：{3}　部門：{4}　到職日期：{5}　申請日期：{6}",
                                dataItem.signoffNo,
                                dataItem.applyUser.empName,
                                dataItem.applyUser.empCode,
                                dataItem.applyUser.titleName,
                                dataItem.applyUser.departName,
                                dataItem.applyUser.hireDate.substring(0, 10),
                                dataItem.applyTime.substring(0, 10));
                //var basicInfo = cmuhBase.stringFormat(
                //                "表單編號：{0}　申請人：{1}",
                //                dataItem.signoffNo,
                //                dataItem.applyUser.empName);
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
                $scope.viewModel.signoffApplyGrid.element.on('dblclick', function (e) {
                    $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
                    var promise = getDependencies(dataItem.templateUrl);
                    promise.then(function (data) {
                        $scope.viewModel.formContent = data;
                    })
                });
                $scope.viewModel.signOffItem = dataItem;
                $scope.viewModel.signoffNo = dataItem.signoffNo;//載入簽核申請畫面時需要
            }
        };
    }
    //#endregion

    //#region 設定 SignoffApplyGrid DataSource
    var setSignoffApplyGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.signoffApplyGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#signoffApplyGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.signoffApplyGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion


    //#region 設定AttachmentGrid DataSource
    var setAttachmentGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.attachmentGridOptions.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#attachmentGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.attachmentGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得表單清單
    var getFormInfos = function () {
        flowEngine.getFormInfos().then(
            function (success) {
                $scope.viewModel.formInfos = [{ "formNo": "0", "formName": "所有表單" }];
                $scope.viewModel.formInfos = $scope.viewModel.formInfos.concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 取得簽核狀態清單
    var getApplyStatusTypes = function () {
        flowEngine.getApplyStatusTypes().then(
            function (success) {
                $scope.viewModel.applyStatusTypes = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 新增加簽者
    var getNewItem = function () {
        this.addReason = "",
        this.addedType = { optionNo: 1, optionName: "使用者加簽" },
        this.addedUser = [],
        this.endTime = "2999-12-31T00:00:00+08:00",
        this.entityState = "Added",
        this.phaseName = "",
        this.phaseNo = "",
        this.seqNo = "",
        this.signStatus = { optionNo: 0, optionName: "等待簽核中" },
        this.signUser = [],
        this.signoffNo = $scope.viewModel.signoffNo,
        this.startTime = new Date(),
        this.suggestion = "",
        this.groupNo = 0
    }
    //#endregion

    //#region 取得加簽者基本資料
    var getEmpInfo = function (newItem) {
        flowEngine.getEmpInfo($scope.viewModel.searchInfo.signUser.empNo).then(
            function (success) {
                newItem.addedUser = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得seqNo
    var getSeqNo = function (i) {
        if ($scope.viewModel.recordMt.recordDts[i - 1].entityState == "Detached" || $scope.viewModel.recordMt.recordDts[i - 1].entityState == "Deleted") {
            return getSeqNo(i - 1);
        } else {
            if ($scope.viewModel.recordMt.recordDts[i].groupNo > 0) {    //平行加簽
                if ($scope.viewModel.recordMt.recordDts[i].groupNo == $scope.viewModel.recordMt.recordDts[i - 1].groupNo) {
                    var left = $scope.viewModel.recordMt.recordDts[i - 1].seqNo.substring(0, $scope.viewModel.recordMt.recordDts[i - 1].seqNo.length - 1);
                    var right = $scope.viewModel.recordMt.recordDts[i - 1].seqNo.substring($scope.viewModel.recordMt.recordDts[i - 1].seqNo.length - 1);
                    var rightInt = Number(right);
                    return left + String(rightInt + 1);
                } else {
                    return $scope.viewModel.recordMt.recordDts[i - 1].seqNo + "1";
                }
            } else {
                return $scope.viewModel.recordMt.recordDts[i - 1].seqNo + "1";
            }
        }
    }
    //#endregion

    //#region 取得離職資料
    var getRecordMt = function (signoffNo) {
        flowEngine.getRecordMt(signoffNo, $scope.shareModel.userInfos.userNo).then(
            function (success) {
                $scope.viewModel.recordMt = success.data;
                $scope.viewModel.content = JSON.parse($scope.viewModel.recordMt.content);
                $scope.viewModel.recordMt.content = JSON.parse($scope.viewModel.recordMt.content);
                for (var i = 0; i < $scope.viewModel.resignReasons.length; i++) {
                    if ($scope.viewModel.content.applyInfo.resignReason.indexOf($scope.viewModel.resignReasons[i].title) > -1) {
                        $scope.viewModel.resignReasons[i].value = "true";
                    }
                }
                if ($scope.viewModel.resignReasons[12].value == "true") {
                    $scope.viewModel.resignReasons[12].other = $scope.viewModel.content.applyInfo.resignReason.split("：")[1];
                }
                $scope.viewModel.content.applyInfo.isAgree = angular.fromJson($scope.viewModel.content.applyInfo.isAgree);
                setAttachmentGridSource($scope.viewModel.recordMt.attachments);
                $scope.getViolationMsg($scope.viewModel.content.applyInfo.resignDate);
                if ($scope.viewModel.recordMt.formInfo.formNo == 1 && $scope.viewModel.signOffItem.seqNo > '07') {
                    $scope.viewModel.hideData = true;
                } else {
                    $scope.viewModel.hideData = false;
                }
                var basicInfo = cmuhBase.stringFormat(
                                "表單編號：{0}　申請人：{1}（{2}）　職稱：{3}　部門：{4}　到職日期：{5}　申請日期：{6}",
                                success.data.signoffNo,
                                success.data.applyUser.empName,
                                success.data.applyUser.empCode,
                                success.data.applyUser.titleName,
                                success.data.applyUser.departName,
                                success.data.applyUser.hireDate.substring(0, 10),
                                success.data.applyTime.substring(0, 10));
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 存檔
    var setRecordDts = function () {
        $scope.viewModel.recordMt.content = angular.toJson($scope.viewModel.content);
        flowEngine.setRecordDts($scope.viewModel.recordMt).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜你，存檔成功");
                    //$scope.shareModel.navigation(400, 1601);
                    if ($scope.executingAppStore.appId == 208) {
                        getRecordMt($scope.viewModel.recordMt.signoffNo);
                        var tabStrip = $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip");
                        tabStrip.select(1);
                    } else {
                        $scope.removeExecuteAppStore();
                    }
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 初始加簽
    var addSignInit = function () {
        $scope.viewModel.addSign = {
            index: "",
            reason: "",
            type: "1",
            user: {
                empCode: "",
                empName: ""
            }
        };
    }
    //#endregion

    //#region 取得SignEndTime
    var getSignEndTime = function () {
        flowEngine.getSignEndTime($scope.appModel.signoffNo, $scope.appModel.seqNo).then(
            function (success) {
                $scope.viewModel.signOffItem.signEndTime = new Date(angular.fromJson(success.data));
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 上傳附件檔案
    $scope.handleFiles = function () {
        for (var i = 0, file; file = document.getElementById("attachment").files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function (file) {
                return function (e) {
                    $scope.viewModel.newAttachment = {
                        attachName: file.name,
                        base64: e.target.result,
                        systemUser: $scope.shareModel.userInfos.userNo,
                        entityState: "Added"
                    };
                    $scope.viewModel.attachmentGrid.dataSource.insert($scope.viewModel.newAttachment);
                };
            })(file);
            reader.readAsDataURL(file);
        }
    }
    //#endregion

    //#region 刪除附件檔案
    $scope.deleteAttachment = function (dataItem) {
        //if ($scope.viewModel.recordMt.signoffNo == undefined) {
        //    $scope.viewModel.attachmentGrid.dataSource.remove(dataItem);
        //}
        if (dataItem.entityState == "Unchanged") {
            dataItem.entityState = "Deleted";
        } else {
            dataItem.entityState = "Detached";
        }
    }
    //#endregion

    //#region 下載附件檔案
    $scope.downloadAttachment = function (dataItem) {
        var fileDownload = document.createElement('a');
        fileDownload.href = dataItem.base64;
        fileDownload.download = dataItem.attachName;
        fileDownload.click();
    }
    //#endregion
    //#region 列印
    $scope.print = function () {
        var printSection = document.getElementById("printSection");
        if (!printSection) {
            var printSection = document.createElement("div");
            printSection.id = "printSection";
            document.body.appendChild(printSection);
        }
        var domClone = document.getElementById("printDiv").cloneNode(true);
        printSection.appendChild(domClone);
        window.print();
        printSection.removeChild(domClone);
    }
    //#endregion
    //#region 取得簽核紀錄
    $scope.getSignoffRecord = function (isSendMail) {
        if (isSendMail == "true") {
            if (confirm("是否送簽？") != true) {
                return;
            }
        }
        $scope.viewModel.content.applyInfo.resignDate = cmuhBase.getDateFormat(new Date($scope.viewModel.content.applyInfo.resignDate));
        $scope.viewModel.content.applyInfo.modifiedDate = cmuhBase.getDateFormat(new Date($scope.viewModel.content.applyInfo.modifiedDate));
        $scope.viewModel.recordMt.content = angular.toJson($scope.viewModel.content);
        $scope.viewModel.recordMt.attachments = $scope.viewModel.attachmentGrid.dataSource.data();
        flowEngine.getSignoffRecord($scope.viewModel.recordMt, isSendMail).then(
            function (success) {
                if (success.data != "null") {
                    $scope.viewModel.recordMt = success.data;
                    $scope.viewModel.recordMt.content = JSON.parse($scope.viewModel.recordMt.content);
                    $scope.viewModel.signoffNo = $scope.viewModel.recordMt.signoffNo;
                    //var newsList = angular.copy($scope.viewModel.recordMt.recordDts);
                    //$scope.viewModel.recordMt.recordDts = newsList.filter(function (item) {
                    //    return (item.signStatus.optionNo == 1) ? false : true;
                    //});
                    var tabStrip = $("#flowEngineTabstrip").kendoTabStrip().data("kendoTabStrip");
                    tabStrip.select(1);
                    tabStrip.enable(tabStrip.tabGroup.children().eq(1));
                    //setSignoffGridSource(filterNews);
                    //$scope.signoffRecordWin.center().open();
                    if (isSendMail == "true") {
                        $scope.removeExecuteAppStore();
                    }
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 載入簽核管理頁面時執行
    $scope.setSignoff = function () {
        document.getElementById("signoffListGrid").style.height = window.innerHeight - 310 + "px";
        if (!!$scope.appModel.formNo == false) {
            var tabStrip = $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip");
            tabStrip.disable(tabStrip.tabGroup.children().eq(1));
            tabStrip.disable(tabStrip.tabGroup.children().eq(2));
            return;
        }
        //以網址開啟該畫面 http://10.20.3.60/web/?userId=A28958#appId=208&pageId=1600&signoffNo=131&formNo=1&appMode=%22A%22
        var url = "";
        $scope.viewModel.today = new Date();
        switch ($scope.appModel.formNo) {
            case "1": //人事離職單
                url = cmuhBase.stringFormat("App/ResignManager/resignApply.html?{0}", cmuhBase.getVersionNo());
                break;
        }
        if ($scope.appModel.signoffNo > 0) {
            $scope.viewModel.signoffNo = $scope.appModel.signoffNo;
            $scope.viewModel.signOffItem = {
                seqNo : $scope.appModel.seqNo
            };
            getSignEndTime();
            $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(1);
            setTimeout(function () {
                var promise = getDependencies(url);
                promise.then(function (data) {
                    $scope.viewModel.formContent = data;
                })
            }, 100);
        }
    }
    //#endregion
    
    //#region 載入簽核申請查詢頁面時執行
    $scope.setSignoffApply = function () {
        document.getElementById("signoffApplyGrid").style.height = window.innerHeight - 260 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass3").length; i++) {
            document.getElementsByClassName("contentClass3")[i].style.height = window.innerHeight - 205 + "px";
        }
    }
    //#endregion

    //#region 限制只能輸入數字，將文字取代掉
    $scope.limitInt = function () {
        if (!!$scope.viewModel.searchInfo.singoffNo == true) { $scope.viewModel.searchInfo.singoffNo = $scope.viewModel.searchInfo.singoffNo.replace(/[^\d]/g, ''); }
        if (!!$scope.viewModel.searchInfo.applyUser.empNo == true) { $scope.viewModel.searchInfo.applyUser.empNo = $scope.viewModel.searchInfo.applyUser.empNo.replace(/[^\d]/g, ''); }
        if (!!$scope.viewModel.searchInfo.signUser.empNo == true) { $scope.viewModel.searchInfo.signUser.empNo = $scope.viewModel.searchInfo.signUser.empNo.replace(/[^\d]/g, ''); }
    }
    //#endregion

    //#region 取得簽核清單
    $scope.getSignoffRecords = function () {
        flowEngine.getSignoffRecords($scope.viewModel.searchInfo).then(
            function (success) {
                setSignoffListGridSource(success.data);
                if (!!success.data.length == false) {
                    toaster.showWarningMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 取得簽核申請清單
    $scope.GetApplyRecords = function () {
        flowEngine.GetApplyRecords($scope.viewModel.searchInfo).then(
            function (success) {
                setSignoffApplyGridSource(success.data);
                if (!!success.data.length == false) {
                    toaster.showWarningMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 取得查詢申請清單
    $scope.GetTrackRecords = function () {
        flowEngine.GetTrackRecords($scope.viewModel.searchInfo).then(
            function (success) {
                setSignoffApplyGridSource(success.data);
                if (!!success.data.length == false) {
                    toaster.showWarningMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (check) {
        $scope.viewModel.select = check == "false" ? "false" : $scope.viewModel.select;
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.signoffListGrid.dataSource.data().length; i++) {
            $scope.viewModel.signoffListGrid.dataSource.data()[i].check = $scope.viewModel.select;
        }
    }
    //#endregion

    //#region 簽核
    $scope.signOff = function (signoffStatus) {
        $scope.viewModel.signOff = {
            recordMt: $scope.viewModel.recordMt,
            signoffNo: $scope.viewModel.recordMt.signoffNo,
            seqNo: $scope.viewModel.signOffItem.seqNo,
            signoffStatus: signoffStatus,
            suggestion: "",
            signUser: "" //轉簽要用的，empNo
        };
        if (signoffStatus == 20) {
            $scope.viewModel.transferWindow.center().open();
        } else {
            $scope.viewModel.confirmWindow.center().open();
        }
    }
    //#endregion

    //#region 簽核
    $scope.signoffOk = function () {
        $scope.viewModel.content.applyInfo.resignDate = cmuhBase.getDateFormat(new Date($scope.viewModel.content.applyInfo.resignDate));
        $scope.viewModel.content.applyInfo.modifiedDate = cmuhBase.getDateFormat(new Date($scope.viewModel.content.applyInfo.modifiedDate));
        $scope.viewModel.signOff.recordMt.content = angular.toJson($scope.viewModel.content);
        $scope.viewModel.signOff.recordMt.attachments = $scope.viewModel.attachmentGrid.dataSource.data();
        flowEngine.signoff($scope.viewModel.signOff).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，簽核成功");
                } else {
                    toaster.showErrorMessage("對不起，簽核失敗");
                }
                $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(0);
                var tabStrip = $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip");
                tabStrip.disable(tabStrip.tabGroup.children().eq(1));
                tabStrip.disable(tabStrip.tabGroup.children().eq(2));
                $scope.getSignoffRecords();
                $scope.viewModel.confirmWindow.close();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 多筆簽核
    $scope.signRecords = function (signoffStatus) {
        var newsList = angular.copy($scope.viewModel.signoffListGrid.dataSource.data());
        var filterNews = newsList.filter(function (item) {
            return (item.check == "true") ? true : false;
        });
        if (filterNews.length == 0) {
            toaster.showErrorMessage("對不起，請先選擇資料");
            return;
        }
        $scope.viewModel.signRecords = {
            signRecords: filterNews,
            signoffStatus: signoffStatus
        };
        var result = signoffStatus == 60 ? "同意" : "否決";
        if (confirm("確認" + result + "這 " + filterNews.length + " 筆資料嗎？") == false) {
            return;
        }
        $scope.signRecordsOk();
    }
    //#endregion

    //#region 多筆簽核
    $scope.signRecordsOk = function () {
        flowEngine.signRecords($scope.viewModel.signRecords).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，簽核成功");
                } else {
                    toaster.showErrorMessage("對不起，簽核失敗");
                }
                $scope.getSignoffRecords();
                $scope.viewModel.confirmWindow.close();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 轉簽
    $scope.transferOk = function () {
        $scope.viewModel.signOff.suggestion = $scope.viewModel.transfer.suggestion;
        $scope.viewModel.signOff.signUser = $scope.viewModel.transfer.user.empNo;
        flowEngine.transferSign($scope.viewModel.signOff).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，轉簽成功");
                } else {
                    toaster.showErrorMessage("對不起，轉簽失敗");
                }
                $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(0);
                var tabStrip = $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip");
                tabStrip.disable(tabStrip.tabGroup.children().eq(1));
                tabStrip.disable(tabStrip.tabGroup.children().eq(2));
                $scope.getSignoffRecords();
                $scope.viewModel.transferWindow.close();
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 加簽-新增
    $scope.addSign = function () {
        if (($scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].signStatus.optionNo == 10 ||
            $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].signStatus.optionNo == 11) &&
            $scope.viewModel.addSign.type == "3") {
            toaster.showErrorMessage("簽核中無法往上加簽，不好意思");
            addSignInit();
            return;
        }
        if ($scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].signStatus.optionNo == 2) {
            if ($scope.viewModel.addSign.type == "1" || $scope.viewModel.addSign.type == "3") {
                toaster.showErrorMessage("無法在平行簽核位置使用往下或往上加簽，不好意思");
                addSignInit();
                return;
            }
        }
        var newItem = new getNewItem();
        getEmpInfo(newItem);
        newItem.signUser = $scope.viewModel.addSign.user;
        newItem.addReason = $scope.viewModel.addSign.reason;
        $scope.viewModel.addSign.index = parseInt($scope.viewModel.addSign.index);
        switch ($scope.viewModel.addSign.type) {
            case "1": //往下
                $scope.viewModel.recordMt.recordDts = $scope.viewModel.recordMt.recordDts.slice(0, $scope.viewModel.addSign.index + 1).concat(newItem).concat($scope.viewModel.recordMt.recordDts.slice($scope.viewModel.addSign.index + 1));
                break;
            case "2": //平行
                newItem.signStatus.optionNo = 2;
                newItem.signStatus.optionName = "平行簽核";
                if ($scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].entityState == "Unchanged") {
                    $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].entityState = "Modified";
                }
                $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].signStatus.optionNo = 2;
                $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].signStatus.optionName = "平行簽核";
                if ($scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].groupNo > 0) {
                    newItem.groupNo = $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].groupNo;
                } else {
                    $scope.viewModel.groupNo += 1;
                    $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].groupNo = $scope.viewModel.groupNo;
                    newItem.groupNo = $scope.viewModel.groupNo;
                }
                if ($scope.viewModel.addSign.index + 1 == $scope.viewModel.recordMt.recordDts.length) {
                    $scope.viewModel.recordMt.recordDts = $scope.viewModel.recordMt.recordDts.slice(0, $scope.viewModel.addSign.index + 1).concat(newItem);
                } else {
                    for (var i = $scope.viewModel.addSign.index + 1; i < $scope.viewModel.recordMt.recordDts.length; i++) {
                        if ($scope.viewModel.recordMt.recordDts[i].groupNo != $scope.viewModel.recordMt.recordDts[$scope.viewModel.addSign.index].groupNo) {
                            $scope.viewModel.recordMt.recordDts = $scope.viewModel.recordMt.recordDts.slice(0, i).concat(newItem).concat($scope.viewModel.recordMt.recordDts.slice(i));
                            break;
                        }
                    }
                }
                break;
            case "3": //往上
                if ($scope.viewModel.addSign.index == 0) {   //加到最上面
                    $scope.viewModel.recordMt.recordDts = [].concat(newItem).concat($scope.viewModel.recordMt.recordDts);
                } else {
                    $scope.viewModel.recordMt.recordDts = $scope.viewModel.recordMt.recordDts.slice(0, $scope.viewModel.addSign.index).concat(newItem).concat($scope.viewModel.recordMt.recordDts.slice($scope.viewModel.addSign.index));
                }
                break;
        }
        addSignInit();
    }
    //#endregion

    //#region 加簽-刪除
    $scope.delete = function (index) {
        if ($scope.viewModel.recordMt.recordDts[index].addedType.optionNo == 0) {
            return;
        }
        if ($scope.viewModel.recordMt.recordDts[index].entityState == "Unchanged") {
            $scope.viewModel.recordMt.recordDts[index].entityState = "Deleted";
        } else {
            $scope.viewModel.recordMt.recordDts[index].entityState = "Detached";
        }
        if ($scope.viewModel.recordMt.recordDts[index].groupNo > 0) {
            var newsList = angular.copy($scope.viewModel.recordMt.recordDts);
            var filterNews = newsList.filter(function (item) {
                return (item.groupNo == $scope.viewModel.recordMt.recordDts[index].groupNo ? true : false);
            });
            if (filterNews.length == 2) {
                for (var i = 0; i < $scope.viewModel.recordMt.recordDts.length; i++) {
                    if ($scope.viewModel.recordMt.recordDts[i].groupNo == $scope.viewModel.recordMt.recordDts[index].groupNo) {
                        $scope.viewModel.recordMt.recordDts[i].signStatus.optionNo = 0;
                        $scope.viewModel.recordMt.recordDts[i].signStatus.optionName = "等待簽核中";
                        $scope.viewModel.recordMt.recordDts[i].groupNo = 0;
                    }
                }
            }
        }
    }
    //#endregion

    //#region 加簽-取得員工基本資料
    $scope.getEmpInfo = function (item) {
        if (!!item.user.empCode == false) {
            empCode = "0";
        } else {
            empCode = item.user.empCode;
        }
        if (Number.isInteger(empCode) == false) {
            empCode = empCode.replace(/[^\d]/g, '');
        }
        flowEngine.getEmpInfo(empCode).then(
            function (success) {
                if (success.data != "null") {
                    item.user = success.data;
                } else {
                    item.user.empName = "";
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 剛載入tabstrip時執行，將簽核頁面鎖住
    $scope.flowEngineTabstripInit = function () {
        var tabStrip = $("#flowEngineTabstrip").kendoTabStrip().data("kendoTabStrip");
        tabStrip.disable(tabStrip.tabGroup.children().eq(1));
    }
    //#endregion

    //#region 簽核完成
    $scope.signature = function () {
        debugger;
        if (confirm("是否送簽？") != true) {
            return;
        }
        for (var i = 0; i < $scope.viewModel.recordMt.recordDts.length; i++) {
            if (!!$scope.viewModel.recordMt.recordDts[i].seqNo == true && $scope.viewModel.recordMt.recordDts[i].seqNo.length == 5 && $scope.viewModel.recordMt.recordDts[i].seqNo != "00001") {
                continue;
            }
            if ($scope.viewModel.recordMt.recordDts[i].entityState == "Detached" || $scope.viewModel.recordMt.recordDts[i].entityState == "Deleted") {
                continue;
            }
            if (i == 0) {
                $scope.viewModel.recordMt.recordDts[i].seqNo = "00001";
            } else {
                $scope.viewModel.recordMt.recordDts[i].seqNo = getSeqNo(i);
            }
        }
        setRecordDts();
    }
    //#endregion
    
    //#region 跳到加簽設定畫面
    $scope.addSignOff = function () {
        $("#signoffManagerTabstrip").kendoTabStrip().data("kendoTabStrip").select(2);
    }
    //#endregion

    //#region 簽核鎖定
    $scope.signOffDisabled = function () {
        if (!!$scope.viewModel.signOffItem == false) {
            return false;
        }
        if (new Date($scope.viewModel.signOffItem.signEndTime).getTime() < $scope.viewModel.today.getTime()) {
            return true;
        }
        if (!!$scope.viewModel.recordMt == false) {
            return false;
        }
        if ($scope.viewModel.recordMt.applyStatus.optionNo == 80) {
            return true;
        }
        return false;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);