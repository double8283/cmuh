﻿angular.module('cmuh').service('flowEngine', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得表單清單
    this.getFormInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetFormInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得簽核清單
    this.getSignoffRecords = function (searchInfo) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/GetSignoffRecords", searchInfo);
    }
    //#endregion

    //#region 取得簽核清單
    this.GetApplyRecords = function (searchInfo) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/GetApplyRecords", searchInfo);
    }
    //#endregion
    
    //#region 取得查詢清單
    this.GetTrackRecords = function (searchInfo) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/GetTrackRecords", searchInfo);
    }
    //#endregion

    //#region 取得簽核項目
    this.getRecordDtSeqNos = function (signoffNo) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetRecordDtSeqNos/{0}", signoffNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得表單資料
    this.getRecordMt = function (signoffNo, userNo) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetRecordMt/{0}/{1}", signoffNo, userNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得簽核紀錄
    this.getSignoffRecord = function (recordMt, isSendMail) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetSignoffRecord/{0}", isSendMail);
        return $http.put(url, recordMt);
    }
    //#endregion

    //#region 取得簽核狀態清單
    this.getApplyStatusTypes = function () {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetApplyStatusTypes" );
        return $http.get(url);
    }
    //#endregion

    //#region 簽核
    this.signoff = function (signoff) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/Signoff", signoff);
    }
    //#endregion

    //#region 多筆簽核
    this.signRecords = function (signoff) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/SignRecords", signoff);
    }
    //#endregion
    
    //#region 轉簽
    this.transferSign = function (signoff) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/TransferSign", signoff);
    }
    //#endregion
   
    //#region 取得員工基本資料
    this.getEmpInfo = function (empCode) {
        var url = cmuhBase.stringFormat("../WebApi/HumanResource/ResignManagerService/GetEmpInfo/{0}", empCode);
        return $http.get(url);
    }
    //#endregion

    //#region 存檔
    this.setRecordDts = function (recordMt) {
        return $http.put("../WebApi/FlowEngine/FlowEngineService/SetRecordDts", recordMt);
    }
    //#endregion

    //#region 取得SignEndTime
    this.getSignEndTime = function (signoffNo, seqNo) {
        var url = cmuhBase.stringFormat("../WebApi/FlowEngine/FlowEngineService/GetSignEndTime/{0}/{1}", signoffNo, seqNo);
        return $http.get(url);
    }
    //#endregion
}]);