﻿angular.module('cmuh').service('maintenanceStatistics', ["$http", "cmuhBase", function ($http, cmuhBase) {
   
    //#region 取得統計報告資料-1
    this.getReport1 = function (departNo, startDate, endDate) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FaciltiesCheckListService/GetReport1/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

}]);