﻿angular.module("cmuh").controller("maintenanceStatisticsController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenanceStatistics", function ($scope, $http, $q, cmuhBase, toaster, maintenanceStatistics) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");
        
        //申請日期起始
        $scope.viewModel.startDate = new Date();
        $scope.viewModel.endDate = new Date();

        //統計報表Grid
        setReportGrid1Options();
        setReportChart1Options();
        setReportGrid2Options();
        setReportGrid3Options();
        setReportChart3Options();
        setReportGrid4Options();
        setReportChart4Options();
        setReportGrid5Options();
        setReportGrid6Options();
        setReportChart6Options();
        setReportGrid7Options();

        var data = [
            { xxx: "第 1 筆", yyy: 10 },
            { xxx: "第 2 筆", yyy: 40 },
            { xxx: "第 3 筆", yyy: 1 },
            { xxx: "第 4 筆", yyy: 8 },
            { xxx: "第 5 筆", yyy: 126 }
        ];
        setReportChart1Source(data);
        setReportChart3Source(data);
        setReportChart4Source(data);
        setReportChart6Source(data);

        //設定高度
        document.getElementById("tabstrip").style.height = window.innerHeight - 260 + "px";
        document.getElementById("reportGrid1").style.height = window.innerHeight - 590 + "px";
        document.getElementById("reportChart1").style.height = window.innerHeight - 650 + "px";
        document.getElementById("reportGrid2").style.height = window.innerHeight - 290 + "px";
        document.getElementById("reportGrid3").style.height = window.innerHeight - 590 + "px";
        document.getElementById("reportChart3").style.height = window.innerHeight - 650 + "px";
        document.getElementById("reportGrid4").style.height = window.innerHeight - 590 + "px";
        document.getElementById("reportChart4").style.height = window.innerHeight - 650 + "px";
        document.getElementById("reportGrid5").style.height = window.innerHeight - 290 + "px";
        document.getElementById("reportGrid6").style.height = window.innerHeight - 590 + "px";
        document.getElementById("reportChart6").style.height = window.innerHeight - 650 + "px";
        document.getElementById("reportGrid7").style.height = window.innerHeight - 290 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 240 + "px";
        }
    }
    //#endregion

    //#region 設定ReportGrid1
    var setReportGrid1Options = function () {
        $scope.viewModel.reportGrid1Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "設備大類別", width: "350px" },
                { field: "", title: "報修數量" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid1 DataSource
    var setReportGrid1Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid1Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid1").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid1Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportChart1
    var setReportChart1Options = function () {
        $scope.viewModel.reportChart1Options = {
            dataSource: new kendo.data.DataSource(),
            series: [{
                field: "yyy"    //值
            }],
            categoryAxis: {
                field: "xxx"    //分類軸（下方的X軸）
            }
        };
    }
    //#endregion

    //#region 設定ReportChart1 DataSource
    var setReportChart1Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportChart1Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportChart1").data("kendoChart");
            if (grid == null) {
                $scope.viewModel.reportChart1Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid2
    var setReportGrid2Options = function () {
        $scope.viewModel.reportGrid2Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "設備大類別", width: "350px" },
                { field: "", title: "報修數量", width: "350px" },
                { field: "", title: "委外數量" },
                { field: "", title: "核銷數量" },
                { field: "", title: "完成數量" },
                { field: "", title: "完成率" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid2 DataSource
    var setReportGrid2Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid2Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid2").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid2Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid3
    var setReportGrid3Options = function () {
        $scope.viewModel.reportGrid3Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "申請日期", width: "350px" },
                { field: "", title: "棟別", width: "350px" },
                { field: "", title: "申請單位" },
                { field: "", title: "設備大類別" },
                { field: "", title: "設備中類別" },
                { field: "", title: "補充說明" },
                { field: "", title: "處理人" },
                { field: "", title: "完成日" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid3 DataSource
    var setReportGrid3Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid3Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid3").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid3Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportChart3
    var setReportChart3Options = function () {
        $scope.viewModel.reportChart3Options = {
            dataSource: new kendo.data.DataSource(),
            series: [{
                field: "yyy"    //值
            }],
            categoryAxis: {
                field: "xxx"    //分類軸（下方的X軸）
            }
        };
    }
    //#endregion

    //#region 設定ReportChart3 DataSource
    var setReportChart3Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportChart3Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportChart3").data("kendoChart");
            if (grid == null) {
                $scope.viewModel.reportChart3Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid4
    var setReportGrid4Options = function () {
        $scope.viewModel.reportGrid4Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "申請日期", width: "350px" },
                { field: "", title: "棟別", width: "350px" },
                { field: "", title: "申請單位" },
                { field: "", title: "設備大類別" },
                { field: "", title: "設備中類別" },
                { field: "", title: "補充說明" },
                { field: "", title: "處理人" },
                { field: "", title: "完成日" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid4 DataSource
    var setReportGrid4Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid4Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid4").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid4Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportChart4
    var setReportChart4Options = function () {
        $scope.viewModel.reportChart4Options = {
            dataSource: new kendo.data.DataSource(),
            series: [{
                type: "line",   //沒加這行就是直條圖，有加這行就是折線圖
                field: "yyy"    //值
            }],
            categoryAxis: {
                field: "xxx"    //分類軸（下方的X軸）
            }
        };
    }
    //#endregion

    //#region 設定ReportChart4 DataSource
    var setReportChart4Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportChart4Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportChart4").data("kendoChart");
            if (grid == null) {
                $scope.viewModel.reportChart4Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid5
    var setReportGrid5Options = function () {
        $scope.viewModel.reportGrid5Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "維修大類別", width: "350px" },
                { field: "", title: "報修數量", width: "350px" },
                { field: "", title: "核銷數量" },
                { field: "", title: "完成數量" },
                { field: "", title: "完成率" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid5 DataSource
    var setReportGrid5Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid5Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid5").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid5Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid6
    var setReportGrid6Options = function () {
        $scope.viewModel.reportGrid6Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "設備大類別", width: "350px" },
                { field: "", title: "設備中類別", width: "350px" },
                { field: "", title: "故障情形" },
                { field: "", title: "維修內容" },
                { field: "", title: "使用的零件" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid6 DataSource
    var setReportGrid6Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid6Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid6").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid6Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportChart6
    var setReportChart6Options = function () {
        $scope.viewModel.reportChart6Options = {
            dataSource: new kendo.data.DataSource(),
            series: [{
                field: "yyy"    //值
            }],
            categoryAxis: {
                field: "xxx"    //分類軸（下方的X軸）
            }
        };
    }
    //#endregion

    //#region 設定ReportChart6 DataSource
    var setReportChart6Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportChart6Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportChart6").data("kendoChart");
            if (grid == null) {
                $scope.viewModel.reportChart6Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 設定ReportGrid7
    var setReportGrid7Options = function () {
        $scope.viewModel.reportGrid7Options = {
            sortable: true,
            scrollable: false,
            columns: [
                { field: "", title: "處理人員", width: "350px" },
                { field: "", title: "3日完成率", width: "350px" },
                { field: "", title: "7日完成率" },
                { field: "", title: "7日以上完成率" },
                { field: "", title: "未完成率" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ReportGrid7 DataSource
    var setReportGrid7Source = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        $scope.viewModel.reportGrid7Options.dataSource = dataSource;
        setTimeout(function () {
            var grid = $("#reportGrid7").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.reportGrid7Options.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 匯出
    $scope.export = function () {
        var data = [{ cells: [{ value: "部門名稱" }, { value: "地點名稱" }, { value: "良好" }, { value: "異常" }, { value: "未檢查" }] }];
        for (var i = 0; i < $scope.viewModel.dayListGridOptions.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].departName },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].locationName },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'true' ? '●' : "" },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'false' ? '●' : "" },
                    { value: $scope.viewModel.dayListGridOptions.dataSource.data()[i].status == 'na' ? '●' : "" }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: cmuhBase.getDateFormat($scope.viewModel.checkDay) + "-日統計報表.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);