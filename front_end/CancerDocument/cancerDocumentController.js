﻿angular.module("cmuh").controller("cancerDocumentController", ["$rootScope", "$scope", "$q", "$http", "$timeout", "$window", "$modal", "toaster", "cmuhBase", "cancerDocument", "$sce", function ($rootScope, $scope, $q, $http, $timeout, $window, $modal, toaster, cmuhBase, cancerDocument, $sce) {

    //#region 方法

    //#region 設定資料結構
    var initViewModel = function (pageModel) {

        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //如果沒抓到病人資料就回到病人選取頁面
        if ($scope.appModel.chartNo == undefined && $scope.appModel.patientInfos == undefined) {
            toaster.showErrorMessage("請先選擇病人");
            if ($scope.executingAppStore.appId == 75) {
                $scope.shareModel.navigation(75, 151);
            } else {
                $scope.shareModel.navigation(74, 151);
            }
        }

        //表單select
        $scope.viewModel.formList = [
            { formNo: 0, formName: "---請選擇表單---", documentFolderName: "", documentBodyName: "", versionId: "" },
            { formNo: 1, documentType: 3535, formName: "吞嚥評估單", documentFolderName: "RehabilitationDocument/SwallowingAssessment", projectName: "SwallowingAssessment", documentBodyName: "swallowingAssessmentBody", versionId: "1.0.0" },
            { formNo: 2, documentType: 3536, formName: "吞嚥攝影紀錄單", documentFolderName: "RehabilitationDocument/SwallowingPhotography", projectName: "SwallowingPhotography", documentBodyName: "swallowingPhotographyBody", versionId: "1.0.0" },
            { formNo: 3, documentType: 3537, formName: "國語構音評量表", documentFolderName: "RehabilitationDocument/Pronunciation", projectName: "Pronunciation", documentBodyName: "pronunciationBody", versionId: "1.0.0" },
            { formNo: 4, documentType: 3538, formName: "功能性由口進食量表", documentFolderName: "RehabilitationDocument/FOIS", projectName: "FOIS", documentBodyName: "foisBody", versionId: "1.0.0" },
            { formNo: 5, documentType: 3539, formName: "UW-QQLV4生活品質問卷", documentFolderName: "RehabilitationDocument/LifeQuality", projectName: "LifeQuality", documentBodyName: "lifeQualityBody", versionId: "1.0.0" },
            { formNo: 6, documentType: 3540, formName: "EAT-10中文評量", documentFolderName: "RehabilitationDocument/EAT10", projectName: "EAT10", documentBodyName: "eat10Body", versionId: "1.0.0" },
            { formNo: 7, documentType: 3541, formName: "運動性言語障礙評估表", documentFolderName: "RehabilitationDocument/SpeechImpairment", projectName: "SpeechImpairment", documentBodyName: "speechImpairmentBody", versionId: "1.0.0" },
            { formNo: 8, documentType: 3542, formName: "嗓音聽覺共識評量表", documentFolderName: "ENTDocument/VoiceCape", projectName: "VoiceCape", documentBodyName: "voiceCapeBody", versionId: "1.0.0" },
            { formNo: 9, documentType: 3543, formName: "耳鼻喉部語言檢查表1", documentFolderName: "ENTDocument/LanguageCheck1", projectName: "LanguageCheck1", documentBodyName: "languageCheck1Body", versionId: "1.0.0" },
            { formNo: 10, documentType: 3544, formName: "耳鼻喉部語言檢查表2", documentFolderName: "ENTDocument/LanguageCheck2", projectName: "LanguageCheck2", documentBodyName: "languageCheck2Body", versionId: "1.0.0" },
            { formNo: 11, documentType: 3545, formName: "Dysphagia_Assessment", documentFolderName: "ENTDocument/DysphagiaAssessment", projectName: "DysphagiaAssessment", documentBodyName: "dysphagiaAssessmentBody", versionId: "1.0.0" },
            { formNo: 12, documentType: 3546, formName: "聲帶功能異常評估表", documentFolderName: "ENTDocument/VocalDysfunction", projectName: "VocalDysfunction", documentBodyName: "vocalDysfunctionBody", versionId: "1.0.0" },
            { formNo: 13, documentType: 3547, formName: "逆流症狀指數問卷", documentFolderName: "ENTDocument/RefluxSymptom", projectName: "RefluxSymptom", documentBodyName: "refluxSymptomBody", versionId: "1.0.0" },
            { formNo: 14, documentType: 3548, formName: "歌唱障礙指數量表", documentFolderName: "ENTDocument/SingingHandicap", projectName: "SingingHandicap", documentBodyName: "singingHandicapBody", versionId: "1.0.0" },
            { formNo: 15, documentType: 3549, formName: "Stroboscopic_Evaluation", documentFolderName: "ENTDocument/StrobosEvaluation", projectName: "StrobosEvaluation", documentBodyName: "strobosEvaluationBody", versionId: "1.0.0" },
            { formNo: 16, documentType: 3550, formName: "音聲評估量表", documentFolderName: "ENTDocument/VoiceEvaluation", projectName: "VoiceEvaluation", documentBodyName: "voiceEvaluationBody", versionId: "1.0.0" },
            { formNo: 17, documentType: 3551, formName: "嗓音障礙指數量表", documentFolderName: "ENTDocument/VoiceHandicap", projectName: "VoiceHandicap", documentBodyName: "voiceHandicapBody", versionId: "1.0.0" }
        ];
        $scope.viewModel.selectFormNo = 0;
        $scope.viewModel.selectFormType = '復健科';
        $scope.viewModel.formUrl = "";
        $scope.viewModel.templateUrl = "";
        $scope.viewModel.documentList = "";
        $scope.viewModel.document = { documentType: 0 };

        // 點選焦點變色索引
        $scope.viewModel.selectedRowIndex = null;

        //左方清單變數
        $scope.viewModel.isShowLeftPanel = true;
        $scope.viewModel.documentBodyCSS = "";
        document.getElementById("contentDiv").style.height = window.innerHeight - 185 + "px";
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 220 + "px";
        } 
        for (var i = 0; i < document.getElementsByClassName("contentClass3").length; i++) {
            document.getElementsByClassName("contentClass3")[i].style.height = window.innerHeight - 240 + "px";
        }

        if ($scope.appModel.chartNo !== undefined) {
            if ($scope.executingAppStore.appId == 75) {
                $scope.viewModel.selectFormType = '耳鼻喉科';
                $scope.viewModel.selectFormNo = 15;
            } else {
                $scope.viewModel.selectFormNo = 2;
            }
            getPatientInfos($scope.appModel.chartNo,$scope.appModel.caseType);
            $scope.getFormUrl();
        }
    }
    //#endregion

    //#region 取得表單內容
    var getDocumentBodyTemplate = function () {
        // 組合 dependencies.json 路徑
        var url = cmuhBase.stringFormat("App/{0}/{1}/dependencies.json?{2}", $scope.shareModel.documentFolderName, $scope.shareModel.versionId, cmuhBase.getVersionNo());
        cmuhBase.getDependencies(url).then(
            function (success) {
                cmuhBase.loadDependencies(success.data).then(
                    function (success) {
                        $scope.viewModel.templateUrl = cmuhBase.stringFormat("App/{0}/{1}/{2}.html?{3}", $scope.shareModel.documentFolderName, $scope.shareModel.versionId, $scope.shareModel.documentBodyName, cmuhBase.getVersionNo());
                    },
                    function (error) {
                        toaster.showErrorMessage("對不起，連線失敗");
                    }
                );
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得預設文件(預設取最新版本)
    var getDefaultDocumentInfo = function () {
        var url = cmuhBase.stringFormat("App/{0}/{1}/{2}.json?{3}", $scope.shareModel.documentFolderName, $scope.shareModel.versionId, $scope.shareModel.documentBodyName, cmuhBase.getVersionNo());
        cancerDocument.getDefaultDocumentInfo(url).then(
            function (success) {
                $scope.viewModel.document = success.data;
                $scope.viewModel.document.documentType = $scope.shareModel.documentType;
                $scope.viewModel.document.branchNo = $scope.shareModel.branchNo;
                $scope.viewModel.document.versionId = $scope.shareModel.versionId;
                $scope.viewModel.document.createUser = { empNo: $scope.shareModel.userInfos.userNo };
                $scope.viewModel.document.body.name.value = $scope.appModel.patientInfos.ptName;
                $scope.viewModel.document.body.chartNo.value = $scope.appModel.patientInfos.chartNo;
                $scope.viewModel.document.body.sex.value = $scope.appModel.patientInfos.sex.optionName;
                $scope.viewModel.document.body.birthday.value = $scope.appModel.patientInfos.birthday.substring(0,10);
                $scope.viewModel.document.body.age.value = $scope.appModel.patientInfos.age;
                $scope.viewModel.document.body.date.value = $scope.viewModel.document.body.date.value == "" ? new Date() : $scope.viewModel.document.body.date.value;
                $scope.viewModel.document.createTime = new Date();
            },
            function (error) {
                toaster.showErrorMessage("對不起，無法預設值");
            }
        );
    }
    //#endregion

    //#region 取得左邊文件清單
    var getDocumentList = function () {
        cancerDocument.getDocumentList($scope.appModel.patientInfos.idNo, $scope.viewModel.document.documentType, $scope.appModel.caseType).then(
            function (success) {
                if (success.data != null || success.data != undefined) {
                    $scope.viewModel.documentList = success.data;
                    if ($scope.viewModel.documentList.length != 0) {
                        $scope.getDocumentInfo($scope.viewModel.documentList[0], 0);
                    }
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    }
    //#endregion

    //#region 取得 Stroboscopic Evaluation 影片
    var getVideoPaths = function (documentNo) {
        cancerDocument.getVideoPaths(documentNo).then(
            function (success) {
                $scope.viewModel.videoPaths = success.data;
                for (var i = 0; i < $scope.viewModel.videoPaths.length; i++) {
                    $scope.viewModel.videoPaths[i] = $sce.trustAsResourceUrl($scope.viewModel.videoPaths[i]);
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 取得病人資料
    var getPatientInfos = function (chartNo, caseType) {
        debugger;
        cancerDocument.getPatientInfos(chartNo, caseType).then(
            function (success) {
                if (success.data == "null") {
                    toaster.showErrorMessage("對不起，沒有此病人資料，請先選擇病人");
                    if ($scope.executingAppStore.appId == 75) {
                        $scope.shareModel.navigation(75, 151);
                    } else {
                        $scope.shareModel.navigation(74, 151);
                    }
                } else {
                    $scope.appModel.patientInfos = success.data;
                    var basicInfo = cmuhBase.stringFormat(
                                            "{0} {1} {2} {3} ({4}歲)",
                                            $scope.appModel.patientInfos.ptName,
                                            $scope.appModel.patientInfos.chartNo,
                                            $scope.appModel.patientInfos.sex.optionName,
                                            $scope.appModel.patientInfos.birthday.substring(0, 10),
                                            $scope.appModel.patientInfos.age);
                    $scope.shareModel.banner.showBanner(true, null, $scope.appModel.patientInfos.sex.optionName, basicInfo, "", "");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 取得表單頁面
    $scope.getFormUrl = function () {
        if ($scope.viewModel.selectFormNo == 0) {
            return;
        }
        $scope.shareModel.documentFolderName = $scope.viewModel.formList[$scope.viewModel.selectFormNo].documentFolderName;
        $scope.shareModel.documentBodyName = $scope.viewModel.formList[$scope.viewModel.selectFormNo].documentBodyName;
        $scope.shareModel.versionId = $scope.viewModel.formList[$scope.viewModel.selectFormNo].versionId;
        $scope.shareModel.documentTypeName = $scope.viewModel.formList[$scope.viewModel.selectFormNo].formName;
        $scope.shareModel.documentType = $scope.viewModel.formList[$scope.viewModel.selectFormNo].documentType;
        $scope.shareModel.projectName = $scope.viewModel.formList[$scope.viewModel.selectFormNo].projectName;
        $scope.shareModel.branchNo = 0;
        cmuhBase.getDependencies(cmuhBase.stringFormat("App/CancerDocument/dependencies.json?{0}", cmuhBase.getVersionNo())).then(
            function (success) {
                cmuhBase.loadDependencies(success.data).then(
                    function (success) {
                        $scope.viewModel.formUrl = cmuhBase.stringFormat("App/CancerDocument/cancerDocument.html?{0}", cmuhBase.getVersionNo());
                        // 取得預設文件
                        getDefaultDocumentInfo();
                        // 取得表單內容
                        getDocumentBodyTemplate();
                        // 取得左邊文件清單
                        setTimeout(function () {
                            getDocumentList();
                        }, 300);
                    },
                    function (error) {
                        toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'頁面發生失敗", $scope.viewModel.formList[$scope.viewModel.selectFormNo].formName));
                    }
                );
            },
            function (error) {
                toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'系統資訊發生失敗", $scope.viewModel.formList[$scope.viewModel.selectFormNo].formName));
            });
    }
    //#endregion

    //#region 顯示左區域
    $scope.showLeftPanel = function () {
        if ($scope.viewModel.isShowLeftPanel == false) {
            $scope.viewModel.isShowLeftPanel = true;
            $scope.viewModel.documentBodyCSS = "";
        } else {
            $scope.viewModel.isShowLeftPanel = false;
            $scope.viewModel.documentBodyCSS = "template-w100";
        }
    };
    //#endregion

    //#region 取得文件資料
    $scope.getDocumentInfo = function (documentInfo, selectedRowIndex) {
        // 此行觸發CSS變色條件(selected)
        $scope.viewModel.selectedRowIndex = selectedRowIndex;
        // 1. 取得版號並組合對應版本的 dependencies.json 路徑
        cancerDocument.getDocumentInfo(documentInfo.documentNo).then(
            function (success1) {
                if (success1.data != null || success1.data != undefined) {
                    // 2. 抓取對應版本的 documentBodyController
                    var url = cmuhBase.stringFormat("App/{0}/{1}/dependencies.json?{2}", $scope.shareModel.documentFolderName, success1.data.versionId, cmuhBase.getVersionNo());
                    cmuhBase.getDependencies(url).then(
                        function (success2) {
                            cmuhBase.loadDependencies(success2.data).then(
                                function (success3) {
                                    // 3. 切換至對應版本的 documentBody.html, 並將document.body 對應至新內容
                                    $scope.viewModel.document = success1.data;
                                    $scope.viewModel.document.body = JSON.parse(success1.data.body);
                                    if ($scope.viewModel.document.body.babyRecord != undefined) {
                                        if (cmuhBase.isNullOrEmpty($scope.viewModel.document.body.babyRecord[0])) {
                                            $scope.viewModel.document.body.babyRecord = [].concat($scope.viewModel.document.body.babyRecord);
                                        }
                                    }
                                    if ($scope.viewModel.document.body.audio != undefined) {
                                        $scope.viewModel.document.body.audio.value = $sce.trustAsResourceUrl($scope.viewModel.document.body.audio.value);
                                    }
                                    if ($scope.viewModel.document.documentType == 3549) {
                                        getVideoPaths($scope.viewModel.document.documentNo);
                                    }
                                    $scope.goTop();
                                },
                                function (error) {
                                    toaster.showErrorMessage("對不起，連線失敗");
                                }
                            );
                        },
                        function (error) {
                            toaster.showErrorMessage("對不起，連線失敗");
                        }
                    );
                } else {
                    toaster.showErrorMessage("對不起，載入失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 畫面至頂
    $scope.goTop = function () {
        $window.scrollTo(0, 0);
    }
    //#endregion

    //#region 新增
    $scope.new = function () {
        $scope.viewModel.selectedRowIndex = null;
        getDefaultDocumentInfo();   //取得預設文件
        getDocumentBodyTemplate();  //取得表單內容
    };
    //#endregion

    //#region 儲存
    $scope.save = function () {
        if ($scope.shareModel.userInfos.userNo != "28958" && $scope.shareModel.userInfos.userNo != "28034") {
            if ($scope.viewModel.document.documentNo != 0 && $scope.viewModel.document.createUser.empNo != $scope.shareModel.userInfos.userNo) {
                toaster.showErrorMessage("使用者不同，禁止存檔");
                return;
            }
        }
        $scope.viewModel.document.caseNo = $scope.appModel.patientInfos.caseNo;
        $scope.viewModel.document.createUser = { empNo: $scope.shareModel.userInfos.userNo };
        $scope.viewModel.document.tranStatus = 0;
        $scope.viewModel.document.branchNo = 0;
        $scope.viewModel.document.templateUrl = $scope.viewModel.templateUrl;
        cancerDocument.setDocumentInfo($scope.viewModel.document).then(
            function (success) {
                if (success.data != "0") {
                    toaster.showSuccessMessage("恭喜您，存檔成功");
                    getDocumentList();
                    $scope.viewModel.selectedRowIndex = null;
                    $scope.new();
                    $scope.viewModel.document.documentNo = success.data;
                } else {
                    toaster.showErrorMessage("對不起，存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        )
    };
    //#endregion

    //#region 刪除
    $scope.delete = function () {
        if (confirm("確認刪除嗎？") != true) {
            return;
        }
        if ($scope.viewModel.document.documentNo != 0 && $scope.viewModel.document.createUser.empNo != $scope.shareModel.userInfos.userNo) {
            toaster.showErrorMessage("使用者不同，禁止刪除");
            return;
        }
        cancerDocument.deleteDocumentInfo($scope.viewModel.document.documentNo).then(
        function (success) {
            if (success.data == "true") {
                toaster.showSuccessMessage("恭喜您，刪除成功");
                getDocumentList();
                $scope.viewModel.selectedRowIndex = null;
                $scope.new();
            } else {
                toaster.showErrorMessage("對不起，刪除失敗");
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        }
    )
    };
    //#endregion

    //#region 列印
    $scope.print = function () {
        //var divToPrint = document.getElementById("printTable");
        //newWin = window.open("");
        //newWin.document.write(divToPrint.outerHTML);
        //newWin.print();
        //newWin.close();


        var $printSection = document.getElementById("printSection");
        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            document.body.appendChild($printSection);
        }
        var domClone = document.getElementById("printDiv").cloneNode(true);
        $printSection.appendChild(domClone);
        window.print();
        $printSection.removeChild(domClone);
    };
    //#endregion
    
    //#region 列印按鈕顯示
    $scope.printShow = function () {
        if ($scope.viewModel.document.documentType >=3535 && $scope.viewModel.document.documentType<=3537){
            return true;
        }
        if ($scope.viewModel.document.documentType >=3546 && $scope.viewModel.document.documentType<=3548){
            return true;
        }
        if ($scope.viewModel.document.documentType >= 3544 && $scope.viewModel.document.documentType <= 3545) {
            return true;
        }
        if ($scope.viewModel.document.documentType == 3551){
            return true;
        }
        return false
    };
    //#endregion
    
    //#region 取得PACS路徑
    $scope.getPacsPath = function () {
        var date = $scope.viewModel.document.createTime.substring(0, 10);
        var user = $scope.shareModel.userInfos.userId == "A28958" || $scope.shareModel.userInfos.userId == "A28034" ? "A12978" : $scope.shareModel.userInfos.userId;
        cancerDocument.getPacsPath(user, $scope.appModel.patientInfos.chartNo, date).then(
        function (success) {
            location.href = "IE:" + success.data;
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        }
    )};
    //#endregion
    
    //#region 複製文字
    $scope.copy = function () {
        document.querySelector('#copyTextarea').select();
        document.execCommand('Copy');
    };
    //#endregion

    //#region 取得治療期的值
    $scope.getTreatmentPeriod = function () {
        switch($scope.viewModel.document.body.treatmentPeriod.code)
        {
            case "":
                $scope.viewModel.document.body.treatmentPeriod.value = "";
                break;
            case "1":
                $scope.viewModel.document.body.treatmentPeriod.value = "基礎評估";
                break;
            case "2":
                $scope.viewModel.document.body.treatmentPeriod.value = "治療期後一個月";
                break;
            case "3":
                $scope.viewModel.document.body.treatmentPeriod.value = "治療期後三個月";
                break;
            case "4":
                $scope.viewModel.document.body.treatmentPeriod.value = "治療期後六個月";
                break;
            case "5":
                $scope.viewModel.document.body.treatmentPeriod.value = "治療期後一年";
                break;
            case "6":
                $scope.viewModel.document.body.treatmentPeriod.value = "";
                break;
        }
    };
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);
