﻿angular.module("cmuh").service("cancerDocument", ["$rootScope", "cmuhBase", "$http", "$q", function ($rootScope, cmuhBase, $http, $q) {

    //#region 取得文件預設值
    //@params {string: url}
    //@return {promise object}
    this.getDefaultDocumentInfo = function (url) {
        return $http.get(url);
    }
    //#endregion

    //#region 取得左邊文件清單
    this.getDocumentList = function (idNo, documentType, caseType) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetDocumentList/{0}/{1}/{2}", idNo, documentType, caseType)
        return $http.get(url);
    }
    //#endregion

    //#region 取得文件資料
    this.getDocumentInfo = function (documentNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetDocumentInfo/{0}", documentNo)
        return $http.get(url);
    }
    //#endregion

    //#region 修改表單
    this.setDocumentInfo = function (document) {
        var doc = {};
        angular.copy(document, doc);
        //存檔前將body轉成JSON格式的字串
        doc.body = JSON.stringify(doc.body);
        var url = "../WebApi/CancerCaseManager/CancerCaseManagerService/SetDocumentInfo";
        return $http.put(url, doc);
    }
    //#endregion

    //#region 刪除表單
    this.deleteDocumentInfo = function (documentNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/DeleteDocumentInfo/{0}", documentNo)
        return $http.delete(url);
    }
    //#endregion

    //#region 取得 Stroboscopic Evaluation 影片
    this.getVideoPaths = function (decumentNo) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetVideoPaths/{0}", decumentNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得PACS路徑
    this.getPacsPath = function (empCode, chartNo, creatTime) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetPacsPath/{0}/{1}/{2}", empCode, chartNo, creatTime);
        return $http.get(url);
    }
    //#endregion
    
    //#region 取得病人資料
    this.getPatientInfos = function (chartNo, caseType) {
        var url = cmuhBase.stringFormat("../WebApi/CancerCaseManager/CancerCaseManagerService/GetCasePatient/{0}/{1}", chartNo, caseType);
        return $http.get(url);
    }
    //#endregion
}]);