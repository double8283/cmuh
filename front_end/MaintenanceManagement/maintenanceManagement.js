﻿angular.module('cmuh').service('maintenanceManagement', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得維修狀況清單
    this.getRepairStatusInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairStatusInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得期間內自己維修的修繕資料清單
    this.getRepairMts = function (searchRequest) {
        return $http.post("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMts/RepairUser", searchRequest);
    }
    //#endregion

}]);