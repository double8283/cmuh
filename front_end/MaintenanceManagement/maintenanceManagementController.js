﻿angular.module("cmuh").controller("maintenanceManagementController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenanceManagement", function ($scope, $http, $q, cmuhBase, toaster, maintenanceManagement) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //定義MaintenanceListGrid
        setMaintenanceListGridOptions();

        //查詢變數
        $scope.viewModel.searchRequest = {
            repairDepartNo: $scope.executingAppStore.appId == 351 ? "1AD0" : $scope.executingAppStore.appId == 352 ? "1A91" : "1A82",
            repairUser: $scope.shareModel.userInfos.userNo,
            repairStatus: {
                optionNo: 0
            },
            period: {
                type: "D",
                value: "-3"
            }
        }

        $scope.viewModel.period = "1";

        //取得修繕狀態資料
        getRepairStatusInfos();

        document.getElementById("contentDiv").style.height = window.innerHeight - 200 + "px";
        document.getElementById("maintenanceListGrid").style.height = window.innerHeight - 265 + "px";
    }
    //#endregion

    //#region 定義MaintenanceListGrid
    var setMaintenanceListGridOptions = function () {
        $scope.viewModel.select = "false";
        $scope.viewModel.maintenanceListGridOptions = {
            selectable: 'row',
            scrollable: false,
            columns: [
                { field: "repairNo", title: "維修編號", width: "80px" },
                { field: "applyTime", title: "申請時間", width: "200px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd hh:mm:ss'}}" },
                { field: "applyDepartNo", title: "申請部門", width: "80px" },
                { field: "applyUser.empName", title: "申請人員", width: "100px" },
                { field: "telNo", title: "連絡電話", width: "80px" },
                { field: "locationNames[0]", title: "棟別", width: "200px" },
                { field: "locationNames[1]", title: "樓層", width: "50px" },
                { field: "", title: "設備類別", template: "<div data-ng-repeat=\"item in dataItem.propertyNames\" style=\"display:inline\">{{item}}&emsp;</div>" },
                { field: "signoffUser.empName", title: "簽核人員", width: "100px" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.shareModel.repairNo = dataItem.repairNo;
                var basicInfo = cmuhBase.stringFormat(
                                "維修編號：{0}　申請人：{1}　地點：{2}　連絡電話：{3}",
                                dataItem.repairNo,
                                dataItem.applyUser.empName,
                                dataItem.locationNames.join("-"),
                                dataItem.telNo);
                $scope.shareModel.banner.showBanner(null, null, "", basicInfo, "", "");
                $scope.viewModel.maintenanceListGrid.element.on('dblclick', function (e) {
                    $scope.appNavMenuCtrl.slider = 'appMenu-open'; $scope.appNavMenuCtrl.fade = 'closeArea-fade';
                });
            }
        };
    }
    //#endregion

    //#region 設定MaintenanceListGrid DataSource
    var setMaintenanceListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#maintenanceListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.maintenanceListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#region 取得修繕狀態資料
    var getRepairStatusInfos = function () {
        maintenanceManagement.getRepairStatusInfos().then(
            function (success) {
                $scope.viewModel.repairStatusInfos = [{ "optionNo": "0", "optionName": "---請選擇---" }];
                $scope.viewModel.repairStatusInfos = $scope.viewModel.repairStatusInfos.concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得維修狀態
    var getRepairStatus = function (option) {
        for (var i = 0; i < $scope.viewModel.repairStatusInfos.length; i++) {
            if (option == $scope.viewModel.repairStatusInfos[i].optionNo) {
                return $scope.viewModel.repairStatusInfos[i].optionName;
            }
        }
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 查詢
    $scope.search = function () {
        maintenanceManagement.getRepairMts($scope.viewModel.searchRequest).then(
            function (success) {
                setMaintenanceListGridSource(success.data);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case "1":
                $scope.viewModel.searchRequest.period.type = "D";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "2":
                $scope.viewModel.searchRequest.period.type = "W";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "3":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "4":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "5":
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 匯出成excel
    $scope.export = function () {
        var data = [{ cells: [{ value: "維修編號" }, { value: "申請時間" }, { value: "申請部門" }, { value: "申請人員" }, { value: "連絡電話" }, { value: "單位地點" }, { value: "設備類別" }, { value: "維修狀況" }] }];
        for (var i = 0; i < $scope.viewModel.maintenanceListGrid.dataSource.data().length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].repairNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyTime },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyDepartNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].applyUser.empName },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].telNo },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].locationNames.join("-") },
                    { value: $scope.viewModel.maintenanceListGrid.dataSource.data()[i].propertyNames.join("-") },
                    { value: getRepairStatus($scope.viewModel.searchRequest.repairStatus.optionNo) }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "維修資料.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);