﻿angular.module("cmuh").controller("maintenanceApplicationManagementController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "maintenanceApplicationManagement", function ($scope, $http, $q, cmuhBase, toaster, maintenanceApplicationManagement) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //查詢變數
        $scope.viewModel.searchRequest = {
            applyUser: $scope.shareModel.userInfos.userNo,
            period: {
                type: "D",
                value: "-3"
            }
        }
        $scope.viewModel.select = "false";
        $scope.viewModel.period = "1";

        //定義ApplyListGrid
        setApplyListGridOptions();

        document.getElementById("contentDiv").style.height = window.innerHeight - 200 + "px";
        document.getElementById("applyListGrid").style.height = window.innerHeight - 265 + "px";
    }
    //#endregion

    //#region 定義ApplyListGrid
    var setApplyListGridOptions = function () {
        $scope.viewModel.applyListGridOptions = {
            scrollable: false,
            selectable: 'row',
            columns: [
                {
                    field: "check", title: "", width: "10px", sortable: false,
                    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/></center>",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem);\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/>"
                },
                { field: "repairNo", title: "維修編號", width: "80px" },
                { field: "applyTime", title: "申請時間", width: "200px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd hh:mm:ss'}}" },
                { field: "locationNo.optionName", title: "申請地點", width: "80px" },
                { field: "propertyType.optionName", title: "設備類別", width: "80px" },
                { field: "repairStatus.optionName", title: "維修狀況", width: "80px" },
                { field: "repairUser.empName", title: "維修人員", width: "110px" },
                { field: "completeTime", title: "完成時間", width: "200px", template: "<div data-ng-if=\"dataItem.completeTime != '2999-12-31T00:00:00+08:00'\">{{dataItem.completeTime | date:'yyyy-MM-dd hh:mm:ss'}}<div>" },
                { field: "signoffUser.empName", title: "簽核人員", width: "110px" }
            ],
            dataSource: new kendo.data.DataSource(),
            change: function (e) {
                var dataItem = this.dataItem(this.select()[0]);
                $scope.shareModel.repairNo = dataItem.repairNo;
                $scope.shareModel.banner.showBanner(false, null, null, "維修編號：" + dataItem.repairNo, "", "");
                $scope.viewModel.applyListGrid.element.on('dblclick', function (e) {
                    $scope.appNavMenuCtrl.slider = 'appMenu-open'; $scope.appNavMenuCtrl.fade = 'closeArea-fade';
                });
            }
        };
    }
    //#endregion

    //#region 設定ApplyListGrid DataSource
    var setApplyListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#applyListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.applyListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 查詢
    $scope.search = function () {
        $scope.viewModel.select = "false";
        maintenanceApplicationManagement.getRepairMts($scope.viewModel.searchRequest).then(
            function (success) {
                setApplyListGridSource(success.data);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                setApplyListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 刪除
    $scope.delete = function () {
        var data = [];
        for (var i = 0; i < $scope.viewModel.applyListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.applyListGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.applyListGrid.dataSource.data()[i].repairStatus.optionNo = 80;
                $scope.viewModel.applyListGrid.dataSource.data()[i].systemUser = $scope.shareModel.userInfos.userNo;
                data.push($scope.viewModel.applyListGrid.dataSource.data()[i]);
            }
        }
        maintenanceApplicationManagement.setRepairMts(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，刪除成功");
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
                $scope.search();
            },
            function (error) {
                setApplyListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case "1":
                $scope.viewModel.searchRequest.period.type = "D";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "2":
                $scope.viewModel.searchRequest.period.type = "W";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "3":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "4":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "5":
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.applyListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.applyListGrid.dataSource.data()[i].repairStatus.optionNo == 5) {
                $scope.viewModel.applyListGrid.dataSource.data()[i].check = $scope.viewModel.select;
            }
        }
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (dataItem) {
        $scope.viewModel.select = dataItem.check == "false" ? "false" : $scope.viewModel.select;
        if (dataItem.check == "true" && dataItem.repairStatus.optionNo != 5) {
            toaster.showErrorMessage("對不起，只有維修狀態為「暫存」的才可以勾選刪除");
            dataItem.check = "false";
        }
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);