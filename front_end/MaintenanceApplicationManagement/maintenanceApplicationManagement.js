﻿angular.module('cmuh').service('maintenanceApplicationManagement', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得期間內自己申請的修繕資料清單
    this.getRepairMts = function (searchRequest) {
        return $http.post("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMts/ApplyUser", searchRequest);
    }
    //#endregion

    //#region 設定修繕資料
    this.setRepairMts = function (repairMtInfo) {
        return $http.put("../WebApi/FaciltiesCheckList/FacilitiesRepairService/SetRepairMts", repairMtInfo);
    }
    //#endregion

}]);