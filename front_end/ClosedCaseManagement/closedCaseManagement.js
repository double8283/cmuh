﻿angular.module('cmuh').service('closedCaseManagement', ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得特定單位人員
    this.getMaintenanceStaff = function (departNo) {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetMaintenanceStaff/{0}", departNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得單位地點清單
    this.getLocationInfos = function () {
        var url = cmuhBase.stringFormat("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetLocationInfos");
        return $http.get(url);
    }
    //#endregion

    //#region 取得期間內尚未完成的修繕資料清單
    this.getRepairMts = function (searchRequest) {
        return $http.post("../WebApi/FaciltiesCheckList/FacilitiesRepairService/GetRepairMts/Location", searchRequest);
    }
    //#endregion

    //#region 設定修繕資料
    this.setRepairMts = function (repairMtInfo) {
        return $http.put("../WebApi/FaciltiesCheckList/FacilitiesRepairService/SetRepairMts", repairMtInfo);
    }
    //#endregion

}]);