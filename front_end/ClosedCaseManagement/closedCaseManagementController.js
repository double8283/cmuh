﻿angular.module("cmuh").controller("closedCaseManagementController", ["$scope", "$http", "$q", "cmuhBase", "toaster", "closedCaseManagement", function ($scope, $http, $q, cmuhBase, toaster, closedCaseManagement) {

    //#region 方法

    //#region 初始化方法定義
    var initViewModel = function (pageModel) {

        // 初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //查詢變數
        $scope.viewModel.searchRequest = {
            repairDepartNo: $scope.executingAppStore.appId == 351 ? "1AD0" : $scope.executingAppStore.appId == 352 ? "1A91" : "1A82",
            locationNo: "0",
            period: {
                type: "D",
                value: "-3"
            }
        }

        $scope.viewModel.period = "1";

        //定義ClosedCaseListGrid
        setClosedCaseListGridOptions();

        document.getElementById("contentDiv").style.height = window.innerHeight - 200 + "px";
        document.getElementById("closedCaseListGrid").style.height = window.innerHeight - 265 + "px";

        //取得維修人員
        getMaintenanceStaff();
        //取得單位地點清單
        getLocationInfos();
    }
    //#endregion

    //#region 取得維修人員
    var getMaintenanceStaff = function () {
        //appId 351=工務1AD0 352=醫工1A91 353=資訊1A82
        var departNo = $scope.executingAppStore.appId == 351 ? "1AD0" : $scope.executingAppStore.appId == 352 ? "1A91" : "1A82";
        closedCaseManagement.getMaintenanceStaff(departNo).then(
            function (success) {
                $scope.viewModel.process = [{ "empCode": "", "empName": "", "departName": "", "departNo": "" }].concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 取得單位地點清單
    var getLocationInfos = function () {
        closedCaseManagement.getLocationInfos().then(
            function (success) {
                $scope.viewModel.locationInfos = [{ "locationNo": "0", "locationName": "所有地點", "details": [] }];
                $scope.viewModel.locationInfos = $scope.viewModel.locationInfos.concat(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 定義ClosedCaseListGrid
    var setClosedCaseListGridOptions = function () {
        $scope.viewModel.select = "false";
        $scope.viewModel.closedCaseListGridOptions = {
            scrollable: false,
            columns: [
                {
                    field: "check", title: "", width: "10px", sortable: false,
                    headerTemplate: "<center><input type=\"checkbox\" data-ng-model=\"viewModel.select\" data-ng-change=\"selectAll()\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/></center>",
                    attributes: { style: "text-align:center" },
                    template: "<input type=\"checkbox\" data-ng-model=\"dataItem.check\" data-ng-change=\"check(dataItem.check);changeEmp(dataItem);\" data-ng-true-value=\"true\" data-ng-false-value=\"false\"/>"
                },
                { field: "repairNo", title: "維修編號", width: "70px" },
                { field: "applyTime", title: "申請時間", width: "160px", template: "{{dataItem.applyTime | date:'yyyy-MM-dd hh:mm:ss'}}" },
                { field: "applyDepartNo", title: "申請部門", width: "70px" },
                { field: "applyUser.empName", title: "申請人員", width: "70px" },
                { field: "telNo", title: "連絡電話", width: "70px" },
                { field: "locationNames[0]", title: "棟別", width: "150px" },
                { field: "locationNames[1]", title: "樓層", width: "50px" },
                { field: "", title: "設備類別", template: "<div data-ng-repeat=\"item in dataItem.propertyNames\" style=\"display:inline\">{{item}}&emsp;</div>" },
                { field: "failureNo.optionName", title: "故障情形", width: "150px" },
                { field: "", title: "維修人員", width: "110px", template: "<select data-kendo-drop-down-list data-k-data-text-field=\"'empName'\" data-k-data-value-field=\"'empNo'\" data-k-data-source=\"viewModel.process\" data-ng-model=\"dataItem.repairUser.empNo\" style=\"width: 90px\"></select>" },
                { field: "repairStatus.optionName", title: "維修狀況", width: "70px" }
            ],
            dataSource: new kendo.data.DataSource()
        };
    }
    //#endregion

    //#region 設定ClosedCaseListGrid DataSource
    var setClosedCaseListGridSource = function (items) {
        var dataSource = new kendo.data.DataSource({
            data: items
        });
        setTimeout(function () {
            var grid = $("#closedCaseListGrid").data("kendoGrid");
            if (grid == null) {
                $scope.viewModel.closedCaseListGridOptions.dataSource = dataSource;
            } else {
                grid.setDataSource(dataSource);
            }
        }, 100);
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 查詢
    $scope.search = function () {
        closedCaseManagement.getRepairMts($scope.viewModel.searchRequest).then(
            function (success) {
                setClosedCaseListGridSource(success.data);
                if (success.data.length == 0) {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 指派
    $scope.assign = function () {
        var data = [];
        for (var i = 0; i < $scope.viewModel.closedCaseListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.closedCaseListGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.closedCaseListGrid.dataSource.data()[i].systemUser = $scope.shareModel.userInfos.userNo;
                data.push($scope.viewModel.closedCaseListGrid.dataSource.data()[i]);
            }
        }
        closedCaseManagement.setRepairMts(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("恭喜您，收案成功");
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
                $scope.search();
            },
            function (error) {
                setApplyListGridSource();
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    //#region 批次指派
    $scope.multiplayerAssign = function () {
        for (var i = 0; i < $scope.viewModel.closedCaseListGrid.dataSource.data().length; i++) {
            if ($scope.viewModel.closedCaseListGrid.dataSource.data()[i].check == "true") {
                $scope.viewModel.closedCaseListGrid.dataSource.data()[i].repairUser.empNo = $scope.viewModel.empNo;
            }
        }
    }
    //#endregion

    //#region 取得期間
    $scope.getPeriod = function () {
        switch ($scope.viewModel.period) {
            case "1":
                $scope.viewModel.searchRequest.period.type = "D";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "2":
                $scope.viewModel.searchRequest.period.type = "W";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "3":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -1;
                break;
            case "4":
                $scope.viewModel.searchRequest.period.type = "M";
                $scope.viewModel.searchRequest.period.value = -3;
                break;
            case "5":
                $scope.viewModel.searchRequest.period.type = "all";
                $scope.viewModel.searchRequest.period.value = 0;
                break;
        }
    }
    //#endregion

    //#region 當選取取消全部都一起取消
    $scope.check = function (check) {
        $scope.viewModel.select = check == "false" ? "false" : $scope.viewModel.select;
    }
    //#endregion

    //#region 點選選取欄頭可以全部選取
    $scope.selectAll = function () {
        for (var i = 0; i < $scope.viewModel.closedCaseListGrid.dataSource.data().length; i++) {
            $scope.viewModel.closedCaseListGrid.dataSource.data()[i].check = $scope.viewModel.select;
            if ($scope.viewModel.select == "true") {
                $scope.viewModel.closedCaseListGrid.dataSource.data()[i].repairUser.empNo = $scope.shareModel.userInfos.userNo;
            }
        }
    }
    //#endregion

    //#region 勾選時將維修人為設為自己
    $scope.changeEmp = function (dataItem) {
        dataItem.repairUser.empNo = dataItem.check == "true" ? $scope.shareModel.userInfos.userNo : dataItem.empNo;
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion

}]);