﻿angular.module("cmuh").service("analysisMedExpenses", ["$http", "$q", "cmuhBase", function ($http, $q, cmuhBase) {

    //#region 取得統計值
    this.getStatistics = function (search) {
        return $http.put("../WebApi/CostAnalysis/MedFeeService/GetStatistics", search);
    }
    //#endregion

    //#region 查詢病人費用
    this.getPatientFees = function (search) {
        return $http.put("../WebApi/CostAnalysis/MedFeeService/GetPatientFees", search);
    }
    //#endregion  

}]);