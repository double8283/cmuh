﻿angular.module("cmuh").controller("analysisMedExpensesController", ["$scope", "$http", "$timeout", "toaster", "cmuhBase", "analysisMedExpenses", "$filter", function ($scope, $http, $timeout, toaster, cmuhBase, analysisMedExpenses, $filter) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        //設置kendo的語言
        kendo.culture("zh-TW");

        //調整倍率的顯示預設為不顯示
        $scope.viewModel.isShowRate = false;

        //預設查詢值
        $scope.viewModel.search = {
            startDate: new Date(2011,0,1),
            endDate: new Date(2011,0,1),
            diagCode0: "",
            diagCode1: "",
            diagCode2: "",
            operCode0: "",
            operCode1: "",
            operCode2: "",
            medCode1: "",
            medCode2: "",
            medCode3: "",
            age1: "",
            age2: "",
            sex: "",
            nhiRate: "",
            genRate: "",
            visitNo: "",
            rate:1
        };

        //統計起日options
        $scope.viewModel.searchStartDateOptions = {
            format: 'yyyy-MM-dd',
            max: new Date(),
            change: function () {
                var datepicker = $("#endDate").data("kendoDatePicker");
                datepicker.setOptions({
                    min: $scope.viewModel.search.startDate,
                    max: new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate()) > new Date() ? new Date() : new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate())
                });
                if ($scope.viewModel.search.startDate > $scope.viewModel.search.endDate) {
                    $scope.viewModel.search.endDate = $scope.viewModel.search.startDate;
                }
                if ($scope.viewModel.search.endDate > new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate())) {
                    $scope.viewModel.search.endDate = new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate());
                }
            }
        };
        //統計迄日options
        $scope.viewModel.searchEndDateOptions = {
            format: 'yyyy-MM-dd',
            min: $scope.viewModel.search.startDate,
            max: new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate()) > new Date() ? new Date() : new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate()),
            change: function () {
                if ($scope.viewModel.search.endDate > new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate())) {
                    $scope.viewModel.search.endDate = new Date($scope.viewModel.search.startDate.getFullYear() + 1, $scope.viewModel.search.startDate.getMonth(), $scope.viewModel.search.startDate.getDate());
                }
            }
        };

        //顯示左方查詢條件區域
        $scope.showLeftPanel();
        for (var i = 0; i < document.getElementsByClassName("contentClass").length; i++) {
            document.getElementsByClassName("contentClass")[i].style.height = window.innerHeight - 196 + "px";
        }
        for (var i = 0; i < document.getElementsByClassName("contentClass2").length; i++) {
            document.getElementsByClassName("contentClass2")[i].style.height = window.innerHeight - 300 + "px";
        }
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 顯示左方查詢條件
    $scope.showLeftPanel = function () {
        $scope.viewModel.isShowLeftPanel = true;
        document.getElementById("contentDiv").style.width = window.innerWidth - 299 + "px";
        document.getElementById("contentDiv").style.marginLeft = "260px";
    }
    //#endregion

    //#region 隱藏左方查詢條件
    $scope.hideLeftPanel = function () {
        $scope.viewModel.isShowLeftPanel = false;
        document.getElementById("contentDiv").style.width = window.innerWidth - 39 + "px";
        document.getElementById("contentDiv").style.marginLeft = "0px";
    }
    //#endregion
    
    //#region 查詢按鈕，取得統計值
    $scope.search = function () {
        $scope.viewModel.feeSummary = [];
        $scope.viewModel.patients = [];
        $scope.viewModel.feeItem = [];
        $scope.viewModel.visitNo = "";
        $scope.viewModel.searchInfo = {
            startDate: $scope.viewModel.search.startDate,
            endDate: $scope.viewModel.search.endDate,
            diagCode0: $scope.viewModel.search.diagCode0.trim(),
            diagCode1: $scope.viewModel.search.diagCode1.trim(),
            diagCode2: $scope.viewModel.search.diagCode2.trim(),
            operCode0: $scope.viewModel.search.operCode0.trim(),
            operCode1: $scope.viewModel.search.operCode1.trim(),
            operCode2: $scope.viewModel.search.operCode2.trim(),
            medCode1: $scope.viewModel.search.medCode1.trim(),
            medCode2: $scope.viewModel.search.medCode2.trim(),
            medCode3: $scope.viewModel.search.medCode3.trim(),
            age1: $scope.viewModel.search.age1.replace(/[^\d]/g, ''),
            age2: $scope.viewModel.search.age2.replace(/[^\d]/g, ''),
            sex: $scope.viewModel.search.sex,
            nhiRate: $scope.viewModel.search.nhiRate,
            genRate: $scope.viewModel.search.genRate,
            rate: $scope.viewModel.search.rate
        };
        analysisMedExpenses.getStatistics($scope.viewModel.searchInfo).then(
            function (success) {
                if (success.data != "null")
                {
                    $scope.viewModel.feeSummary = success.data.feeSummary;
                    $scope.viewModel.patients = success.data.patient;
                    for (var i = 0; i < $scope.viewModel.patients.length; i++) {
                        $scope.viewModel.patients[i].backgroundColor = "#ACD5E4";
                    }
                } else {
                    toaster.showErrorMessage("對不起，沒有資料");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion

    
    //#region 選擇病人
    $scope.selectPatient = function (item, index) {
        $scope.viewModel.searchInfo.visitNo = item.visitNo;
        $scope.getPatientFees(index);
        $scope.viewModel.visitNo = '住院號碼' + item.oldVisitNo + '的';
        $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(2);
    }
    //#endregion
    
    //#region 查詢病人費用
    $scope.getPatientFees = function (index) {
        for (var i = 0; i < $scope.viewModel.patients.length; i++) {
            $scope.viewModel.patients[i].backgroundColor = "#ACD5E4";
        }
        $scope.viewModel.patients[index].backgroundColor = "#428bca";

        analysisMedExpenses.getPatientFees($scope.viewModel.searchInfo).then(
            function (success) {
                $scope.viewModel.feeItem = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            }
        );
    }
    //#endregion
    
    //#region 查詢按鈕禁用條件
    $scope.searchDisable = function () {
        if(cmuhBase.isNullOrEmpty($scope.viewModel.search.nhiRate) == true ||
           cmuhBase.isNullOrEmpty($scope.viewModel.search.genRate) == true ||
           cmuhBase.isNullOrEmpty($scope.viewModel.search.rate) == true ||
           (cmuhBase.isNullOrEmpty($scope.viewModel.search.diagCode0) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.diagCode1) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.diagCode2) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.operCode0) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.operCode1) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.operCode2) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.medCode1) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.medCode2) == true &&
           cmuhBase.isNullOrEmpty($scope.viewModel.search.medCode3) == true))
        {
            return true;
        }
        return false;
    }
    //#endregion

    //#region 匯出統計結果成excel
    $scope.export1 = function () {
        var data = [];
        data.push({
            cells: [{ value: "統計起日" }, { value: "統計迄日" }, { value: "主診斷碼" }, { value: "次診斷碼" }, { value: "次診斷碼" }, { value: "主處置碼" }, { value: "次處置碼" },
                { value: "次處置碼" }, { value: "醫令碼１" }, { value: "醫令碼２" }, { value: "醫令碼３" }, { value: "病人年齡" }, { value: "病人性別" },
                { value: "健保倍率" }, { value: "自費倍率" }, { value: "調整倍率" }]
        });
        data.push({
            cells: [{ value: $scope.viewModel.searchInfo.startDate }, { value: $scope.viewModel.searchInfo.endDate },
                { value: $scope.viewModel.searchInfo.diagCode0 },{ value: $scope.viewModel.searchInfo.diagCode1 }, { value: $scope.viewModel.searchInfo.diagCode2 },
                { value: $scope.viewModel.searchInfo.operCode0 },{ value: $scope.viewModel.searchInfo.operCode1 }, { value: $scope.viewModel.searchInfo.operCode2 },
                { value: $scope.viewModel.searchInfo.medCode1 },{ value: $scope.viewModel.searchInfo.medCode2 }, { value: $scope.viewModel.searchInfo.medCode3 },
                { value: $scope.viewModel.searchInfo.age1 + "～" +  $scope.viewModel.searchInfo.age2 },
                { value: $scope.viewModel.searchInfo.sex }, { value: $scope.viewModel.searchInfo.genRate },
                { value: $scope.viewModel.searchInfo.nhiRate }, { value: $scope.viewModel.searchInfo.rate }]
        });
        data.push({ cells: [] });

        data.push({ cells: [{ value: "統計欄位" }, { value: "25%" }, { value: "50%" }, { value: "75%" }, { value: "100%" }, { value: "眾數" }] });

        for (var i = 0; i < $scope.viewModel.feeSummary.length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.feeSummary[i].itemName },
                    { value: $scope.viewModel.feeSummary[i].percent25 },
                    { value: $scope.viewModel.feeSummary[i].percent50 },
                    { value: $scope.viewModel.feeSummary[i].percent75 },
                    { value: $scope.viewModel.feeSummary[i].percent100 },
                    { value: $scope.viewModel.feeSummary[i].mode == -1 ? "無" : $scope.viewModel.feeSummary[i].mode }
                ]
            });
        }

        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: "醫療費用分析統計資料.xlsx"
        });
    }
    //#endregion

    //#region 匯出費用明細成excel
    $scope.export2 = function () {
        var data = [{ cells: [{ value: $scope.viewModel.visitNo + "費用明細" }] }];
        data.push({ cells: [{ value: "名稱" }, { value: "費用" }] });
        for (var i = 0; i < $scope.viewModel.feeItem.length; i++) {
            data.push({
                cells: [
                    { value: $scope.viewModel.feeItem[i].feeName },
                    { value: $scope.viewModel.feeItem[i].feeSum }
                ]
            });
        }
        var workbook = new kendo.ooxml.Workbook({
            sheets: [{
                rows: data
            }]
        });
        kendo.saveAs({
            dataURI: workbook.toDataURL(),
            fileName: $scope.viewModel.visitNo + "費用明細.xlsx"
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);
