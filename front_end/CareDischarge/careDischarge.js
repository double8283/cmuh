﻿angular.module('cmuh').service("careDischarge", ["$http", "cmuhBase", function ($http, cmuhBase) {

    //#region 取得頁籤項目清單
    this.getSectionTitles = function () {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetSectionTitles");
        return $http.get(url);
    }
    //#endregion

    //#region 取得出院帶藥清單
    this.getDischargeDrugs = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetDischargeDrugs/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion  

    //#region 取得預約門診掛號資訊
    this.getAppointRecords = function (idNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetAppointRecords/{0}", idNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得出院照護樣板資訊
    this.getDischargeSheets = function (visitNo, careType) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetDischargeSheets/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region 儲存
    this.setCareDischargeInfo = function (data) {
        return $http.put("../WebApi/NursingCare/CareDischargeService/SetCareDischargeInfo", data);
    }
    //#endregion

    //#region 得到病人資料
    this.getPatient = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/PatientInfo/QueryInpPatient/GetPatient/VisitNo/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region 得到上次存檔日期、護理師
    this.getDischargeRecord = function (chartNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetDischargeRecord/{0}", chartNo);
        return $http.get(url);
    }
    //#endregion

    //#region 得到護理站分機
    this.getSubTelNo = function (stationNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetSubTelNo/{0}", stationNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得未結案之護理計劃
    this.getActivePlanNames = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CarePlanService/GetActivePlanNames/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region 取得相關復健
    this.getRehabTxContent = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/GetRehabTxContent/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region 是否有門診注射單
    this.isOpdInjection = function (visitNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareDischargeService/IsOpdInjection/{0}", visitNo);
        return $http.get(url);
    }
    //#endregion

    //#region getEmpWorkStation（門診注射單用）
    this.getEmpWorkStation = function (empNo) {
        var url = cmuhBase.stringFormat("../WebApi/NursingCare/CareManagerService/GetEmpWorkStation/{0}", empNo);
        return $http.get(url);
    }
    //#endregion
}]);