﻿angular.module('cmuh').controller("careDischargeController", ["$scope", "$sce", "$q", "cmuhBase", "toaster", "careDischarge", function ($scope, $sce, $q, cmuhBase, toaster, careDischarge) {

    //#region 方法

    //#region 初始化
    var initViewModel = function (pageModel) {

        //初始化ViewModel
        $scope.viewModel = pageModel;

        //如果沒抓到病人資料就回到病人選取頁面
        if ($scope.appModel.patientInfos == undefined) {
            toaster.showErrorMessage("請先選擇病人");
            $scope.shareModel.navigation(50001, 407);
        }

        //初始化變數
        $scope.viewModel.middleContent = cmuhBase.stringFormat("App/CareDischarge/Pages/middlePage.html?{0}", cmuhBase.getVersionNo()); //中間區域
        $scope.viewModel.careType = 70;                                             //照護型態
        $scope.viewModel.leftTabs = [];                                             //左頁籤
        $scope.viewModel.rightTabs = [];                                            //右頁籤
        $scope.viewModel.isExpand = false;                                          //預設為不展開
        $scope.viewModel.sheetInfo = [];                                            //出院照護樣板
        $scope.viewModel.chtFormContents = [];                                      //存放中文表單內容
        $scope.viewModel.engFormContents = [];                                      //存放英文表單內容
        $scope.viewModel.chtHealthEducations = [];                                  //存放中文衛教單張內容
        $scope.viewModel.engHealthEducations = [];                                  //存放英文衛教單張內容
        $scope.viewModel.leftTabContent = null;                                     //左邊內容
        $scope.viewModel.rightTabContent = null;                                    //右邊內容
        $scope.viewModel.getBabyBack = false;                                       //領回嬰兒切結書
        $scope.viewModel.chtPrintTitle = "中國醫藥大學附設醫院";                    //中文列印title
        $scope.viewModel.engPrintTitle = "China Medical University Hospital";       //英文列印title

        //今天日期
        $scope.viewModel.toDay = (new Date().getFullYear() - 1911) + " 年 " + (new Date().getMonth() + 1) + " 月 " + new Date().getDate() + " 日";

        //得到病人資料
        getPatient();
        //取得頁籤項目清單
        getSectionTitles();
        
    }
    //#endregion

    //#region 得到病人資料
    var getPatient = function () {
        careDischarge.getPatient($scope.appModel.patientInfos.visitNo).then(
            function (success) {
                $scope.appModel.patientInfos.ptName = success.data.ptName;
                $scope.appModel.patientInfos.birthday = success.data.birthday;
                $scope.appModel.patientInfos.sex = success.data.sex;
                $scope.appModel.patientInfos.idNo = success.data.idNo;
                $scope.appModel.patientInfos.bed = { stationNo: success.data.bed.stationNo, roomBed: success.data.bed.roomBed };
                $scope.appModel.patientInfos.visitNo = success.data.visitNo;
                $scope.appModel.patientInfos.chartNo = success.data.chartNo;
                $scope.appModel.patientInfos.division = { chineseName: success.data.division.chineseName, divNo: success.data.division.divNo };
                if ($scope.appModel.patientInfos.division.divNo.substring(0, 2) == "33") {
                    $scope.viewModel.chtPrintTitle = "中國醫藥大學兒童醫院";
                    $scope.viewModel.engPrintTitle = "China Medical University Children's Hospital";
                }
                //病人生日（民國年/月/日）
                $scope.viewModel.patientBirthday = "";
                var birthday = new Date($scope.appModel.patientInfos.birthday);
                $scope.viewModel.patientBirthday = cmuhBase.stringFormat("{0}/{1}/{2}", birthday.getFullYear() - 1911, cmuhBase.addLeftZero(birthday.getMonth() + 1), cmuhBase.addLeftZero(birthday.getDate()));
                //病人性別
                $scope.viewModel.patientSex = $scope.appModel.patientInfos.sex == '男' ? 'male' : 'female';
                //取得預約門診掛號資訊
                getAppointRecords();
                //取得出院帶藥清單
                getDischargeDrugs();
                //取得上次存檔日期、護理師
                getDischargeRecord();
                //是否有門診注射單
                isOpdInjection();
                //banner顯示
                var basicInfo = cmuhBase.stringFormat("{0}-{1} {2} {3} {4} {5} ({6})", success.data.bed.stationNo, success.data.bed.roomBed, success.data.ptName, success.data.chartNo, success.data.sex, success.data.birthday.substring(0, 10), success.data.age);
                $scope.shareModel.banner.showBanner(true, null, success.data.sex, basicInfo, "", "");
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 改變左側tab狀態
    var changeLeftTab = function (page) {
        //選定之tab設為active
        for (var i = 0; i < $scope.viewModel.leftTabs.length; i++) {
            if ($scope.viewModel.leftTabs[i].tabName === page.tabName) {
                $scope.viewModel.leftTabs[i].active = "active";
            } else {
                $scope.viewModel.leftTabs[i].active = "";
            }
        }
        //右側tab狀態清除
        for (var i = 0; i < $scope.viewModel.rightTabs.length; i++) {
            $scope.viewModel.rightTabs[i].active = "";
        }
    }
    //#endregion

    //#region 改變右側tab狀態
    var changeRightTab = function (page) {
        //選定之tab設為active
        for (var i = 0; i < $scope.viewModel.rightTabs.length; i++) {
            if ($scope.viewModel.rightTabs[i].tabName === page.tabName) {
                $scope.viewModel.rightTabs[i].active = "active";
            } else {
                $scope.viewModel.rightTabs[i].active = "";
            }
        }
        //左側tab狀態清除
        for (var i = 0; i < $scope.viewModel.leftTabs.length; i++) {
            $scope.viewModel.leftTabs[i].active = "";
        }
    }
    //#endregion

    //#region 設定splitter高度
    var setSplitterHeight = function () {
        var height = window.innerHeight - 216 + "px";

        var table = document.getElementById("table");
        if (table != null) {
            table.style.height = height;
        }
        document.getElementById("leftDiv").style.height = height;
        document.getElementById("middleDiv").style.height = height;
        document.getElementById("rightDiv").style.height = height;

        var leftContent = document.getElementsByClassName("left-content");
        for (var i = 0; i < leftContent.length; i++) {
            leftContent[i].style.height = window.innerHeight - 237 + "px";
        }

        $scope.viewModel.halfHeight = window.innerHeight - 237 - 50 + "px";
        $scope.viewModel.fullHeight = window.innerHeight - 237 + "px";
        $scope.viewModel.footerHeight = window.innerHeight - 60 + "px";

        table.style.width = window.innerWidth - 80 + "px";
    }
    //#endregion

    //#region 載入dependencies
    var getDependencies = function (url) {
        var deferred = $q.defer();
        var paths = url.split('/');
        var depPath = cmuhBase.stringFormat("{0}/{1}/{2}?{3}", paths[0], paths[1], 'dependencies.json', cmuhBase.getVersionNo());
        cmuhBase.getDependencies(depPath).then(
            function (success) {
                return cmuhBase.loadDependencies(success.data).then(
                     function (success) {
                         deferred.resolve(cmuhBase.stringFormat("{0}?{1}", url, cmuhBase.getVersionNo()));
                     },
                     function (error) {
                         toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'頁面發生失敗", pageUrl));
                     }
                 );
            },
            function (error) {
                toaster.showErrorMessage(cmuhBase.stringFormat("取得'{0}'系統資訊發生失敗", pageUrl));
            });

        return deferred.promise;
    }
    //#endregion

    //#region 取得splitter寬度
    var getSplitterWidth = function () {
        var table = document.getElementById("table");
        if (table != null) {
            return parseInt(angular.copy(table.style.width).replace(/px/, ""));
        }
        return 0;
    }
    //#endregion

    //#region 設定splitter內容寬度
    var setSplitterContentWidth = function (position) {
        var tableWidth = getSplitterWidth();
        var leftDiv = document.getElementById("leftDiv");
        var middleDiv = document.getElementById("middleDiv");
        var rightDiv = document.getElementById("rightDiv");
        switch (position) {
            case "left":
                leftDiv.style.width = tableWidth * 0.6 + "px";
                middleDiv.style.width = tableWidth * 0.4 + "px";
                rightDiv.style.width = "0px";
                $scope.viewModel.isShowLeftContext = true;
                $scope.viewModel.isShowRightContext = false;
                break;
            case "right":
                leftDiv.style.width = "0px";
                middleDiv.style.width = tableWidth * 0.4 + "px";
                rightDiv.style.width = tableWidth * 0.6 + "px";
                $scope.viewModel.isShowLeftContext = false;
                $scope.viewModel.isShowRightContext = true;
                break;
            case "none":
                leftDiv.style.width = "0px";
                middleDiv.style.width = tableWidth + "px";
                rightDiv.style.width = "0px";
                $scope.viewModel.isShowLeftContext = false;
                $scope.viewModel.isShowRightContext = false;
                break;
            case "left-expand":
                leftDiv.style.width = tableWidth * 0.7 + "px";
                middleDiv.style.width = tableWidth * 0.3 + "px";
                rightDiv.style.width = "0px";
                $scope.viewModel.isShowLeftContext = true;
                $scope.viewModel.isShowRightContext = false;
                break;
            case "right-expand":
                leftDiv.style.width = "0px";
                middleDiv.style.width = tableWidth * 0.3 + "px";
                rightDiv.style.width = tableWidth * 0.7 + "px";
                $scope.viewModel.isShowLeftContext = false;
                $scope.viewModel.isShowRightContext = true;
                break;
        }
    }
    //#endregion

    //#region 重設大小
    var resizing = function () {
        if ($scope.viewModel.isFirstClick == true) {
            getSplitterElement();
            $scope.viewModel.isFirstClick = false;
        }
        var activateTab = "";
        for (var i = 0; i < $scope.viewModel.leftTabs.length; i++) {
            if ($scope.viewModel.leftTabs[i].active == "active") {
                activateTab = "left";
                break;
            }
        }
        if (activateTab == "") {
            for (var i = 0; i < $scope.viewModel.rightTabs.length; i++) {
                if ($scope.viewModel.rightTabs[i].active == "active") {
                    activateTab = "right";
                    break;
                }
            }
        }
        if (activateTab == "") {
            activateTab = "none";
        }
        setSplitterContentWidth(activateTab);
    }
    //#endregion

    //#region 取得頁籤項目清單
    var getSectionTitles = function () {
        careDischarge.getSectionTitles().then(
        function (success) {
            if (success.data != null || success.data.length != 0) {
                for (var i = 0; i < success.data.length; i++) {
                    var item = { tabName: success.data[i].chtTitle, tempNos: success.data[i].sectionItems, active: "", templateUrl: "" };
                    $scope.viewModel.chtFormContents.push({ formName: success.data[i].chtTitle, formContent: "" });
                    $scope.viewModel.engFormContents.push({ formName: success.data[i].engTitle, formContent: "" });
                    if (i < 7) {
                        item.templateUrl = cmuhBase.stringFormat("App/CareDischarge/Pages/leftPage.html?{0}", cmuhBase.getVersionNo());
                        if (success.data[i].chtTitle == "管路狀況") {
                            item.templateUrl = cmuhBase.stringFormat("App/ExamView/BundleCare/bundleCareForm.html?{0}", cmuhBase.getVersionNo());
                        }
                        $scope.viewModel.leftTabs.push(item);
                    } else {
                        item.templateUrl = cmuhBase.stringFormat("App/CareDischarge/Pages/rightPage.html?{0}", cmuhBase.getVersionNo());
                        if (success.data[i].chtTitle == "諮詢服務") {
                            item.templateUrl = cmuhBase.stringFormat("App/CareDischarge/Pages/counseling.html?{0}", cmuhBase.getVersionNo());
                        }
                        $scope.viewModel.rightTabs.push(item);
                    }
                }
                for (var i = 0; i < success.data[4].sectionItems.length; i++) {
                    $scope.viewModel.chtHealthEducations.push({ itemName: success.data[4].sectionItems[i].chtName, tempNo: success.data[4].sectionItems[i].tempNo, content: "" });
                    $scope.viewModel.engHealthEducations.push({ itemName: success.data[4].sectionItems[i].engName, tempNo: success.data[4].sectionItems[i].tempNo, content: "" });
                }
                var promise1 = getActivePlanNames();    //取得未結案之護理計劃
                var promise2 = getRehabTxContent();     //取得相關復健
                var promise = $q.all([promise1, promise2]).then(
                    function (values) {
                        $scope.viewModel.chtFormContents.push({ formName: "健康照護問題", formContent: values[0] });
                        $scope.viewModel.engFormContents.push({ formName: "健康照護問題", formContent: values[0] });
                        $scope.viewModel.chtFormContents.push({ formName: "復健建議", formContent: values[1] });
                        $scope.viewModel.engFormContents.push({ formName: "復健建議", formContent: values[1] });
                    });
                
                //設定splitter高度
                setSplitterHeight();
                //取得出院照護樣板資訊
                getDischargeSheets();
                //關閉左側內容
                $scope.closeLeft();
                //關閉右側內容
                $scope.closeRight();
            } else {
                toaster.showErrorMessage("對不起，沒有相關的頁籤項目");
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
    }
    //#endregion

    //#region 取得未結案之護理計劃
    var getActivePlanNames = function () {
        var deferred = $q.defer();
        careDischarge.getActivePlanNames($scope.appModel.patientInfos.visitNo).then(
        function (success) {
            if (success.data != "null") {
                deferred.resolve(success.data.join("<br/>"));
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
        return deferred.promise;
    }
    //#endregion

    //#region 取得相關復健
    var getRehabTxContent = function () {
        var deferred = $q.defer();
        careDischarge.getRehabTxContent($scope.appModel.patientInfos.visitNo).then(
        function (success) {
            if (success.data != "null") {
                var data = angular.fromJson(success.data);
                deferred.resolve(data.replace(/\r\n/g, "<br/>"));
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
        return deferred.promise;
    }
    //#endregion

    //#region 取得出院帶藥清單
    var getDischargeDrugs = function () {
        careDischarge.getDischargeDrugs($scope.appModel.patientInfos.visitNo).then(
            function (success) {
                $scope.viewModel.dischargeDrug = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 取得上次存檔日期、護理師
    var getDischargeRecord = function () {
        careDischarge.getDischargeRecord($scope.appModel.patientInfos.visitNo).then(
            function (success) {
                if (success.data != "null") {
                    $scope.viewModel.recordInfo = success.data;
                    $scope.viewModel.recordInfo.recordTime = cmuhBase.stringFormat("{0} 年 {1} 月 {2} 日", cmuhBase.addLeftZero(new Date($scope.viewModel.recordInfo.recordTime).getFullYear()), cmuhBase.addLeftZero(new Date($scope.viewModel.recordInfo.recordTime).getMonth() + 1), cmuhBase.addLeftZero(new Date($scope.viewModel.recordInfo.recordTime).getDate()));
                    $scope.appModel.patientInfos.bed.stationNo = $scope.viewModel.recordInfo.stationNo;
                    $scope.appModel.patientInfos.bed.roomBed = $scope.viewModel.recordInfo.bed;
                    //取得護理站分機
                    getSubTelNo();
                } else {
                    $scope.viewModel.recordInfo = {
                        empNo: $scope.shareModel.userInfos.userNo,
                        empName: $scope.shareModel.userInfos.userName,
                        recordTime: cmuhBase.stringFormat("{0} 年 {1} 月 {2} 日", cmuhBase.addLeftZero(new Date().getFullYear()), cmuhBase.addLeftZero(new Date().getMonth() + 1), cmuhBase.addLeftZero(new Date().getDate()))
                    };
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion


    //#region 取得預約門診掛號資訊
    var getAppointRecords = function () {
        var idNo = $scope.appModel.patientInfos.idNo == "9999999999" ? $scope.appModel.patientInfos.chartNo : $scope.appModel.patientInfos.idNo;
        careDischarge.getAppointRecords(idNo).then(
            function (success) {
                $scope.viewModel.appointmentRecord = success.data;
            },
            function (error) {
                toaster.showErrorMessage("對不起，連線失敗");
            });
    }
    //#endregion

    //#region 取得出院照護樣板資訊
    var getDischargeSheets = function () {
        careDischarge.getDischargeSheets($scope.appModel.patientInfos.visitNo).then(
        function (success) {
            if (success.data != null) {
                $scope.viewModel.sheetInfo = success.data;
                $scope.apply();
                for (var i = 0; i < $scope.viewModel.sheetInfo.length; i++) {
                    if ($scope.viewModel.sheetInfo[i].chtTitle == "管路狀況") {
                        for (var j = 0; j < $scope.viewModel.chtFormContents.length; j++) {
                            if ($scope.viewModel.chtFormContents[j].formName == "管路狀況") {
                                $scope.viewModel.chtFormContents[j].formContent = $scope.viewModel.sheetInfo[i].formContent;
                                $scope.viewModel.engFormContents[j].formContent = $scope.viewModel.sheetInfo[i].formContent;
                            }
                        }
                    }
                }
            } else {
                toaster.showErrorMessage("對不起，沒有相關的表單");
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
    }
    //#endregion

    //#region 取得護理站分機
    var getSubTelNo = function () {
        careDischarge.getSubTelNo($scope.appModel.patientInfos.bed.stationNo).then(
        function (success) {
            $scope.viewModel.ext = success.data == 0 ? "" : success.data;
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
    }
    //#endregion
    
    //#region 是否有門診注射單
    var isOpdInjection = function () {
        careDischarge.isOpdInjection($scope.appModel.patientInfos.visitNo).then(
        function (success) {
            if (success.data == true || success.data == "true") {
                $scope.viewModel.opdInjection = true;
            } else {
                $scope.viewModel.opdInjection = false;
            }
        },
        function (error) {
            toaster.showErrorMessage("對不起，連線失敗");
        });
    }
    //#endregion

    //#region getEmpWorkStation
    var getEmpWorkStation = function (empNo) {
        var deferred = $q.defer();
        careDischarge.getEmpWorkStation(empNo).then(
            function (success) {
                deferred.resolve(success.data);
            },
            function (error) {
                toaster.showErrorMessage("對不起,取得護理站失敗");
            }
        );
        return deferred.promise;
    }
    //#endregion

    //#endregion

    //#region 事件

    //#region 存檔禁用事件
    $scope.saveDisable = function () {
        for (var i = 0; i < $scope.viewModel.chtFormContents.length; i++) {
            if ($scope.viewModel.chtFormContents[i].formName == "活動限制" && $scope.viewModel.chtFormContents[i].formContent == "") {
                return true;
            }
            if ($scope.viewModel.chtFormContents[i].formName == "營養指導" && $scope.viewModel.chtFormContents[i].formContent == "") {
                return true;
            }
            if ($scope.viewModel.chtFormContents[i].formName == "即時返院" && $scope.viewModel.chtFormContents[i].formContent == "") {
                return true;
            }
        }
        return false;
    }
    //#endregion

    //#region 儲存
    $scope.save = function () {
        $scope.viewModel.sheetInfos = [];
        for (var i = 0; i < $scope.viewModel.sheetInfo.length; i++) {
            if ($scope.viewModel.sheetInfo[i].chtTitle == "管路狀況") {
                for (j = 0; j < $scope.viewModel.chtFormContents.length; j++) {
                    if ($scope.viewModel.chtFormContents[j].formName == "管路狀況") {
                        $scope.viewModel.sheetInfo[i].formContent = $scope.viewModel.chtFormContents[j].formContent;
                    }
                }
            }
            $scope.viewModel.sheetInfos.push($scope.viewModel.sheetInfo[i]);
        }
        var data = {
            "visitNo": $scope.appModel.patientInfos.visitNo,
            "recordUser": $scope.shareModel.userInfos.userNo,
            "recordTime": $scope.viewModel.recordInfo.recordTime.trim(),
            "sheetInfos": $scope.viewModel.sheetInfos
        };
        $scope.viewModel.recordInfo.empName = $scope.shareModel.userInfos.userName;
        careDischarge.setCareDischargeInfo(data).then(
            function (success) {
                if (success.data == "true") {
                    toaster.showSuccessMessage("存檔成功");
                } else {
                    toaster.showErrorMessage("對不起,存檔失敗");
                }
            },
            function (error) {
                toaster.showErrorMessage("對不起,連線失敗");
            }
        );
    }
    //#endregion

    //#region 中文列印
    $scope.chtPrint = function () {
        var printSection = document.getElementById("printSection");
        if (!printSection) {
            var printSection = document.createElement("div");
            printSection.id = "printSection";
            document.body.appendChild(printSection);
        }
        var domClone = document.getElementById("chtPrintDiv").cloneNode(true);
        printSection.appendChild(domClone);
        window.print();
        printSection.removeChild(domClone);
    }
    //#endregion

    //#region 英文列印
    $scope.engPrint = function () {
        var printSection = document.getElementById("printSection");
        if (!printSection) {
            var printSection = document.createElement("div");
            printSection.id = "printSection";
            document.body.appendChild(printSection);
        }
        var domClone = document.getElementById("engPrintDiv").cloneNode(true);
        printSection.appendChild(domClone);
        window.print();
        printSection.removeChild(domClone);
    }
    //#endregion

    //#region 顯示左側內容
    $scope.showLeftContent = function (item) {
        var promise = getDependencies(item.templateUrl);
        promise.then(
            function (data) {
                $scope.viewModel.leftTabContent = data;
            });
        //title為tab名稱
        $scope.viewModel.selectedTabTitle = item.tabName;
        //修改tab狀態
        changeLeftTab(item);
        //設定splitter內容寬度
        if ($scope.viewModel.isExpand == true) {
            setSplitterContentWidth("left-expand");
        } else {
            setSplitterContentWidth("left");
        }
        //清除右側內容
        $scope.viewModel.rightTabContent = null;
        //如果只有一個項目則顯示表單
        if (item.tempNos.length == 1) {
            $scope.viewModel.isFormShow = false;
            for (var i = 0; i < $scope.viewModel.sheetInfo.length; i++) {
                if ($scope.viewModel.sheetInfo[i] != null) {
                    if ($scope.viewModel.sheetInfo[i].chtTitle == item.tabName) {
                        $scope.viewModel.sheetNo = i;
                    }
                }
            }
        } else {
            $scope.viewModel.isFormShow = true;
            $scope.viewModel.nowTemplate = item.tempNos;
        }
    }
    //#endregion

    //#region 顯示右側內容
    $scope.showRightContent = function (item) {
        var promise = getDependencies(item.templateUrl);
        promise.then(
            function (data) {
                $scope.viewModel.rightTabContent = data;
            });
        //title為tab名稱
        $scope.viewModel.selectedTabTitle = item.tabName;
        //修改tab狀態
        changeRightTab(item);
        if ($scope.viewModel.isExpand == true) {
            setSplitterContentWidth("right-expand");
        } else {
            setSplitterContentWidth("right");
        }
        //清除左側內容
        $scope.viewModel.leftTabContent = null;
        for (var i = 0; i < $scope.viewModel.sheetInfo.length; i++) {
            if ($scope.viewModel.sheetInfo[i] != null) {
                if ($scope.viewModel.sheetInfo[i].chtTitle == item.tabName) {
                    $scope.viewModel.sheetNo = i;
                }
            }
        }
    }
    //#endregion

    //#region 關閉左側內容
    $scope.closeLeft = function () {
        //修改tab狀態
        for (var i = 0; i < $scope.viewModel.leftTabs.length; i++) {
            $scope.viewModel.leftTabs[i].active = "";
        }
        //設定splitter內容寬度
        setSplitterContentWidth("none");
        //清除內容
        $scope.viewModel.leftTabContent = null;
        $scope.viewModel.isExpand = false;
    }
    //#endregion

    //#region 關閉右側內容
    $scope.closeRight = function () {
        //修改tab狀態
        for (var i = 0; i < $scope.viewModel.rightTabs.length; i++) {
            $scope.viewModel.rightTabs[i].active = "";
        }
        //設定splitter內容寬度
        setSplitterContentWidth("none");
        //清除內容
        $scope.viewModel.rightTabContent = null;
        $scope.viewModel.isExpand = false;
    }
    //#endregion

    //#region 展開左側內容
    $scope.expandLeftContent = function (isExpand) {
        if (isExpand == false) {
            setSplitterContentWidth("left-expand");
            $scope.viewModel.isExpand = true;
        } else {
            setSplitterContentWidth("left");
            $scope.viewModel.isExpand = false;
        }
    }
    //#endregion

    //#region 展開右側內容
    $scope.expandRightContent = function (isExpand) {
        if (isExpand == false) {
            setSplitterContentWidth("right-expand");
            $scope.viewModel.isExpand = true;
        } else {
            setSplitterContentWidth("right");
            $scope.viewModel.isExpand = false;
        }
    }
    //#endregion

    //#region 視窗大小改變
    window.onresize = function () {
        setSplitterHeight();
        setTimeout(resizing, 100);
    }
    //#endregion

    //#region 選擇問題列表表單
    $scope.chooseForm = function (item) {
        $scope.viewModel.isFormShow = false;
        for (var i = 0; i < $scope.viewModel.sheetInfo.length; i++) {
            if ($scope.viewModel.sheetInfo[i].sections[0].chtTitle == item.chtName) {
                $scope.viewModel.sheetNo = i;
            }
        }
    }
    //#endregion

    //#region 接收管路狀況值
    $scope.$on('copiedJson', function (e, copyJson) {
        var chtContent = '';
        var engContent = '';
        var no = 1;
        for (var i = 0; i < copyJson.careRecord.length; i++) {
            chtContent += no + "." + copyJson.careRecord[i].context.bundleType.optionName + "（";
            engContent += no + "." + copyJson.careRecord[i].context.bundleType.optionName + "（";
            if (copyJson.careRecord[i].context.bundleGeneral.optionName != "" && copyJson.careRecord[i].context.bundleGeneral.optionName != null) {
                chtContent += copyJson.careRecord[i].context.bundleGeneral.optionName + "）　";
                engContent += copyJson.careRecord[i].context.bundleGeneral.optionName + "）　";
            } else if (copyJson.careRecord[i].context.bundleDrain.optionName != "" && copyJson.careRecord[i].context.bundleDrain.optionName != null) {
                chtContent += copyJson.careRecord[i].context.bundleDrain.optionName + "）　";
                engContent += copyJson.careRecord[i].context.bundleDrain.optionName + "）　";
            } else {
                chtContent += copyJson.careRecord[i].context.bundleDrain.remark + "）　";
                engContent += copyJson.careRecord[i].context.bundleDrain.remark + "）　";
            }
            if (copyJson.careRecord[i].context.bundlePosition.optionName != "" && copyJson.careRecord[i].context.bundlePosition.optionName != null) {
                chtContent += copyJson.careRecord[i].context.bundlePosition.optionName + "　";
                engContent += copyJson.careRecord[i].context.bundlePosition.optionName + "　";
            } else {
                chtContent += copyJson.careRecord[i].context.bundlePosition.remark + "　";
                engContent += copyJson.careRecord[i].context.bundlePosition.remark + "　";
            }
            chtContent += "植入日：" + copyJson.careRecord[i].context.bundleStart.value.substring(0, 10) + "　";
            engContent += "start day：" + copyJson.careRecord[i].context.bundleStart.value.substring(0, 10) + "　";
            if (copyJson.careRecord[i].context.bundleEnd.value != "" && copyJson.careRecord[i].context.bundleEnd.value != null) {
                chtContent += "到期日：" + copyJson.careRecord[i].context.bundleEnd.value + "　";
                engContent += "end day：" + copyJson.careRecord[i].context.bundleEnd.value + "　";
            }
            chtContent += "<br/>";
            engContent += "<br/>";
            no++;
        }
        for (var i = 0; i < $scope.viewModel.chtFormContents.length; i++) {
            if ($scope.viewModel.chtFormContents[i].formName == "管路狀況") {
                $scope.viewModel.chtFormContents[i].formContent = chtContent;
                $scope.viewModel.engFormContents[i].formContent = engContent;
            }
        }
    });
    //#endregion

    //#region text格式限定
    $scope.formatLimited = function (format, block) {
        if (format == "double") {
            block.value = block.value.replace(/[^\d\.]/g, '');
        }
        if (format == "integer") {
            block.value = block.value.replace(/[^\d]/g, '');
        }
    }
    //#endregion

    //#region 套用
    $scope.apply = function () {
        $scope.viewModel.getBabyBack = false;
        for (var sheetInfoNo = 0; sheetInfoNo < $scope.viewModel.sheetInfo.length; sheetInfoNo++) {
            if ($scope.viewModel.sheetInfo[sheetInfoNo] != null) {
                var chtContent = '';
                var engContent = '';
                var no = 1;
                for (var i = 0; i < $scope.viewModel.sheetInfo[sheetInfoNo].sections.length; i++) {
                    for (var j = 0; j < $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields.length; j++) {
                        if ($scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].type == "checkbox" && $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].value == "true") {
                            if (sheetInfoNo == 49) {//諮詢服務表單
                                chtContent += no + ".";
                                chtContent += "諮詢場所：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[0].chtTitle + "<br/>　";
                                chtContent += "服務項目：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[1].chtTitle + "<br/>　";
                                chtContent += "服務地點：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[2].chtTitle + "<br/>　";
                                chtContent += "服務時間：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[3].chtTitle + "<br/>　";
                                chtContent += "聯絡人：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[4].chtTitle + "<br/>　";
                                chtContent += "諮詢電話：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[5].chtTitle;
                                engContent += no + ".";
                                engContent += "Counseling Site：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[0].engTitle + "<br/>　";
                                engContent += "Services：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[1].engTitle + "<br/>　";
                                engContent += "Service Site：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[2].engTitle + "<br/>　";
                                engContent += "Service Hours：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[3].engTitle + "<br/>　";
                                engContent += "Contact Person：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[4].engTitle + "<br/>　";
                                engContent += "Counseling Number：" + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[5].engTitle;
                            } else {//其他表單
                                if ($scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].chtRemark == "領回嬰兒切結書") { $scope.viewModel.getBabyBack = true; }
                                chtContent += no + "." + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].chtRemark;
                                engContent += no + "." + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].engRemark;
                                for (var l = 0; l < $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks.length; l++) {
                                    chtContent += $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].chtTitle;
                                    chtContent += $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].value;
                                    chtContent += $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].chtRemark;
                                    engContent += $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].engTitle;
                                    engContent += " " + $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].value + " ";
                                    engContent += $scope.viewModel.sheetInfo[sheetInfoNo].sections[i].fields[j].blocks[l].engRemark;
                                }
                            }
                            chtContent += "<br/>";
                            engContent += "<br/>";
                            no++;
                        }
                    }
                }
                if (sheetInfoNo >= 4 && sheetInfoNo <= 43) {//衛教單張的表單
                    var chtX = '';//暫存中文內容
                    var engX = '';//暫存英文內容
                    var chtY = [];//暫存切割後的中文內容
                    var engY = [];//暫存切割後的英文內容
                    if ($scope.viewModel.chtFormContents[4] != undefined) { $scope.viewModel.chtFormContents[4].formContent = ''; }
                    if ($scope.viewModel.engFormContents[4] != undefined) { $scope.viewModel.engFormContents[4].formContent = ''; }
                    for (var i = 0; i < $scope.viewModel.chtHealthEducations.length; i++) {
                        if ($scope.viewModel.sheetInfo[sheetInfoNo].sections[0].chtTitle == $scope.viewModel.chtHealthEducations[i].itemName) {
                            $scope.viewModel.chtHealthEducations[i].content = $sce.trustAsHtml(chtContent);
                            $scope.viewModel.engHealthEducations[i].content = $sce.trustAsHtml(engContent);
                        }
                        chtX += $scope.viewModel.chtHealthEducations[i].content;
                        engX += $scope.viewModel.engHealthEducations[i].content;
                    }
                    chtY = chtX.split("<br/>");
                    engY = engX.split("<br/>");
                    no = 1;
                    for (var i = 0; i < chtY.length - 1; i++) {
                        $scope.viewModel.chtFormContents[4].formContent += no + "." + chtY[i].substring(2) + "<br/>";
                        $scope.viewModel.engFormContents[4].formContent += no + "." + engY[i].substring(2) + "<br/>";
                        no++;
                    }
                } else {
                    debugger;
                    for (var i = 0; i < $scope.viewModel.chtFormContents.length; i++) {
                        if ($scope.viewModel.sheetInfo[sheetInfoNo].chtTitle == $scope.viewModel.chtFormContents[i].formName) {
                            $scope.viewModel.chtFormContents[i].formContent = $sce.trustAsHtml(chtContent);
                            $scope.viewModel.engFormContents[i].formContent = $sce.trustAsHtml(engContent);
                        }
                    }
                }
            }
        }
    }
    //#endregion

    //#region change Patient
    $scope.$watch('appModel.patientInfos', function (newValue, oldValue) {
        if (newValue === oldValue || newValue === undefined || newValue === null) {
            return;
        }
        initViewModel($scope.pageModel);
    });
    //#endregion

    //#region 開啟衛教網頁
    $scope.openWeb = function () {
        document.getElementById("openWeb").click();
    }
    //#endregion
    
    //#region 門診注射單
    $scope.opd = function () {
        var promise = getEmpWorkStation($scope.shareModel.userInfos.userNo)//getStationByIP();
        promise.then(function (data) {
            var chartNo = $scope.appModel.patientInfos === undefined ? "0000000000" : $scope.appModel.patientInfos.chartNo;
            var registerNo = $scope.appModel.patientInfos === undefined ? "00000000" : $scope.appModel.patientInfos.registerNo;
            var userId = $scope.shareModel.userInfos.userId;
            var stationNo = $scope.appModel.patientInfos === undefined ? (data == "" ? "-" : data.replace(/"/g, '')) : $scope.appModel.patientInfos.bed.stationNo;
            location.href = " cmuh: 1132 " + chartNo + " " + registerNo + " " + userId + " " + stationNo;
        });
    }
    //#endregion

    //#endregion

    //#region 建構
    initViewModel($scope.pageModel);
    //#endregion
}]);